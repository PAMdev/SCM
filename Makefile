#
# Makefile for PAM library
# Author: Andrei Ryjkov, ARQI
# History:
# 	Oct 15, 2018 - created
#	Oct 24, 2019 - migrated to ppp2
#	Oct 29, 2019 - adapted for SCM code (Sarah Hanna)
#       Jan 8, 2020  - moved SCM compilation to Makefile, migrated to ppp4

SHELL = /bin/bash

FC = ifort

NETCDFINC_ifort = $(shell nf-config --fflags)  
NETCDFLIB_ifort = $(shell nf-config --flibs)
OPTS_ifort = -O0 -fpp -fpe0 -traceback -check all -fp-model precise -init=zero -init=arrays -assume protect_parens -convert big_endian  


#CCCMA flags
#OPTS_ifort = -qopenmp -list -traceback -O2 -mp1 -assume buffered_stdout,protect_parens -convert big_endian -threads  

LIBNAME    = $(PWD)/libpam_$(FC).a
SCMLIBNAME = $(PWD)/libscm_$(FC).a

.SUFFIXES: $(SUFFIXES) .f90

.FMODULES = gen/sdtmp_mod.f90 gen/cnevdt_mod.f90 gen/fpdef_mod.f90 gen/mbnds_mod.f90 gen/sdparm_mod.f90 gen/sdcode_mod.f90 \
	    gen/sdphys_mod.f90 gen/sdpio_mod.f90 gen/sdtblf_mod.f90 \
	    cloud/cnparmb_mod.f90 cloud/cnparm_mod.f90 cloud/cnparmt_mod.f90 cloud/scparm_mod.f90 \
	    coag/coadat_mod.f90 dust/duparm1_mod.f90 emis/sdemi_mod.f90 \
	    iface/compar_mod.f90 iface/sdi_mod.f90 iface/sdinfo_mod.f90 rad/rdmod_mod.f90

.FFILES = cloud/ccnact.f90 cloud/ccnc.f90 cloud/ccnpar.f90 cloud/cdncem.f90 cloud/cnbnds.f90 cloud/cncdnca.f90 cloud/cncdncb.f90 cloud/cnextr.f90 \
	  cloud/cnextrp.f90 cloud/cngrw.f90 cloud/cnigge.f90 cloud/cnigwm.f90 cloud/cnintp.f90  \
	  cloud/cnrat.f90 cloud/cnrcal.f90 cloud/cnsbnd.f90 cloud/cntscl.f90 cloud/colleff.f90 cloud/equip.f90 cloud/fillh.f90 cloud/scconf.f90 cloud/scldsp.f90 \
	  cloud/scmicp.f90 cloud/scmom.f90 cloud/scwatm.f90 cloud/sicprd.f90 cloud/suloxi.f90 cloud/swetdep.f90 \
	  coag/coag.f90 coag/coagm.f90 coag/coagn.f90 coag/sdmap.f90 \
	  depo/drydep.f90 depo/grvstl.f90 depo/gspar.f90 depo/vsrad.f90 diag/amdiag.f90 \
	  diag/cndiag.f90 diag/diagflx.f90 diag/diagsdw.f90 diag/extac.f90 diag/intsd.f90 diag/intsdpm.f90 diag/pamd.f90 \
      diag/pamdi.f90 diag/sdfilt.f90 diag/vintsd.f90 \
	  dust/dplaout.f90 dust/dufcc.f90 dust/duinit1.f90 dust/dustem.f90 dust/dustp.f90 \
	  emis/priaem.f90 emis/sdpemi.f90 \
	  gen/agebc.f90 gen/avgtnd.f90 gen/avgtndd.f90 gen/avgtndg.f90 gen/cnconf.f90 gen/cornme.f90 gen/cornmi.f90 \
	  gen/nm2par.f90 gen/nm2pari.f90 gen/nm2parx.f90 gen/nm2pla.f90 gen/pam.f90 gen/pla2nm.f90 \
	  gen/sdaprop.f90 gen/sdconf.f90 gen/sddens.f90 \
	  gen/sdread.f90 gen/sdsum.f90 gen/trc2nm.f90 gen/wrn.f90 gen/xit.f90 gtpconv/bhnucl.f90 \
	  gtpconv/dunnepar.f90 gtpconv/dunucl.f90 gtpconv/grwtrm.f90 gtpconv/gtpcnv.f90 gtpconv/gtsoa.f90 gtpconv/gtss.f90 \
	  gtpconv/pgrpar.f90 gtpconv/pgrwth0.f90 gtpconv/pgrwthc.f90 gtpconv/pgrwthn.f90 gtpconv/pgrwths.f90 gtpconv/tbnucl.f90 \
	  iface/geninit.f90 iface/initphys.f90 iface/pamdiag.f90 iface/pamdriv.f90 iface/sdcopye.f90 iface/sdcopyi.f90 \
	  iface/sdinit.f90 iface/sdinp.f90 iface/trburd.f90 iface/trconc.f90 iface/trnum.f90 \
	  rad/aeromx3data.f90 rad/aeromx3op.f90 rad/dustaerop.f90 rad/effrv.f90 rad/effrve.f90 rad/mvidx.f90 rad/radpare.f90 \
	  rad/radpari.f90 rad/rdconf.f90 rad/ssaltaerop.f90 \
	  seas/ssfcc.f90 seas/ssgen.f90

.SCMMODULES = gen/fpdef_mod.f90 gen/iodatif_mod.f90 gen/iodat_mod.f90 gen/psizes_mod.f90 \
              gen/runinfo_mod.f90 gen/sdphys_mod.f90 gen/trcind_mod.f90 rwflds/wfld_mod.f90

.SCMFILES = chem/xtchempam.f90 chem/sunlit_hours_per_day.f90 cloud/cldphys.f90 cloud/statcld.f90 cloud/zcr.f90 depo/drydepg.f90 depo/wetdep4.f90 emis/emidms.f90 \
            gen/initio.f90 gen/trinfo.f90 gen/chkio.f90 gen/wrn.f90 gen/xit.f90 \
            netcdf/rdncdat.f90 netcdf/rdncdatrs.f90 netcdf/rdncdima.f90 netcdf/setncdim.f90 netcdf/setncdimpre.f90 \
            netcdf/setncdimprepam.f90 netcdf/wncdat.f90 netcdf/wncdatrs.f90 \
            rwflds/r0dfld.f90 rwflds/r1dfld.f90 rwflds/r2dfld.f90 rwflds/r3dfld.f90 rwflds/r4dfld.f90 \
            rwflds/w0dfld.f90 rwflds/w1dfld.f90 rwflds/w2dfld.f90 rwflds/w3dfld.f90 rwflds/w4dfld.f90 \
            rwflds/r0dfldrs.f90 rwflds/r1dfldrs.f90 rwflds/r2dfldrs.f90 rwflds/r3dfldrs.f90 rwflds/r4dfldrs.f90 \
            rwflds/w0dfldrs.f90 rwflds/w1dfldrs.f90 rwflds/w2dfldrs.f90 rwflds/w3dfldrs.f90 rwflds/w4dfldrs.f90 rwflds/w1ddim.f90 \
            vrtadv/vadv.f90 vrtdf/abcvdq6.f90 vrtdf/drcoef.f90 vrtdf/implvd7.f90 vrtdf/vrossr.f90 vrtdf/vrtdf.f90

.OFILES   = $(.FFILES:.f90=.o)
.OFILESSCM   = $(.SCMFILES:.f90=.o)

.OMODULES = $(.FMODULES:.f90=.o)
.OMODULESSCM = $(.SCMMODULES:.f90=.o)

.OMODS    = $(.FMODULES:.f90=.mod)

%.o: %.mod

.f90.o:
	@echo "$< =-> $@"
	$(FC) $(OPTS_$(FC)) $(INCLUDE) -c $< -o $@

.f90.mod:
	@echo "$< =-> $(<D)/$(@F:.mod=.o)"
	$(FC) $(OPTS_$(FC)) -c $< -module $(@D) -o $(<D)/$(@F:.mod=.o)

all:
	@echo "Target all"
#	$(MAKE) clean
	$(MAKE) pamfast
	$(MAKE) scmfast
	$(MAKE) pamlibrary
	$(MAKE) scmlibrary
	$(MAKE) scm

pamfast:
	@echo "Target fast"
	cd ./PAM; $(MAKE) -f ../Makefile  $(.OMODULES) INCLUDE=-I.
	cd ./PAM; $(MAKE) -f ../Makefile -j 16 $(.OFILES) INCLUDE="-I. $(NETCDFINC_$(FC))"

scmfast:
	@echo "Compile SCM files"
	cd scmpam; $(MAKE) -f ../Makefile  $(.OMODULESSCM) INCLUDE=-I.
	cd scmpam; $(MAKE) -f ../Makefile  $(.OFILESSCM) INCLUDE="-I. $(NETCDFINC_$(FC))"

pamlibrary:
	@echo "Target library"
	rm -f $(LIBNAME)
	cd ./PAM; ar r $(LIBNAME) $(.OMODULES) $(.OFILES);

scmlibrary:
	@echo "Target SCM library"
	rm -f $(SCMLIBNAME)
	cd scmpam; ar r $(SCMLIBNAME) $(.OMODULESSCM) $(.OFILESSCM);

scm:
	@echo "Target SCM"
	@echo "Compile i/o list manager"
	$(FC) $(OPTS_$(FC)) configlist.f -o configlist
	@echo "Compile the PAM/SCM preprocessor to generate tracer boundary conditions"
	$(FC) $(OPTS_$(FC)) ./preproc_pam.f90 $(LIBNAME) $(SCMLIBNAME) -I./PAM/ -I./scmpam/ $(NETCDFINC_$(FC)) $(NETCDFLIB_$(FC)) -o preproc_pam
	@echo "Compile the SCM meteorology preprocessor"
	$(FC) $(OPTS_$(FC)) ./preproc.f90 $(SCMLIBNAME) -I./scmpam/ $(NETCDFINC_$(FC)) $(NETCDFLIB_$(FC)) -o preproc
	@echo "Compile the SCM"
	$(FC) $(OPTS_$(FC)) ./scm_pam.f90 $(LIBNAME) $(SCMLIBNAME) -I./PAM/ -I./scmpam/ $(NETCDFINC_$(FC)) $(NETCDFLIB_$(FC)) -o scm_pam

clean:
	@echo "Target clean"
	rm -f $(SCMNAME) $(LIBNAME) $(SCMLIBNAME) preproc_pam preproc scm_pam ./scmpam/*.mod ./scmpam/*/*.o ./PAM/*/*.o ./PAM/*.mod *.o *.mod ./PAM/*__genmod.f90 *__genmod.f90
