Scripts
-------

run_scm_08 - this script runs the basic single column model (without PAM)

run_scm_08_pam_test - this script runs a version of the single column model with the PLA aerosol model (PAM) 
                      for any of the scenarios in /test_cases/

More details on running the SCM can be found in the [Wiki](https://gitlab.com/PAMdev/SCM/-/wikis/Running-PAM-in-the-Single-Column-Model-(SCM))


Source code
-----------

scm.f90 - basic SCM

scm_pam.f90 - SCM with PAM 

preproc.f90 - SCM meteorological boundary and initial conditions preprocessor

preproc_pam.f90 - PAM boundary and initial conditions preprocessor 

PAM/ - the PLA Aerosol Model (as a submodule)


Model input directories and files 
---------------------------------

test_cases/ - directory containing meteorological input vertical profiles and configuration data for 10 test cases

IOLIST - input/output variables for SCM and preprocessor 

IOLIST_PAM - input/output variables for SCM, PAM and preprocessors 

ACTDAT1 - PAM configuration data 

COAGDAT - PAM configuration data 

SSDAT - PAM configuration data 
