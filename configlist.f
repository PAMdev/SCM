      PROGRAM CONFIGLIST
C
      IMPLICIT NONE
C
      CHARACTER(LEN=1) ::BUFFER 
      LOGICAL :: KARG
      CHARACTER(LEN=6) :: CFILATM
      CHARACTER(LEN=10) :: CFILAER
C
C     * COMMAND LINE ARGUMENT. 1=ATMOSPHERE, 2=ATMOSPHERE+AEROSOL
C 
      CALL GETARG(1,BUFFER)
      READ(BUFFER,*) KARG
      CFILATM='IOLIST'
      CFILAER='IOLIST_PAM'
      CALL REFORML(CFILATM,CFILAER,KARG)
C
      END PROGRAM CONFIGLIST
      SUBROUTINE REFORML(CFILATM,CFILAER,KARG)
C
      IMPLICIT NONE
C
      CHARACTER(LEN=6), INTENT(IN) :: CFILATM
      CHARACTER(LEN=10), INTENT(IN) :: CFILAER
      LOGICAL, INTENT(IN) :: KARG
      CHARACTER(LEN=4), DIMENSION(3,3) :: CPRE
      CHARACTER(LEN=18) :: CLAB
      INTEGER, PARAMETER :: ILARGE=10000000
      CHARACTER(LEN=100) :: CDUM,CDUMX
      INTEGER :: I,IS,IX,NF,NFX,IFN
      INTEGER, DIMENSION(3,3) :: IFIL,IC
C
      OPEN(10,FILE=CFILATM)
      DO I=1,ILARGE
        READ(10,'(A100)',END=999) CDUM
        IX=0
        IF ( CDUM(1:29) == '* MODEL BOUNDARY CONDITIONS *' ) THEN
          IS=1
          NF=1
          IFIL(IS,NF)=20
          OPEN(IFIL(IS,NF),FILE='OUTPUT_PRE')
          WRITE(IFIL(IS,NF),'(A8)') '&OVARNML'
          CPRE(IS,NF)='OVAR'
          IC(IS,NF)=0
          NF=NF+1
          IFIL(IS,NF)=21
          OPEN(IFIL(IS,NF),FILE='INPUT_MOD')
          WRITE(IFIL(IS,NF),'(A8)') '&IVARNML'
          CPRE(IS,NF)='IVAR'
          IC(IS,NF)=0
          IX=1
        ENDIF
        IF ( CDUM(1:16) == '* OUTPUT MODEL *' ) THEN
          IS=2
          NF=1
          IFIL(IS,NF)=30
          OPEN(IFIL(IS,NF),FILE='OUTPUT_MOD')
          WRITE(IFIL(IS,NF),'(A8)') '&OVARNML'
          CPRE(IS,NF)='OVAR'
          IC(IS,NF)=0
          IX=1
        ENDIF
        IF ( CDUM(1:17) == '* RESTART MODEL *' ) THEN
          IS=3
          NF=1
          IFIL(IS,NF)=40
          OPEN(IFIL(IS,NF),FILE='RESTART_PRE')
          WRITE(IFIL(IS,NF),'(A8)') '&RVARNML'
          CPRE(IS,NF)='RVAR'
          IC(IS,NF)=1
          WRITE(IFIL(IS,NF),'()')
          WRITE(IFIL(IS,NF),'(A22)') '! INVARIANT PARAMETERS'         
          WRITE(IFIL(IS,NF),'()')          
          CDUMX='NAME=''tstep'''
          CALL LABLL(CLAB,IC(IS,NF),CDUMX)
          WRITE(IFIL(IS,NF),CLAB) CPRE(IS,NF),'(',IC(IS,NF),')%'
     1                              ,TRIM(CDUMX)
          CDUMX='LNAME=''Time step index'''
          CALL LABLL(CLAB,IC(IS,NF),CDUMX)
          WRITE(IFIL(IS,NF),CLAB) CPRE(IS,NF),'(',IC(IS,NF),')%'
     1                              ,TRIM(CDUMX)
          CDUMX='UNITS=''1'''
          CALL LABLL(CLAB,IC(IS,NF),CDUMX)
          WRITE(IFIL(IS,NF),CLAB) CPRE(IS,NF),'(',IC(IS,NF),')%'
     1                              ,TRIM(CDUMX)
          NF=NF+1
          IFIL(IS,NF)=41
          OPEN(IFIL(IS,NF),FILE='RESTART_MOD')
          WRITE(IFIL(IS,NF),'(A8)') '&RVARNML'
          CPRE(IS,NF)='RVAR'
          IC(IS,NF)=1
          WRITE(IFIL(IS,NF),'()')
          WRITE(IFIL(IS,NF),'(A22)') '! INVARIANT PARAMETERS'         
          WRITE(IFIL(IS,NF),'()')          
          CDUMX='NAME=''tstep'''
          CALL LABLL(CLAB,IC(IS,NF),CDUMX)
          WRITE(IFIL(IS,NF),CLAB) CPRE(IS,NF),'(',IC(IS,NF),')%'
     1                              ,TRIM(CDUMX)
          CDUMX='LNAME=''Time step index'''
          CALL LABLL(CLAB,IC(IS,NF),CDUMX)
          WRITE(IFIL(IS,NF),CLAB) CPRE(IS,NF),'(',IC(IS,NF),')%'
     1                              ,TRIM(CDUMX)
          CDUMX='UNITS=''1'''
          CALL LABLL(CLAB,IC(IS,NF),CDUMX)
          WRITE(IFIL(IS,NF),CLAB) CPRE(IS,NF),'(',IC(IS,NF),')%'
     1                              ,TRIM(CDUMX)
          IX=1
        ENDIF
        IF ( CDUM(1:4) == 'NAME' .OR. CDUM(1:5) == 'LNAME' 
     1      .OR. CDUM(1:5) == 'UNITS' .OR. CDUM(1:3) == 'DIM') THEN
          DO NFX=1,NF
            IF ( CDUM(1:4) == 'NAME' ) IC(IS,NFX)=IC(IS,NFX)+1
            CALL LABLL(CLAB,IC(IS,NFX),CDUM)
            WRITE(IFIL(IS,NFX),CLAB) CPRE(IS,NFX),'(',IC(IS,NFX),')%'
     1                              ,TRIM(CDUM)
          ENDDO
          IX=1
        ENDIF
        IF ( IX == 0 ) THEN
          DO NFX=1,NF
            IFN=IFIL(IS,NFX)
            CALL WRTP(IFN,CDUM)
          ENDDO
        ENDIF
      ENDDO
 999  CONTINUE
      CLOSE(10)
C
C     * ADDITIONAL VARIABLES FOR AEROSOLS.
C
      IF ( KARG ) THEN
        OPEN(10,FILE=CFILAER)
        DO I=1,ILARGE
          READ(10,'(A100)',END=9999) CDUM
          IX=0
          IF ( CDUM(1:29) == '* MODEL BOUNDARY CONDITIONS *' ) THEN
            IS=1
            NF=1
            IFIL(IS,NF)=IFIL(IS,2)
            CPRE(IS,NF)=CPRE(IS,2)
            IC(IS,NF)=IC(IS,2)
            NF=NF+1
            IFIL(IS,NF)=22
            OPEN(IFIL(IS,NF),FILE='OUTPUT_PRE_PAM')
            WRITE(IFIL(IS,NF),'(A8)') '&OVARNML'
            CPRE(IS,NF)='OVAR'
            IC(IS,NF)=0
            IX=1
          ENDIF
          IF ( CDUM(1:16) == '* OUTPUT MODEL *' ) THEN
            IS=2
            NF=1
            IX=1
          ENDIF
          IF ( CDUM(1:17) == '* RESTART MODEL *' ) THEN
            IS=3
            NF=1
            IFIL(IS,NF)=IFIL(IS,2)
            CPRE(IS,NF)=CPRE(IS,2)
            IC(IS,NF)=IC(IS,2)
            NF=NF+1
            IFIL(IS,NF)=42
            OPEN(IFIL(IS,NF),FILE='RESTART_PRE_PAM')
            WRITE(IFIL(IS,NF),'(A8)') '&RVARNML'
            CPRE(IS,NF)='RVAR'
            IC(IS,NF)=1
            WRITE(IFIL(IS,NF),'()')
            WRITE(IFIL(IS,NF),'(A22)') '! INVARIANT PARAMETERS'         
            WRITE(IFIL(IS,NF),'()')          
            CDUMX='NAME=''tstep'''
            CALL LABLL(CLAB,IC(IS,NF),CDUMX)
            WRITE(IFIL(IS,NF),CLAB) CPRE(IS,NF),'(',IC(IS,NF),')%'
     1                              ,TRIM(CDUMX)
            CDUMX='LNAME=''Time step index'''
            CALL LABLL(CLAB,IC(IS,NF),CDUMX)
            WRITE(IFIL(IS,NF),CLAB) CPRE(IS,NF),'(',IC(IS,NF),')%'
     1                              ,TRIM(CDUMX)
            CDUMX='UNITS=''1'''
            CALL LABLL(CLAB,IC(IS,NF),CDUMX)
            WRITE(IFIL(IS,NF),CLAB) CPRE(IS,NF),'(',IC(IS,NF),')%'
     1                              ,TRIM(CDUMX)
            IX=1
          ENDIF
          IF ( CDUM(1:4) == 'NAME' .OR. CDUM(1:5) == 'LNAME' 
     1        .OR. CDUM(1:5) == 'UNITS' .OR. CDUM(1:3) == 'DIM') THEN
            DO NFX=1,NF
              IF ( CDUM(1:4) == 'NAME' ) IC(IS,NFX)=IC(IS,NFX)+1
              CALL LABLL(CLAB,IC(IS,NFX),CDUM)
              WRITE(IFIL(IS,NFX),CLAB) CPRE(IS,NFX),'(',IC(IS,NFX),')%'
     1                                ,TRIM(CDUM)
            ENDDO
            IX=1
          ENDIF
          IF ( IX == 0 ) THEN
            DO NFX=1,NF
              IFN=IFIL(IS,NFX)
              CALL WRTP(IFN,CDUM)
            ENDDO
          ENDIF
        ENDDO
 9999   CONTINUE
C
C       * CLOSE FILES.
C
        CLOSE(10)
        WRITE(22,'(A1)') '/'
        CLOSE(22)
        WRITE(42,'(A1)') '/'
        CLOSE(42)
      ENDIF
C
C     * CLOSE FILES.
C
      WRITE(20,'(A1)') '/'
      WRITE(21,'(A1)') '/'
      WRITE(30,'(A1)') '/'
      WRITE(40,'(A1)') '/'
      WRITE(41,'(A1)') '/'
      CLOSE(20)
      CLOSE(21)
      CLOSE(30)
      CLOSE(40)
      CLOSE(41)
C
      END SUBROUTINE REFORML
      SUBROUTINE WRTP(IFN,CDUM)
C
      IMPLICIT NONE
C
      INTEGER, INTENT(IN) :: IFN
      CHARACTER(LEN=100), INTENT(IN) :: CDUM
      CHARACTER(LEN=6) :: CLAB
      INTEGER :: IC
C
      IC=LEN_TRIM(CDUM)
      IF ( IC > 0 ) THEN
        IF ( IC < 10 ) THEN
          WRITE(CLAB,'(A2,I1,A1)') '(A',IC,')'
        ELSE IF ( IC < 100 ) THEN
          WRITE(CLAB,'(A2,I2,A1)') '(A',IC,')'
        ELSE IF ( IC < 1000 ) THEN
          WRITE(CLAB,'(A2,I3,A1)') '(A',IC,')'
        ENDIF
        WRITE(IFN,CLAB) TRIM(CDUM)
      ELSE
        WRITE(IFN,'()')
      ENDIF
C
      END SUBROUTINE WRTP
      SUBROUTINE LABLL(CLAB,IC,CDUM)
C
      IMPLICIT NONE
C
      CHARACTER(LEN=18), INTENT(OUT) :: CLAB
      INTEGER, INTENT(IN) :: IC
      CHARACTER(LEN=100), INTENT(IN) :: CDUM
      INTEGER :: ITR
      CHARACTER(LEN=5) :: CLX
C
      ITR=LEN_TRIM(CDUM)
      IF ( ITR < 10 ) THEN
        WRITE(CLX,'(A1,I1,A1)') 'A',ITR,')'
      ELSE IF ( ITR < 100 ) THEN
        WRITE(CLX,'(A1,I2,A1)') 'A',ITR,')'
      ELSE IF ( ITR < 1000 ) THEN
        WRITE(CLX,'(A1,I3,A1)') 'A',ITR,')'
      ENDIF
      IF ( IC < 10 ) THEN
        WRITE(CLAB,'(A13,A5)') '(A4,A1,I1,A2,',CLX
      ELSE IF ( IC < 100 ) THEN
        WRITE(CLAB,'(A13,A5)') '(A4,A1,I2,A2,',CLX
      ELSE IF ( IC < 1000 ) THEN
        WRITE(CLAB,'(A13,A5)') '(A4,A1,I3,A2,',CLX
      ELSE IF ( IC < 10000 ) THEN
        WRITE(CLAB,'(A13,A5)') '(A4,A1,I4,A2,',CLX
      ENDIF
C
      END SUBROUTINE LABLL
      SUBROUTINE XIT(NAME,N)
C 
C     * OCT 01/92 - E.CHAN. (CHANGE STOP 1 TO STOP)
C     * JUN 10/91 - E.CHAN. (TRANSLATE HOLLERITH LITERALS AND 
C     *                      DIMENSION STRINGS) 
C 
C     * OCT 10/78 - J.D.HENDERSON.
C     * TERMINATES A PROGRAM BY PRINTING THE PROGRAM NAME AND 
C     * A LINE ACROSS THE PAGE FOLLOWED BY A NUMBER N. 
C 
C     * N.GE.0 IS FOR A NORMAL END. THE LINE IS DASHED. 
C     * NORMAL ENDS TERMINATE WITH   STOP.
C 
C     * N.LT.0 IS FOR AN ABNORMAL END. THE LINE IS DOTTED.
C     * IF N IS LESS THAN -100 THE PROGRAM SIMPLY TERMINATES. 
C     * OTHERWISE IF N IS LESS THAN ZERO THE PROGRAM ABORTS.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      CHARACTER*(*) NAME
      CHARACTER*8   NAME8, DASH, STAR
C 
      DATA DASH /'--------'/, STAR /'********'/ 
C---------------------------------------------------------------------
C 
      NAME8 = NAME
      IF(N.GE.0) WRITE(6,6010) DASH,NAME8,(DASH,I=1,9),N 
C 
      IF(N.LT.0) WRITE(6,6010) STAR,NAME8,(STAR,I=1,9),N 
C 
      IF ( N.GE.0 .OR. N.LT.-100 ) THEN
        STOP 
      ELSE
        CALL ABORT
      ENDIF
C 
C---------------------------------------------------------------------
 6010 FORMAT('0',A8,'  END  ',A8,9A8,I8)
      END   
