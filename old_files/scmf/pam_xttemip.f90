SUBROUTINE XTTEMIP(XEMIS,        &
    DOCEM1,DOCEM2,DOCEM3,DOCEM4, &
    DBCEM1,DBCEM2,DBCEM3,DBCEM4, &
    DSUEM1,DSUEM2,DSUEM3,DSUEM4, &
    PSUEF1,PSUEF2,PSUEF3,PSUEF4, &
    SAIRROW,SSFCROW,SBIOROW,SSHIROW,SSTKROW,SFIRROW, &
    BAIRROW,BSFCROW,BBIOROW,BSHIROW,BSTKROW,BFIRROW, &
    OAIRROW,OSFCROW,OBIOROW,OSHIROW,OSTKROW,OFIRROW, &
    FBBCROW,FAIRROW,PRESSG,DSHJ,ZF,ZFS,ZTMST,LEVWF,  &
    LEVAIR,ISO2,IL1,IL2,ILG,ILEV,NTRAC)

! ILGA                  I       Number of horizontal grid points
! IL1:IL2               I       Range of horizontal grid point indices
! ILEV                  I       Number of vertical levels, where we count levels from the top to the bottom
! PSUEF                 I       Fraction of SO2 that is emitted as primary sulfate particles, i.e. converted to sulfate at the source
! PRESSG                I       Surface pressure
! DSHJ                  I       Vertical depth of the grid cells (hybrid variable),
! ZF                    I       Height (in m) of the grid cell interfaces
! ZTMST                 I       Time step (in sec).
!
! The following fields are the 2D emission input fields:
! SAIRROW,SSFCROW,SBIOROW,SSHIROW,SSTKROW,SFIRROW,  BAIRROW,BSFCROW,BBIOROW,BSHIROW,BSTKROW,BFIRROW,  OAIRROW,OSFCROW,OBIOROW,OSHIROW,OSTKROW,OFIRROW,
! Where the first letter refers to the emitted species (B - BC, S - Sulfur, O - organic aerosol) and the following 3 letters to the type of emissions
! (AIR - aircraft, SFC - surface emissions, BIO - biomass burning emissions, SHI - shipping emissions, STK - emissions from stacks, FIR - 3D forest fire emissions).
!
! XEMIS(:,:,ISO2)       O       3D SO2 emissions
!
! Emissions of primary aerosol species are saved in DBCEM#  (BC), DSUEM# (ammonium sulfate), DOCEM# (organic aerosol),
! where the number (1..4) refers to the type of emissions (we use different size distributions for different sources).
!
      IMPLICIT NONE
!
      INTEGER, INTENT(IN) :: LEVWF,LEVAIR,ISO2,IL1,IL2,ILG,ILEV,NTRAC
      REAL, INTENT(IN) :: PSUEF1,PSUEF2,PSUEF3,PSUEF4,ZTMST
      REAL, INTENT(IN), DIMENSION(ILG) :: PRESSG
      REAL, INTENT(IN), DIMENSION(ILG,ILEV)  :: DSHJ,ZF
      REAL, INTENT(IN), DIMENSION(ILG,ILEV+1):: ZFS
      REAL, INTENT(IN), DIMENSION(ILG,LEVWF) :: FBBCROW
      REAL, INTENT(IN), DIMENSION(ILG,LEVAIR):: FAIRROW
      REAL, INTENT(IN), DIMENSION(ILG) :: &
                    SAIRROW,SSFCROW,SBIOROW,SSHIROW,SSTKROW,SFIRROW, &
                    BAIRROW,BSFCROW,BBIOROW,BSHIROW,BSTKROW,BFIRROW, &
                    OAIRROW,OSFCROW,OBIOROW,OSHIROW,OSTKROW,OFIRROW
      REAL, INTENT(OUT), DIMENSION(ILG,ILEV) :: &
                                         DOCEM1,DOCEM2,DOCEM3,DOCEM4, &
                                         DBCEM1,DBCEM2,DBCEM3,DBCEM4, &
                                         DSUEM1,DSUEM2,DSUEM3,DSUEM4
      REAL, INTENT(OUT), DIMENSION(ILG,ILEV,NTRAC) :: XEMIS

! Parameters
      REAL, PARAMETER :: WS      = 32.064
      REAL, PARAMETER :: WSO2    = 64.059
      REAL, PARAMETER :: WH2SO4  = 98.073
      REAL, PARAMETER :: WNH3    = 17.030
      REAL, PARAMETER :: WAMSUL  = WH2SO4+2.0*WNH3              ! H2SO4 + 2NH3 = (NH4)2SO4
      REAL, PARAMETER :: WRAT    = WAMSUL/WSO2
      REAL, PARAMETER :: OC2POM  = 1.4
      REAL, PARAMETER :: GRAV    = 9.80616
      REAL, PARAMETER, DIMENSION(0:6)  :: ZWF  = [ 0.,  100.,  500., 1000., 2000., 3000., 6000.]
      REAL, PARAMETER, DIMENSION(0:25) :: ZAIR = [ 0.,  610., 1220., 1830., 2440., 3050.,  &
                                                       3660., 4270., 4880., 5490., 6100.,  &
                                                       6710., 7320., 7930., 8540., 9150.,  &
                                                       9760.,10370.,10980.,11590.,12200.,  &
                                                      12810.,13420.,14030.,14640.,15250.     ]

! Local variables
      INTEGER, DIMENSION(ILG) ::  LLOW, LHGH
      INTEGER :: IL, L, K
      REAL :: FACT, FACTA(ILG,ILEV), SFFE, BFFE, OFFE, SBBE, BBBE, OBBE, ZMIN, ZMAX, DZ, WGT, RUH, RLH, DUL

! Check dimensions
      IF (LEVWF  /=  6) CALL XIT("XTTEMI",-1)
      IF (LEVAIR /= 25) CALL XIT("XTTEMI",-2)

!-------------------------------------------------------------------
! Surface emissions. Insert 2d emissions into first model layer above ground
      DO IL=IL1,IL2
          FACTA(IL,:) = ZTMST*GRAV/(DSHJ(IL,:)*PRESSG(IL))
      ENDDO

      DO IL=IL1,IL2
        FACT = FACTA(IL,ILEV)  !ZTMST*GRAV/(DSHJ(IL,ILEV)*PRESSG(IL))

! FOSSIL-FUEL SURFACE EMISSIONS.
        SFFE=MAX(SSFCROW(IL)-SBIOROW(IL)-SSHIROW(IL),0.)
        BFFE=MAX(BSFCROW(IL)-BBIOROW(IL)-BSHIROW(IL),0.)
        OFFE=MAX(OSFCROW(IL)-OBIOROW(IL)-OSHIROW(IL),0.)
        XEMIS (IL,ILEV,ISO2) = XEMIS (IL,ILEV,ISO2) + SFFE*FACT*(1.-PSUEF2)
        DSUEM2(IL,ILEV)      = DSUEM2(IL,ILEV)      + SFFE*FACT/ZTMST*PSUEF2*WAMSUL/WS
        DBCEM2(IL,ILEV)      = DBCEM2(IL,ILEV)      + BFFE*FACT/ZTMST
        DOCEM2(IL,ILEV)      = DOCEM2(IL,ILEV)      + OFFE*FACT/ZTMST*OC2POM

! BIOMASS/BIOFUEL BURNING EMISSIONS.
        SBBE=SBIOROW(IL)
        BBBE=BBIOROW(IL)
        OBBE=OBIOROW(IL)
        XEMIS (IL,ILEV,ISO2) = XEMIS (IL,ILEV,ISO2) + SBBE*FACT*(1.-PSUEF1)
        DSUEM1(IL,ILEV)      = DSUEM1(IL,ILEV)      + SBBE*FACT/ZTMST*PSUEF1*WAMSUL/WS
        DBCEM1(IL,ILEV)      = DBCEM1(IL,ILEV)      + BBBE*FACT/ZTMST
        DOCEM1(IL,ILEV)      = DOCEM1(IL,ILEV)      + OBBE*FACT/ZTMST*OC2POM

! SHIPPING SURFACE EMISSIONS.
        SBBE=SSHIROW(IL)
        BBBE=BSHIROW(IL)
        OBBE=OSHIROW(IL)
        XEMIS (IL,ILEV,ISO2) = XEMIS (IL,ILEV,ISO2) + SBBE*FACT*(1.-PSUEF4)
        DSUEM4(IL,ILEV)      = DSUEM4(IL,ILEV)      + SBBE*FACT/ZTMST*PSUEF4*WAMSUL/WS
        DBCEM4(IL,ILEV)      = DBCEM4(IL,ILEV)      + BBBE*FACT/ZTMST
        DOCEM4(IL,ILEV)      = DOCEM4(IL,ILEV)      + OBBE*FACT/ZTMST*OC2POM
      ENDDO

!-------------------------------------------------------------------
!     * DETERMINE LAYER CORRESPONDING TO STACK EMISSIONS (100-300M)
!
      ZMIN=100.
      ZMAX=300.
      DZ=ZMAX-ZMIN
      LLOW(:) = 0
      LHGH(:) = 0
      DO L=ILEV,1,-1
        DO IL=IL1,IL2
          IF(ZF(IL,L) .LE. ZMIN) LLOW(IL)=L
          IF(ZF(IL,L) .LE. ZMAX) LHGH(IL)=L
        ENDDO
      ENDDO

!     * FOSSIL-FUEL STACK EMISSIONS.
      DO L=1,ILEV
         DO IL=IL1,IL2
            FACT = FACTA(IL,L)  !ZTMST*GRAV/(DSHJ(IL,L)*PRESSG(IL))
            IF ( L.EQ.LLOW(IL) .AND. L.EQ.LHGH(IL) ) THEN
              WGT=1.
            ELSE IF ( L.EQ.LHGH(IL) ) THEN
              WGT=(ZMAX-ZF(IL,L))/DZ
            ELSE IF ( L.GT.LHGH(IL) .AND. L.LT.LLOW(IL) .AND. L.GT.1 ) THEN
              WGT=(ZF(IL,L-1)-ZF(IL,L))/DZ
            ELSE IF ( L.EQ.LLOW(IL) .AND. L.GT. 1 ) THEN
              WGT=(ZF(IL,L-1)-ZMIN)/DZ
            ELSE
              WGT=0.
            ENDIF
            if (WGT/=0) then
                XEMIS (IL,L,ISO2) = XEMIS(IL,L,ISO2) + WGT*SSTKROW(IL)*FACT*(1.-PSUEF2)
                DSUEM2(IL,L)      = DSUEM2(IL,L)     + WGT*SSTKROW(IL)*FACT/ZTMST*PSUEF2*WAMSUL/WS
                DBCEM2(IL,L)      = DBCEM2(IL,L)     + WGT*BSTKROW(IL)*FACT/ZTMST
                DOCEM2(IL,L)      = DOCEM2(IL,L)     + WGT*OSTKROW(IL)*FACT/ZTMST*OC2POM
            endif
         ENDDO
      ENDDO

!-------------------------------------------------------------------
!     * OPEN VEGETATION BURNING EMISSIONS
!
      DO K=1,LEVWF
        RUH = ZWF(K)
        RLH = ZWF(K-1)
        DUL = RUH-RLH

!       * DETERMINE LAYER CORRESPONDING TO EMISSIONS.
        LLOW(:) = 0
        LHGH(:) = 0

        DO L=ILEV,1,-1
          DO IL=IL1,IL2
            IF( ZF(IL,L) .LE. RLH)   LLOW(IL)=L
            IF( ZF(IL,L) .LE. RUH)   LHGH(IL)=L
            IF( RLH.LE.0.)           LLOW(IL)=ILEV
          ENDDO
        ENDDO
        DO L=1,ILEV
           DO IL=IL1,IL2
              if (FBBCROW(IL,K)/=0) then
                  FACT = FACTA(IL,L)  !ZTMST*GRAV/(DSHJ(IL,L)*PRESSG(IL))
                  IF ( L.EQ.LLOW(IL) .AND. L.EQ.LHGH(IL) ) THEN
                    WGT=1.
                  ELSE IF( L.EQ.LHGH(IL) ) THEN
                    WGT=( RUH-ZF(IL,L) )/DUL
                  ELSE IF( L.GT.LHGH(IL) .AND. L.LT.LLOW(IL) .AND. L.GT.1 ) THEN
                    WGT=(ZF(IL,L-1)-ZF(IL,L))/DUL
                  ELSE IF( L.EQ.LLOW(IL) .AND. L.GT.1 ) THEN
                    WGT=( ZF(IL,L-1)-RLH )/DUL
                  ELSE
                    WGT=0.
                  ENDIF
                  if (WGT/=0) then
                      XEMIS (IL,L,ISO2) = XEMIS(IL,L,ISO2) + WGT*SFIRROW(IL)*FACT*FBBCROW(IL,K)*(1.-PSUEF1)
                      DSUEM1(IL,L)      = DSUEM1(IL,L)     + WGT*SFIRROW(IL)*FACT*FBBCROW(IL,K)/ZTMST*PSUEF1*WAMSUL/WS
                      DBCEM1(IL,L)      = DBCEM1(IL,L)     + WGT*BFIRROW(IL)*FACT*FBBCROW(IL,K)/ZTMST
                      DOCEM1(IL,L)      = DOCEM1(IL,L)     + WGT*OFIRROW(IL)*FACT*FBBCROW(IL,K)/ZTMST*OC2POM
                  endif
              endif
              ENDDO
         ENDDO
      ENDDO

!-------------------------------------------------------------------
!     * AIRCRAFT EMISSIONS
!
      DO K=1,LEVAIR
        RUH = ZAIR(K)
        RLH = ZAIR(K-1)
        DUL = RUH-RLH

!       * DETERMINE LAYER CORRESPONDING TO EMISSIONS.
        LLOW(:) = 0
        LHGH(:) = 0

        DO L=ILEV+1,1,-1
          DO IL=IL1,IL2
            IF (ZFS(IL,L) .LE. RLH) LLOW(IL)=L
            IF (ZFS(IL,L) .LE. RUH) LHGH(IL)=L
            IF (RLH.LE.0.)          LLOW(IL)=ILEV
          ENDDO
        ENDDO
        DO L=1,ILEV
           DO IL=IL1,IL2
              if (FAIRROW(IL,K)/=0) then
                  FACT = FACTA(IL,L)  !ZTMST*GRAV/(DSHJ(IL,L)*PRESSG(IL))
                  IF ( L.EQ.LLOW(IL) .AND. L.EQ.LHGH(IL) ) THEN
                    WGT=1.
                  ELSE IF( L.EQ.LHGH(IL) ) THEN
                    WGT=(RUH-ZFS(IL,L))/DUL
                  ELSE IF( L.GT.LHGH(IL) .AND. L.LT.LLOW(IL) .AND. L.GT.1 ) THEN
                    WGT=(ZFS(IL,L-1)-ZFS(IL,L))/DUL
                  ELSE IF( L.EQ.LLOW(IL) .AND. L.GT.1 ) THEN
                    WGT=(ZFS(IL,L-1)-RLH)/DUL
                  ELSE
                    WGT=0.
                  ENDIF
                  if (WGT/=0) then
                      XEMIS (IL,L,ISO2) = XEMIS(IL,L,ISO2) + WGT*SAIRROW(IL)*FACT*FAIRROW(IL,K)*(1.-PSUEF3)
                      DSUEM3(IL,L)      = DSUEM3(IL,L)     + WGT*SAIRROW(IL)*FACT*FAIRROW(IL,K)/ZTMST*PSUEF3*WAMSUL/WS
                      DBCEM3(IL,L)      = DBCEM3(IL,L)     + WGT*BAIRROW(IL)*FACT*FAIRROW(IL,K)/ZTMST
                      DOCEM3(IL,L)      = DOCEM3(IL,L)     + WGT*OAIRROW(IL)*FACT*FAIRROW(IL,K)/ZTMST*OC2POM
                  endif
              endif
           ENDDO
        ENDDO
      ENDDO

END SUBROUTINE XTTEMIP
