      SUBROUTINE CLDPHYS(ZCLF,ZRFLN,CLRFR,CLRFS,ZFRAIN,ZFSNOW,ZFEVAP, &
                         ZFSUBL,ZMLWC,ZMRATEP,RH,RHC,HMN,QT,QR,QLR, &
                         ZCDNC,QCWA,TH,PH,ZH,DP,ALMC,ALMX,DT,ILG,ILEV, &
                         ICASE)
!
      USE SDPHYS
      USE SDCODE
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL, INTENT(INOUT), DIMENSION(ILG,ILEV) :: QR, QLR, TH, ZCLF
      REAL, INTENT(IN), DIMENSION(ILG,ILEV) :: PH, ZH, ALMC, ALMX, &
                                               DP, ZCDNC
      REAL, INTENT(OUT), DIMENSION(ILG,ILEV) :: HMN, QT, RH, RHC, &
                                                ZMLWC, CLRFR, CLRFS, &
                                                ZFRAIN, ZFSNOW, ZFEVAP, &
                                                ZFSUBL, ZMRATEP, QCWA
      REAL, INTENT(OUT), DIMENSION(ILG) :: ZRFLN
      REAL, DIMENSION(ILG,ILEV) :: QTN, HMNN, CVAR, ZCRAUT, QCWVAR, &
                                   CVSG, CVDU, ZFRAC, QC
      REAL, DIMENSION(ILG) :: PMBS, RRL, CPM, ALMIX, DHLDZ, DRWDZ, &
                              FLAGEST, QCW, SSH
      REAL, DIMENSION(ILG) :: ZCLFRM, ZCLFR, ZCLRR, ZRNFT, &
                              ZRCLR, ZCLFSM, ZCLFS, ZSFLN, ZCLRS, &
                              ZSNFT, ZSCLR, VTR, ZRPR, ZEVP, ZXLB
      INTEGER, PARAMETER :: ITER=10
      INTEGER, PARAMETER :: ISPCDNC=0  ! SWITCH TO SPECIFIED CDNC
      REAL, PARAMETER :: SPCDNC=300.E+06
!
      ESW(TTT) = EXP(RW1+RW2/TTT)*TTT**RW3

!
!     * ADIABATIC CORE.
!
!     * TOTAL WATER MIXING RATIO AND CLOUD WATER STATIC ENERGY.
!
      DO L=1,ILEV
        QTN(:,L)=QR(:,ILEV)+QLR(:,ILEV)
        HMNN(:,L)=CPRES*TH(:,ILEV)+GRAV*ZH(:,ILEV)-RL*QLR(:,ILEV)
      ENDDO
      FACTE=1./EPS1-1.
      I2NDIE=1
      FLAGEST=1.
      CVSG=0.
      CVDU=0.
      ZFRAC=0.
      ICVSG=1
      ISUBG=0
      ICALL=1
      DO JK=1,ILEV
        DO IL=1,ILG
          PMBS(IL)=0.01*PH(IL,JK)
          RRL(IL)=RL
          CPM(IL)=CPRES
          ALMIX(IL)=MAX(ALMC(IL,JK),ALMX(IL,JK),10.)
          IF(JK.GT.1)                              THEN
            DZ       =ZH   (IL,JK-1)-ZH  (IL,JK)
            DHLDZ(IL)=(HMNN(IL,JK-1)-HMNN(IL,JK))/DZ
            DRWDZ(IL)=(QTN (IL,JK-1)-QTN (IL,JK))/DZ
          ELSE
            DHLDZ(IL)=0.
            DRWDZ(IL)=0.
          ENDIF
        ENDDO
        CALL STATCLD (QCW,ZCLF(1,JK),CVAR(1,JK),ZCRAUT(1,JK), &
                      QCWVAR(1,JK),SSH,CVSG(1,JK),QTN(1,JK), &
                      HMNN(1,JK),ZFRAC(1,JK),CPM, &
                      PMBS,ZH(1,JK),RRL,CVDU(1,JK),FLAGEST, &
                      ALMIX,DHLDZ,DRWDZ,I2NDIE, &
                      DT,ILEV,ILG,1,ILG,ICVSG,ISUBG, &
                      JK,ICALL)
!
        DO IL=1,ILG
          QCWA(IL,JK)=QCW(IL)
        ENDDO
      ENDDO
!
!     * TOTAL WATER MIXING RATIO AND CLOUD WATER STATIC ENERGY.
!
      QTN=QR+QLR
      HMNN=CPRES*TH+GRAV*ZH-RL*QLR
!
!     * CLOUD FRACTION, EVAPORATION, AND CONDENSATION FROM STATISTICAL
!     * CLOUD SCHEME.
!
      FACTE=1./EPS1-1.
      I2NDIE=1
      FLAGEST=1.
      CVSG=0.
      CVDU=0.
      ZFRAC=0.
      ICVSG=1
      ISUBG=0
      ICALL=1
      DO JK=1,ILEV
        DO IL=1,ILG
          PMBS(IL)=0.01*PH(IL,JK)
          RRL(IL)=RL
          CPM(IL)=CPRES
          ALMIX(IL)=MAX(ALMC(IL,JK),ALMX(IL,JK),10.)
          IF(JK.GT.1)                              THEN
            DZ       =ZH   (IL,JK-1)-ZH  (IL,JK)
            DHLDZ(IL)=(HMNN(IL,JK-1)-HMNN(IL,JK))/DZ
            DRWDZ(IL)=(QTN (IL,JK-1)-QTN (IL,JK))/DZ
          ELSE
            DHLDZ(IL)=0.
            DRWDZ(IL)=0.
          ENDIF
        ENDDO
        CALL STATCLD (QCW,ZCLF(1,JK),CVAR(1,JK),ZCRAUT(1,JK), &
                      QCWVAR(1,JK),SSH,CVSG(1,JK),QTN(1,JK), &
                      HMNN(1,JK),ZFRAC(1,JK),CPM, &
                      PMBS,ZH(1,JK),RRL,CVDU(1,JK),FLAGEST, &
                      ALMIX,DHLDZ,DRWDZ,I2NDIE, &
                      DT,ILEV,ILG,1,ILG,ICVSG,ISUBG, &
                      JK,ICALL)
!
        DO IL=1,ILG
          QCWI=ZFRAC(IL,JK)*QCW(IL)
          QCWL=(1.-ZFRAC(IL,JK))*QCW(IL)
!
!         * CONDENSATION AND EVAPORATION.
!
          ZCND=QCW(IL)-QLR(IL,JK)
          ZCND=MAX(ZCND,-QLR(IL,JK))
          ASCL=1.
          IF ( ZCND.GT.0. ) THEN
            ASCL=MIN(MAX((QR(IL,JK)-1.E-20)/ZCND,0.),1.)
          ENDIF
          ZCND=ASCL*ZCND
!
!         * UPDATE TEMPERATURE, VAPOUR, AND CONDENSATE.
!
          TH(IL,JK)=TH(IL,JK)+ZCND*RL/CPRES
          QR(IL,JK)=QR(IL,JK)-ZCND
          QLR(IL,JK)=QLR(IL,JK)+ZCND
!
!         * RELATIVE HUMIDITY FOR ALL- AND CLEAR-SKY. THE RELATIVE
!         * HUMIDITY IS CALCULATED BASED ON THE VAPOUR PRESSURE IF THE
!         * SATURATION VAPOUR PRESSURE EXCEEDS THE TOTAL AIR PRESSURE,
!         * I.E. FOR CONDITIONS THAT DO NOT PERMIT FORMATION OF CLOUDS.
!         * OTHERWISE, THE RELATIVE HUMIDITY IS CALCULATED BASED ON
!         * SPECIFIC HUMIDITY (SEE CHAPTER 4 IN EMANUEL'S TEXTBOOK ON
!         * CONVECTION) IN ORDER TO PERMIT CALCULATIONS FOR CLEAR-SKY.
!
          IF(ZCLF(IL,JK).LT..99) THEN
            QC (IL,JK)=(QR(IL,JK)-ZCLF(IL,JK)*SSH(IL))/(1.-ZCLF(IL,JK))
            RHC(IL,JK)=MIN(MAX((QC(IL,JK)*(1.+SSH(IL)*FACTE))/(SSH(IL)*(1.+QC(IL,JK)*FACTE)),0.),1.)
            RH (IL,JK)=ZCLF(IL,JK)+(1.-ZCLF(IL,JK))*RHC(IL,JK)
          ELSE
            QC (IL,JK)=QR(IL,JK)
            RH (IL,JK)=1.
            RHC(IL,JK)=RH(IL,JK)
          ENDIF
!
!         * DIAGNOSE LIQUID WATER STATIC ENERGY AND TOTAL WATER
!         * IN THE CLEAR-SKY ENVIRONMENT OF THE CLOUD.
!
          QT(IL,JK)=QC(IL,JK)
          HMN(IL,JK)=CPRES*TH(IL,JK)+GRAV*ZH(IL,JK)
        ENDDO
      ENDDO
!
!     * CONSTANTS AND INTIALIZATIONS.
!
      ZSEC=1.E-9
      ZEPCLC=1.E-2
      API=3.1415926535897
      COAUT=3.
      COACC=1.15
      COTMP1=COAUT-2.
      COTMP2=COACC-1.
      ZLVDCP=RL/CPRES
      AKR=141.4
      ZKA=0.024
      ZRFLNL_MAX=0.028
      FACACC=1.
      ZCONS2=1./(DT*GRAV)
      RGASV=461.50
      EPSLIM=0.001
      ZXSEC=1.-1.E-12
      ZMRATEP=0.
      ZMLWC=0.
      CLRFR=0.
      CLRFS=0.
      ZFRAIN=0.
      ZFSNOW=0.
      ZFEVAP=0.
      ZFSUBL=0.
      ZCLFR=0.
      ZRFLN=0.
      ZCLFS=0.
      ZSFLN=0.
      ZCLRR=0.
      ZCLRS=0.
      DO JK=1,ILEV
      ZSR=0.
      DO IL=1,ILG
!
!     * RAIN AREA FRACTION FROM MAXIMUM/RANDOM CLOUD OVERLAP ASSUMPTION.
!     * ONLY CLOUDS THAT ARE SUBJECT TO WARM RAIN PROCESSES ARE
!     * CONSIDERED.
!
       ZCLFRM(IL)=ZCLFR(IL)
       IF ( ZRFLN(IL) .GT. 1.E-20 ) THEN
         ZCLFR(IL)=ZCLF(IL,JK)
       ELSE
         ZCLFR(IL)=0.
       ENDIF
       IF ( ZCLFRM(IL).LT.1. ) THEN
         ZCLRR(IL)=ZCLRR(IL)*(1.-MAX(ZCLFR(IL),ZCLFRM(IL))) &
                            /(1.-ZCLFRM(IL))
       ENDIF
       ZRNFT(IL)=1.-ZCLRR(IL)
!
!      * FRACTION OF GRID CELL FOR WHICH CLEAR-SKY IS AFFECTED BY RAIN.
!
       ZRCLR(IL)=MIN(MAX(ZRNFT(IL)-ZCLFR(IL),0.),1.)
!
!     * SNOW AREA FRACTION FROM MAXIMUM/RANDOM CLOUD OVERLAP ASSUMPTION.
!     * ONLY CLOUDS THAT ARE AFFECTED BY SNOW ARE CONSIDERED.
!
       ZCLFSM(IL)=ZCLFS(IL)
       IF ( ZSFLN(IL) .GT. 1.E-20 ) THEN
         ZCLFS(IL)=ZCLF(IL,JK)
       ELSE
         ZCLFS(IL)=0.
       ENDIF
       IF ( ZCLFSM(IL).LT.1. ) THEN
         ZCLRS(IL)=ZCLRS(IL)*(1.-MAX(ZCLFS(IL),ZCLFSM(IL))) &
                            /(1.-ZCLFSM(IL))
       ENDIF
       ZSNFT(IL)=1.-ZCLRS(IL)
!
!      * FRACTION OF GRID CELL FOR WHICH CLEAR-SKY IS AFFECTED BY SNOW.
!
       ZSCLR(IL)=MIN(MAX(ZSNFT(IL)-ZCLFS(IL),0.),1.)
!
!      * RAIN WATER MIXING RATIO FROM RAINFALL RATE (FROM ROTSTAYN)
!
       ZRHO0=PH(IL,JK)/(RGAS*TH(IL,JK))
       ZQRHO0=1.3/ZRHO0
       ZXRP1=0.
       VTR(IL)=0.
       IF ( ZRNFT(IL).GT.ZEPCLC ) THEN
         ZRFLNL=ZRFLN(IL)/ZRNFT(IL)
         ZRFLNL=MIN(ZRFLNL,ZRFLNL_MAX)
         IF ( ZRFLNL.GT.1.E-20 ) THEN
           ALAMBR=714.*(1.2/ZRHO0)**(1./9.)*ZRFLNL**(-0.22)
           VTR(IL)=1.94*AKR*SQRT(1.2/(ZRHO0*ALAMBR))
           ZXRP1=ZRFLNL/(ZRHO0*VTR(IL))
         ENDIF
       ENDIF
!
!      * AUTOCONVERSION AND ACCRETION. 4 DIFFERENT AUTOCONVERSION
!      * PARAMETERIZATIONS ARE AVAILABLE.
!
       ZXLB(IL)=QLR(IL,JK)
       IF( ZCLF(IL,JK).GT.ZEPCLC .AND. ZXLB(IL).GT.ZSEC ) THEN
         ZXLB(IL)=QLR(IL,JK)/ZCLF(IL,JK)
       ELSE
         ZXLB(IL)=QLR(IL,JK)
       ENDIF
       IF ( ISPCDNC == 1 ) THEN
         CDNC=SPCDNC
       ELSE
         CDNC=ZCDNC(IL,JK)
       ENDIF
       IF ( ZXLB(IL)*ZCLF(IL,JK).GE.ZSEC ) THEN
         IF ( ICASE <= 3 ) THEN
!
!          * PARAMETERIZATION BY WOOD (2005)
!
           IF ( ICASE .EQ. 1 ) THEN
!            * WITH CONSTANT CLOUD DROPLET NUMBER.
!
             CDNR=120.E+06
             CDNP=0.
             FACAUTN=1.3E+09
           ELSE IF ( ICASE .EQ. 2 ) THEN
!            * ORIGINAL WOOD PARAMETERIZATION.
!
             CDNR=0.
             CDNP=1.
             FACAUTN=1.3E+09
           ELSE IF ( ICASE .EQ. 3 ) THEN
!            * WITH PARAMETERS SPECIFIED ACCORDING TO LIU AND DAUM (2004).
!
             CDNR=0.
             CDNP=1.
             FACAUTN=1.08E+10
           ENDIF
           CDNE=(CDNR**(1.-CDNP))*CDNC**CDNP
           RVOL=1.E+06*(3.*ZXLB(IL)/(4.*API*RHOH2O*CDNE))**(1./3.)
           BETA6=((RVOL+3.)/RVOL)**(1./3.)
           R6=BETA6*RVOL
           R6C=7.5/(ZXLB(IL)**(1./6.)*SQRT(R6))
           EP=FACAUTN*(BETA6**6)
           IF ( R6 >= R6C ) THEN
             FAAUT=ZCRAUT(IL,JK)*EP/CDNE
           ELSE
             FAAUT=0.
           ENDIF
         ELSE IF ( ICASE .EQ. 4 ) THEN
!          * KHAIROUTDINOV AND KOGAN (2000)
!
           COAUT=2.47
           COTMP1=COAUT-2.
           FACAUT=2.5
           FAAUT=FACAUT*ZCRAUT(IL,JK)*1350.
           FAAUT=FAAUT*(CDNC/1.E+06)**(-1.79)
         ELSE
           CALL XIT('CLDPHYS',-1)
         ENDIF
!
!        * INITIAL GUESS AND SUBSEQUENT ITERATIONS.
!
         ZQEST1=(ZXLB(IL)**(1.-COAUT) &
               -DT*FAAUT*(1.-COAUT))**(1./(1.-COAUT))
         ZQEST=MIN(MAX(ZQEST1,ZSEC),ZXLB(IL))
         FAACC=0.
         IF ( ZXRP1.GT.ZSEC ) THEN
           FAACC=FACACC*67.*ZXRP1**COACC
           ZQEST2=(ZXLB(IL)**(1.-COACC) &
                 -DT*FAACC*(1.-COACC))**(1./(1.-COACC))
           ZQEST=MAX(MIN(ZQEST,ZQEST2),ZSEC)
           APK=-LOG(ZQEST/ZXLB(IL))/DT
           IF ( ZQEST.LT.ZXLB(IL) .AND. APK.GT.0. ) THEN
             ALPHA=((1.-ZQEST/ZXLB(IL))*ZXLB(IL)/(APK*DT)-ZQEST) &
                  /(ZXLB(IL)-ZQEST)
             ALPHA=MIN(MAX(ALPHA,0.),1.)
             DO IT=1,ITER
               AQHAT =ALPHA*ZXLB(IL)+(1.-ALPHA)*ZQEST
               AQHAT1=AQHAT**COTMP1
               AQHAT2=AQHAT**COTMP2
               ATERM1=FAAUT*AQHAT1*ZXLB(IL)+FAACC*AQHAT2
               ATERM2=FAAUT*AQHAT1*ZQEST   +FAACC*AQHAT2
               AEXPF =EXP(-FAACC*AQHAT2*DT)
               AFUN=ATERM1*ZQEST-AEXPF*ATERM2*ZXLB(IL)
               DQHDQ=1.-ALPHA
               ATERM3=FAAUT*COTMP1*AQHAT1*DQHDQ/AQHAT
               ATERM4=FAACC*COTMP2*AQHAT2*DQHDQ/AQHAT
               ADFUN=ATERM1+ZQEST*(ATERM3*ZXLB(IL)+ATERM4) &
                    +AEXPF*ZXLB(IL)*(DT*ATERM4*ATERM2 &
                                -ATERM3*ZQEST+FAAUT*AQHAT1+ATERM4)
               ZQEST=MAX(ZQEST-AFUN/ADFUN,ZSEC)
             ENDDO
           ENDIF
         ENDIF
         ZRPR(IL)=MIN(ZCLF(IL,JK)*ZXLB(IL), &
                      MAX(-ZCLF(IL,JK)*(ZQEST-ZXLB(IL)),0.))
       ELSE
         ZRPR(IL)=ZCLF(IL,JK)*ZXLB(IL)
       ENDIF
       IF(ZCLF(IL,JK).GT.ZEPCLC) THEN
         ZMRATEP(IL,JK)=ZRPR(IL)/ZCLF(IL,JK)
         ZMLWC(IL,JK)=ZXLB(IL)
       ENDIF
       ZZDRR=MAX(0.,ZCONS2*DP(IL,JK)*ZRPR(IL))
       ZRFLN(IL)=ZRFLN(IL)+ZZDRR
       ZSR=ZSR+ZRFLN(IL)
!
!      * EVAPORATION OF RAIN BELOW CLOUDS (FROM LOHMANN).
!
       ZEVP(IL)=0.
       EPS2=1.-EPS1
       IF ( ZSR .GT. 0. .AND. ZRFLN(IL).GT.0. .AND. ZRCLR(IL).GT.0. &
          ) THEN
         ZRFLNL=ZRFLN(IL)/ZRNFT(IL)
         ZRFLNL=MIN(ZRFLNL,ZRFLNL_MAX)
         RHODZ=DP(IL,JK)/GRAV
         ETMP=ESW(TH(IL,JK))
         ESTREF=0.01*PH(IL,JK)*(1.-EPSLIM)/(1.-EPSLIM*EPS2)
         IF ( ETMP.LT.ESTREF ) THEN
           ESAT=ETMP
         ELSE
           ESAT=ESTREF
         ENDIF
         QSW=EPS1*ESAT/(0.01*PH(IL,JK)-EPS2*ESAT)
         RHCW=MIN(MAX((QC(IL,JK)*(1.+QSW*FACTE)) &
                     /(QSW*(1.+QC(IL,JK)*FACTE)),0.),1.)
         SUSATW=RHCW-1.
         ZDV=2.21/PH(IL,JK)
         ZLV=ZLVDCP*CPRES
         ZAST=ZLV*(ZLV/(RGASV*TH(IL,JK))-1.)/(ZKA*TH(IL,JK))
         ZBST=RGASV*TH(IL,JK)/(ZDV*ESAT)
         ZZEPR1=870.*SUSATW*ZRFLNL**0.61/(SQRT(ZRHO0)*(ZAST+ZBST))
         ZZEPR=ZZEPR1*RHODZ
         ZPLIM=-ZXSEC*ZRFLN(IL)/ZRNFT(IL)
         ITOTEVP=0
         IF ( ZZEPR .LT. ZPLIM ) THEN
           ZZEPR=ZPLIM
           ITOTEVP=1
         ENDIF
         ZEVP(IL)=-ZZEPR*DT*ZRCLR(IL)/RHODZ
         ZEVP(IL)=MIN(ZEVP(IL), &
                 MAX(ZXSEC*((1.-RHCW)*QC(IL,JK))*ZRCLR(IL),0.))
         ZEVP(IL)=MAX(ZEVP(IL),0.)
         ZZEPR=-ZEVP(IL)*RHODZ/DT
         ZFRAIN(IL,JK)=ZFRAIN(IL,JK)+ZRFLN(IL)
         CLRFR(IL,JK)=ZRCLR(IL)
         IF ( ITOTEVP .EQ. 1 ) THEN
            ZFEVAP(IL,JK)=MIN(MAX(0.,-ZZEPR/ZRFLN(IL)),1.)
         ENDIF
         ZRFLN(IL)=ZRFLN(IL)+ZZEPR
       ENDIF
!
!      * TENDENCIES.
!
       TH(IL,JK)=TH(IL,JK)-ZEVP(IL)*RL/CPRES
       QR(IL,JK)=QR(IL,JK)+ZEVP(IL)
       QLR(IL,JK)=QLR(IL,JK)-ZRPR(IL)
      ENDDO
      ENDDO
!
      END SUBROUTINE CLDPHYS

      SUBROUTINE STATCLD (QCW,ZCLF,SIGMA,ZCRAUT,QCWVAR,SSH,CVSG, &
                          QT,HMN,ZFRAC,CPM,P,Z,RRL,CONS,CRH, &
                          ALMIX,DHLDZ,DRWDZ,I2NDIE, &
                          ZTMST,ILEV,ILG,IL1,IL2,ICVSG,ISUBG, &
                          LVL,ICALL)
!
!     * AUG 16/2013 - K.VONSALZEN. Cosmetic change to abort condition
!     *                            where now is QCW.GT.QT instead of
!     *                            QCW.GE.QT. This does not change
!     *                            the answer but prevents infrequent
!     *                            crashes with small downward
!     *                            moisture fluxes causing very small
!     *                            negative total water.
!     * JUL 31/2013 - M.LAZARE.    Cosmetic change to pass in LVL
!     *                            and ICALL to aid in future debugging.
!     * JUN 25/2013 - K.VONSALZEN. New version for gcm17:
!     *                            - Second indirect effect parameter
!     *                              I2NDIE passed in and used.
!     *                            - Function ZCR replaces look-up
!     *                              table ZCR to calculate autoconversion
!     *                              efficiency.
!     * APR 29/2012 - K.VONSALZEN. PREVIOUS VERSION STATCLD4 FOR GCM16:
!     *                            ELIMINATE CLOUDS AND CONDENSATE IF
!     *                            CLOUD FRACTION OR CONDENSATE MIXING
!     *                            RATIO TOO LOW TO ENSURE CONSISTENT
!     *                            RESULTS FOR CLOUD FRACTION AND CLOUD
!     *                            CONDENSATE.
!     * MAR 25/2009 - L.SOLHEIM.   REVISED COSMETIC CHANGE:
!     *                            - USE REAL*4 OR REAL*8 ARGUMENT TO
!     *                              "ERF", DEPENDING ON THE VALUE OF
!     *                              "MACHINE", SO WILL WORK SEAMLESSLY ON
!     *                              PGI COMPILER AS WELL
!     * DEC 19/2007 - K.VONSALZEN/ PREVIOUS VERSION STATCLD3 FOR GCM15G/H/I:
!     *               M.LAZARE.    - ELIMINATE UNPHYSICAL RESULTS
!     *                              BY NOT PERMITTING THE INFERRED
!     *                              WATER VAPOUR MIXING RATIO (QCW)
!     *                              TO EXCEED A THRESHOLD (DEFINED
!     *                              BY QCWMAX).
!     *                            - PASSES IN ADELT FROM PHYSICS
!     *                              AS ZTMST INSTEAD OF ZTMST=2.*DELT.
!     * JAN 13/2007 - K.VONSALZEN. CALCULATE NORMALIZED CLOUD WATER
!     *                            VARIANCE TO BE USED IN CLOUD
!     *                            INHOMOGENEITY (QCWVAR).
!     * JUN 19/2006 - M.LAZARE.  NEW VERSION FOR GCM15F:
!     *                          - COSMETIC: USE VARIABLE INSTEAD OF
!     *                            CONSTANT IN INTRINSICS SUCH AS "MAX",
!     *                            SO THAT CAN COMPILE IN 32-BIT MODE
!     *                            WITH REAL*8.
!     * MAY 06/2006 - M.LAZARE/  PREVIOUS VERSION STATCLD FOR GCM15E.
!     *               K.VONSALZEN:

!
!     * THIS SUBROUTINE PERFORMS THE BULK OF THE STATISTICAL CLOUD
!     * SCHEME BASED ON CHABOUREAU AND BECHTHOLD (C&B). IT TAKES AS
!     * INPUT LIQUID STATIC ENERGY AND TOTAL WATER, AND OUTPUTS
!     * CONDENSED WATER/ICE, CLOUD FRACTION, THE VARIANCE OF MELLORS
!     * VARIABLE ("S"), THE EFFICIENCY FACTOR FOR AUTOCONVERSION
!     * FROM CLOUD WATER TO RAIN WATER, AND THE SATURATION SPECIFIC
!     * HUMIDITY.
!
!**** ***************************************************************
!     * DICTIONARY OF VARIABLES:
!
!     * OUTPUT:
!     * ------
!
!     * QCW:     TOTAL CONDENSED WATER/ICE (BOTH PHASES) IN KG/KG.
!     * ZCLF:    CLOUD FRACTION (DIMENSIONLESS)
!     * SIGMA:   VARIANCE OF MELLOR'S VARIABLE S IN KG/KG
!     * ZCRAUT:  EFFICIENCY OF AUTOCONVERSION FROM CLOUD WATER TO
!     *          RAIN WATER.
!     * QCWVAR:  NORMALIZED CLOUD WATER VARIANCE (<W'**2>/<W>**2)
!     *          (DIMENSIONLESS).
!     * SSH:     SATURATION SPECIFIC HUMIDITY IN KG/KG
!     * CVSG:    CONTRIBUTION TO TOTAL VARIANCE OF S FROM CONVECTIVE
!     *          PROCESSES IN KG/KG (ONLY IF SWITCH ICVSG=1,
!     *          OTHERWISE IT IS INPUT!).
!
!     * INPUT:
!     * -----
!
!     * QT:     TOTAL WATER (BOTH PHASES) IN KG/KG.
!     * HMN:    TOTAL LIQUID STATIC ENERGY IN JOULES/KG.
!     * ZFRAC:  FRACTION OF ICE PHASE.
!     * CPM:    SPECIFIC HEAT IN JOULES/(KG-DEGK).
!     * P:      MID-LAYER PRESSURE IN MBS.
!     * Z:      MID-LAYER HEIGHT ABOVE GROUND IN METRES.
!     * PBLT:   LAYER INDEX OF TOP OF PBL.
!     * RRL:    WATER/ICE WEIGHTED LATENT HEAT IN JOULES/KG.
!     * CONS:   FIELD CONTRIBUTING TO VARIANCE FROM CONVECTION.
!     * CRH:    FLAG (0/1) TO CONTROL CALCULATION.
!     * ALMIX:  MIXING LENGTH FOR C&B PARAMETERIZATION IN METRES.
!     * DHLDZ:  TOTAL WATER VERTICAL GRADIENT IN (M-1).
!     * DRWDZ:  LIQUID STATIC ENERGY GRADIENT IN JOULES/(KG-M). &
!**** ***************************************************************
!
      USE SDPHYS
      USE FPDEF
!
      IMPLICIT REAL   (A-H,O-Z), &
      INTEGER (I-N)
!
      REAL  , DIMENSION(ILG), INTENT(OUT) :: QCW,ZCLF,SIGMA,ZCRAUT, &
                                             QCWVAR,SSH,CVSG
      REAL  , DIMENSION(ILG), INTENT(IN)  :: HMN,QT,P,Z,CPM,ZFRAC, &
                                             CONS,CRH,RRL,ALMIX,DHLDZ, &
                                             DRWDZ
!
      REAL*4 :: R4VAL
      REAL*8 :: R8VAL
      INTEGER :: MACHINE
!
      ESW(TTT)    = EXP(RW1+RW2/TTT)*TTT**RW3
      ESI(TTT)    = EXP(RI1+RI2/TTT)*TTT**RI3
!
      DATA ZERO,ONE /0., 1./
!==============================================================================================
!     * PARAMETERS.
!
      ZEPCLC=1.E-2
      ZSEC=1.E-9
      CSIGMA=0.2
      YFR=0.50
      SQRT2=SQRT(2.)
      PI=3.141592653589793
      SQRT2PI=SQRT(2.*PI)
      Q1MIN1=-3.
      Q1MIN2=TAN((ZEPCLC-0.5)/0.36)/1.55
      Q1MIN=MAX(Q1MIN1,Q1MIN2)
      Q1MINM=Q1MIN/2.
      Q1MAX=2.
      Q1MAXM=Q1MAX/2.
      EXPM1=EXP(-1.)
      QDELT=2./30.
      TAUSIG=21600.
      SIGFAC=EXP(-ZTMST/TAUSIG)
      EPS2=1.-EPS1
      MACHINE=1
!
!     * INITIALIZATION.
!
      ILBAD=0
      DO IL=IL1,IL2
        QCW   (IL)=0.
        ZCLF  (IL)=0.
        ZCRAUT(IL)=1.
        SIGMA (IL)=0.
        SSH   (IL)=0.
      ENDDO
!
      DO IL=IL1,IL2
        IF ( CRH(IL).NE.0. ) THEN
!
!         * TEMPERATURE UNDER CLEAR-SKY CONDITIONS AS FIRST GUESS.
!
          TX=(HMN(IL)-GRAV*Z(IL))/CPM(IL)
          TL=TX
!
!         * WATER VAPOUR SATURATION MIXING RATIO UNDER CLEAR-SKY CONDITIONS.
!
          EST=(1.-ZFRAC(IL))*ESW(TX)+ZFRAC(IL)*ESI(TX)
          RSS=EPS1*EST/( P(IL)-EPS2*EST )
          DRSSDT=EPS1*P(IL) &
                   *((1.-ZFRAC(IL))*ESW(TX)*(RW3-RW2/TX) &
                        +ZFRAC(IL) *ESI(TX)*(RI3-RI2/TX)) &
                    /(TX*(P(IL)-EPS2*EST)**2)
          RSSTL=RSS
          DRSSDTL=DRSSDT
!
!         * ACCOUNT FOR CLOUD WATER/ICE IF SATURATION. USE
!         * SERIES REVERSION AND ITERATE.
!
          IF ( RSS.LT.QT(IL) ) THEN
!
!           * FIRST ITERATION.
!
            HMNPR=CPM(IL)*TX+GRAV*Z(IL)-HMN(IL)
            DHMNPR=CPM(IL)
            RC=QT(IL)-RSS
            HMNPR=HMNPR-RRL(IL)*RC
            DHMNPR=DHMNPR+RRL(IL)*DRSSDT
            TX=TX-HMNPR/DHMNPR
            EST=(1.-ZFRAC(IL))*ESW(TX)+ZFRAC(IL)*ESI(TX)
            RSS=EPS1*EST/( P(IL)-EPS2*EST )
            DRSSDT=EPS1*P(IL) &
                   *((1.-ZFRAC(IL))*ESW(TX)*(RW3-RW2/TX) &
                        +ZFRAC(IL) *ESI(TX)*(RI3-RI2/TX)) &
                    /(TX*(P(IL)-EPS2*EST)**2)
!
!           * SECOND ITERATION.
!
            HMNPR=CPM(IL)*TX+GRAV*Z(IL)-HMN(IL)
            DHMNPR=CPM(IL)
            RC=QT(IL)-RSS
            HMNPR=HMNPR-RRL(IL)*RC
            DHMNPR=DHMNPR+RRL(IL)*DRSSDT
            TX=TX-HMNPR/DHMNPR
            EST=(1.-ZFRAC(IL))*ESW(TX)+ZFRAC(IL)*ESI(TX)
            RSS=EPS1*EST/( P(IL)-EPS2*EST )
            DRSSDT=EPS1*P(IL) &
                   *((1.-ZFRAC(IL))*ESW(TX)*(RW3-RW2/TX) &
                        +ZFRAC(IL) *ESI(TX)*(RI3-RI2/TX)) &
                    /(TX*(P(IL)-EPS2*EST)**2)
!
!           * THIRD ITERATION.
!
            HMNPR=CPM(IL)*TX+GRAV*Z(IL)-HMN(IL)
            DHMNPR=CPM(IL)
            RC=QT(IL)-RSS
            HMNPR=HMNPR-RRL(IL)*RC
            DHMNPR=DHMNPR+RRL(IL)*DRSSDT
            TX=TX-HMNPR/DHMNPR
            EST=(1.-ZFRAC(IL))*ESW(TX)+ZFRAC(IL)*ESI(TX)
            RSS=EPS1*EST/( P(IL)-EPS2*EST )
            DRSSDT=EPS1*P(IL) &
                   *((1.-ZFRAC(IL))*ESW(TX)*(RW3-RW2/TX) &
                        +ZFRAC(IL) *ESI(TX)*(RI3-RI2/TX)) &
                    /(TX*(P(IL)-EPS2*EST)**2)
!
!           * TEMPERATURE-DERIVATIVE OF RSS FOR T=TL. THE DERIVATIVE
!           * IS USED IN A TAYLOR SERIES APPROXIMATION TO DETERMINE THE
!           * WATER VAPOUR SATURATION MIXING RATIO AT TEMPERATURE TX
!           * (I.E. AMBIENT TEMPERATURE) ABOUT TL (I.E. LIQUID WATER
!           * TEMPERATURE).
!
            IF( TX.NE.TL .AND. RSS > RSSTL ) THEN
              DRSSDT=(RSS-RSSTL)/(TX-TL)
            ELSE
              DRSSDT=DRSSDTL
              RSS=RSSTL
            ENDIF
          ENDIF
!
!         * SAVE FINAL ITTERATION OF SATURATION SPECIFIC HUMIDITY FOR
!         * USE ELSEWHERE IN CALCULATION RELATIVE HUMIDITIES.
!
          SSH(IL)=RSS
          IF (ISUBG.EQ.1)                                           THEN
!
!           * A AND B PARAMETERS FOR STATISTICAL CLOUD SCHEME.
!
            APA=1./(1.+RRL(IL)*DRSSDT/CPM(IL))
            APB=APA*DRSSDT
!
!           * DETERMINE VARIANCE BASED ON LOCAL APPROACH (CHABOUREAU AND
!           * BECHTHOLD).
!
            SIGMACB=CSIGMA*ALMIX(IL)* &
                    ABS(APA*DRWDZ(IL)-APB*DHLDZ(IL)/CPM(IL))
!
            IF(ICVSG.EQ.1)                                          THEN
!
!             * CALCULATE CONVECTIVE CONTRIBUTION TO VARIANCE.
!             * THIS MUST BE OUTPUT BECAUSE OF "AGEING".
!             * NOTE ** THIS IS ONLY DONE ONCE NEAR THE BEGINNING OF THE
!             *         PHYSICS, DUE TO THE "AGEING" CALCULATION (IE
!             *         ONE DOESN'T WANT MULTIPLE AGEING PER TIMESTEP FOR
!             *         EACH PHYSICS ROUTINE WHICH CALLS THIS SUBROUTINE.
!             *         THIS IS CONTROLLED BY THE SWITCH "ICVSG").
!
              IF(CONS(IL).GT.0.) THEN
                CVSG(IL)=3.E-03*CONS(IL)/APA
!old            CVSG(IL)=1.E-05*APA
              ELSE
                CVSG(IL)=CVSG(IL)*SIGFAC
              ENDIF
            ENDIF
!
!           * ADD CONVECTIVE CONTRIBUTION TO VARIANCE.
!
            SIGMASUM=SIGMACB+CVSG(IL)
!
!           * LIMIT VARIANCE TO AVOID UNPHYSICAL CASES WITH LARGE VARIANCES
!           * AN LOW MEAN VALUES FOR THE TOTAL WATER PROBABILITY DISTRIBUTION.
!
            SIGMAMAX=YFR*APA*QT(IL)/2.
            SIGMA(IL)=MIN(SIGMASUM,SIGMAMAX)
          ENDIF
!
!         * CLOUD FRACTION AND CLOUD WATER CONTENT BASED ON CHABOUREAU
!         * AND BECHTHOLD'S STATISTICAL CLOUD SCHEME AND AUTOCONVERSION
!         * SCALING FACTOR BASED ON MELLORS'S STATISTICAL CLOUD SCHEME
!         * (FOR THE 2.47TH MOMENT OF THE TOTAL WATER PROBABILITY
!         * DISTRIBUTION).
!
          QCWT=(QT(IL)-RSSTL)
          IF ( ISUBG .EQ. 1 .AND. SIGMA(IL) .GT. ZSEC ) THEN
            QCWT=QCWT*APA
            Q1=QCWT/SIGMA(IL)
            Q1M=Q1/2.
            INDX=NINT((Q1/2.+2.)/QDELT)+1
            IF ( Q1.LT.Q1MIN ) THEN
              ZCLF(IL)=0.
              QCW (IL)=0.
              ZCRAUT(IL)=ZCR(Q1MINM,I2NDIE)
              QCWVAR(IL)=0.
            ELSE IF ( Q1.GE.Q1MIN .AND. Q1.LT.0. ) THEN
              ZCLF(IL)=0.5+0.36*ATAN(1.55*Q1)
              QCW (IL)=SIGMA(IL)*EXP(1.2*Q1-1.)
              ZCRAUT(IL)=ZCR(Q1M,I2NDIE)
!
              IF (MACHINE.EQ.2) THEN
                R4VAL=Q1M/SQRT2
                AR=.5*(1.+AERF(R4VAL))
              ELSE
                R8VAL=Q1M/SQRT2
                AR=.5*(1.+AERF(R8VAL))
              ENDIF
!
              AEXP=EXP(-.5*Q1M**2)
              QL=AR*Q1M+AEXP/SQRT2PI
              QLVAR=AR*(1.+(Q1M-QL)**2)+AEXP*(Q1M/SQRT2PI-QL)
              QCWVAR(IL)=QLVAR/QL**2
            ELSE IF ( Q1.GE.0. .AND. Q1.LE.Q1MAX ) THEN
              ZCLF(IL)=0.5+0.36*ATAN(1.55*Q1)
              QCW (IL)=SIGMA(IL)*(EXPM1+0.66*Q1+0.086*Q1**2)
              ZCRAUT(IL)=ZCR(Q1M,I2NDIE)
!
              IF (MACHINE.EQ.2) THEN
                R4VAL=Q1M/SQRT2
                AR=.5*(1.+AERF(R4VAL))
              ELSE
                R8VAL=Q1M/SQRT2
                AR=.5*(1.+AERF(R8VAL))
              ENDIF
!
              AEXP=EXP(-.5*Q1M**2)
              QL=AR*Q1M+AEXP/SQRT2PI
              QLVAR=AR*(1.+(Q1M-QL)**2)+AEXP*(Q1M/SQRT2PI-QL)
              QCWVAR(IL)=QLVAR/QL**2
            ELSE
              ZCLF(IL)=1.
              QCW (IL)=QT(IL)-RSS
              ZCRAUT(IL)=1.
              QCWVAR(IL)=0.
            ENDIF
!
!           * IMPOSE UPPER BOUND ON CLOUD WATER. THIS IS NECESSARY SINCE
!           * THE EMPIRICAL FITS ON WHICH THE STATISTICAL CLOUD SCHEME IS
!           * IS BASED MAY PRODUCE UNPHYSICAL RESULTS UNDER CERTAIN
!           * CIRCUMSTANCES (I.E. EXCESSIVELY LARGE CLOUD WATER CONTENTS).
!           * THE APPROACH HERE IS NOT TO ALLOW THE INFERRED WATER VAPOUR
!           * MIXING RATIO IN THE CLEAR-SKY TO BECOME SMALLER THAN A
!           * GIVEN THRESHOLD (ZSEC).
!
            QCWMAX=MAX(QT(IL)-((1.-ZCLF(IL))*ZSEC+ZCLF(IL)*RSS),0.)
            IF ( Q1.GE.Q1MIN .AND. Q1.LE.Q1MAX &
                                         .AND. QCW(IL).GE.QCWMAX ) THEN
              QCW(IL)=QCWMAX
            ENDIF
          ELSE
            IF ( QCWT .LT. 0. ) THEN
              ZCLF(IL)=0.
              QCW (IL)=0.
              ZCRAUT(IL)=ZCR(Q1MINM,I2NDIE)
              QCWVAR(IL)=0.
            ELSE
              ZCLF(IL)=1.
              QCW (IL)=QT(IL)-RSS
              ZCRAUT(IL)=1.
              QCWVAR(IL)=0.
            ENDIF
          ENDIF
          QCW(IL)=MAX(MIN(QCW(IL),QT(IL)),ZERO)
          ZCLF(IL)=MAX(MIN(ZCLF(IL),ONE),ZERO)
!
!         * ELIMINATE CLOUDSAND CONDENSATE IF CLOUD FRACTION OR
!         * CONDENSATE MIXING RATIO TOO LOW TO ENSURE CONSISTENT
!         * RESULTS FOR CLOUD FRACTION AND CLOUD CONDENSATE.
!
          IF ( ZCLF(IL).LT.0.01 .OR. QCW(IL).LT.1.E-12 ) THEN
            ZCLF(IL)=0.
            QCW (IL)=0.
            ZCRAUT(IL)=ZCR(Q1MINM,I2NDIE)
            QCWVAR(IL)=0.
          ENDIF
!
!         * TRACK UNPHYSICAL VALUES IN PREPARATION FOR ABORTING
!         * (DONE OUTSIDE LOOP IN ORDER TO KEEP OPTIMIZATION).
!
          IF ( ISUBG.EQ.1 .AND. QCW(IL).GT.QT(IL) .AND. ILBAD.EQ.0 ) &
                                                                 THEN
            ILBAD=IL
            SIGCBBAD=SIGMACB
            SIGMAXBAD=SIGMAMAX
            QCWTBAD=QCWT
          ENDIF
        ENDIF
      ENDDO
!
!     * PRINT OUT PROBLEM POINTS AND ABORT, IF THEY EXIST.
!
      IF(ILBAD.NE.0)                                            THEN
        IL=ILBAD
        PRINT*,'ICALL,LVL,IL,QCW,QT,RSS=',ICALL,LVL,ILBAD,QCW(IL), &
                QT(IL),SSH(IL)
        PRINT*,'SIGMA,SIGMACB,CVSG,SIGMAMAX,ZCLF,ZCRAUT,QCWT=', &
                SIGMA(IL),SIGCBBAD,CVSG(IL),SIGMAXBAD,ZCLF(IL), &
                ZCRAUT(IL),QCWTBAD
        CALL XIT('STATCLD5',-1)
      ENDIF
!
      END SUBROUTINE STATCLD
      FUNCTION ZCR(Q1,IMOD)
!
!     * JUN 26/2013 - K.VONSALZEN.
!
!     * CALLED BY STATCLD5.
!
!     * RATIO OF MOMENTS OF CLOUD LIQUID WATER CONTENT. THE RATIO
!     * <QLWC**COEFF>/<QLWC>**COEFF IS CALCULATED BY INTEGRATING
!     * OVER A GAUSSIAN DISTRIBUTION USING THE STATISTICAL CLOUD
!     * SCHEME BY MELLOR (1977). FOR IMOD=0 -> COEFF=2.47 AND
!     * FOR IMOD=1 -> COEFF=3.
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL :: ZCR
      REAL, PARAMETER :: Q1MIN=-2., Q1MAX=2.
      INTEGER, PARAMETER :: INUM=61
      REAL, DIMENSION(INUM) :: ZCRLD, ZCRKK, ZCRR
      DATA ZCRLD / 8889.942, 6454.475, 4721.926, 3480.747, 2584.975, &
                   1934.007, 1457.707, 1106.815,  846.462,  652.041, &
                    505.868,  395.250,  311.001,  246.407,  196.574, &
                    157.888,  127.668,  103.925,   85.150,   70.220, &
                     58.278,   48.672,   40.903,   34.586,   29.420, &
                     25.173,   21.665,   18.752,   16.322,   14.284, &
                     12.568,   11.117,    9.883,    8.830,    7.928, &
                      7.152,    6.482,    5.900,    5.395,    4.953, &
                      4.567,    4.227,    3.927,    3.662,    3.427, &
                      3.218,    3.032,    2.866,    2.716,    2.582, &
                      2.462,    2.353,    2.254,    2.165,    2.084, &
                      2.011,    1.944,    1.883,    1.827,    1.776, &
                      1.729 /
      DATA ZCRKK /  708.636,  560.461,  445.761,  356.524,  286.721, &
                    231.848,  188.502,  154.094,  126.637,  104.629, &
                     86.902,   72.556,   60.895,   51.368,   43.552, &
                     37.112,   31.781,   27.350,   23.650,   20.549, &
                     17.939,   15.733,   13.862,   12.269,   10.908, &
                      9.739,    8.733,    7.864,    7.111,    6.456, &
                      5.885,    5.385,    4.946,    4.559,    4.218, &
                      3.916,    3.649,    3.410,    3.198,    3.008, &
                      2.838,    2.685,    2.548,    2.424,    2.312, &
                      2.211,    2.119,    2.036,    1.961,    1.892, &
                      1.829,    1.772,    1.720,    1.672,    1.628, &
                      1.588,    1.551,    1.517,    1.485,    1.457, &
                      1.430 /
!
      Q1DELT=(Q1MAX-Q1MIN)/REAL(INUM-1)
      IF ( IMOD == 0 ) THEN
        ZCRR=ZCRKK
      ELSE
        ZCRR=ZCRLD
      ENDIF
!
      IF ( Q1 < Q1MIN ) THEN
        ZCR=ZCRR(1)
      ELSE IF ( Q1 > Q1MAX ) THEN
        ZCR=ZCRR(INUM)
      ELSE
        Q1V=(Q1-Q1MIN)/Q1DELT+1.
        IND=INT(Q1V)
        WGT=Q1V-REAL(IND)
        ZCR=WGT*ZCRR(IND+1)+(1.-WGT)*ZCRR(IND)
      ENDIF
!
      END FUNCTION ZCR
