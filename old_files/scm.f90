      MODULE RUNINFO
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     BASIC INFORMATION ABOUT EXPERIMENT AND AIR PROPERIES.
!
!     HISTORY:
!     --------
!     * SEP 19/2008 - K.VONSALZEN   ADD MUTLI LEVEL INFO
!     * AUG 11/2006 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
!-----------------------------------------------------------------------
!     * GENERAL MODEL INFORMATION (NAME, LEVELS, TIME STEP).
!
      INTEGER, PARAMETER :: IMAXL=50
      TYPE MPRP
        CHARACTER(LEN=IMAXL) :: RUNNAME
        REAL :: NUGALT, NUGVD, TAUX, TAUT, TAUQ, TAUM
        REAL :: CDN, CDNN, ZREF
        INTEGER :: PLTFQ, TRAX, TRAT, TRAQ, TRUP, CASE
      END TYPE MPRP
      TYPE(MPRP) :: MODL
      NAMELIST /MODNML/ MODL
!
      END MODULE RUNINFO
      MODULE FPDEF
!
      INTEGER, PARAMETER :: R8 = selected_real_kind(12) ! 8 byte real
      INTEGER, PARAMETER :: R4 = selected_real_kind( 6) ! 4 byte real
!
      END MODULE FPDEF
      MODULE PSIZES
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     BASIC ARRAY DIMENSIONS.
!
!     HISTORY:
!     --------
!     * FEB 10/2010 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      USE RUNINFO
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
!-----------------------------------------------------------------------
!     * Number of grid points in horizontal direction
      INTEGER :: ILG
!     * Vertical levels in driving atmospheric model.
      INTEGER :: ILEV
      INTEGER :: ILEVP1
      INTEGER :: LEVS
!     * Array dimensions for PLA aerosol calculations
      INTEGER :: MSGT
      INTEGER :: MSGP1
      INTEGER :: MSGP2
!     * Tracers
      INTEGER :: NTRAC
!
      END MODULE PSIZES
      MODULE SDPHYS
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     BASIC PHYISCAL CONSTANTS.
!
!     HISTORY:
!     --------
!     * AUG 10/2006 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
!     * BASIC PHYSICAL CONSTANTS.
!
      REAL, PARAMETER :: RHOH2O = 1.E+03         ! DENSITY OF WATER (KG/M3)
      REAL, PARAMETER :: WH2O   = 18.015E-03     ! MOLECULAR WEIGHT OF H2O (KG/MOL)
      REAL, PARAMETER :: WA     = 28.97E-03      ! MOLECULAR WEIGHT OF AIR (KG/MOL)
      REAL, PARAMETER :: RGASM  = 8.31441        ! MOLAR GAS CONSTANT (J/MOL/K)
      REAL, PARAMETER :: RGAS   = 287.04         ! GAS CONSTANT OF DRY AIR (J/KG/K)
      REAL, PARAMETER :: RGOCP  = 2./7.
      REAL, PARAMETER :: CPRES  = RGAS/RGOCP     ! HEAT CAPACITY AT CONSTANT PRESSURE
                                                 ! FOR DRY AIR (J/KG/K)
      REAL, PARAMETER :: EPS1   = 0.622          ! RATIO OF GAS CONSTANT FOR DRY
                                                 ! AIR OVER GAS CONSTANT FOR VAPOUR
      REAL, PARAMETER :: GRAV   = 9.80616        ! GRAVITATIONAL CONTSTANT (M/S2)
      REAL, PARAMETER :: RL     = 2.501E+06      ! LATENT HEAT OF VAPOURIZATION (J/KG)
!
!     * FITTING PARAMETERS FOR CALCULATION OF WATER VAPOUR SATURATION
!     * MIXING RATIO.
!
      REAL, PARAMETER :: RW1    = 53.67957
      REAL, PARAMETER :: RW2    = -6743.769
      REAL, PARAMETER :: RW3    = -4.8451
      REAL, PARAMETER :: RI1    = 23.33086
      REAL, PARAMETER :: RI2    = -6111.72784
      REAL, PARAMETER :: RI3    = 0.15215
!
      REAL, PARAMETER :: PPA    = 21.656
      REAL, PARAMETER :: PPB    = 5418.
!
      END MODULE SDPHYS
      MODULE IODAT
!
      USE FPDEF
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      INTEGER, PARAMETER :: IMAXL=100  ! MAXIMUM LENGTH OF VARIABLE LONG NAMES
      INTEGER, PARAMETER :: ITOT=1000  ! MAXIMUM NUMBER OF FIELDS PER FILE
      INTEGER, PARAMETER :: ISMX=20    ! MAXIMUM NUMBER OF DIMENSIONS FOR IO FIELDS
!
      INTEGER, PARAMETER :: INAX=-9
      REAL(R4), PARAMETER :: YNAX=-9999., YSMALLX=1.E-20
      INTEGER, DIMENSION(ISMX) :: IDIMA
      CHARACTER(LEN=IMAXL) :: CNA, CTMP
      TYPE MODPRPV
        REAL, ALLOCATABLE, DIMENSION(:,:,:,:) :: FLD4D
        REAL, ALLOCATABLE, DIMENSION(:,:,:) :: FLD3D
        REAL, ALLOCATABLE, DIMENSION(:,:) :: FLD2D
        REAL, ALLOCATABLE, DIMENSION(:) :: FLD1D
        REAL :: FLD0D
      END TYPE MODPRPV
      TYPE(MODPRPV), DIMENSION(ITOT) :: IVARV,OVARV,RVARV
      TYPE MODPRP
        CHARACTER(LEN=IMAXL) :: NAME,LNAME,UNITS
        CHARACTER(LEN=IMAXL), DIMENSION(ISMX) :: DIM
      END TYPE MODPRP
      TYPE(MODPRP), DIMENSION(ITOT) :: IVAR,OVAR,RVAR
      NAMELIST /IVARNML/ IVAR
      NAMELIST /OVARNML/ OVAR
      NAMELIST /RVARNML/ RVAR
!
      INTEGER :: NDIM
      TYPE DIMPRP
        CHARACTER(LEN=IMAXL) :: NAME,LNAME,UNITS
        INTEGER :: SIZE,FLGI,FLGO,FLGRS,FLGX
        REAL, ALLOCATABLE, DIMENSION(:) :: VAL
      END TYPE DIMPRP
      TYPE(DIMPRP), ALLOCATABLE, DIMENSION(:) :: DIM,DIMT
!
      END MODULE IODAT
      MODULE IODATIF
!
      INTERFACE
        SUBROUTINE W1DDIM(CNAME,AVAR,IDIM1,J1,J2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(OUT), DIMENSION(IDIM1) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1
        INTEGER, OPTIONAL :: J1,J2
        END SUBROUTINE W1DDIM
!
        SUBROUTINE W1DFLD(CNAME,AVAR,IDIM1,J1,J2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1
        INTEGER, OPTIONAL :: J1,J2
        END SUBROUTINE W1DFLD
!
        SUBROUTINE W2DFLD(CNAME,AVAR,IDIM1,IDIM2,J1,J2,K1,K2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1,IDIM2
        INTEGER, OPTIONAL :: J1,J2,K1,K2
        END SUBROUTINE W2DFLD
!
        SUBROUTINE W3DFLD(CNAME,AVAR,IDIM1,IDIM2,IDIM3, &
                          J1,J2,K1,K2,L1,L2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2,IDIM3) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3
        INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2
        END SUBROUTINE W3DFLD
!
        SUBROUTINE W4DFLD(CNAME,AVAR,IDIM1,IDIM2,IDIM3,IDIM4, &
                          J1,J2,K1,K2,L1,L2,M1,M2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2,IDIM3,IDIM4) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3,IDIM4
        INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2,M1,M2
        END SUBROUTINE W4DFLD
!
        SUBROUTINE W1DFLDRS(CNAME,AVAR,IDIM1,J1,J2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1
        INTEGER, OPTIONAL :: J1,J2
        END SUBROUTINE W1DFLDRS
!
        SUBROUTINE W2DFLDRS(CNAME,AVAR,IDIM1,IDIM2,J1,J2,K1,K2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1,IDIM2
        INTEGER, OPTIONAL :: J1,J2,K1,K2
        END SUBROUTINE W2DFLDRS
!
        SUBROUTINE W3DFLDRS(CNAME,AVAR,IDIM1,IDIM2,IDIM3, &
                            J1,J2,K1,K2,L1,L2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2,IDIM3) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3
        INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2
        END SUBROUTINE W3DFLDRS
!
        SUBROUTINE W4DFLDRS(CNAME,AVAR,IDIM1,IDIM2,IDIM3,IDIM4, &
                            J1,J2,K1,K2,L1,L2,M1,M2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2,IDIM3,IDIM4) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3,IDIM4
        INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2,M1,M2
        END SUBROUTINE W4DFLDRS
!
        SUBROUTINE R1DFLD(CNAME,AVAR,IDIM1,J1,J2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1
        INTEGER, OPTIONAL :: J1,J2
        END SUBROUTINE R1DFLD
!
        SUBROUTINE R2DFLD(CNAME,AVAR,IDIM1,IDIM2,J1,J2,K1,K2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1,IDIM2
        INTEGER, OPTIONAL :: J1,J2,K1,K2
        END SUBROUTINE R2DFLD
!
        SUBROUTINE R3DFLD(CNAME,AVAR,IDIM1,IDIM2,IDIM3, &
                          J1,J2,K1,K2,L1,L2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2,IDIM3) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3
        INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2
        END SUBROUTINE R3DFLD
!
        SUBROUTINE R4DFLD(CNAME,AVAR,IDIM1,IDIM2,IDIM3,IDIM4, &
                          J1,J2,K1,K2,L1,L2,M1,M2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2,IDIM3,IDIM4) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3,IDIM4
        INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2,M1,M2
        END SUBROUTINE R4DFLD
!
        SUBROUTINE R1DFLDRS(CNAME,AVAR,IDIM1,J1,J2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1
        INTEGER, OPTIONAL :: J1,J2
        END SUBROUTINE R1DFLDRS
!
        SUBROUTINE R2DFLDRS(CNAME,AVAR,IDIM1,IDIM2,J1,J2,K1,K2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1,IDIM2
        INTEGER, OPTIONAL :: J1,J2,K1,K2
        END SUBROUTINE R2DFLDRS
!
        SUBROUTINE R3DFLDRS(CNAME,AVAR,IDIM1,IDIM2,IDIM3, &
                            J1,J2,K1,K2,L1,L2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2,IDIM3) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3
        INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2
        END SUBROUTINE R3DFLDRS
!
        SUBROUTINE R4DFLDRS(CNAME,AVAR,IDIM1,IDIM2,IDIM3,IDIM4, &
                            J1,J2,K1,K2,L1,L2,M1,M2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2,IDIM3,IDIM4) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3,IDIM4
        INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2,M1,M2
        END SUBROUTINE R4DFLDRS
      END INTERFACE
!
      END MODULE IODATIF
      PROGRAM DRIVER
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     SIMPLE SINGLE COLUMN MODELLING FRAMEWORK BASED ON IDEALIZED
!     ATMOSPHERIC CONDITIONS FOR DRIVING PAM.
!
!     HISTORY:
!     --------
!     * JUL 19/2017 - K.VONSALZEN   COMPLETELY REWRITTEN. AEROSOL CODE WAS
!     *                             REMOVED AND THE CODE RECONFIGURED TO
!     *                             BETTER SUPPORT USE OF AIRCRAFT DATA
!     * MAY 7/2013  - K.VONSALZEN   UPDATED FOR I/O FROM NETCDF AND CODE
!     *                             UPDATES FROM CANAM4.3
!     * FEB 10/2010 - K.VONSALZEN   UPDATED FOR GCM15H; INCLUDED
!     *                             REMAINING PARAMETERIZATIONS.
!     * JAN 15/2009 - K.VONSALZEN   UPDATED FOR GCM15G AND NEW WET
!     *                             DEPOSITION PARAMETERIZATION.
!     * NOV 27/2007 - K.VONSALZEN   NEW, BASED ON ADIABATIC PARCEL MODEL.
!
!-----------------------------------------------------------------------
!
      USE FPDEF            ! FLOATING POINT PRECISION INFORMATION
      USE RUNINFO          ! ATMOSPHERIC PROPERTIES AND OTHER INFORMATION
                           ! FOR GENERAL SETUP OF SCM SIMULATION
      USE PSIZES           ! CONTAINS DIMENSIONAL PARAMETERS FOR
                           ! DRIVING ATMOSPHERIC MODEL
      USE IODAT            ! FOR INPUT/OUTPUT OF MODEL DATA
      USE IODATIF          ! CONTAINS I/O SUBROUTINE INTERFACE DEFITIONS
      USE SDPHYS           ! PHYSICAL CONSTANTS
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
!     * PHYSICAL AND CHEMICAL FIELDS FOR DRIVING ATMOSPHERIC MODEL.
!
      REAL, ALLOCATABLE, DIMENSION(:,:)   :: DP,ZMRATEP,ZMLWC,ZFSNOW, &
                                             ZFRAIN,ZCLF,CLRFR,CLRFS, &
                                             ZFEVAP,ZFSUBL,ZH,ZF,PH,PF, &
                                             THROW,QROW,QTN,HMN,RHC,WG, &
                                             WSUB,RHOC,TFROW,SHTJ,SHJ, &
                                             DSHJ,BGHPO,TAUS,TAUOP, &
                                             XBURD,TMPSCL,DPRAD,TAUT, &
                                             TAUQ,THROWR,QROWR,RH,QLWC, &
                                             QLWCR,ALMX,ALMC,DTDT,DQDT, &
                                             DQLDT,DUDT,UROW,VROW, &
                                             UROWR,TAUM,RKM,RKH,RI, &
                                             DQNDG,DTNDG,DQVDF,DTVDF, &
                                             DQADV,DTADV,PERTA,PERRV, &
                                             TVP,BETA,RADEQV,TAUTR, &
                                             TAUQR,TAUMR,QCWA,RADEQVA, &
                                             ZCDNROW,CDNROW,RHOA,DQPHS, &
                                             DTPHS
      REAL, ALLOCATABLE, DIMENSION(:)   :: FT,FT1
      REAL, ALLOCATABLE, DIMENSION(:,:) :: FLVT,FLVT1,FLVT2,FLV1T,FLV1T1
      REAL, ALLOCATABLE, DIMENSION(:) :: PCP,CDM,USTAR,TG,THLIQG,ZO, &
                                         ZSPDA,QSENS,QLAT
      REAL, ALLOCATABLE, DIMENSION(:) :: ZSPD,PBLTROW,PFT,GTROW, &
                                         PRESSG,ATIME,WGT
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: XROW,DXDT,TAUX,TAUXR
!
!     * OTHER STUFF.
!
      INTEGER*4 :: LENG
      LOGICAL :: KMETOUT,KTIME1
      CHARACTER(LEN=50) :: CFILE,CFILB,CFILR
      INTEGER, PARAMETER :: YSPDAY=24*60*60
      INTEGER, PARAMETER :: YSPHR=60*60
      INTEGER, PARAMETER :: YSPMIN=60
      REAL, PARAMETER :: ZCTHR=.99
      REAL, PARAMETER :: SPV=99999.
!
!     * INPUT/OUTPUT FILE UNIT NUMBERS.
!
      DATA NUPAR /5 /
!
!-----------------------------------------------------------------------
!     * OPEN FILE THAT CONTAINS BASIC INFORMATION ABOUT
!     * AIR PROPERTIES.
!
      OPEN(NUPAR,FILE='PARAM')
!
!-----------------------------------------------------------------------
!     * READ IN BASIC INFORMATION ABOUT SIMULATION.
!
      MODL%PLTFQ=1
      MODL%TAUX=0.
      MODL%TAUQ=0.
      MODL%TAUT=0.
      MODL%TAUM=0.
      MODL%TRAX=0
      MODL%TRAQ=0
      MODL%TRAT=0
      MODL%TRUP=0
      MODL%NUGVD=SPV
      MODL%CDN=-SPV
      MODL%CDNN=-SPV
      MODL%ZREF=-SPV
      MODL%CASE=2   ! DEFAULT AUTOCONVERSION PARAMETERIZATION IS WOOD
      READ(NUPAR,NML=MODNML)
      REWIND(NUPAR)
!
!-----------------------------------------------------------------------
!     * READ FILES AND FIELD DIMENSIONS FOR BOUNDARY CONDITIONS AND
!     * RESTART FIELDS, INCLUDING METEOROLOGY, EMISSIONS AND INITIAL
!     * TRACER MIXING RATIOS.
!
      CFILE=TRIM(MODL%RUNNAME)
      CFILB=TRIM(TRIM(CFILE)//'_bnd')
      CFILR=TRIM(TRIM(CFILE)//'_tini')
!
!     * READ AND SET FIELD DIMENSIONS.
!
      IF ( MODL%PLTFQ /= 1 ) THEN
        KTIME1=.TRUE.
      ELSE
        KTIME1=.FALSE.
      ENDIF
      NTRAC=1
      CALL RDNCDIM(CFILB,CFILR,NLVL,NLVL1,NSTP,KTIME1)
      NSTPO=0
      DO IT=1,NSTP
        IF ( MOD(IT+MODL%PLTFQ-1,MODL%PLTFQ) == 0 ) NSTPO=NSTPO+1
      ENDDO
      IF ( KTIME1 ) THEN
        ALLOCATE(ATIME(NSTPO))
        CALL SETNCDIM(NSTPO,NDIM)
      ENDIF
!
!     * CHECK DIMENSIONS.
!
      IF ( NSTP <= 2 ) CALL XIT('MAIN',-1)
      IF ( MODL%PLTFQ < 1 .OR. MODL%PLTFQ > NSTP ) CALL WRN('MAIN',-2)
!
!     * INITIALIZE NETCDF I/O FIELDS TO SAVE THE MODEL DATA.
!
      CALL INITIO(KTIME1)
!
!     * READ BOUNDARY CONDITIONS AND RESTART DATA FROM THE FILES
!     * AND SAVE IN NETCDF I/O FIELDS AS SPECIFIED IN THE IO LISTS.
!
      CALL RDNCDAT(CFILB)
      CALL RDNCDATRS(CFILR)
!
!-----------------------------------------------------------------------
!     * GRID DIMENSIONS AND OTHER MODEL PARAMETERS.
!
      ILEV=NLVL        ! NUMBER OF VERTICAL GRID CELLS FOR ATMOSPHERE
      ILG=1            ! NUMBER OF HORIZONTAL GRID CELLS FOR ATMOSPHERE
!
      ILEV1=NLVL1
      ILEVP1=ILEV
      LEVS=ILEV
      MSGT=ILEV-LEVS
      MSGP1=MSGT+1
      MSGP2=MSGP1
      IL1=1
      IL2=1
!
!-----------------------------------------------------------------------
!     * GET SCALAR INPUT FIELDS SUPPLIED IN FILE WITH BOUNDARY CONDITIONS.
!
      CALL R0DFLD('delt',DT)
      IF ( NINT(DT-YNA) == 0 ) CALL XIT('MAIN',-3)
!
!     * SWITCHES FOR DIAGNOSTIC OUTPUT.
!
      KMETOUT=.TRUE.
!
!-----------------------------------------------------------------------
!     * ALLOCATE MEMORY.
!
      ALLOCATE(ZH     (ILG,ILEV))
      ALLOCATE(ZF     (ILG,ILEV))
      ALLOCATE(RHC    (ILG,ILEV))
      ALLOCATE(RH     (ILG,ILEV))
      ALLOCATE(RHOC   (ILG,ILEV))
      ALLOCATE(WSUB   (ILG,ILEV))
      ALLOCATE(WG     (ILG,ILEV))
      ALLOCATE(PH     (ILG,ILEV))
      ALLOCATE(PF     (ILG,ILEV))
      ALLOCATE(DP     (ILG,ILEV))
      ALLOCATE(DPRAD  (ILG,ILEV))
      ALLOCATE(THROW  (ILG,ILEVP1))
      ALLOCATE(THROWR (ILG,ILEVP1))
      ALLOCATE(UROW   (ILG,ILEV))
      ALLOCATE(VROW   (ILG,ILEV))
      ALLOCATE(UROWR  (ILG,ILEV))
      ALLOCATE(TFROW  (ILG,ILEV))
      ALLOCATE(QROW   (ILG,ILEVP1))
      ALLOCATE(QROWR  (ILG,ILEVP1))
      ALLOCATE(QLWC   (ILG,ILEV))
      ALLOCATE(QLWCR  (ILG,ILEV))
      ALLOCATE(QTN    (ILG,ILEV))
      ALLOCATE(HMN    (ILG,ILEV))
      ALLOCATE(ZMRATEP(ILG,ILEV))
      ALLOCATE(ZMLWC  (ILG,ILEV))
      ALLOCATE(ZFSNOW (ILG,ILEV))
      ALLOCATE(ZFRAIN (ILG,ILEV))
      ALLOCATE(ZCLF   (ILG,ILEV))
      ALLOCATE(BETA   (ILG,ILEV))
      ALLOCATE(RADEQV (ILG,ILEV))
      ALLOCATE(RADEQVA(ILG,ILEV))
      ALLOCATE(CLRFR  (ILG,ILEV))
      ALLOCATE(CLRFS  (ILG,ILEV))
      ALLOCATE(ZFEVAP (ILG,ILEV))
      ALLOCATE(ZFSUBL (ILG,ILEV))
      ALLOCATE(SHTJ   (ILG,ILEVP1))
      ALLOCATE(SHJ    (ILG,ILEV))
      ALLOCATE(DSHJ   (ILG,ILEV))
      ALLOCATE(TAUT   (ILG,ILEVP1))
      ALLOCATE(TAUQ   (ILG,ILEVP1))
      ALLOCATE(TAUM   (ILG,ILEV))
      ALLOCATE(TAUTR  (ILG,ILEVP1))
      ALLOCATE(TAUQR  (ILG,ILEVP1))
      ALLOCATE(TAUMR  (ILG,ILEV))
      ALLOCATE(ALMX   (ILG,ILEV))
      ALLOCATE(ALMC   (ILG,ILEV))
      ALLOCATE(TVP    (ILG,ILEV))
      ALLOCATE(DTDT   (ILG,ILEVP1))
      ALLOCATE(DQDT   (ILG,ILEVP1))
      ALLOCATE(DQLDT  (ILG,ILEV))
      ALLOCATE(DUDT   (ILG,ILEVP1))
      ALLOCATE(RKH    (ILG,ILEV))
      ALLOCATE(RKM    (ILG,ILEV))
      ALLOCATE(RI     (ILG,ILEV))
      ALLOCATE(DQNDG  (ILG,ILEV))
      ALLOCATE(DTNDG  (ILG,ILEV))
      ALLOCATE(DQVDF  (ILG,ILEV))
      ALLOCATE(DTVDF  (ILG,ILEV))
      ALLOCATE(DQADV  (ILG,ILEV))
      ALLOCATE(DTADV  (ILG,ILEV))
      ALLOCATE(DQPHS  (ILG,ILEV))
      ALLOCATE(DTPHS  (ILG,ILEV))
      ALLOCATE(PCP    (ILG))
      ALLOCATE(CDM    (ILG))
      ALLOCATE(USTAR  (ILG))
      ALLOCATE(TG     (ILG))
      ALLOCATE(THLIQG (ILG))
      ALLOCATE(ZO     (ILG))
      ALLOCATE(QSENS  (ILG))
      ALLOCATE(QLAT   (ILG))
      ALLOCATE(PERTA  (ILG,ILEV))
      ALLOCATE(PERRV  (ILG,ILEV))
      ALLOCATE(WGT    (ILG))
!
      ALLOCATE(ZCDNROW(ILG,ILEV))
      ALLOCATE(CDNROW(ILG,ILEV))
      ALLOCATE(RHOA(ILG,ILEV))
      ALLOCATE(QCWA(ILG,ILEV))
!
      ALLOCATE(FT(NSTP))
      ALLOCATE(FT1(NSTPO))
      ALLOCATE(FLV1T(ILEV1,NSTP))
      ALLOCATE(FLV1T1(ILEV1,NSTPO))
      ALLOCATE(FLVT(ILEV,NSTP))
      ALLOCATE(FLVT1(ILEV,NSTPO))
      ALLOCATE(FLVT2(ILEV+1,NSTPO))
!
      ALLOCATE(ZSPDA  (ILG))
      ALLOCATE(ZSPD   (ILG))
      ALLOCATE(PBLTROW(ILG))
      ALLOCATE(PFT    (ILG))
!
      ALLOCATE(GTROW  (ILG))
      ALLOCATE(PRESSG (ILG))
!
!     * TRACERS.
!
      ALLOCATE(XROW   (ILG,ILEVP1,NTRAC))
      ALLOCATE(DXDT   (ILG,ILEVP1,NTRAC))
!
!     * INITILIAZE TRACERS.
!
      XROW=0.
!
!-----------------------------------------------------------------------
!     * HEIGHT OF VERTICAL LEVELS, GRID CELL INTERFACES, AND SURFACE
!     * FROM INPUT DATA (ASSUMED TO BE TIME-INVARIANT HERE).
!
      ICHK=0
      DO ND=1,NDIM
        IF ( DIM(ND)%NAME(1:6)=='level ' ) THEN
          ICHK=ICHK+1
          DO IZ=1,ILEV
            ZH(:,IZ)=DIM(ND)%VAL(IZ)
          ENDDO
        ELSE IF ( DIM(ND)%NAME(1:7)=='level1 ' ) THEN
          ICHK=ICHK+1
          DO IZ=1,ILEV
            ZF(:,IZ)=DIM(ND)%VAL(IZ+1)
          ENDDO
        ENDIF
      ENDDO
      IF ( ICHK /= 2 ) CALL XIT('MAIN',-4)
!
!-----------------------------------------------------------------------
!     * READ INITIAL PROGNOSTIC FIELDS FROM RESTART FILE.
!
      CALL R0DFLDRS('tstep',AITS)
      INITS=AITS
      CALL R0DFLDRS('ustar',USTAR(1))
      CALL R1DFLDRS('ua',UROW(1,:),ILEV)
      CALL R1DFLDRS('ta',THROW(1,:),ILEV)
      CALL R1DFLDRS('rv',QROW(1,:),ILEV)
      CALL R1DFLDRS('rl',QLWC(1,:),ILEV)
      CALL R1DFLDRS('clf',ZCLF(1,:),ILEV)
      CALL R1DFLDRS('cldnc',ZCDNROW(1,:),ILEV)
      CALL R1DFLDRS('ri',RI(1,:),ILEV)
      CALL R0DFLDRS('indpbl',PBLTROW(1))
!
!-----------------------------------------------------------------------
!     * LOWER BOUND ON CLOUD DROPLET NUMBER CONCENTRATION.
!
      ZCDNROW=MAX(ZCDNROW,1.E+06)
!
!     * NUDGING TIME SCALES FOR TRACERS, TEMPERATURE, AND MOISTURE.
!
      ALLOCATE(TAUX (ILG,ILEVP1,NTRAC))
      ALLOCATE(TAUXR(ILG,ILEVP1,NTRAC))
      TAUR=60.*60.*24.
      TAUXR=0.
      TAUTR=0.
      TAUQR=0.
      TAUMR=0.
!
      ZREF=MODL%NUGALT
!      DO IZ=1,ILEV
!        IF ( ZH(1,IZ) <= ZREF ) THEN
!          FACT=TAUR*ERF((ZREF-ZH(1,IZ))/MODL%NUGVD)
!!!!
!          TAUXR(:,IZ,:)=MODL%TAUX*FACT
!          TAUTR(:,IZ)=MODL%TAUT*FACT
!          TAUQR(:,IZ)=MODL%TAUQ*FACT
!          TAUMR(:,IZ)=MODL%TAUM*FACT
!        ENDIF
!      ENDDO
      DO IZ=1,ILEV
        IF ( ZH(1,IZ) <= MODL%NUGALT &
            .AND. ZH(1,IZ) > MODL%NUGALT-MODL%NUGVD ) THEN
          TAUXR(:,IZ,:)=MODL%TAUX
          TAUTR(:,IZ)=MODL%TAUT
          TAUQR(:,IZ)=MODL%TAUQ
          TAUMR(:,IZ)=MODL%TAUM
        ENDIF
      ENDDO
!
!     * PROGNOSTIC RESULTS BELOW LOWEST LEVEL OF AIRCRAFT FLIGHT PROFILE.
!
      IF ( ABS(MODL%ZREF + SPV) > 0.001 ) THEN
        ZREF=MODL%ZREF
      ELSE
        ZREF=0.
      ENDIF
      TAUL=1.E+20
      DO IZ=1,ILEV
        IF ( ZH(1,IZ) < ZREF ) THEN
          TAUXR(:,IZ,:)=TAUL
          TAUTR(:,IZ)=TAUL
          TAUQR(:,IZ)=TAUL
          TAUMR(:,IZ)=TAUL
        ENDIF
      ENDDO
!
!-----------------------------------------------------------------------
!     * PROGNOSTIC GROWTH CALCULATIONS AND DIAGNOSIS OF PROPERTIES OF
!     * THE SIZE DISTRIBUTION. LOOP OVER ALL TIME STEPS AS SPECIFIED
!     * IN MODEL BOUNDARY CONDITIONS.
!
      WRITE(6,'(A19)') 'TIME INTEGRATION...'
      WRITE(6,'(1X)')
      ICHK=0
      DO ND=1,NDIM
        IF ( DIM(ND)%NAME(1:5)=='time ' ) THEN
          ICHK=ICHK+1
          NDTIM=ND
        ENDIF
      ENDDO
      IF ( ICHK /= 1 ) CALL XIT('MAIN',-5)
      ITY=0
      DO IT=1,NSTP
!
!----------------------------------------------------------------------
!       TIME INFORMATION.
!----------------------------------------------------------------------
!
        ITX=IT+INITS
        NDAY=INT(NINT(DIM(NDTIM)%VAL(ITX))/YSPDAY)
        NHR=INT((NINT(DIM(NDTIM)%VAL(ITX))-NDAY*YSPDAY)/YSPHR)
        NMIN=INT((NINT(DIM(NDTIM)%VAL(ITX))-NDAY*YSPDAY-NHR*YSPHR) &
                 /YSPMIN)
        NSEC=NINT(DIM(NDTIM)%VAL(ITX))-NDAY*YSPDAY-NHR*YSPHR &
                 -NMIN*YSPMIN
        WRITE(6,'(A21,I3,A2,I2,A1,I2,A1,I2,A1)') &
          'DAY (HOUR:MIN:SEC) = ',NDAY+1,' (',NHR,':',NMIN,':',NSEC,')'
!
!----------------------------------------------------------------------
!       LOAD CURRENT BOUNDARY CONDITIONS FOR PAM AND OTHER ROUTINES.
!----------------------------------------------------------------------
!
        CALL R2DFLD('paf',FLV1T,ILEV1,NSTP,1,ILEV1,IT,IT)
        PFT(:)=FLV1T(1,IT)
        DO IZ=1,ILEV
          PF(1,IZ)=FLV1T(IZ+1,IT)
        ENDDO
        PRESSG(1)=FLV1T(ILEV1,IT)
        CALL R2DFLD('pa',FLVT,ILEV,NSTP,1,ILEV,IT,IT)
        PH(1,:)=FLVT(:,IT)
        CALL R2DFLD('taf',FLV1T,ILEV1,NSTP,1,ILEV1,IT,IT)
        DO IZ=1,ILEV
          TFROW(1,IZ)=FLV1T(IZ+1,IT)
        ENDDO
        CALL R2DFLD('ta',FLVT,ILEV,NSTP,1,ILEV,IT,IT)
        THROWR(1,:)=FLVT(:,IT)
        CALL R2DFLD('ua',FLVT,ILEV,NSTP,1,ILEV,IT,IT)
        UROWR(1,:)=FLVT(:,IT)
        CALL R2DFLD('wa',FLVT,ILEV,NSTP,1,ILEV,IT,IT)
        WG(1,:)=FLVT(:,IT)
        CALL R2DFLD('rv',FLVT,ILEV,NSTP,1,ILEV,IT,IT)
        QROWR(1,:)=FLVT(:,IT)
        CALL R2DFLD('rl',FLVT,ILEV,NSTP,1,ILEV,IT,IT)
        QLWCR(1,:)=FLVT(:,IT)
        CALL R2DFLD('perta',FLVT,ILEV,NSTP,1,ILEV,IT,IT)
        PERTA(1,:)=FLVT(:,IT)
        CALL R2DFLD('perrv',FLVT,ILEV,NSTP,1,ILEV,IT,IT)
        PERRV(1,:)=FLVT(:,IT)
        CALL R1DFLD('rliqgnd',FT,NSTP,IT,IT)
        THLIQG(1)=FT(IT)
        CALL R1DFLD('zo',FT,NSTP,IT,IT)
        ZO(1)=FT(IT)
        CALL R1DFLD('tsurf',FT,NSTP,IT,IT)
        GTROW(1)=FT(IT)
!
        VROW=0.
!
!----------------------------------------------------------------------
!       PRESSURE-LEVEL INFORMATION.
!----------------------------------------------------------------------
!
        SHTJ(:,1)=PFT(:)/PRESSG(:)
        DP(:,1)=PF(:,1)-PFT(:)
        DO IZ=2,ILEV
          SHTJ(:,IZ)=PF(:,IZ-1)/PRESSG(:)
          DP(:,IZ)=PF(:,IZ)-PF(:,IZ-1)
        ENDDO
        DO IZ=1,ILEV
          SHJ(:,IZ)=PH(:,IZ)/PRESSG(:)
          DSHJ(:,IZ)=DP(:,IZ)/PRESSG(:)
        ENDDO
!
!----------------------------------------------------------------------
!       INSERT TEMPERATURE AND MOISTURE PERTURBATIONS (IF APPLICABLE).
!----------------------------------------------------------------------
!
        THROW=THROW+DT*PERTA
        QROW=QROW+DT*PERRV
        TAUX=TAUXR
        TAUT=TAUTR
        TAUQ=TAUQR
        TAUM=TAUMR
        IF ( IT < MODL%TRAX ) TAUX=0.
        IF ( IT < MODL%TRAT ) TAUT=0.
        IF ( IT < MODL%TRAQ ) TAUQ=0.
!
!----------------------------------------------------------------------
!       VERTICAL ADVECTION (SUBSIDENCE). THE CODE CONTAINS THE OPTION
!       TO ADVECT TRACERS (XROW), WHICH IS CURRENTLY UNUSED.
!----------------------------------------------------------------------
!
        CALL VADV(DXDT,XROW,DTDT,THROW,DQDT,QROW,DQLDT,QLWC, &
                  DUDT,UROW,WG,ZF,ZH,PH,DP,ALMC,ALMX, &
                  DT,IL1,IL2,ILG,ILEV,ILEVP1,MSGP2,NTRAC)
        XROW=MAX(XROW+DXDT*DT,0.)
        THROW=THROW+DTDT*DT
        QROW = QROW+DQDT*DT
        QLWC = QLWC+DQLDT*DT
        UROW = UROW+DUDT*DT
!
        DTADV=DTDT*REAL(YSPDAY)
        DQADV=DQDT*REAL(YSPDAY)
!
!       * SPECIFIED UPDRAFT WIND SPEED AND/OR CLOUD DROPLET NUMBER
!
        IF ( IT >= MODL%TRUP .AND. ABS(MODL%CDNN + SPV) > 0.001 ) &
          MODL%CDN=MODL%CDNN
        IF ( ABS(MODL%CDN + SPV) > 0.001 ) ZCDNROW=MODL%CDN
        CDNROW=1.E-06*ZCDNROW
!
!----------------------------------------------------------------------
!       VERTICAL DIFFUSION. THE CODE CONTAINS THE OPTION
!       TO MIX TRACERS (XROW), WHICH IS CURRENTLY UNUSED.
!----------------------------------------------------------------------
        ISUB=30
        DTS=DT/REAL(ISUB)
        DTVDF=0.
        DQVDF=0.
        DO IS=1,ISUB
          CALL VRTDF(DXDT,XROW,DTDT,THROW,DQDT,QROW,DQLDT,QLWC, &
                     DUDT,UROW,VROW,WSUB,RKH,RKM,RI,CDM,USTAR,ZSPD, &
                     ZSPDA,ZF,PF,TFROW,ZH,PH,DP,ALMC,ALMX,PBLTROW,TVP, &
                     QSENS,QLAT,ZCLF,GTROW,THLIQG,ZO,SHTJ,SHJ,DSHJ, &
                     PRESSG,DTS,MODL%NUGALT,IL1,IL2,ILG,ILEV,ILEVP1, &
                     MSGP2,NTRAC)
          XROW=MAX(XROW+DXDT*DTS,0.)
          THROW=THROW+DTDT*DTS
          QROW = QROW+DQDT*DTS
          QLWC = QLWC+DQLDT*DTS
          UROW = UROW+DUDT*DTS
          DTVDF=DTVDF+DTDT*REAL(YSPDAY)/REAL(ISUB)
          DQVDF=DQVDF+(DQDT+DQLDT)*REAL(YSPDAY)/REAL(ISUB)
        ENDDO
!
!----------------------------------------------------------------------
!       SIMPLE MICROPHYSICS FOR WARM CLOUDS.
!----------------------------------------------------------------------
!
        DTDT=THROW
        DQDT=QROW+QLWC
        CALL CLDPHYS(ZCLF,PCP,CLRFR,CLRFS,ZFRAIN,ZFSNOW,ZFEVAP, &
                     ZFSUBL,ZMLWC,ZMRATEP,RH,RHC,HMN,QTN,QROW,QLWC, &
                     ZCDNROW,QCWA,THROW,PH,ZH,DP,ALMC,ALMX,DT,ILG,ILEV, &
                     MODL%CASE)
        DTDT=(THROW-DTDT)/DT
        DQDT=(QROW+QLWC-DQDT)/DT
!
        DTPHS=DTDT*REAL(YSPDAY)
        DQPHS=DQDT*REAL(YSPDAY)
!
!----------------------------------------------------------------------
!       NUDGE TRACERS, TEMPERATURE, AND MOISTURE PROFILES TOWARDS
!       SPECIFIED LARGE-SCALE VALUES.
!----------------------------------------------------------------------
!
!!!
!        WHERE ( TAUQ < TAUL - 1.E-20)
!          QROWR=QROWR*0.9
!       ENDWHERE
        WHERE ( TAUX < TAUL - 1.E-20)
          DXDT=-(XROW-XROWR)/MAX(TAUX,DT)
        ELSEWHERE
          DXDT=0.
        ENDWHERE
        WHERE ( TAUT < TAUL - 1.E-20)
          DTDT=-(THROW-THROWR)/MAX(TAUT,DT)
        ELSEWHERE
          DTDT=0.
        ENDWHERE
        WHERE ( TAUQ < TAUL - 1.E-20)
          DQDT=-(QROW-QROWR)/MAX(TAUQ,DT)
        ELSEWHERE
          DQDT=0.
        ENDWHERE
        WHERE ( TAUQ < TAUL - 1.E-20)
          DQLDT=-(QLWC-QLWCR)/MAX(TAUQ,DT)
        ELSEWHERE
          DQLDT=0.
        ENDWHERE
        WHERE ( TAUM < TAUL - 1.E-20)
          DUDT=-(UROW-UROWR)/MAX(TAUM,DT)
        ELSEWHERE
          DUDT=0.
        ENDWHERE
!
        XROW=MAX(XROW+DXDT*DT,0.)
        THROW=THROW+DT*DTDT
        QROW=QROW+DT*DQDT
        QLWC=QLWC+DT*DQLDT
        UROW=UROW+DT*DUDT
!
        WHERE ( TAUX < 1.E-03 )
          DXDT=DXDT+(XROWR-XROW)/DT
          XROW=XROWR
        ENDWHERE
        WHERE ( TAUT < 1.E-03 )
          DTDT=DTDT+(THROWR-THROW)/DT
          THROW=THROWR
        ENDWHERE
        WHERE ( TAUQ < 1.E-03 )
          DQDT=DQDT+(QROWR-QROW)/DT
          QROW=QROWR
        ENDWHERE
        WHERE ( TAUQ < 1.E-03 )
          DQLDT=DQLDT+(QLWCR-QLWC)/DT
          QLWC=QLWCR
        ENDWHERE
        WHERE ( UROW < 1.E-03 ) UROW=UROWR
!
        DTNDG=DTDT*REAL(YSPDAY)
        DQNDG=(DQDT+DQLDT)*REAL(YSPDAY)
!
!       * TEMPERATURE AT GRID CELL INTERFACES
!
        WHERE ( TAUT(:,ILEV) > 1.E-03 ) TFROW(:,ILEV)=GTROW(:)
        DO IZ=ILEV-1,1,-1
          WHERE ( TAUT(:,IZ) > 1.E-03 )
            WGT=(ZF(:,IZ)-ZH(:,IZ+1))/(ZH(:,IZ)-ZH(:,IZ+1))
            TFROW(:,IZ)=WGT*THROW(:,IZ)+(1.-WGT)*THROW(:,IZ+1)
          ENDWHERE
        ENDDO
!
!       * ENSURE CONSISTENCY FOR CLOUD FRACTION.
!
        WHERE ( QLWC < 1.E-06 ) ZCLF=0.
!
!----------------------------------------------------------------------
!       DIAGNOSE CLOUD LIQUID WATER EFFECTIVE RADIUS.
!----------------------------------------------------------------------
!
        RHOA=PH/(RGAS*THROW)
        DO IZ=1,ILEV
          WHERE (CDNROW(:,IZ) > 1. .AND. QLWC(:,IZ) > YSMALL &
               .AND. ZCLF(:,IZ) > YSMALL )
!            BETA(:,IZ)=1.22+0.00084*CDNROW(:,IZ)
            BETA(:,IZ)=1.18+0.00045*CDNROW(:,IZ)
!            BETA(:,IZ)=1.
            RADEQV(:,IZ)=BETA(:,IZ)*62.035*(QLWC(:,IZ)*RHOA(:,IZ)*1000. &
                        /(ZCLF(:,IZ)*CDNROW(:,IZ)))**(1./3.)
            RADEQVA(:,IZ)=BETA(:,IZ)*62.035*(QCWA(:,IZ)*RHOA(:,IZ)*1000. &
                        /(CDNROW(:,IZ)))**(1./3.)
          ELSEWHERE
            RADEQV(:,IZ)=0.
            RADEQVA(:,IZ)=0.
          ENDWHERE
        ENDDO
!----------------------------------------------------------------------
!       COPY PAM RESULTS TO OUTPUT AT SPECIFIED OUTPUT TIME INTERVAL.
!----------------------------------------------------------------------
!
        IF ( MOD(IT+MODL%PLTFQ-1,MODL%PLTFQ) == 0 ) THEN
          ITY=ITY+1
          IF ( KTIME1 ) THEN
            ATIME(ITY)=DIM(NDTIM)%VAL(IT)
            CALL W1DDIM('time1',ATIME,NSTPO,ITY,ITY)
          ENDIF
          IF ( KMETOUT ) THEN
            FLVT1(:,ITY)=ZCLF(1,:)
            CALL W2DFLD('clf',FLVT1,ILEV,NSTPO,1,ILEV,ITY,ITY)
            FLVT1(:,ITY)=UROW(1,:)
            CALL W2DFLD('ua',FLVT1,ILEV,NSTPO,1,ILEV,ITY,ITY)
            FLVT1(:,ITY)=THROW(1,:)
            CALL W2DFLD('ta',FLVT1,ILEV,NSTPO,1,ILEV,ITY,ITY)
            FLVT1(:,ITY)=QROW(1,:)
            CALL W2DFLD('rv',FLVT1,ILEV,NSTPO,1,ILEV,ITY,ITY)
            FLVT1(:,ITY)=QLWC(1,:)
            CALL W2DFLD('rl',FLVT1,ILEV,NSTPO,1,ILEV,ITY,ITY)
            FLVT1(:,ITY)=RH(1,:)
            CALL W2DFLD('relhum',FLVT1,ILEV,NSTPO,1,ILEV,ITY,ITY)
            FLVT1(:,ITY)=RHC(1,:)
            CALL W2DFLD('relhumclr',FLVT1,ILEV,NSTPO,1,ILEV,ITY,ITY)
            FLVT1(:,ITY)=RI(1,:)
            CALL W2DFLD('ri',FLVT1,ILEV,NSTPO,1,ILEV,ITY,ITY)
            FLVT1(:,ITY)=RKM(1,:)
            CALL W2DFLD('rkm',FLVT1,ILEV,NSTPO,1,ILEV,ITY,ITY)
            FLVT2(ILEV+1,ITY)=QLAT(1)/RL*REAL(YSPDAY)
            DO IZ=ILEV,1,-1
              FAC=GRAV/DP(1,IZ)
              FLVT2(IZ,ITY)=FLVT2(IZ+1,ITY)-DQVDF(1,IZ)/FAC
            ENDDO
            CALL W2DFLD('rvflx',FLVT2,ILEV+1,NSTPO,1,ILEV+1,ITY,ITY)
            FT1(ITY)=PCP(1)*REAL(YSPDAY)
            CALL W1DFLD('pr',FT1,NSTPO,ITY,ITY)
            FLVT1(:,ITY)=CLRFR(1,:)
            CALL W2DFLD('frsclr',FLVT1,ILEV,NSTPO,1,ILEV,ITY,ITY)
            FLVT1(:,ITY)=ZMRATEP(1,:)
            CALL W2DFLD('prrscld',FLVT1,ILEV,NSTPO,1,ILEV,ITY,ITY)
            FLVT1(:,ITY)=ZMLWC(1,:)
            CALL W2DFLD('rlrscld',FLVT1,ILEV,NSTPO,1,ILEV,ITY,ITY)
            FLVT1(:,ITY)=ZFRAIN(1,:)
            CALL W2DFLD('prclr',FLVT1,ILEV,NSTPO,1,ILEV,ITY,ITY)
            FLVT1(:,ITY)=ZFEVAP(1,:)
            CALL W2DFLD('fprevapclr',FLVT1,ILEV,NSTPO,1,ILEV,ITY,ITY)
            FT1(ITY)=QSENS(1)
            CALL W1DFLD('hfss',FT1,NSTPO,ITY,ITY)
            FT1(ITY)=QLAT(1)
            CALL W1DFLD('hfls',FT1,NSTPO,ITY,ITY)
            FT1(ITY)=ZF(1,MIN(ILEV,MAX(1,NINT(PBLTROW(1))-1)))
            CALL W1DFLD('pblh',FT1,NSTPO,ITY,ITY)
            FLVT1(:,ITY)=TVP(1,:)
            CALL W2DFLD('tavp',FLVT1,ILEV,NSTPO,1,ILEV,ITY,ITY)
            FT1(ITY)=0.
            DO IZ=ILEV,1,-1
              FAC=GRAV/DP(1,IZ)
!              FT1(ITY)=FT1(ITY)+(DQNDG(1,IZ)+DQVDF(1,IZ))/FAC
              FT1(ITY)=FT1(ITY)+(DQNDG(1,IZ))/FAC
            ENDDO
            CALL W1DFLD('qfndg',FT1,NSTPO,ITY,ITY)
            FLVT1(:,ITY)=DQNDG(1,:)
            CALL W2DFLD('drdtndg',FLVT1,ILEV,NSTPO,1,ILEV,ITY,ITY)
            FLVT1(:,ITY)=DTNDG(1,:)
            CALL W2DFLD('dtadtndg',FLVT1,ILEV,NSTPO,1,ILEV,ITY,ITY)
            FLVT1(:,ITY)=DQADV(1,:)
            CALL W2DFLD('drdtadv',FLVT1,ILEV,NSTPO,1,ILEV,ITY,ITY)
            FLVT1(:,ITY)=DTADV(1,:)
            CALL W2DFLD('dtadtadv',FLVT1,ILEV,NSTPO,1,ILEV,ITY,ITY)
            FLVT1(:,ITY)=DQVDF(1,:)
            CALL W2DFLD('drdtvdf',FLVT1,ILEV,NSTPO,1,ILEV,ITY,ITY)
            FLVT1(:,ITY)=DTVDF(1,:)
            CALL W2DFLD('dtadtvdf',FLVT1,ILEV,NSTPO,1,ILEV,ITY,ITY)
            FLVT1(:,ITY)=DQPHS(1,:)
            CALL W2DFLD('drdtphs',FLVT1,ILEV,NSTPO,1,ILEV,ITY,ITY)
            FLVT1(:,ITY)=DTPHS(1,:)
            CALL W2DFLD('dtadtphs',FLVT1,ILEV,NSTPO,1,ILEV,ITY,ITY)
            FLVT1(:,ITY)=RADEQV(1,:)
            CALL W2DFLD('cldreff',FLVT1,ILEV,NSTPO,1,ILEV,ITY,ITY)
            FLVT1(:,ITY)=RADEQVA(1,:)
            CALL W2DFLD('cldreffadiab',FLVT1,ILEV,NSTPO,1,ILEV,ITY,ITY)
            FLVT1(:,ITY)=QCWA(1,:)
            CALL W2DFLD('rladiab',FLVT1,ILEV,NSTPO,1,ILEV,ITY,ITY)
            FLVT1(:,ITY)=RHOA(1,:)
            CALL W2DFLD('rhoa',FLVT1,ILEV,NSTPO,1,ILEV,ITY,ITY)
            FLVT1(:,ITY)=ZCDNROW(1,:)*1.E-06
            CALL W2DFLD('cldnc',FLVT1,ILEV,NSTPO,1,ILEV,ITY,ITY)
            FLVT1(:,ITY)=RHOA(1,:)*QLWC(1,:)*1.E+03
            CALL W2DFLD('clw',FLVT1,ILEV,NSTPO,1,ILEV,ITY,ITY)
            FT1(ITY)=0.
            DO L=1,ILEV
              FT1(ITY)=FT1(ITY)+DP(1,L)*QLWC(1,L)*1.E+03/GRAV
            ENDDO
            CALL W1DFLD('clwp',FT1,NSTPO,ITY,ITY)
            FLVT1(:,ITY)=TAUQ(1,:)
            CALL W2DFLD('tauq',FLVT1,ILEV,NSTPO,1,ILEV,ITY,ITY)
          ENDIF
        ENDIF
      ENDDO
!----------------------------------------------------------------------
!     * COPY FINAL RESULTS OF PAM SIMULATION. THESE FIELDS WILL
!     * BE NEEDED FOR MODEL RESTARTS.
!
      WRITE(6,'(A26)') 'WRITING OF MODEL OUTPUT...'
!
!     * WRITE INITIAL PROGNOSTIC FIELDS TO RESTART FILE.
!
      CALL W0DFLDRS('tstep',REAL(0))
      CALL W0DFLDRS('ustar',USTAR(1))
      CALL W1DFLDRS('ua',UROW(1,:),ILEV)
      CALL W1DFLDRS('ta',THROW(1,:),ILEV)
      CALL W1DFLDRS('rv',QROW(1,:),ILEV)
      CALL W1DFLDRS('rl',QLWC(1,:),ILEV)
      CALL W1DFLDRS('clf',ZCLF(1,:),ILEV)
      CALL W1DFLDRS('cldnc',ZCDNROW(1,:),ILEV)
      CALL W1DFLDRS('ri',RI(1,:),ILEV)
      CALL W0DFLDRS('indpbl',PBLTROW(1))
!
!-----------------------------------------------------------------------
!     * WRITE RESTART INFORMATION TO OUTPUT FILE.
!
      CFILR=TRIM(CFILE)//'_tfin'
      CALL WNCDATRS(CFILR)
!
!-----------------------------------------------------------------------
!     * WRITE PAM TIME-DEPENDENT DIAGNOSTIC RESULTS TO OUTPUT FILE.
!
      CFILB=TRIM(CFILE)//'_out'
      CALL WNCDAT(CFILB)
!
      END PROGRAM DRIVER
      SUBROUTINE RDNCDIM(FILE1,FILE2,ILEV,ILEV1,ITIME,KTIME1)
!
      USE IODAT
      USE NETCDF
!
      IMPLICIT NONE
!
      CHARACTER(LEN=*), INTENT(IN) :: FILE1,FILE2
      INTEGER, INTENT(OUT) :: ILEV,ILEV1,ITIME
      LOGICAL, INTENT(IN) :: KTIME1
      INTEGER, PARAMETER :: NFIL=2
      INTEGER, DIMENSION(NFIL) :: ncid
      INTEGER :: status,ND,NF,I
      INTEGER, DIMENSION(:), ALLOCATABLE :: lvl_dimid
!
!     * INITIALIZATION OF FIELD DIMENSIONS.
!
      ILEV=INAX
      ILEV1=INAX
      ITIME=INAX
!
!     * OPEN NETCDF FILES WITH INPUT AND RESTART DATA.
!
      status=nf90_open(TRIM(FILE1)//'.nc', nf90_nowrite, ncid(1))
      IF (status /= nf90_NoErr) CALL XIT('RDNCDIM',-1)
      status=nf90_open(TRIM(FILE2)//'.nc', nf90_nowrite, ncid(2))
      IF (status /= nf90_NoErr) CALL XIT('RDNCDIM',-2)
!
!     * DEFINE 10 NETCDF DIMENSION NAMES. AN ADDITIONAL TIME DIMENSION
!     * MAY BE GENERATED TO SUPPORT OUTPUT FIELDS, IF NECESSARY.
!
      NDIM=3
      IF ( KTIME1 ) NDIM=NDIM+1
!
!     * INITIALIZATION.
!
      ALLOCATE(DIM(NDIM))
      DO ND=1,NDIM
      DO I=1,IMAXL
        DIM(ND)%NAME(I:I)=' '
      ENDDO
      ENDDO
!
!     * DIMENSION NAMES.
!
      DIM(1)%NAME(1:10)='level     '
      DIM(2)%NAME(1:10)='level1    '
      DIM(3)%NAME(1:10)='time      '
!
!     * EXTRA TIME DIMENSION FOR OUTPUT.
!
      IF ( KTIME1 ) DIM(NDIM)%NAME(1:10)='time1     '
!
!     * INITIALIZATIONS.
!
      ALLOCATE(lvl_dimid(NDIM))
      DIM(:)%FLGI=0
      DIM(:)%FLGO=0
      DIM(:)%FLGRS=0
      DIM(:)%FLGX=0
!
!     * READ THE DIMENSIONS AND COORDINATES FROM THE FILES.
!
      DO NF=1,NFIL
      DO ND=1,NDIM
        IF ( DIM(ND)%FLGX==0 ) THEN
          status=nf90_inq_dimid(ncid(NF), TRIM(DIM(ND)%NAME), &
                                          lvl_dimid(ND))
          IF (status == nf90_NoErr) THEN
            DIM(ND)%FLGX=1
            status=nf90_inquire_dimension(ncid(NF), lvl_dimid(ND), &
                                          len=DIM(ND)%SIZE)
            status=nf90_get_att(ncid(NF), lvl_dimid(ND), "units", &
                                          DIM(ND)%UNITS )
            status=nf90_get_att(ncid(NF), lvl_dimid(ND), "long_name", &
                                          DIM(ND)%LNAME )
            status=nf90_inq_varid(ncid(NF), TRIM(DIM(ND)%NAME), &
                                            lvl_dimid(ND))
            ALLOCATE(DIM(ND)%VAL(DIM(ND)%SIZE))
            status=nf90_get_var(ncid(NF), lvl_dimid(ND), DIM(ND)%VAL)
!
!           * ASSOCIATE DIMENSIONS IN MODEL WITH NETCDF DIMENSIONS.
!
            SELECT CASE (ND)
              CASE (1)
                ILEV =DIM(ND)%SIZE
              CASE (2)
                ILEV1=DIM(ND)%SIZE
              CASE (3)
                ITIME=DIM(ND)%SIZE
            END SELECT
          ENDIF
        ENDIF
      ENDDO
      ENDDO
      DO ND=1,NDIM
        IF ( DIM(ND)%FLGX==0 ) THEN
          PRINT*,'COULD NOT FIND DIMENSION ',TRIM(DIM(ND)%NAME), &
                  ' IN INPUT FILES'
        ENDIF
      ENDDO
!
      END SUBROUTINE RDNCDIM
      SUBROUTINE SETNCDIM(NSTPO,IDIM)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
!     * DEFINE NETCDF DIMENSIONS AND ASSOCIATE WITH DIMENSIONS
!     * IN THE MODEL.
!
      PRINT*,'SETTING DIMENSION ',TRIM(DIM(IDIM)%NAME)
      DIM(IDIM)%LNAME(1:12)='Output time '
      DIM(IDIM)%UNITS(1:12)='s           '
      DIM(IDIM)%SIZE=NSTPO
      ALLOCATE(DIM(IDIM)%VAL(DIM(IDIM)%SIZE))
!
      END SUBROUTINE SETNCDIM
      SUBROUTINE INITIO(KTIME1)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      LOGICAL, INTENT(IN) :: KTIME1
!
!     * INITIALIZATIONS.
!
      DO IT=1,IMAXL
        CNA(IT:IT) = '*'
      ENDDO
      DO IT=1,ITOT
        IVAR(IT)%NAME=CNA
        IVAR(IT)%LNAME=CNA
        IVAR(IT)%UNITS=CNA
        IVAR(IT)%DIM=CNA
        IVARV(IT)%FLD0D=-YNAX
      ENDDO
      DO IT=1,ITOT
        OVAR(IT)%NAME=CNA
        OVAR(IT)%LNAME=CNA
        OVAR(IT)%UNITS=CNA
        OVAR(IT)%DIM=CNA
        OVARV(IT)%FLD0D=-YNAX
      ENDDO
      DO IT=1,ITOT
        RVAR(IT)%NAME=CNA
        RVAR(IT)%LNAME=CNA
        RVAR(IT)%UNITS=CNA
        RVAR(IT)%DIM=CNA
        RVARV(IT)%FLD0D=-YNAX
      ENDDO
!
!     * OPEN AND READ NAMELISTS.
!
      OPEN(10,FILE='INPUT')
      OPEN(20,FILE='OUTPUT')
      OPEN(30,FILE='RESTART')
      READ(10,NML=IVARNML)
      READ(20,NML=OVARNML)
      READ(30,NML=RVARNML)
!
!     * SAVE DIMENSIONS AND ALLOCATE MEMORY FOR I/O ARRAYS.
!
      DO IT=1,ITOT
        IF ( IVAR(IT)%NAME /= CNA ) THEN
          IF ( .NOT.(IVAR(IT)%LNAME /= CNA &
               .AND. IVAR(IT)%UNITS /= CNA) ) THEN
            PRINT*,'MISSING LNAME OR UNITS FOR INPUT VARIABLE ', &
                    TRIM(IVAR(IT)%NAME)
            CALL XIT('INITIO',-1)
          ENDIF
          DO ITX=1,IT-1
            IF ( TRIM(IVAR(IT)%NAME) == TRIM(IVAR(ITX)%NAME) ) THEN
              PRINT*,'MULTIPLE OCCURRENCES OF INPUT VARIABLE ', &
                      TRIM(IVAR(IT)%NAME)
              CALL XIT('INITIO',-2)
            ENDIF
          ENDDO
          IDIM=0
          DO IS=1,ISMX
            CTMP=IVAR(IT)%DIM(IS)
            IF ( CTMP /= CNA ) THEN
              IDIM=IDIM+1
              ICHK=0
              DO ND=1,NDIM
                IF ( TRIM(CTMP) == TRIM(DIM(ND)%NAME) ) THEN
                  DIM(ND)%FLGI=1
                  IDIMA(IDIM)=DIM(ND)%SIZE
                  IF ( DIM(ND)%SIZE == INAX ) THEN
                    PRINT*,'DIMENSION ',TRIM(DIM(ND)%NAME),' UNDEFINED'
                    CALL XIT ('INITIO',-3)
                  ENDIF
                  ICHK=1
                ENDIF
              ENDDO
              IF ( ICHK == 0 ) THEN
                PRINT*,'INCORRECT DIMENSION FOR INPUT VARIABLE ', &
                       TRIM(IVAR(IT)%NAME)
                CALL XIT('INITIO',-4)
              ENDIF
            ENDIF
          ENDDO
          IF ( IDIM == 0 ) THEN
            IVARV(IT)%FLD0D=YNAX
          ELSE IF ( IDIM == 1 ) THEN
            ALLOCATE(IVARV(IT)%FLD1D(IDIMA(1)))
            IVARV(IT)%FLD1D(:)=YNAX
          ELSE IF ( IDIM == 2 ) THEN
            ALLOCATE(IVARV(IT)%FLD2D(IDIMA(1),IDIMA(2)))
            IVARV(IT)%FLD2D(:,:)=YNAX
          ELSE IF ( IDIM == 3 ) THEN
            ALLOCATE(IVARV(IT)%FLD3D(IDIMA(1),IDIMA(2),IDIMA(3)))
            IVARV(IT)%FLD3D(:,:,:)=YNAX
          ELSE IF ( IDIM == 4 ) THEN
            ALLOCATE(IVARV(IT)%FLD4D(IDIMA(1),IDIMA(2),IDIMA(3), &
                                                            IDIMA(4)))
            IVARV(IT)%FLD4D(:,:,:,:)=YNAX
          ELSE
            CALL XIT('INITIO',-5)
          ENDIF
        ENDIF
      ENDDO
      DO IT=1,ITOT
        IF ( OVAR(IT)%NAME /= CNA ) THEN
          IF ( .NOT.(OVAR(IT)%LNAME /= CNA &
               .AND. OVAR(IT)%UNITS /= CNA) ) THEN
            PRINT*,'MISSING LNAME OR UNITS FOR OUTPUT VARIABLE ', &
                    TRIM(OVAR(IT)%NAME)
            CALL XIT('INITIO',-6)
          ENDIF
          DO ITX=1,IT-1
            IF ( TRIM(OVAR(IT)%NAME) == TRIM(OVAR(ITX)%NAME) ) THEN
              PRINT*,'MULTIPLE OCCURRENCES OF OUTPUT VARIABLE ', &
                      TRIM(OVAR(IT)%NAME)
              CALL XIT('INITIO',-7)
            ENDIF
          ENDDO
          IDIM=0
          DO IS=1,ISMX
            CTMP=OVAR(IT)%DIM(IS)
            IF ( KTIME1 .AND. TRIM(CTMP) == 'time' ) THEN
              OVAR(IT)%DIM(IS)='time1'
              CTMP=OVAR(IT)%DIM(IS)
            ENDIF
            IF ( CTMP /= CNA ) THEN
              IDIM=IDIM+1
              ICHK=0
              DO ND=1,NDIM
                IF ( TRIM(CTMP) == TRIM(DIM(ND)%NAME) ) THEN
                  DIM(ND)%FLGO=1
                  IDIMA(IDIM)=DIM(ND)%SIZE
                  IF ( DIM(ND)%SIZE == INAX ) THEN
                    PRINT*,'DIMENSION ',TRIM(DIM(ND)%NAME),' UNDEFINED'
                    CALL XIT ('INITIO',-8)
                  ENDIF
                  ICHK=1
                ENDIF
              ENDDO
              IF ( ICHK == 0 ) THEN
                PRINT*,'INCORRECT DIMENSION FOR OUTPUT VARIABLE ', &
                       TRIM(OVAR(IT)%NAME)
                CALL XIT('INITIO',-9)
              ENDIF
            ENDIF
          ENDDO
          IF ( IDIM == 0 ) THEN
            OVARV(IT)%FLD0D=YNAX
          ELSE IF ( IDIM == 1 ) THEN
            ALLOCATE(OVARV(IT)%FLD1D(IDIMA(1)))
            OVARV(IT)%FLD1D(:)=YNAX
          ELSE IF ( IDIM == 2 ) THEN
            ALLOCATE(OVARV(IT)%FLD2D(IDIMA(1),IDIMA(2)))
            OVARV(IT)%FLD2D(:,:)=YNAX
          ELSE IF ( IDIM == 3 ) THEN
            ALLOCATE(OVARV(IT)%FLD3D(IDIMA(1),IDIMA(2),IDIMA(3)))
            OVARV(IT)%FLD3D(:,:,:)=YNAX
          ELSE IF ( IDIM == 4 ) THEN
            ALLOCATE(OVARV(IT)%FLD4D(IDIMA(1),IDIMA(2),IDIMA(3), &
                                                            IDIMA(4)))
            OVARV(IT)%FLD4D(:,:,:,:)=YNAX
          ELSE
            CALL XIT('INITIO',-10)
          ENDIF
        ENDIF
      ENDDO
      DO IT=1,ITOT
        IF ( RVAR(IT)%NAME /= CNA ) THEN
          IF ( .NOT.(RVAR(IT)%LNAME /= CNA &
               .AND. RVAR(IT)%UNITS /= CNA) ) THEN
            PRINT*,'MISSING LNAME OR UNITS FOR RESTART VARIABLE ', &
                    TRIM(RVAR(IT)%NAME)
            CALL XIT('INITIO',-11)
          ENDIF
          DO ITX=1,IT-1
            IF ( TRIM(RVAR(IT)%NAME) == TRIM(RVAR(ITX)%NAME) ) THEN
              PRINT*,'MULTIPLE OCCURRENCES OF RESTART VARIABLE ', &
                      TRIM(RVAR(IT)%NAME)
              CALL XIT('INITIO',-12)
            ENDIF
          ENDDO
          IDIM=0
          DO IS=1,ISMX
            CTMP=RVAR(IT)%DIM(IS)
            IF ( CTMP /= CNA ) THEN
              IDIM=IDIM+1
              ICHK=0
              DO ND=1,NDIM
                IF ( TRIM(CTMP) == TRIM(DIM(ND)%NAME) ) THEN
                  DIM(ND)%FLGRS=1
                  IDIMA(IDIM)=DIM(ND)%SIZE
                  IF ( DIM(ND)%SIZE == INAX ) THEN
                    PRINT*,'DIMENSION ',TRIM(DIM(ND)%NAME),' UNDEFINED'
                    CALL XIT ('INITIO',-13)
                  ENDIF
                  ICHK=1
                ENDIF
              ENDDO
              IF ( ICHK == 0 ) THEN
                PRINT*,'INCORRECT DIMENSION FOR RESTART VARIABLE ', &
                       TRIM(RVAR(IT)%NAME)
                CALL XIT('INITIO',-14)
              ENDIF
            ENDIF
          ENDDO
          IF ( IDIM == 0 ) THEN
            RVARV(IT)%FLD0D=YNAX
          ELSE IF ( IDIM == 1 ) THEN
            ALLOCATE(RVARV(IT)%FLD1D(IDIMA(1)))
            RVARV(IT)%FLD1D(:)=YNAX
          ELSE IF ( IDIM == 2 ) THEN
            ALLOCATE(RVARV(IT)%FLD2D(IDIMA(1),IDIMA(2)))
            RVARV(IT)%FLD2D(:,:)=YNAX
          ELSE IF ( IDIM == 3 ) THEN
            ALLOCATE(RVARV(IT)%FLD3D(IDIMA(1),IDIMA(2),IDIMA(3)))
            RVARV(IT)%FLD3D(:,:,:)=YNAX
          ELSE IF ( IDIM == 4 ) THEN
            ALLOCATE(RVARV(IT)%FLD4D(IDIMA(1),IDIMA(2),IDIMA(3), &
                                                             IDIMA(4)))
            RVARV(IT)%FLD4D(:,:,:,:)=YNAX
          ELSE
            CALL XIT('INITIO',-15)
          ENDIF
        ENDIF
      ENDDO
!
!     * CLOSE INPUT FILES.
!
      CLOSE(10)
      CLOSE(11)
      CLOSE(12)
!
      END SUBROUTINE INITIO
      SUBROUTINE W1DDIM(CNAME,AVAR,IDIM1,J1,J2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(IN), DIMENSION(IDIM1) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1
      INTEGER, OPTIONAL :: J1,J2
      INTEGER :: J1T,J2T
      INTEGER, DIMENSION(1) :: ISHAPE
!
      IF ( PRESENT(J1) .AND. PRESENT(J2) ) THEN
        IF ( J1 < 1 .OR. J2 > IDIM1 ) CALL XIT('W1DDIM',-1)
        J1T=J1
        J2T=J2
      ELSE
        J1T=1
        J2T=IDIM1
      ENDIF
!
      ICHCK=0
      DO IT=1,NDIM
        IF ( ALLOCATED(DIM(IT)%VAL) &
              .AND. TRIM(DIM(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(DIM(IT)%VAL)
          IF ( ISHAPE(1) == IDIM1 ) THEN
            DIM(IT)%VAL(J1T:J2T)=AVAR(J1T:J2T)
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('W1DDIM',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO DIMENSION ',TRIM(CNAME),' IN LIST OF DIMENSIONS'
        CALL XIT('W1DDIM',-3)
      ENDIF
!
      END SUBROUTINE W1DDIM
      SUBROUTINE W0DFLD(CNAME,AVAR)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL :: AVAR
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ABS(OVARV(IT)%FLD0D+YNAX) > YSMALLX &
              .AND. TRIM(OVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
          OVARV(IT)%FLD0D=AVAR
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME),' IN LIST OF IO VARIABLES'
        CALL XIT('W0DFLD',-1)
      ENDIF
!
      END SUBROUTINE W0DFLD
      SUBROUTINE W1DFLD(CNAME,AVAR,IDIM1,J1,J2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(IN), DIMENSION(IDIM1) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1
      INTEGER, OPTIONAL :: J1,J2
      INTEGER :: J1T,J2T
      INTEGER, DIMENSION(1) :: ISHAPE
!
      IF ( PRESENT(J1) .AND. PRESENT(J2) ) THEN
        IF ( J1 < 1 .OR. J2 > IDIM1 ) CALL XIT('W1DFLD',-1)
        J1T=J1
        J2T=J2
      ELSE
        J1T=1
        J2T=IDIM1
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(OVARV(IT)%FLD1D) &
              .AND. TRIM(OVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(OVARV(IT)%FLD1D)
          IF ( ISHAPE(1) == IDIM1 ) THEN
            OVARV(IT)%FLD1D(J1T:J2T)=AVAR(J1T:J2T)
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('W1DFLD',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME),' IN LIST OF IO VARIABLES'
        CALL XIT('W1DFLD',-3)
      ENDIF
!
      END SUBROUTINE W1DFLD
      SUBROUTINE W2DFLD(CNAME,AVAR,IDIM1,IDIM2,J1,J2,K1,K2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1,IDIM2
      INTEGER, OPTIONAL :: J1,J2,K1,K2
      INTEGER :: J1T,J2T,K1T,K2T
      INTEGER, DIMENSION(2) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) ) THEN
        IF ( J1 < 1 .OR. K1 < 1 .OR. J2 > IDIM1 .OR. K2 > IDIM2 ) &
           CALL XIT('W2DFLD',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(OVARV(IT)%FLD2D) &
              .AND. TRIM(OVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(OVARV(IT)%FLD2D)
          IF ( ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 ) THEN
            OVARV(IT)%FLD2D(J1T:J2T,K1T:K2T)=AVAR(J1T:J2T,K1T:K2T)
          ELSE IF ( ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM1 ) THEN
            DO I1=K1T,K2T
            DO I2=J1T,J2T
              OVARV(IT)%FLD2D(I1,I2)=AVAR(I2,I1)
            ENDDO
            ENDDO
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('W2DFLD',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME),' IN LIST OF IO VARIABLES'
        CALL XIT('W2DFLD',-3)
      ENDIF
!
      END SUBROUTINE W2DFLD
      SUBROUTINE W3DFLD(CNAME,AVAR,IDIM1,IDIM2,IDIM3, &
                        J1,J2,K1,K2,L1,L2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2,IDIM3) :: AVAR
      INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2
      INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3
      INTEGER :: J1T,J2T,K1T,K2T,L1T,L2T
      INTEGER, DIMENSION(3) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) &
           .AND. PRESENT(L1) .AND. PRESENT(L2) ) THEN
        IF (      J1 < 1 .OR. K1 < 1 .OR. L1 < 1 &
             .OR. J2 > IDIM1 .OR. K2 > IDIM2 .OR. L2 > IDIM3 ) &
           CALL XIT('W3DFLD',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
        L1T=L1
        L2T=L2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
        L1T=1
        L2T=IDIM3
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(OVARV(IT)%FLD3D) &
              .AND. TRIM(OVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(OVARV(IT)%FLD3D)
          IF (       ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM3 ) THEN
            OVARV(IT)%FLD3D(J1T:J2T,K1T:K2T,L1T:L2T)= &
                       AVAR(J1T:J2T,K1T:K2T,L1T:L2T)
          ELSE IF (  ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM3 &
               .AND. ISHAPE(3) == IDIM2 ) THEN
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              OVARV(IT)%FLD3D(J1T:J2T,I3,I2)=AVAR(J1T:J2T,I2,I3)
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM1 &
               .AND. ISHAPE(3) == IDIM3 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
              OVARV(IT)%FLD3D(I2,I1,L1T:L2T)=AVAR(I1,I2,L1T:L2T)
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM3 &
               .AND. ISHAPE(3) == IDIM1 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              OVARV(IT)%FLD3D(I2,I3,I1)=AVAR(I1,I2,I3)
            ENDDO
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM3 .AND. ISHAPE(2) == IDIM1 &
               .AND. ISHAPE(3) == IDIM2 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              OVARV(IT)%FLD3D(I3,I1,I2)=AVAR(I1,I2,I3)
            ENDDO
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM3 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM1 ) THEN
            DO I1=J1T,J2T
            DO I3=L1T,L2T
              OVARV(IT)%FLD3D(I3,K1T:K2T,I1)=AVAR(I1,K1T:K2T,I3)
            ENDDO
            ENDDO
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('W3DFLD',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME),' IN LIST OF IO VARIABLES'
        CALL XIT('W3DFLD',-3)
      ENDIF
!
      END SUBROUTINE W3DFLD
      SUBROUTINE W4DFLD(CNAME,AVAR,IDIM1,IDIM2,IDIM3,IDIM4, &
                        J1,J2,K1,K2,L1,L2,M1,M2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2,IDIM3,IDIM4) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3,IDIM4
      INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2,M1,M2
      INTEGER :: J1T,J2T,K1T,K2T,L1T,L2T,M1T,M2T
      INTEGER, DIMENSION(4) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) &
           .AND. PRESENT(L1) .AND. PRESENT(L2) &
           .AND. PRESENT(M1) .AND. PRESENT(M2) ) THEN
        IF (      J1 < 1 .OR. K1 < 1 .OR. L1 < 1 .OR. M1 < 1 &
             .OR. J2 > IDIM1 .OR. K2 > IDIM2 .OR. L2 > IDIM3 &
             .OR. M2 > IDIM4 ) &
           CALL XIT('W4DFLD',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
        L1T=L1
        L2T=L2
        M1T=M1
        M2T=M2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
        L1T=1
        L2T=IDIM3
        M1T=1
        M2T=IDIM4
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(OVARV(IT)%FLD4D) &
              .AND. TRIM(OVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(OVARV(IT)%FLD4D)
          IF (       ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM3 .AND. ISHAPE(4) == IDIM4 ) THEN
            OVARV(IT)%FLD4D(J1T:J2T,K1T:K2T,L1T:L2T,M1T:M2T)= &
                       AVAR(J1T:J2T,K1T:K2T,L1T:L2T,M1T:M2T)
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('W4DFLD',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME),' IN LIST OF IO VARIABLES'
        CALL XIT('W4DFLD',-3)
      ENDIF
!
      END SUBROUTINE W4DFLD
      SUBROUTINE W0DFLDRS(CNAME,AVAR)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(IN) :: AVAR
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ABS(RVARV(IT)%FLD0D+YNAX) > YSMALLX &
              .AND. TRIM(RVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
          RVARV(IT)%FLD0D=AVAR
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME), &
               ' IN LIST OF RESTART VARIABLES'
        CALL XIT('W0DFLDRS',-1)
      ENDIF
!
      END SUBROUTINE W0DFLDRS
      SUBROUTINE W1DFLDRS(CNAME,AVAR,IDIM1,J1,J2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(IN), DIMENSION(IDIM1) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1
      INTEGER, OPTIONAL :: J1,J2
      INTEGER :: J1T,J2T
      INTEGER, DIMENSION(1) :: ISHAPE
!
      IF ( PRESENT(J1) .AND. PRESENT(J2) ) THEN
        IF ( J1 < 1 .OR. J2 > IDIM1 ) CALL XIT('W1DFLDRS',-1)
        J1T=J1
        J2T=J2
      ELSE
        J1T=1
        J2T=IDIM1
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(RVARV(IT)%FLD1D) &
              .AND. TRIM(RVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(RVARV(IT)%FLD1D)
          IF ( ISHAPE(1) == IDIM1 ) THEN
            RVARV(IT)%FLD1D(J1T:J2T)=AVAR(J1T:J2T)
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('W1DFLDRS',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME), &
               ' IN LIST OF RESTART VARIABLES'
        CALL XIT('W1DFLDRS',-3)
      ENDIF
!
      END SUBROUTINE W1DFLDRS
      SUBROUTINE W2DFLDRS(CNAME,AVAR,IDIM1,IDIM2,J1,J2,K1,K2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1,IDIM2
      INTEGER, OPTIONAL :: J1,J2,K1,K2
      INTEGER :: J1T,J2T,K1T,K2T
      INTEGER, DIMENSION(2) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) ) THEN
        IF ( J1 < 1 .OR. K1 < 1 .OR. J2 > IDIM1 .OR. K2 > IDIM2 ) &
           CALL XIT('W2DFLDRS',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(RVARV(IT)%FLD2D) &
              .AND. TRIM(RVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(RVARV(IT)%FLD2D)
          IF ( ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 ) THEN
            RVARV(IT)%FLD2D(J1T:J2T,K1T:K2T)=AVAR(J1T:J2T,K1T:K2T)
          ELSE IF ( ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM1 ) THEN
            DO I1=K1T,K2T
            DO I2=J1T,J2T
              RVARV(IT)%FLD2D(I1,I2)=AVAR(I2,I1)
            ENDDO
            ENDDO
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('W2DFLDRS',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME), &
               ' IN LIST OF RESTART VARIABLES'
        CALL XIT('W2DFLDRS',-3)
      ENDIF
!
      END SUBROUTINE W2DFLDRS
      SUBROUTINE W3DFLDRS(CNAME,AVAR,IDIM1,IDIM2,IDIM3, &
                          J1,J2,K1,K2,L1,L2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2,IDIM3) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3
      INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2
      INTEGER :: J1T,J2T,K1T,K2T,L1T,L2T
      INTEGER, DIMENSION(3) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) &
           .AND. PRESENT(L1) .AND. PRESENT(L2) ) THEN
        IF (      J1 < 1 .OR. K1 < 1 .OR. L1 < 1 &
             .OR. J2 > IDIM1 .OR. K2 > IDIM2 .OR. L2 > IDIM3 ) &
           CALL XIT('W3DFLDRS',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
        L1T=L1
        L2T=L2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
        L1T=1
        L2T=IDIM3
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(RVARV(IT)%FLD3D) &
              .AND. TRIM(RVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(RVARV(IT)%FLD3D)
          IF (       ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM3 ) THEN
            RVARV(IT)%FLD3D(J1T:J2T,K1T:K2T,L1T:L2T)= &
                        AVAR(J1T:J2T,K1T:K2T,L1T:L2T)
          ELSE IF (  ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM3 &
               .AND. ISHAPE(3) == IDIM2 ) THEN
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              RVARV(IT)%FLD3D(J1T:J2T,I3,I2)=AVAR(J1T:J2T,I2,I3)
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM1 &
               .AND. ISHAPE(3) == IDIM3 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
              RVARV(IT)%FLD3D(I2,I1,L1T:L2T)=AVAR(I1,I2,L1T:L2T)
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM3 &
               .AND. ISHAPE(3) == IDIM1 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              RVARV(IT)%FLD3D(I2,I3,I1)=AVAR(I1,I2,I3)
            ENDDO
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM3 .AND. ISHAPE(2) == IDIM1 &
               .AND. ISHAPE(3) == IDIM2 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              RVARV(IT)%FLD3D(I3,I1,I2)=AVAR(I1,I2,I3)
            ENDDO
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM3 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM1 ) THEN
            DO I1=J1T,J2T
            DO I3=L1T,L2T
              RVARV(IT)%FLD3D(I3,K1T:K2T,I1)=AVAR(I1,K1T:K2T,I3)
            ENDDO
            ENDDO
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('W3DFLDRS',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME), &
               ' IN LIST OF RESTART VARIABLES'
        CALL XIT('W3DFLDRS',-3)
      ENDIF
!
      END SUBROUTINE W3DFLDRS
      SUBROUTINE W4DFLDRS(CNAME,AVAR,IDIM1,IDIM2,IDIM3,IDIM4, &
                          J1,J2,K1,K2,L1,L2,M1,M2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2,IDIM3,IDIM4) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3,IDIM4
      INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2,M1,M2
      INTEGER :: J1T,J2T,K1T,K2T,L1T,L2T,M1T,M2T
      INTEGER, DIMENSION(4) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) &
           .AND. PRESENT(L1) .AND. PRESENT(L2) &
           .AND. PRESENT(M1) .AND. PRESENT(M2) ) THEN
        IF (      J1 < 1 .OR. K1 < 1 .OR. L1 < 1 .OR. M1 < 1 &
             .OR. J2 > IDIM1 .OR. K2 > IDIM2 .OR. L2 > IDIM3 &
             .OR. M2 > IDIM4 ) &
           CALL XIT('W4DFLDRS',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
        L1T=L1
        L2T=L2
        M1T=M1
        M2T=M2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
        L1T=1
        L2T=IDIM3
        M1T=1
        M2T=IDIM4
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(RVARV(IT)%FLD4D) &
              .AND. TRIM(RVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(RVARV(IT)%FLD4D)
          IF (       ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM3 .AND. ISHAPE(4) == IDIM4 ) THEN
            RVARV(IT)%FLD4D(J1T:J2T,K1T:K2T,L1T:L2T,M1T:M2T)= &
                        AVAR(J1T:J2T,K1T:K2T,L1T:L2T,M1T:M2T)
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('W4DFLDRS',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME), &
               ' IN LIST OF RESTART VARIABLES'
        CALL XIT('W4DFLDRS',-3)
      ENDIF
!
      END SUBROUTINE W4DFLDRS
      SUBROUTINE R0DFLD(CNAME,AVAR)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL :: AVAR
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ABS(IVARV(IT)%FLD0D+YNAX) > YSMALLX &
              .AND. TRIM(IVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
          AVAR=IVARV(IT)%FLD0D
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME),' IN LIST OF IO VARIABLES'
        CALL XIT('R0DFLD',-1)
      ENDIF
!
      END SUBROUTINE R0DFLD
      SUBROUTINE R1DFLD(CNAME,AVAR,IDIM1,J1,J2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(OUT), DIMENSION(IDIM1) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1
      INTEGER, OPTIONAL :: J1,J2
      INTEGER :: J1T,J2T
      INTEGER, DIMENSION(1) :: ISHAPE
!
      IF ( PRESENT(J1) .AND. PRESENT(J2) ) THEN
        IF ( J1 < 1 .OR. J2 > IDIM1 ) CALL XIT('R1DFLD',-1)
        J1T=J1
        J2T=J2
      ELSE
        J1T=1
        J2T=IDIM1
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(IVARV(IT)%FLD1D) &
              .AND. TRIM(IVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(IVARV(IT)%FLD1D)
          IF ( ISHAPE(1) == IDIM1 ) THEN
            AVAR(J1T:J2T)=IVARV(IT)%FLD1D(J1T:J2T)
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('R1DFLD',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME),' IN LIST OF IO VARIABLES'
        CALL XIT('R1DFLD',-3)
      ENDIF
!
      END SUBROUTINE R1DFLD
      SUBROUTINE R2DFLD(CNAME,AVAR,IDIM1,IDIM2,J1,J2,K1,K2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(OUT), DIMENSION(IDIM1,IDIM2) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1,IDIM2
      INTEGER, OPTIONAL :: J1,J2,K1,K2
      INTEGER :: J1T,J2T,K1T,K2T
      INTEGER, DIMENSION(2) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) ) THEN
        IF ( J1 < 1 .OR. K1 < 1 .OR. J2 > IDIM1 .OR. K2 > IDIM2 ) &
           CALL XIT('R2DFLD',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(IVARV(IT)%FLD2D) &
              .AND. TRIM(IVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(IVARV(IT)%FLD2D)
          IF ( ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 ) THEN
            AVAR(J1T:J2T,K1T:K2T)=IVARV(IT)%FLD2D(J1T:J2T,K1T:K2T)
          ELSE IF ( ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM1 ) THEN
            DO I1=K1T,K2T
            DO I2=J1T,J2T
              AVAR(I2,I1)=IVARV(IT)%FLD2D(I1,I2)
            ENDDO
            ENDDO
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('R2DFLD',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME),' IN LIST OF IO VARIABLES'
        CALL XIT('R2DFLD',-3)
      ENDIF
!
      END SUBROUTINE R2DFLD
      SUBROUTINE R3DFLD(CNAME,AVAR,IDIM1,IDIM2,IDIM3, &
                        J1,J2,K1,K2,L1,L2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(OUT), DIMENSION(IDIM1,IDIM2,IDIM3) :: AVAR
      INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2
      INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3
      INTEGER :: J1T,J2T,K1T,K2T,L1T,L2T
      INTEGER, DIMENSION(3) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) &
           .AND. PRESENT(L1) .AND. PRESENT(L2) ) THEN
        IF (      J1 < 1 .OR. K1 < 1 .OR. L1 < 1 &
             .OR. J2 > IDIM1 .OR. K2 > IDIM2 .OR. L2 > IDIM3 ) &
           CALL XIT('R3DFLD',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
        L1T=L1
        L2T=L2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
        L1T=1
        L2T=IDIM3
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(IVARV(IT)%FLD3D) &
              .AND. TRIM(IVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(IVARV(IT)%FLD3D)
          IF (       ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM3 ) THEN
            AVAR(J1T:J2T,K1T:K2T,L1T:L2T)= &
                IVARV(IT)%FLD3D(J1T:J2T,K1T:K2T,L1T:L2T)
          ELSE IF (  ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM3 &
               .AND. ISHAPE(3) == IDIM2 ) THEN
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              AVAR(J1T:J2T,I2,I3)=IVARV(IT)%FLD3D(J1T:J2T,I3,I2)
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM1 &
               .AND. ISHAPE(3) == IDIM3 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
              AVAR(I1,I2,L1T:L2T)=IVARV(IT)%FLD3D(I2,I1,L1T:L2T)
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM3 &
               .AND. ISHAPE(3) == IDIM1 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              AVAR(I1,I2,I3)=IVARV(IT)%FLD3D(I2,I3,I1)
            ENDDO
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM3 .AND. ISHAPE(2) == IDIM1 &
               .AND. ISHAPE(3) == IDIM2 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              AVAR(I1,I2,I3)=IVARV(IT)%FLD3D(I3,I1,I2)
            ENDDO
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM3 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM1 ) THEN
            DO I1=J1T,J2T
            DO I3=L1T,L2T
              AVAR(I1,K1T:K2T,I3)=IVARV(IT)%FLD3D(I3,K1T:K2T,I1)
            ENDDO
            ENDDO
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('R3DFLD',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME),' IN LIST OF IO VARIABLES'
        CALL XIT('R3DFLD',-3)
      ENDIF
!
      END SUBROUTINE R3DFLD
      SUBROUTINE R4DFLD(CNAME,AVAR,IDIM1,IDIM2,IDIM3,IDIM4, &
                        J1,J2,K1,K2,L1,L2,M1,M2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(OUT), DIMENSION(IDIM1,IDIM2,IDIM3,IDIM4) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3,IDIM4
      INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2,M1,M2
      INTEGER :: J1T,J2T,K1T,K2T,L1T,L2T,M1T,M2T
      INTEGER, DIMENSION(4) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) &
           .AND. PRESENT(L1) .AND. PRESENT(L2) &
           .AND. PRESENT(M1) .AND. PRESENT(M2) ) THEN
        IF (      J1 < 1 .OR. K1 < 1 .OR. L1 < 1 .OR. M1 < 1 &
             .OR. J2 > IDIM1 .OR. K2 > IDIM2 .OR. L2 > IDIM3 &
             .OR. M2 > IDIM4 ) &
           CALL XIT('R4DFLD',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
        L1T=L1
        L2T=L2
        M1T=M1
        M2T=M2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
        L1T=1
        L2T=IDIM3
        M1T=1
        M2T=IDIM4
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(IVARV(IT)%FLD4D) &
              .AND. TRIM(IVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(IVARV(IT)%FLD4D)
          IF (       ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM3 .AND. ISHAPE(4) == IDIM4 ) THEN
            AVAR(J1T:J2T,K1T:K2T,L1T:L2T,M1T:M2T)= &
                IVARV(IT)%FLD4D(J1T:J2T,K1T:K2T,L1T:L2T,M1T:M2T)
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('R4DFLD',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME),' IN LIST OF IO VARIABLES'
        CALL XIT('R4DFLD',-3)
      ENDIF
!
      END SUBROUTINE R4DFLD
      SUBROUTINE R0DFLDRS(CNAME,AVAR)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(OUT) :: AVAR
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ABS(RVARV(IT)%FLD0D+YNAX) > YSMALLX &
              .AND. TRIM(RVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
          AVAR=RVARV(IT)%FLD0D
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME), &
               ' IN LIST OF RESTART VARIABLES'
        CALL XIT('R0DFLDRS',-1)
      ENDIF
!
      END SUBROUTINE R0DFLDRS
      SUBROUTINE R1DFLDRS(CNAME,AVAR,IDIM1,J1,J2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(OUT), DIMENSION(IDIM1) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1
      INTEGER, OPTIONAL :: J1,J2
      INTEGER :: J1T,J2T
      INTEGER, DIMENSION(1) :: ISHAPE
!
      IF ( PRESENT(J1) .AND. PRESENT(J2) ) THEN
        IF ( J1 < 1 .OR. J2 > IDIM1 ) CALL XIT('R1DFLDRS',-1)
        J1T=J1
        J2T=J2
      ELSE
        J1T=1
        J2T=IDIM1
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(RVARV(IT)%FLD1D) &
              .AND. TRIM(RVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(RVARV(IT)%FLD1D)
          IF ( ISHAPE(1) == IDIM1 ) THEN
            AVAR(J1T:J2T)=RVARV(IT)%FLD1D(J1T:J2T)
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('R1DFLDRS',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME), &
               ' IN LIST OF RESTART VARIABLES'
        CALL XIT('R1DFLDRS',-3)
      ENDIF
!
      END SUBROUTINE R1DFLDRS
      SUBROUTINE R2DFLDRS(CNAME,AVAR,IDIM1,IDIM2,J1,J2,K1,K2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(OUT), DIMENSION(IDIM1,IDIM2) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1,IDIM2
      INTEGER, OPTIONAL :: J1,J2,K1,K2
      INTEGER :: J1T,J2T,K1T,K2T
      INTEGER, DIMENSION(2) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) ) THEN
        IF ( J1 < 1 .OR. K1 < 1 .OR. J2 > IDIM1 .OR. K2 > IDIM2 ) &
           CALL XIT('R2DFLDRS',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(RVARV(IT)%FLD2D) &
              .AND. TRIM(RVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(RVARV(IT)%FLD2D)
          IF ( ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 ) THEN
            AVAR(J1T:J2T,K1T:K2T)=RVARV(IT)%FLD2D(J1T:J2T,K1T:K2T)
          ELSE IF ( ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM1 ) THEN
            DO I1=K1T,K2T
            DO I2=J1T,J2T
              AVAR(I2,I1)=RVARV(IT)%FLD2D(I1,I2)
            ENDDO
            ENDDO
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('R2DFLDRS',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME), &
               ' IN LIST OF RESTART VARIABLES'
        CALL XIT('R2DFLDRS',-3)
      ENDIF
!
      END SUBROUTINE R2DFLDRS
      SUBROUTINE R3DFLDRS(CNAME,AVAR,IDIM1,IDIM2,IDIM3, &
                          J1,J2,K1,K2,L1,L2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(OUT), DIMENSION(IDIM1,IDIM2,IDIM3) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3
      INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2
      INTEGER :: J1T,J2T,K1T,K2T,L1T,L2T
      INTEGER, DIMENSION(3) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) &
           .AND. PRESENT(L1) .AND. PRESENT(L2) ) THEN
        IF (      J1 < 1 .OR. K1 < 1 .OR. L1 < 1 &
             .OR. J2 > IDIM1 .OR. K2 > IDIM2 .OR. L2 > IDIM3 ) &
           CALL XIT('R3DFLDRS',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
        L1T=L1
        L2T=L2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
        L1T=1
        L2T=IDIM3
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(RVARV(IT)%FLD3D) &
              .AND. TRIM(RVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(RVARV(IT)%FLD3D)
          IF (       ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM3 ) THEN
            AVAR(J1T:J2T,K1T:K2T,L1T:L2T)= &
                RVARV(IT)%FLD3D(J1T:J2T,K1T:K2T,L1T:L2T)
          ELSE IF (  ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM3 &
               .AND. ISHAPE(3) == IDIM2 ) THEN
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              AVAR(J1T:J2T,I2,I3)=RVARV(IT)%FLD3D(J1T:J2T,I3,I2)
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM1 &
               .AND. ISHAPE(3) == IDIM3 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
              AVAR(I1,I2,L1T:L2T)=RVARV(IT)%FLD3D(I2,I1,L1T:L2T)
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM3 &
               .AND. ISHAPE(3) == IDIM1 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              AVAR(I1,I2,I3)=RVARV(IT)%FLD3D(I2,I3,I1)
            ENDDO
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM3 .AND. ISHAPE(2) == IDIM1 &
               .AND. ISHAPE(3) == IDIM2 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              AVAR(I1,I2,I3)=RVARV(IT)%FLD3D(I3,I1,I2)
            ENDDO
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM3 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM1 ) THEN
            DO I1=J1T,J2T
            DO I3=L1T,L2T
              AVAR(I1,K1T:K2T,I3)=RVARV(IT)%FLD3D(I3,K1T:K2T,I1)
            ENDDO
            ENDDO
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('R3DFLDRS',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME), &
               ' IN LIST OF RESTART VARIABLES'
        CALL XIT('R3DFLDRS',-3)
      ENDIF
!
      END SUBROUTINE R3DFLDRS
      SUBROUTINE R4DFLDRS(CNAME,AVAR,IDIM1,IDIM2,IDIM3,IDIM4, &
                          J1,J2,K1,K2,L1,L2,M1,M2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(OUT), DIMENSION(IDIM1,IDIM2,IDIM3,IDIM4) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3,IDIM4
      INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2,M1,M2
      INTEGER :: J1T,J2T,K1T,K2T,L1T,L2T,M1T,M2T
      INTEGER, DIMENSION(4) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) &
           .AND. PRESENT(L1) .AND. PRESENT(L2) &
           .AND. PRESENT(M1) .AND. PRESENT(M2) ) THEN
        IF (      J1 < 1 .OR. K1 < 1 .OR. L1 < 1 .OR. M1 < 1 &
             .OR. J2 > IDIM1 .OR. K2 > IDIM2 .OR. L2 > IDIM3 &
             .OR. M2 > IDIM4 ) &
           CALL XIT('R4DFLDRS',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
        L1T=L1
        L2T=L2
        M1T=M1
        M2T=M2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
        L1T=1
        L2T=IDIM3
        M1T=1
        M2T=IDIM4
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(RVARV(IT)%FLD4D) &
              .AND. TRIM(RVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(RVARV(IT)%FLD4D)
          IF (       ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM3 .AND. ISHAPE(4) == IDIM4 ) THEN
            AVAR(J1T:J2T,K1T:K2T,L1T:L2T,M1T:M2T)= &
                RVARV(IT)%FLD4D(J1T:J2T,K1T:K2T,L1T:L2T,M1T:M2T)
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('R4DFLDRS',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME), &
               ' IN LIST OF RESTART VARIABLES'
        CALL XIT('R4DFLDRS',-3)
      ENDIF
!
      END SUBROUTINE R4DFLDRS
      SUBROUTINE WNCDAT(FILE_NAME)
!
      USE IODAT
      USE NETCDF
!
      IMPLICIT NONE
!
      INTEGER :: IS,IT,ND,IV,ICHCK,IDIM

      CHARACTER(LEN=*), INTENT(IN) :: FILE_NAME
      INTEGER :: ncid, fil_varid
      INTEGER, DIMENSION(NDIM) :: lvl_dimid, lvl_varid
      INTEGER, DIMENSION(ITOT) :: var_varid
      INTEGER, DIMENSION(1) :: DIMD1D, CNT1D
      INTEGER, DIMENSION(2) :: DIMD2D, CNT2D
      INTEGER, DIMENSION(3) :: DIMD3D, CNT3D
      INTEGER, DIMENSION(4) :: DIMD4D, CNT4D
!
!     * CREATE OUTPUT FILE.
!
      CALL CHECK (nf90_create(TRIM(FILE_NAME)//'.nc', &
                              nf90_clobber, ncid) )
!
!     * DEFINE THE DIMENSIONS.
!
      DO ND=1,NDIM
        IF ( DIM(ND)%FLGO == 1 ) THEN
          CALL CHECK( nf90_def_dim(ncid, TRIM(DIM(ND)%NAME), &
                                   DIM(ND)%SIZE, lvl_dimid(ND)) )
        ENDIF
      ENDDO
!
!     * DEFINE THE COORDINDATE VARIABLES.
!
      DO ND=1,NDIM
        IF ( DIM(ND)%FLGO == 1 ) THEN
          CALL CHECK( nf90_def_var(ncid, TRIM(DIM(ND)%NAME), &
                                   NF90_REAL, lvl_dimid(ND), &
                                   lvl_varid(ND)) )
        ENDIF
      ENDDO
!
!     * ASSIGN ATTRIBUTES TO COORDINATE VARIABLES.
!
      DO ND=1,NDIM
        IF ( DIM(ND)%FLGO == 1 ) THEN
          CALL CHECK( nf90_put_att(ncid, lvl_varid(ND), "units", &
                                   TRIM(DIM(ND)%UNITS)) )
          CALL CHECK( nf90_put_att(ncid, lvl_varid(ND), "long_name", &
                                    TRIM(DIM(ND)%LNAME)) )
        ENDIF
      ENDDO
!
!      DEFINE THE NETCDF VARIABLES FOR EACH FIELD.
!
      IV=0
      DO IT=1,ITOT
        ICHCK=0
        IF ( ABS(OVARV(IT)%FLD0D+YNAX) > YSMALLX ) THEN
          ICHCK=1
          IV=IV+1
          CALL CHECK( nf90_def_var(ncid,TRIM(OVAR(IT)%NAME), &
                                   NF90_REAL, var_varid(IV)) )
        ELSE IF ( ALLOCATED(OVARV(IT)%FLD1D) ) THEN
          ICHCK=1
          IV=IV+1
          IDIM=0
          DO IS=1,ISMX
            CTMP=OVAR(IT)%DIM(IS)
            IF ( CTMP /= CNA ) THEN
              IDIM=IDIM+1
              DO ND=1,NDIM
                IF ( TRIM(CTMP) == TRIM(DIM(ND)%NAME) ) THEN
                  DIMD1D(IDIM)=lvl_varid(ND)
                ENDIF
              ENDDO
            ENDIF
          ENDDO
          CALL CHECK( nf90_def_var(ncid,TRIM(OVAR(IT)%NAME), &
                                   NF90_REAL, DIMD1D, var_varid(IV)) )
        ELSE IF ( ALLOCATED(OVARV(IT)%FLD2D) ) THEN
          ICHCK=1
          IV=IV+1
          IDIM=0
          DO IS=1,ISMX
            CTMP=OVAR(IT)%DIM(IS)
            IF ( CTMP /= CNA ) THEN
              IDIM=IDIM+1
              DO ND=1,NDIM
                IF ( TRIM(CTMP) == TRIM(DIM(ND)%NAME) ) THEN
                  DIMD2D(IDIM)=lvl_varid(ND)
                ENDIF
              ENDDO
            ENDIF
          ENDDO
          CALL CHECK( nf90_def_var(ncid,TRIM(OVAR(IT)%NAME), &
                                   NF90_REAL, DIMD2D, var_varid(IV)) )
        ELSE IF ( ALLOCATED(OVARV(IT)%FLD3D) ) THEN
          ICHCK=1
          IV=IV+1
          IDIM=0
          DO IS=1,ISMX
            CTMP=OVAR(IT)%DIM(IS)
            IF ( CTMP /= CNA ) THEN
              IDIM=IDIM+1
              DO ND=1,NDIM
                IF ( TRIM(CTMP) == TRIM(DIM(ND)%NAME) ) THEN
                  DIMD3D(IDIM)=lvl_varid(ND)
                ENDIF
              ENDDO
            ENDIF
          ENDDO
          CALL CHECK( nf90_def_var(ncid,TRIM(OVAR(IT)%NAME), &
                                   NF90_REAL, DIMD3D, var_varid(IV)) )
        ELSE IF ( ALLOCATED(OVARV(IT)%FLD4D) ) THEN
          ICHCK=1
          IV=IV+1
          IDIM=0
          DO IS=1,ISMX
            CTMP=OVAR(IT)%DIM(IS)
            IF ( CTMP /= CNA ) THEN
              IDIM=IDIM+1
              DO ND=1,NDIM
                IF ( TRIM(CTMP) == TRIM(DIM(ND)%NAME) ) THEN
                  DIMD4D(IDIM)=lvl_varid(ND)
                ENDIF
              ENDDO
            ENDIF
          ENDDO
          CALL CHECK( nf90_def_var(ncid,TRIM(OVAR(IT)%NAME), &
                                   NF90_REAL, DIMD4D, var_varid(IV)) )
        ENDIF
!
!       * ASSIGN ATTRIBUTES TO THE VARIABLE.
!
        IF ( ICHCK == 1 ) THEN
          CALL CHECK( nf90_put_att(ncid, var_varid(IV), "units", &
                                   TRIM(OVAR(IT)%UNITS) ) )
          CALL CHECK( nf90_put_att(ncid, var_varid(IV), "long_name", &
                                   TRIM(OVAR(IT)%LNAME) ) )
          CALL CHECK( nf90_put_att(ncid, var_varid(IV), "_FillValue", &
                                   YNAX) )
        ENDIF
      ENDDO
!
!     * END DEFINE MODE.
!
      CALL CHECK( nf90_enddef(ncid) )
!
!     * WRITE THE LEVEL DATA.
!
      DO ND=1,NDIM
        IF ( DIM(ND)%FLGO == 1 ) THEN
          CALL CHECK( nf90_put_var(ncid, lvl_varid(ND), DIM(ND)%VAL) )
        ENDIF
      ENDDO
!
!     * WRITE DATA FOR EACH FIELD.
!
      IV=0
      DO IT=1,ITOT
        IF ( ABS(OVARV(IT)%FLD0D+YNAX) > YSMALLX ) THEN
          IV=IV+1
          CALL CHECK( nf90_put_var(ncid, var_varid(IV), &
                      OVARV(IT)%FLD0D) )
        ELSE IF ( ALLOCATED(OVARV(IT)%FLD1D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_put_var(ncid, var_varid(IV), &
                      OVARV(IT)%FLD1D) )
        ELSE IF ( ALLOCATED(OVARV(IT)%FLD2D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_put_var(ncid, var_varid(IV), &
                      OVARV(IT)%FLD2D) )
        ELSE IF ( ALLOCATED(OVARV(IT)%FLD3D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_put_var(ncid, var_varid(IV), &
                      OVARV(IT)%FLD3D) )
        ELSE IF ( ALLOCATED(OVARV(IT)%FLD4D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_put_var(ncid, var_varid(IV), &
                      OVARV(IT)%FLD4D) )
        ENDIF
      ENDDO
!
!     * CLOSE THE FILE.
!
      CALL CHECK( nf90_close(ncid) )
!
      CONTAINS
      SUBROUTINE CHECK(status)
      integer, intent (in) :: status

      if(status /= nf90_noerr) then
        print *, trim(nf90_strerror(status))
        stop 2
      end if
      END SUBROUTINE CHECK
      END SUBROUTINE WNCDAT
      SUBROUTINE RDNCDATRS(FILE_NAME)
!
      USE IODAT
      USE NETCDF
!
      IMPLICIT NONE
!
      INTEGER :: ND,IT,IV
      CHARACTER(LEN=*), INTENT(IN) :: FILE_NAME
      INTEGER :: ncid, fil_varid
      INTEGER, DIMENSION(NDIM) :: lvl_dimid, lvl_varid
      INTEGER, DIMENSION(ITOT) :: var_varid
!
      CALL CHECK( nf90_open(TRIM(FILE_NAME)//'.nc', &
                                        nf90_nowrite, ncid) )
!
!     * ALLOCATE MEMORY.
!
      ALLOCATE(DIMT(NDIM))
!
!     * READ THE DIMENSIONS.
!
      DO ND=1,NDIM
        IF ( DIM(ND)%FLGRS == 1 ) THEN
          CALL CHECK( nf90_inq_dimid(ncid, TRIM(DIM(ND)%NAME), &
                                     lvl_dimid(ND)) )
          CALL CHECK( nf90_inquire_dimension(ncid, lvl_dimid(ND), &
                                 len=DIMT(ND)%SIZE) )
          IF ( DIMT(ND)%SIZE /= DIM(ND)%SIZE ) THEN
            PRINT*,'CONFLICTING FIELD DIMENSION ',TRIM(DIM(ND)%NAME)
            CALL XIT('RDNCDATRS',-1)
          ENDIF
        ENDIF
      ENDDO
!
!      FIND AND READ FIELDS.
!
      IV=0
      DO IT=1,ITOT
        IF ( ABS(RVARV(IT)%FLD0D+YNAX) > YSMALLX ) THEN
          IV=IV+1
          CALL CHECK( nf90_inq_varid(ncid,TRIM(RVAR(IT)%NAME), &
                                     var_varid(IV)) )
          CALL CHECK( nf90_get_var(ncid, var_varid(IV), &
                                     RVARV(IT)%FLD0D) )
        ELSE IF ( ALLOCATED(RVARV(IT)%FLD1D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_inq_varid(ncid,TRIM(RVAR(IT)%NAME), &
                                     var_varid(IV)) )
          CALL CHECK( nf90_get_var(ncid, var_varid(IV), &
                                     RVARV(IT)%FLD1D) )
        ELSE IF ( ALLOCATED(RVARV(IT)%FLD2D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_inq_varid(ncid,TRIM(RVAR(IT)%NAME), &
                                     var_varid(IV)) )
          CALL CHECK( nf90_get_var(ncid, var_varid(IV), &
                                     RVARV(IT)%FLD2D) )
        ELSE IF ( ALLOCATED(RVARV(IT)%FLD3D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_inq_varid(ncid,TRIM(RVAR(IT)%NAME), &
                                     var_varid(IV)) )
          CALL CHECK( nf90_get_var(ncid, var_varid(IV), &
                                     RVARV(IT)%FLD3D) )
        ELSE IF ( ALLOCATED(RVARV(IT)%FLD4D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_inq_varid(ncid,TRIM(RVAR(IT)%NAME), &
                                     var_varid(IV)) )
          CALL CHECK( nf90_get_var(ncid, var_varid(IV), &
                                     RVARV(IT)%FLD4D) )
        ENDIF
      ENDDO
!
!     * DEALLOCATION.
!
      DEALLOCATE(DIMT)
!
      CONTAINS
      SUBROUTINE CHECK(status)
      integer, intent (in) :: status

      if(status /= nf90_noerr) then
        print *, trim(nf90_strerror(status))
        stop 2
      end if
      END SUBROUTINE CHECK
      END SUBROUTINE RDNCDATRS
      SUBROUTINE RDNCDAT(FILE_NAME)
!
      USE IODAT
      USE NETCDF
!
      IMPLICIT NONE
!
      INTEGER :: ND,IT,IV
      CHARACTER(LEN=*), INTENT(IN) :: FILE_NAME
      INTEGER :: ncid, fil_varid
      INTEGER, DIMENSION(NDIM) :: lvl_dimid, lvl_varid
      INTEGER, DIMENSION(ITOT) :: var_varid
!
      CALL CHECK( nf90_open(TRIM(FILE_NAME)//'.nc', &
                                        nf90_nowrite, ncid) )
!
!     * ALLOCATE MEMORY.
!
      ALLOCATE(DIMT(NDIM))
!
!     * READ THE DIMENSIONS.
!
      DO ND=1,NDIM
        IF ( DIM(ND)%FLGI == 1 ) THEN
          CALL CHECK( nf90_inq_dimid(ncid, TRIM(DIM(ND)%NAME), &
                                     lvl_dimid(ND)) )
          CALL CHECK( nf90_inquire_dimension(ncid, lvl_dimid(ND), &
                                 len=DIMT(ND)%SIZE) )
          IF ( DIMT(ND)%SIZE /= DIM(ND)%SIZE ) THEN
            PRINT*,'CONFLICTING FIELD DIMENSION ',TRIM(DIM(ND)%NAME)
            CALL XIT('RDNCDATRS',-2)
          ENDIF
        ENDIF
      ENDDO
!
!      FIND AND READ FIELDS.
!
      IV=0
      DO IT=1,ITOT
        IF ( ABS(IVARV(IT)%FLD0D+YNAX) > YSMALLX ) THEN
          IV=IV+1
          CALL CHECK( nf90_inq_varid(ncid,TRIM(IVAR(IT)%NAME), &
                                     var_varid(IV)) )
          CALL CHECK( nf90_get_var(ncid, var_varid(IV), &
                                     IVARV(IT)%FLD0D) )
        ELSE IF ( ALLOCATED(IVARV(IT)%FLD1D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_inq_varid(ncid,TRIM(IVAR(IT)%NAME), &
                                     var_varid(IV)) )
          CALL CHECK( nf90_get_var(ncid, var_varid(IV), &
                                     IVARV(IT)%FLD1D) )
        ELSE IF ( ALLOCATED(IVARV(IT)%FLD2D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_inq_varid(ncid,TRIM(IVAR(IT)%NAME), &
                                     var_varid(IV)) )
          CALL CHECK( nf90_get_var(ncid, var_varid(IV), &
                                     IVARV(IT)%FLD2D) )
        ELSE IF ( ALLOCATED(IVARV(IT)%FLD3D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_inq_varid(ncid,TRIM(IVAR(IT)%NAME), &
                                     var_varid(IV)) )
          CALL CHECK( nf90_get_var(ncid, var_varid(IV), &
                                     IVARV(IT)%FLD3D) )
        ELSE IF ( ALLOCATED(IVARV(IT)%FLD4D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_inq_varid(ncid,TRIM(IVAR(IT)%NAME), &
                                     var_varid(IV)) )
          CALL CHECK( nf90_get_var(ncid, var_varid(IV), &
                                     IVARV(IT)%FLD4D) )
        ENDIF
      ENDDO
!
!     * DEALLOCATION.
!
      DEALLOCATE(DIMT)
!
      CONTAINS
      SUBROUTINE CHECK(status)
      integer, intent (in) :: status

      if(status /= nf90_noerr) then
        print *, trim(nf90_strerror(status))
        stop 2
      end if
      END SUBROUTINE CHECK
      END SUBROUTINE RDNCDAT
      SUBROUTINE WNCDATRS(FILE_NAME)
!
      USE IODAT
      USE NETCDF
!
      IMPLICIT NONE
!
      INTEGER :: IS,IT,ND,IV,ICHCK,IDIM

      CHARACTER(LEN=*), INTENT(IN) :: FILE_NAME
      INTEGER :: ncid, fil_varid
      INTEGER, DIMENSION(NDIM) :: lvl_dimid, lvl_varid
      INTEGER, DIMENSION(ITOT) :: var_varid
      INTEGER, DIMENSION(1) :: DIMD1D, CNT1D
      INTEGER, DIMENSION(2) :: DIMD2D, CNT2D
      INTEGER, DIMENSION(3) :: DIMD3D, CNT3D
      INTEGER, DIMENSION(4) :: DIMD4D, CNT4D
!
!     * CREATE OUTPUT FILE.
!
      CALL CHECK (nf90_create(TRIM(FILE_NAME)//'.nc', &
                              nf90_clobber, ncid) )
!
!     * DEFINE THE DIMENSIONS.
!
      DO ND=1,NDIM
        IF ( DIM(ND)%FLGRS == 1 ) THEN
          CALL CHECK( nf90_def_dim(ncid, TRIM(DIM(ND)%NAME), &
                                   DIM(ND)%SIZE, lvl_dimid(ND)) )
        ENDIF
      ENDDO
!
!     * DEFINE THE COORDINDATE VARIABLES.
!
      DO ND=1,NDIM
        IF ( DIM(ND)%FLGRS == 1 ) THEN
          CALL CHECK( nf90_def_var(ncid, TRIM(DIM(ND)%NAME), &
                                   NF90_REAL, lvl_dimid(ND), &
                                   lvl_varid(ND)) )
        ENDIF
      ENDDO
!
!     * ASSIGN ATTRIBUTES TO COORDINATE VARIABLES.
!
      DO ND=1,NDIM
        IF ( DIM(ND)%FLGRS == 1 ) THEN
          CALL CHECK( nf90_put_att(ncid, lvl_varid(ND), "units", &
                                   TRIM(DIM(ND)%UNITS)) )
          CALL CHECK( nf90_put_att(ncid, lvl_varid(ND), "long_name", &
                                    TRIM(DIM(ND)%LNAME)) )
        ENDIF
      ENDDO
!
!      DEFINE THE NETCDF VARIABLES FOR EACH FIELD.
!
      IV=0
      DO IT=1,ITOT
        ICHCK=0
        IF ( ABS(RVARV(IT)%FLD0D+YNAX) > YSMALLX ) THEN
          ICHCK=1
          IV=IV+1
          CALL CHECK( nf90_def_var(ncid,TRIM(RVAR(IT)%NAME), &
                                   NF90_REAL, var_varid(IV)) )
        ELSE IF ( ALLOCATED(RVARV(IT)%FLD1D) ) THEN
          ICHCK=1
          IV=IV+1
          IDIM=0
          DO IS=1,ISMX
            CTMP=RVAR(IT)%DIM(IS)
            IF ( CTMP /= CNA ) THEN
              IDIM=IDIM+1
              DO ND=1,NDIM
                IF ( TRIM(CTMP) == TRIM(DIM(ND)%NAME) ) THEN
                  DIMD1D(IDIM)=lvl_varid(ND)
                ENDIF
              ENDDO
            ENDIF
          ENDDO
          CALL CHECK( nf90_def_var(ncid,TRIM(RVAR(IT)%NAME), &
                                   NF90_REAL, DIMD1D, var_varid(IV)) )
        ELSE IF ( ALLOCATED(RVARV(IT)%FLD2D) ) THEN
          ICHCK=1
          IV=IV+1
          IDIM=0
          DO IS=1,ISMX
            CTMP=RVAR(IT)%DIM(IS)
            IF ( CTMP /= CNA ) THEN
              IDIM=IDIM+1
              DO ND=1,NDIM
                IF ( TRIM(CTMP) == TRIM(DIM(ND)%NAME) ) THEN
                  DIMD2D(IDIM)=lvl_varid(ND)
                ENDIF
              ENDDO
            ENDIF
          ENDDO
          CALL CHECK( nf90_def_var(ncid,TRIM(RVAR(IT)%NAME), &
                                   NF90_REAL, DIMD2D, var_varid(IV)) )
        ELSE IF ( ALLOCATED(RVARV(IT)%FLD3D) ) THEN
          ICHCK=1
          IV=IV+1
          IDIM=0
          DO IS=1,ISMX
            CTMP=RVAR(IT)%DIM(IS)
            IF ( CTMP /= CNA ) THEN
              IDIM=IDIM+1
              DO ND=1,NDIM
                IF ( TRIM(CTMP) == TRIM(DIM(ND)%NAME) ) THEN
                  DIMD3D(IDIM)=lvl_varid(ND)
                ENDIF
              ENDDO
            ENDIF
          ENDDO
          CALL CHECK( nf90_def_var(ncid,TRIM(RVAR(IT)%NAME), &
                                   NF90_REAL, DIMD3D, var_varid(IV)) )
        ELSE IF ( ALLOCATED(RVARV(IT)%FLD4D) ) THEN
          ICHCK=1
          IV=IV+1
          IDIM=0
          DO IS=1,ISMX
            CTMP=RVAR(IT)%DIM(IS)
            IF ( CTMP /= CNA ) THEN
              IDIM=IDIM+1
              DO ND=1,NDIM
                IF ( TRIM(CTMP) == TRIM(DIM(ND)%NAME) ) THEN
                  DIMD4D(IDIM)=lvl_varid(ND)
                ENDIF
              ENDDO
            ENDIF
          ENDDO
          CALL CHECK( nf90_def_var(ncid,TRIM(RVAR(IT)%NAME), &
                                   NF90_REAL, DIMD4D, var_varid(IV)) )
        ENDIF
!
!       * ASSIGN ATTRIBUTES TO THE VARIABLE.
!
        IF ( ICHCK == 1 ) THEN
          CALL CHECK( nf90_put_att(ncid, var_varid(IV), "units", &
                                   TRIM(RVAR(IT)%UNITS) ) )
          CALL CHECK( nf90_put_att(ncid, var_varid(IV), "long_name", &
                                   TRIM(RVAR(IT)%LNAME) ) )
          CALL CHECK( nf90_put_att(ncid, var_varid(IV), "_FillValue", &
                                   YNAX) )
        ENDIF
      ENDDO
!
!     * END DEFINE MODE.
!
      CALL CHECK( nf90_enddef(ncid) )
!
!     * WRITE THE LEVEL DATA.
!
      DO ND=1,NDIM
        IF ( DIM(ND)%FLGRS == 1 ) THEN
          CALL CHECK( nf90_put_var(ncid, lvl_varid(ND), DIM(ND)%VAL) )
        ENDIF
      ENDDO
!
!     * WRITE DATA FOR EACH FIELD.
!
      IV=0
      DO IT=1,ITOT
        IF ( ABS(RVARV(IT)%FLD0D+YNAX) > YSMALLX ) THEN
          IV=IV+1
          CALL CHECK( nf90_put_var(ncid, var_varid(IV), &
                      RVARV(IT)%FLD0D) )
        ELSE IF ( ALLOCATED(RVARV(IT)%FLD1D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_put_var(ncid, var_varid(IV), &
                      RVARV(IT)%FLD1D) )
        ELSE IF ( ALLOCATED(RVARV(IT)%FLD2D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_put_var(ncid, var_varid(IV), &
                      RVARV(IT)%FLD2D) )
        ELSE IF ( ALLOCATED(RVARV(IT)%FLD3D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_put_var(ncid, var_varid(IV), &
                      RVARV(IT)%FLD3D) )
        ELSE IF ( ALLOCATED(RVARV(IT)%FLD4D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_put_var(ncid, var_varid(IV), &
                      RVARV(IT)%FLD4D) )
        ENDIF
      ENDDO
!
!     * CLOSE THE FILE.
!
      CALL CHECK( nf90_close(ncid) )
!
      CONTAINS
      SUBROUTINE CHECK(status)
      integer, intent (in) :: status

      if(status /= nf90_noerr) then
        print *, trim(nf90_strerror(status))
        stop 2
      end if
      END SUBROUTINE CHECK
      END SUBROUTINE WNCDATRS
      SUBROUTINE CLDPHYS(ZCLF,ZRFLN,CLRFR,CLRFS,ZFRAIN,ZFSNOW,ZFEVAP, &
                         ZFSUBL,ZMLWC,ZMRATEP,RH,RHC,HMN,QT,QR,QLR, &
                         ZCDNC,QCWA,TH,PH,ZH,DP,ALMC,ALMX,DT,ILG,ILEV, &
                         ICASE)
!
      USE SDPHYS
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL, INTENT(INOUT), DIMENSION(ILG,ILEV) :: QR, QLR, TH, ZCLF
      REAL, INTENT(IN), DIMENSION(ILG,ILEV) :: PH, ZH, ALMC, ALMX, &
                                               DP, ZCDNC
      REAL, INTENT(OUT), DIMENSION(ILG,ILEV) :: HMN, QT, RH, RHC, &
                                                ZMLWC, CLRFR, CLRFS, &
                                                ZFRAIN, ZFSNOW, ZFEVAP, &
                                                ZFSUBL, ZMRATEP, QCWA
      REAL, INTENT(OUT), DIMENSION(ILG) :: ZRFLN
      REAL, DIMENSION(ILG,ILEV) :: QTN, HMNN, CVAR, ZCRAUT, QCWVAR, &
                                   CVSG, CVDU, ZFRAC, QC
      REAL, DIMENSION(ILG) :: PMBS, RRL, CPM, ALMIX, DHLDZ, DRWDZ, &
                              FLAGEST, QCW, SSH
      REAL, DIMENSION(ILG) :: ZCLFRM, ZCLFR, ZCLRR, ZRNFT, &
                              ZRCLR, ZCLFSM, ZCLFS, ZSFLN, ZCLRS, &
                              ZSNFT, ZSCLR, VTR, ZRPR, ZEVP, ZXLB
      INTEGER, PARAMETER :: ITER=10
      INTEGER, PARAMETER :: ISPCDNC=0  ! SWITCH TO SPECIFIED CDNC
      REAL, PARAMETER :: SPCDNC=300.E+06
!
      ESW(TTT) = EXP(RW1+RW2/TTT)*TTT**RW3

!
!     * ADIABATIC CORE.
!
!     * TOTAL WATER MIXING RATIO AND CLOUD WATER STATIC ENERGY.
!
      DO L=1,ILEV
        QTN(:,L)=QR(:,ILEV)+QLR(:,ILEV)
        HMNN(:,L)=CPRES*TH(:,ILEV)+GRAV*ZH(:,ILEV)-RL*QLR(:,ILEV)
      ENDDO
      FACTE=1./EPS1-1.
      I2NDIE=1
      FLAGEST=1.
      CVSG=0.
      CVDU=0.
      ZFRAC=0.
      ICVSG=1
      ISUBG=0
      ICALL=1
      DO JK=1,ILEV
        DO IL=1,ILG
          PMBS(IL)=0.01*PH(IL,JK)
          RRL(IL)=RL
          CPM(IL)=CPRES
          ALMIX(IL)=MAX(ALMC(IL,JK),ALMX(IL,JK),10.)
          IF(JK.GT.1)                              THEN
            DZ       =ZH   (IL,JK-1)-ZH  (IL,JK)
            DHLDZ(IL)=(HMNN(IL,JK-1)-HMNN(IL,JK))/DZ
            DRWDZ(IL)=(QTN (IL,JK-1)-QTN (IL,JK))/DZ
          ELSE
            DHLDZ(IL)=0.
            DRWDZ(IL)=0.
          ENDIF
        ENDDO
        CALL STATCLD (QCW,ZCLF(1,JK),CVAR(1,JK),ZCRAUT(1,JK), &
                      QCWVAR(1,JK),SSH,CVSG(1,JK),QTN(1,JK), &
                      HMNN(1,JK),ZFRAC(1,JK),CPM, &
                      PMBS,ZH(1,JK),RRL,CVDU(1,JK),FLAGEST, &
                      ALMIX,DHLDZ,DRWDZ,I2NDIE, &
                      DT,ILEV,ILG,1,ILG,ICVSG,ISUBG, &
                      JK,ICALL)
!
        DO IL=1,ILG
          QCWA(IL,JK)=QCW(IL)
        ENDDO
      ENDDO
!
!     * TOTAL WATER MIXING RATIO AND CLOUD WATER STATIC ENERGY.
!
      QTN=QR+QLR
      HMNN=CPRES*TH+GRAV*ZH-RL*QLR
!
!     * CLOUD FRACTION, EVAPORATION, AND CONDENSATION FROM STATISTICAL
!     * CLOUD SCHEME.
!
      FACTE=1./EPS1-1.
      I2NDIE=1
      FLAGEST=1.
      CVSG=0.
      CVDU=0.
      ZFRAC=0.
      ICVSG=1
      ISUBG=0
      ICALL=1
      DO JK=1,ILEV
        DO IL=1,ILG
          PMBS(IL)=0.01*PH(IL,JK)
          RRL(IL)=RL
          CPM(IL)=CPRES
          ALMIX(IL)=MAX(ALMC(IL,JK),ALMX(IL,JK),10.)
          IF(JK.GT.1)                              THEN
            DZ       =ZH   (IL,JK-1)-ZH  (IL,JK)
            DHLDZ(IL)=(HMNN(IL,JK-1)-HMNN(IL,JK))/DZ
            DRWDZ(IL)=(QTN (IL,JK-1)-QTN (IL,JK))/DZ
          ELSE
            DHLDZ(IL)=0.
            DRWDZ(IL)=0.
          ENDIF
        ENDDO
        CALL STATCLD (QCW,ZCLF(1,JK),CVAR(1,JK),ZCRAUT(1,JK), &
                      QCWVAR(1,JK),SSH,CVSG(1,JK),QTN(1,JK), &
                      HMNN(1,JK),ZFRAC(1,JK),CPM, &
                      PMBS,ZH(1,JK),RRL,CVDU(1,JK),FLAGEST, &
                      ALMIX,DHLDZ,DRWDZ,I2NDIE, &
                      DT,ILEV,ILG,1,ILG,ICVSG,ISUBG, &
                      JK,ICALL)
!
        DO IL=1,ILG
          QCWI=ZFRAC(IL,JK)*QCW(IL)
          QCWL=(1.-ZFRAC(IL,JK))*QCW(IL)
!
!         * CONDENSATION AND EVAPORATION.
!
          ZCND=QCW(IL)-QLR(IL,JK)
          ZCND=MAX(ZCND,-QLR(IL,JK))
          ASCL=1.
          IF ( ZCND.GT.0. ) THEN
            ASCL=MIN(MAX((QR(IL,JK)-1.E-20)/ZCND,0.),1.)
          ENDIF
          ZCND=ASCL*ZCND
!
!         * UPDATE TEMPERATURE, VAPOUR, AND CONDENSATE.
!
          TH(IL,JK)=TH(IL,JK)+ZCND*RL/CPRES
          QR(IL,JK)=QR(IL,JK)-ZCND
          QLR(IL,JK)=QLR(IL,JK)+ZCND
!
!         * RELATIVE HUMIDITY FOR ALL- AND CLEAR-SKY. THE RELATIVE
!         * HUMIDITY IS CALCULATED BASED ON THE VAPOUR PRESSURE IF THE
!         * SATURATION VAPOUR PRESSURE EXCEEDS THE TOTAL AIR PRESSURE,
!         * I.E. FOR CONDITIONS THAT DO NOT PERMIT FORMATION OF CLOUDS.
!         * OTHERWISE, THE RELATIVE HUMIDITY IS CALCULATED BASED ON
!         * SPECIFIC HUMIDITY (SEE CHAPTER 4 IN EMANUEL'S TEXTBOOK ON
!         * CONVECTION) IN ORDER TO PERMIT CALCULATIONS FOR CLEAR-SKY.
!
          IF(ZCLF(IL,JK).LT..99) THEN
            QC (IL,JK)=(QR(IL,JK)-ZCLF(IL,JK)*SSH(IL))/(1.-ZCLF(IL,JK))
            RHC(IL,JK)=MIN(MAX((QC(IL,JK)*(1.+SSH(IL)*FACTE)) &
                              /(SSH(IL)*(1.+QC(IL,JK)*FACTE)),0.),1.)
            RH (IL,JK)=ZCLF(IL,JK)+(1.-ZCLF(IL,JK))*RHC(IL,JK)
          ELSE
            QC (IL,JK)=QR(IL,JK)
            RH (IL,JK)=1.
            RHC(IL,JK)=RH(IL,JK)
          ENDIF
!
!         * DIAGNOSE LIQUID WATER STATIC ENERGY AND TOTAL WATER
!         * IN THE CLEAR-SKY ENVIRONMENT OF THE CLOUD.
!
          QT(IL,JK)=QC(IL,JK)
          HMN(IL,JK)=CPRES*TH(IL,JK)+GRAV*ZH(IL,JK)
        ENDDO
      ENDDO
!
!     * CONSTANTS AND INTIALIZATIONS.
!
      ZSEC=1.E-9
      ZEPCLC=1.E-2
      API=3.1415926535897
      COAUT=3.
      COACC=1.15
      COTMP1=COAUT-2.
      COTMP2=COACC-1.
      ZLVDCP=RL/CPRES
      AKR=141.4
      ZKA=0.024
      ZRFLNL_MAX=0.028
      FACACC=1.
      ZCONS2=1./(DT*GRAV)
      RGASV=461.50
      EPSLIM=0.001
      ZXSEC=1.-1.E-12
      ZMRATEP=0.
      ZMLWC=0.
      CLRFR=0.
      CLRFS=0.
      ZFRAIN=0.
      ZFSNOW=0.
      ZFEVAP=0.
      ZFSUBL=0.
      ZCLFR=0.
      ZRFLN=0.
      ZCLFS=0.
      ZSFLN=0.
      DO JK=1,ILEV
      ZSR=0.
      DO IL=1,ILG
!
!     * RAIN AREA FRACTION FROM MAXIMUM/RANDOM CLOUD OVERLAP ASSUMPTION.
!     * ONLY CLOUDS THAT ARE SUBJECT TO WARM RAIN PROCESSES ARE
!     * CONSIDERED.
!
       ZCLFRM(IL)=ZCLFR(IL)
       IF ( ZRFLN(IL) .GT. 1.E-20 ) THEN
         ZCLFR(IL)=ZCLF(IL,JK)
       ELSE
         ZCLFR(IL)=0.
       ENDIF
       IF ( ZCLFRM(IL).LT.1. ) THEN
         ZCLRR(IL)=ZCLRR(IL)*(1.-MAX(ZCLFR(IL),ZCLFRM(IL))) &
                            /(1.-ZCLFRM(IL))
       ENDIF
       ZRNFT(IL)=1.-ZCLRR(IL)
!
!      * FRACTION OF GRID CELL FOR WHICH CLEAR-SKY IS AFFECTED BY RAIN.
!
       ZRCLR(IL)=MIN(MAX(ZRNFT(IL)-ZCLFR(IL),0.),1.)
!
!     * SNOW AREA FRACTION FROM MAXIMUM/RANDOM CLOUD OVERLAP ASSUMPTION.
!     * ONLY CLOUDS THAT ARE AFFECTED BY SNOW ARE CONSIDERED.
!
       ZCLFSM(IL)=ZCLFS(IL)
       IF ( ZSFLN(IL) .GT. 1.E-20 ) THEN
         ZCLFS(IL)=ZCLF(IL,JK)
       ELSE
         ZCLFS(IL)=0.
       ENDIF
       IF ( ZCLFSM(IL).LT.1. ) THEN
         ZCLRS(IL)=ZCLRS(IL)*(1.-MAX(ZCLFS(IL),ZCLFSM(IL))) &
                            /(1.-ZCLFSM(IL))
       ENDIF
       ZSNFT(IL)=1.-ZCLRS(IL)
!
!      * FRACTION OF GRID CELL FOR WHICH CLEAR-SKY IS AFFECTED BY SNOW.
!
       ZSCLR(IL)=MIN(MAX(ZSNFT(IL)-ZCLFS(IL),0.),1.)
!
!      * RAIN WATER MIXING RATIO FROM RAINFALL RATE (FROM ROTSTAYN)
!
       ZRHO0=PH(IL,JK)/(RGAS*TH(IL,JK))
       ZQRHO0=1.3/ZRHO0
       ZXRP1=0.
       VTR(IL)=0.
       IF ( ZRNFT(IL).GT.ZEPCLC ) THEN
         ZRFLNL=ZRFLN(IL)/ZRNFT(IL)
         ZRFLNL=MIN(ZRFLNL,ZRFLNL_MAX)
         IF ( ZRFLNL.GT.1.E-20 ) THEN
           ALAMBR=714.*(1.2/ZRHO0)**(1./9.)*ZRFLNL**(-0.22)
           VTR(IL)=1.94*AKR*SQRT(1.2/(ZRHO0*ALAMBR))
           ZXRP1=ZRFLNL/(ZRHO0*VTR(IL))
         ENDIF
       ENDIF
!
!      * AUTOCONVERSION AND ACCRETION. 4 DIFFERENT AUTOCONVERSION
!      * PARAMETERIZATIONS ARE AVAILABLE.
!
       ZXLB(IL)=QLR(IL,JK)
       IF( ZCLF(IL,JK).GT.ZEPCLC .AND. ZXLB(IL).GT.ZSEC ) THEN
         ZXLB(IL)=QLR(IL,JK)/ZCLF(IL,JK)
       ELSE
         ZXLB(IL)=QLR(IL,JK)
       ENDIF
       IF ( ISPCDNC == 1 ) THEN
         CDNC=SPCDNC
       ELSE
         CDNC=ZCDNC(IL,JK)
       ENDIF
       IF ( ZXLB(IL)*ZCLF(IL,JK).GE.ZSEC ) THEN
         IF ( ICASE <= 3 ) THEN
!
!          * PARAMETERIZATION BY WOOD (2005)
!
           IF ( ICASE .EQ. 1 ) THEN
!            * WITH CONSTANT CLOUD DROPLET NUMBER.
!
             CDNR=120.E+06
             CDNP=0.
             FACAUTN=1.3E+09
           ELSE IF ( ICASE .EQ. 2 ) THEN
!            * ORIGINAL WOOD PARAMETERIZATION.
!
             CDNR=0.
             CDNP=1.
             FACAUTN=1.3E+09
           ELSE IF ( ICASE .EQ. 3 ) THEN
!            * WITH PARAMETERS SPECIFIED ACCORDING TO LIU AND DAUM (2004).
!
             CDNR=0.
             CDNP=1.
             FACAUTN=1.08E+10
           ENDIF
           CDNE=(CDNR**(1.-CDNP))*CDNC**CDNP
           RVOL=1.E+06*(3.*ZXLB(IL)/(4.*API*RHOH2O*CDNE))**(1./3.)
           BETA6=((RVOL+3.)/RVOL)**(1./3.)
           R6=BETA6*RVOL
           R6C=7.5/(ZXLB(IL)**(1./6.)*SQRT(R6))
           EP=FACAUTN*(BETA6**6)
           IF ( R6 >= R6C ) THEN
             FAAUT=ZCRAUT(IL,JK)*EP/CDNE
           ELSE
             FAAUT=0.
           ENDIF
         ELSE IF ( ICASE .EQ. 4 ) THEN
!          * KHAIROUTDINOV AND KOGAN (2000)
!
           COAUT=2.47
           COTMP1=COAUT-2.
           FACAUT=2.5
           FAAUT=FACAUT*ZCRAUT(IL,JK)*1350.
           FAAUT=FAAUT*(CDNC/1.E+06)**(-1.79)
         ELSE
           CALL XIT('CLDPHYS',-1)
         ENDIF
!
!        * INITIAL GUESS AND SUBSEQUENT ITERATIONS.
!
         ZQEST1=(ZXLB(IL)**(1.-COAUT) &
               -DT*FAAUT*(1.-COAUT))**(1./(1.-COAUT))
         ZQEST=MIN(MAX(ZQEST1,ZSEC),ZXLB(IL))
         FAACC=0.
         IF ( ZXRP1.GT.ZSEC ) THEN
           FAACC=FACACC*67.*ZXRP1**COACC
           ZQEST2=(ZXLB(IL)**(1.-COACC) &
                 -DT*FAACC*(1.-COACC))**(1./(1.-COACC))
           ZQEST=MAX(MIN(ZQEST,ZQEST2),ZSEC)
           APK=-LOG(ZQEST/ZXLB(IL))/DT
           IF ( ZQEST.LT.ZXLB(IL) .AND. APK.GT.0. ) THEN
             ALPHA=((1.-ZQEST/ZXLB(IL))*ZXLB(IL)/(APK*DT)-ZQEST) &
                  /(ZXLB(IL)-ZQEST)
             ALPHA=MIN(MAX(ALPHA,0.),1.)
             DO IT=1,ITER
               AQHAT =ALPHA*ZXLB(IL)+(1.-ALPHA)*ZQEST
               AQHAT1=AQHAT**COTMP1
               AQHAT2=AQHAT**COTMP2
               ATERM1=FAAUT*AQHAT1*ZXLB(IL)+FAACC*AQHAT2
               ATERM2=FAAUT*AQHAT1*ZQEST   +FAACC*AQHAT2
               AEXPF =EXP(-FAACC*AQHAT2*DT)
               AFUN=ATERM1*ZQEST-AEXPF*ATERM2*ZXLB(IL)
               DQHDQ=1.-ALPHA
               ATERM3=FAAUT*COTMP1*AQHAT1*DQHDQ/AQHAT
               ATERM4=FAACC*COTMP2*AQHAT2*DQHDQ/AQHAT
               ADFUN=ATERM1+ZQEST*(ATERM3*ZXLB(IL)+ATERM4) &
                    +AEXPF*ZXLB(IL)*(DT*ATERM4*ATERM2 &
                                -ATERM3*ZQEST+FAAUT*AQHAT1+ATERM4)
               ZQEST=MAX(ZQEST-AFUN/ADFUN,ZSEC)
             ENDDO
           ENDIF
         ENDIF
         ZRPR(IL)=MIN(ZCLF(IL,JK)*ZXLB(IL), &
                      MAX(-ZCLF(IL,JK)*(ZQEST-ZXLB(IL)),0.))
       ELSE
         ZRPR(IL)=ZCLF(IL,JK)*ZXLB(IL)
       ENDIF
       IF(ZCLF(IL,JK).GT.ZEPCLC) THEN
         ZMRATEP(IL,JK)=ZRPR(IL)/ZCLF(IL,JK)
         ZMLWC(IL,JK)=ZXLB(IL)
       ENDIF
       ZZDRR=MAX(0.,ZCONS2*DP(IL,JK)*ZRPR(IL))
       ZRFLN(IL)=ZRFLN(IL)+ZZDRR
       ZSR=ZSR+ZRFLN(IL)
!
!      * EVAPORATION OF RAIN BELOW CLOUDS (FROM LOHMANN).
!
       ZEVP(IL)=0.
       IF ( ZSR .GT. 0. .AND. ZRFLN(IL).GT.0. .AND. ZRCLR(IL).GT.0. &
          ) THEN
         ZRFLNL=ZRFLN(IL)/ZRNFT(IL)
         ZRFLNL=MIN(ZRFLNL,ZRFLNL_MAX)
         RHODZ=DP(IL,JK)/GRAV
         ETMP=ESW(TH(IL,JK))
         ESTREF=0.01*PH(IL,JK)*(1.-EPSLIM)/(1.-EPSLIM*EPS2)
         IF ( ETMP.LT.ESTREF ) THEN
           ESAT=ETMP
         ELSE
           ESAT=ESTREF
         ENDIF
         QSW=EPS1*ESAT/(0.01*PH(IL,JK)-EPS2*ESAT)
         RHCW=MIN(MAX((QC(IL,JK)*(1.+QSW*FACTE)) &
                     /(QSW*(1.+QC(IL,JK)*FACTE)),0.),1.)
         SUSATW=RHCW-1.
         ZDV=2.21/PH(IL,JK)
         ZLV=ZLVDCP*CPRES
         ZAST=ZLV*(ZLV/(RGASV*TH(IL,JK))-1.)/(ZKA*TH(IL,JK))
         ZBST=RGASV*TH(IL,JK)/(ZDV*ESAT)
         ZZEPR1=870.*SUSATW*ZRFLNL**0.61/(SQRT(ZRHO0)*(ZAST+ZBST))
         ZZEPR=ZZEPR1*RHODZ
         ZPLIM=-ZXSEC*ZRFLN(IL)/ZRNFT(IL)
         ITOTEVP=0
         IF ( ZZEPR .LT. ZPLIM ) THEN
           ZZEPR=ZPLIM
           ITOTEVP=1
         ENDIF
         ZEVP(IL)=-ZZEPR*DT*ZRCLR(IL)/RHODZ
         ZEVP(IL)=MIN(ZEVP(IL), &
                 MAX(ZXSEC*((1.-RHCW)*QC(IL,JK))*ZRCLR(IL),0.))
         ZEVP(IL)=MAX(ZEVP(IL),0.)
         ZZEPR=-ZEVP(IL)*RHODZ/DT
         ZFRAIN(IL,JK)=ZFRAIN(IL,JK)+ZRFLN(IL)
         CLRFR(IL,JK)=ZRCLR(IL)
         IF ( ITOTEVP .EQ. 1 ) THEN
            ZFEVAP(IL,JK)=MIN(MAX(0.,-ZZEPR/ZRFLN(IL)),1.)
         ENDIF
         ZRFLN(IL)=ZRFLN(IL)+ZZEPR
       ENDIF
!
!      * TENDENCIES.
!
       TH(IL,JK)=TH(IL,JK)-ZEVP(IL)*RL/CPRES
       QR(IL,JK)=QR(IL,JK)+ZEVP(IL)
       QLR(IL,JK)=QLR(IL,JK)-ZRPR(IL)
      ENDDO
      ENDDO
!
      END SUBROUTINE CLDPHYS
      SUBROUTINE STATCLD (QCW,ZCLF,SIGMA,ZCRAUT,QCWVAR,SSH,CVSG, &
                          QT,HMN,ZFRAC,CPM,P,Z,RRL,CONS,CRH, &
                          ALMIX,DHLDZ,DRWDZ,I2NDIE, &
                          ZTMST,ILEV,ILG,IL1,IL2,ICVSG,ISUBG, &
                          LVL,ICALL)
!
!     * AUG 16/2013 - K.VONSALZEN. Cosmetic change to abort condition
!     *                            where now is QCW.GT.QT instead of
!     *                            QCW.GE.QT. This does not change
!     *                            the answer but prevents infrequent
!     *                            crashes with small downward
!     *                            moisture fluxes causing very small
!     *                            negative total water.
!     * JUL 31/2013 - M.LAZARE.    Cosmetic change to pass in LVL
!     *                            and ICALL to aid in future debugging.
!     * JUN 25/2013 - K.VONSALZEN. New version for gcm17:
!     *                            - Second indirect effect parameter
!     *                              I2NDIE passed in and used.
!     *                            - Function ZCR replaces look-up
!     *                              table ZCR to calculate autoconversion
!     *                              efficiency.
!     * APR 29/2012 - K.VONSALZEN. PREVIOUS VERSION STATCLD4 FOR GCM16:
!     *                            ELIMINATE CLOUDS AND CONDENSATE IF
!     *                            CLOUD FRACTION OR CONDENSATE MIXING
!     *                            RATIO TOO LOW TO ENSURE CONSISTENT
!     *                            RESULTS FOR CLOUD FRACTION AND CLOUD
!     *                            CONDENSATE.
!     * MAR 25/2009 - L.SOLHEIM.   REVISED COSMETIC CHANGE:
!     *                            - USE REAL*4 OR REAL*8 ARGUMENT TO
!     *                              "ERF", DEPENDING ON THE VALUE OF
!     *                              "MACHINE", SO WILL WORK SEAMLESSLY ON
!     *                              PGI COMPILER AS WELL
!     * DEC 19/2007 - K.VONSALZEN/ PREVIOUS VERSION STATCLD3 FOR GCM15G/H/I:
!     *               M.LAZARE.    - ELIMINATE UNPHYSICAL RESULTS
!     *                              BY NOT PERMITTING THE INFERRED
!     *                              WATER VAPOUR MIXING RATIO (QCW)
!     *                              TO EXCEED A THRESHOLD (DEFINED
!     *                              BY QCWMAX).
!     *                            - PASSES IN ADELT FROM PHYSICS
!     *                              AS ZTMST INSTEAD OF ZTMST=2.*DELT.
!     * JAN 13/2007 - K.VONSALZEN. CALCULATE NORMALIZED CLOUD WATER
!     *                            VARIANCE TO BE USED IN CLOUD
!     *                            INHOMOGENEITY (QCWVAR).
!     * JUN 19/2006 - M.LAZARE.  NEW VERSION FOR GCM15F:
!     *                          - COSMETIC: USE VARIABLE INSTEAD OF
!     *                            CONSTANT IN INTRINSICS SUCH AS "MAX",
!     *                            SO THAT CAN COMPILE IN 32-BIT MODE
!     *                            WITH REAL*8.
!     * MAY 06/2006 - M.LAZARE/  PREVIOUS VERSION STATCLD FOR GCM15E.
!     *               K.VONSALZEN:

!
!     * THIS SUBROUTINE PERFORMS THE BULK OF THE STATISTICAL CLOUD
!     * SCHEME BASED ON CHABOUREAU AND BECHTHOLD (C&B). IT TAKES AS
!     * INPUT LIQUID STATIC ENERGY AND TOTAL WATER, AND OUTPUTS
!     * CONDENSED WATER/ICE, CLOUD FRACTION, THE VARIANCE OF MELLORS
!     * VARIABLE ("S"), THE EFFICIENCY FACTOR FOR AUTOCONVERSION
!     * FROM CLOUD WATER TO RAIN WATER, AND THE SATURATION SPECIFIC
!     * HUMIDITY.
!
!**** ***************************************************************
!     * DICTIONARY OF VARIABLES:
!
!     * OUTPUT:
!     * ------
!
!     * QCW:     TOTAL CONDENSED WATER/ICE (BOTH PHASES) IN KG/KG.
!     * ZCLF:    CLOUD FRACTION (DIMENSIONLESS)
!     * SIGMA:   VARIANCE OF MELLOR'S VARIABLE S IN KG/KG
!     * ZCRAUT:  EFFICIENCY OF AUTOCONVERSION FROM CLOUD WATER TO
!     *          RAIN WATER.
!     * QCWVAR:  NORMALIZED CLOUD WATER VARIANCE (<W'**2>/<W>**2)
!     *          (DIMENSIONLESS).
!     * SSH:     SATURATION SPECIFIC HUMIDITY IN KG/KG
!     * CVSG:    CONTRIBUTION TO TOTAL VARIANCE OF S FROM CONVECTIVE
!     *          PROCESSES IN KG/KG (ONLY IF SWITCH ICVSG=1,
!     *          OTHERWISE IT IS INPUT!).
!
!     * INPUT:
!     * -----
!
!     * QT:     TOTAL WATER (BOTH PHASES) IN KG/KG.
!     * HMN:    TOTAL LIQUID STATIC ENERGY IN JOULES/KG.
!     * ZFRAC:  FRACTION OF ICE PHASE.
!     * CPM:    SPECIFIC HEAT IN JOULES/(KG-DEGK).
!     * P:      MID-LAYER PRESSURE IN MBS.
!     * Z:      MID-LAYER HEIGHT ABOVE GROUND IN METRES.
!     * PBLT:   LAYER INDEX OF TOP OF PBL.
!     * RRL:    WATER/ICE WEIGHTED LATENT HEAT IN JOULES/KG.
!     * CONS:   FIELD CONTRIBUTING TO VARIANCE FROM CONVECTION.
!     * CRH:    FLAG (0/1) TO CONTROL CALCULATION.
!     * ALMIX:  MIXING LENGTH FOR C&B PARAMETERIZATION IN METRES.
!     * DHLDZ:  TOTAL WATER VERTICAL GRADIENT IN (M-1).
!     * DRWDZ:  LIQUID STATIC ENERGY GRADIENT IN JOULES/(KG-M). &
!**** ***************************************************************
!
      USE SDPHYS
      USE FPDEF
!
      IMPLICIT REAL   (A-H,O-Z), &
      INTEGER (I-N)
!
      REAL  , DIMENSION(ILG), INTENT(OUT) :: QCW,ZCLF,SIGMA,ZCRAUT, &
                                             QCWVAR,SSH,CVSG
      REAL  , DIMENSION(ILG), INTENT(IN)  :: HMN,QT,P,Z,CPM,ZFRAC, &
                                             CONS,CRH,RRL,ALMIX,DHLDZ, &
                                             DRWDZ
!
      REAL*4 :: R4VAL
      REAL*8 :: R8VAL
      INTEGER :: MACHINE
!
      ESW(TTT)    = EXP(RW1+RW2/TTT)*TTT**RW3
      ESI(TTT)    = EXP(RI1+RI2/TTT)*TTT**RI3
!
      DATA ZERO,ONE /0., 1./
!==============================================================================================
!     * PARAMETERS.
!
      ZEPCLC=1.E-2
      ZSEC=1.E-9
      CSIGMA=0.2
      YFR=0.50
      SQRT2=SQRT(2.)
      PI=3.141592653589793
      SQRT2PI=SQRT(2.*PI)
      Q1MIN1=-3.
      Q1MIN2=TAN((ZEPCLC-0.5)/0.36)/1.55
      Q1MIN=MAX(Q1MIN1,Q1MIN2)
      Q1MINM=Q1MIN/2.
      Q1MAX=2.
      Q1MAXM=Q1MAX/2.
      EXPM1=EXP(-1.)
      QDELT=2./30.
      TAUSIG=21600.
      SIGFAC=EXP(-ZTMST/TAUSIG)
      EPS2=1.-EPS1
      MACHINE=1
!
!     * INITIALIZATION.
!
      ILBAD=0
      DO IL=IL1,IL2
        QCW   (IL)=0.
        ZCLF  (IL)=0.
        ZCRAUT(IL)=1.
        SIGMA (IL)=0.
        SSH   (IL)=0.
      ENDDO
!
      DO IL=IL1,IL2
        IF ( CRH(IL).NE.0. ) THEN
!
!         * TEMPERATURE UNDER CLEAR-SKY CONDITIONS AS FIRST GUESS.
!
          TX=(HMN(IL)-GRAV*Z(IL))/CPM(IL)
          TL=TX
!
!         * WATER VAPOUR SATURATION MIXING RATIO UNDER CLEAR-SKY CONDITIONS.
!
          EST=(1.-ZFRAC(IL))*ESW(TX)+ZFRAC(IL)*ESI(TX)
          RSS=EPS1*EST/( P(IL)-EPS2*EST )
          DRSSDT=EPS1*P(IL) &
                   *((1.-ZFRAC(IL))*ESW(TX)*(RW3-RW2/TX) &
                        +ZFRAC(IL) *ESI(TX)*(RI3-RI2/TX)) &
                    /(TX*(P(IL)-EPS2*EST)**2)
          RSSTL=RSS
          DRSSDTL=DRSSDT
!
!         * ACCOUNT FOR CLOUD WATER/ICE IF SATURATION. USE
!         * SERIES REVERSION AND ITERATE.
!
          IF ( RSS.LT.QT(IL) ) THEN
!
!           * FIRST ITERATION.
!
            HMNPR=CPM(IL)*TX+GRAV*Z(IL)-HMN(IL)
            DHMNPR=CPM(IL)
            RC=QT(IL)-RSS
            HMNPR=HMNPR-RRL(IL)*RC
            DHMNPR=DHMNPR+RRL(IL)*DRSSDT
            TX=TX-HMNPR/DHMNPR
            EST=(1.-ZFRAC(IL))*ESW(TX)+ZFRAC(IL)*ESI(TX)
            RSS=EPS1*EST/( P(IL)-EPS2*EST )
            DRSSDT=EPS1*P(IL) &
                   *((1.-ZFRAC(IL))*ESW(TX)*(RW3-RW2/TX) &
                        +ZFRAC(IL) *ESI(TX)*(RI3-RI2/TX)) &
                    /(TX*(P(IL)-EPS2*EST)**2)
!
!           * SECOND ITERATION.
!
            HMNPR=CPM(IL)*TX+GRAV*Z(IL)-HMN(IL)
            DHMNPR=CPM(IL)
            RC=QT(IL)-RSS
            HMNPR=HMNPR-RRL(IL)*RC
            DHMNPR=DHMNPR+RRL(IL)*DRSSDT
            TX=TX-HMNPR/DHMNPR
            EST=(1.-ZFRAC(IL))*ESW(TX)+ZFRAC(IL)*ESI(TX)
            RSS=EPS1*EST/( P(IL)-EPS2*EST )
            DRSSDT=EPS1*P(IL) &
                   *((1.-ZFRAC(IL))*ESW(TX)*(RW3-RW2/TX) &
                        +ZFRAC(IL) *ESI(TX)*(RI3-RI2/TX)) &
                    /(TX*(P(IL)-EPS2*EST)**2)
!
!           * THIRD ITERATION.
!
            HMNPR=CPM(IL)*TX+GRAV*Z(IL)-HMN(IL)
            DHMNPR=CPM(IL)
            RC=QT(IL)-RSS
            HMNPR=HMNPR-RRL(IL)*RC
            DHMNPR=DHMNPR+RRL(IL)*DRSSDT
            TX=TX-HMNPR/DHMNPR
            EST=(1.-ZFRAC(IL))*ESW(TX)+ZFRAC(IL)*ESI(TX)
            RSS=EPS1*EST/( P(IL)-EPS2*EST )
            DRSSDT=EPS1*P(IL) &
                   *((1.-ZFRAC(IL))*ESW(TX)*(RW3-RW2/TX) &
                        +ZFRAC(IL) *ESI(TX)*(RI3-RI2/TX)) &
                    /(TX*(P(IL)-EPS2*EST)**2)
!
!           * TEMPERATURE-DERIVATIVE OF RSS FOR T=TL. THE DERIVATIVE
!           * IS USED IN A TAYLOR SERIES APPROXIMATION TO DETERMINE THE
!           * WATER VAPOUR SATURATION MIXING RATIO AT TEMPERATURE TX
!           * (I.E. AMBIENT TEMPERATURE) ABOUT TL (I.E. LIQUID WATER
!           * TEMPERATURE).
!
            IF( TX.NE.TL .AND. RSS > RSSTL ) THEN
              DRSSDT=(RSS-RSSTL)/(TX-TL)
            ELSE
              DRSSDT=DRSSDTL
              RSS=RSSTL
            ENDIF
          ENDIF
!
!         * SAVE FINAL ITTERATION OF SATURATION SPECIFIC HUMIDITY FOR
!         * USE ELSEWHERE IN CALCULATION RELATIVE HUMIDITIES.
!
          SSH(IL)=RSS
          IF (ISUBG.EQ.1)                                           THEN
!
!           * A AND B PARAMETERS FOR STATISTICAL CLOUD SCHEME.
!
            APA=1./(1.+RRL(IL)*DRSSDT/CPM(IL))
            APB=APA*DRSSDT
!
!           * DETERMINE VARIANCE BASED ON LOCAL APPROACH (CHABOUREAU AND
!           * BECHTHOLD).
!
            SIGMACB=CSIGMA*ALMIX(IL)* &
                    ABS(APA*DRWDZ(IL)-APB*DHLDZ(IL)/CPM(IL))
!
            IF(ICVSG.EQ.1)                                          THEN
!
!             * CALCULATE CONVECTIVE CONTRIBUTION TO VARIANCE.
!             * THIS MUST BE OUTPUT BECAUSE OF "AGEING".
!             * NOTE ** THIS IS ONLY DONE ONCE NEAR THE BEGINNING OF THE
!             *         PHYSICS, DUE TO THE "AGEING" CALCULATION (IE
!             *         ONE DOESN'T WANT MULTIPLE AGEING PER TIMESTEP FOR
!             *         EACH PHYSICS ROUTINE WHICH CALLS THIS SUBROUTINE.
!             *         THIS IS CONTROLLED BY THE SWITCH "ICVSG").
!
              IF(CONS(IL).GT.0.) THEN
                CVSG(IL)=3.E-03*CONS(IL)/APA
!old            CVSG(IL)=1.E-05*APA
              ELSE
                CVSG(IL)=CVSG(IL)*SIGFAC
              ENDIF
            ENDIF
!
!           * ADD CONVECTIVE CONTRIBUTION TO VARIANCE.
!
            SIGMASUM=SIGMACB+CVSG(IL)
!
!           * LIMIT VARIANCE TO AVOID UNPHYSICAL CASES WITH LARGE VARIANCES
!           * AN LOW MEAN VALUES FOR THE TOTAL WATER PROBABILITY DISTRIBUTION.
!
            SIGMAMAX=YFR*APA*QT(IL)/2.
            SIGMA(IL)=MIN(SIGMASUM,SIGMAMAX)
          ENDIF
!
!         * CLOUD FRACTION AND CLOUD WATER CONTENT BASED ON CHABOUREAU
!         * AND BECHTHOLD'S STATISTICAL CLOUD SCHEME AND AUTOCONVERSION
!         * SCALING FACTOR BASED ON MELLORS'S STATISTICAL CLOUD SCHEME
!         * (FOR THE 2.47TH MOMENT OF THE TOTAL WATER PROBABILITY
!         * DISTRIBUTION).
!
          QCWT=(QT(IL)-RSSTL)
          IF ( ISUBG .EQ. 1 .AND. SIGMA(IL) .GT. ZSEC ) THEN
            QCWT=QCWT*APA
            Q1=QCWT/SIGMA(IL)
            Q1M=Q1/2.
            INDX=NINT((Q1/2.+2.)/QDELT)+1
            IF ( Q1.LT.Q1MIN ) THEN
              ZCLF(IL)=0.
              QCW (IL)=0.
              ZCRAUT(IL)=ZCR(Q1MINM,I2NDIE)
              QCWVAR(IL)=0.
            ELSE IF ( Q1.GE.Q1MIN .AND. Q1.LT.0. ) THEN
              ZCLF(IL)=0.5+0.36*ATAN(1.55*Q1)
              QCW (IL)=SIGMA(IL)*EXP(1.2*Q1-1.)
              ZCRAUT(IL)=ZCR(Q1M,I2NDIE)
!
              IF (MACHINE.EQ.2) THEN
                R4VAL=Q1M/SQRT2
                AR=.5*(1.+ERF(R4VAL))
              ELSE
                R8VAL=Q1M/SQRT2
                AR=.5*(1.+ERF(R8VAL))
              ENDIF
!
              AEXP=EXP(-.5*Q1M**2)
              QL=AR*Q1M+AEXP/SQRT2PI
              QLVAR=AR*(1.+(Q1M-QL)**2)+AEXP*(Q1M/SQRT2PI-QL)
              QCWVAR(IL)=QLVAR/QL**2
            ELSE IF ( Q1.GE.0. .AND. Q1.LE.Q1MAX ) THEN
              ZCLF(IL)=0.5+0.36*ATAN(1.55*Q1)
              QCW (IL)=SIGMA(IL)*(EXPM1+0.66*Q1+0.086*Q1**2)
              ZCRAUT(IL)=ZCR(Q1M,I2NDIE)
!
              IF (MACHINE.EQ.2) THEN
                R4VAL=Q1M/SQRT2
                AR=.5*(1.+ERF(R4VAL))
              ELSE
                R8VAL=Q1M/SQRT2
                AR=.5*(1.+ERF(R8VAL))
              ENDIF
!
              AEXP=EXP(-.5*Q1M**2)
              QL=AR*Q1M+AEXP/SQRT2PI
              QLVAR=AR*(1.+(Q1M-QL)**2)+AEXP*(Q1M/SQRT2PI-QL)
              QCWVAR(IL)=QLVAR/QL**2
            ELSE
              ZCLF(IL)=1.
              QCW (IL)=QT(IL)-RSS
              ZCRAUT(IL)=1.
              QCWVAR(IL)=0.
            ENDIF
!
!           * IMPOSE UPPER BOUND ON CLOUD WATER. THIS IS NECESSARY SINCE
!           * THE EMPIRICAL FITS ON WHICH THE STATISTICAL CLOUD SCHEME IS
!           * IS BASED MAY PRODUCE UNPHYSICAL RESULTS UNDER CERTAIN
!           * CIRCUMSTANCES (I.E. EXCESSIVELY LARGE CLOUD WATER CONTENTS).
!           * THE APPROACH HERE IS NOT TO ALLOW THE INFERRED WATER VAPOUR
!           * MIXING RATIO IN THE CLEAR-SKY TO BECOME SMALLER THAN A
!           * GIVEN THRESHOLD (ZSEC).
!
            QCWMAX=MAX(QT(IL)-((1.-ZCLF(IL))*ZSEC+ZCLF(IL)*RSS),0.)
            IF ( Q1.GE.Q1MIN .AND. Q1.LE.Q1MAX &
                                         .AND. QCW(IL).GE.QCWMAX ) THEN
              QCW(IL)=QCWMAX
            ENDIF
          ELSE
            IF ( QCWT .LT. 0. ) THEN
              ZCLF(IL)=0.
              QCW (IL)=0.
              ZCRAUT(IL)=ZCR(Q1MINM,I2NDIE)
              QCWVAR(IL)=0.
            ELSE
              ZCLF(IL)=1.
              QCW (IL)=QT(IL)-RSS
              ZCRAUT(IL)=1.
              QCWVAR(IL)=0.
            ENDIF
          ENDIF
          QCW(IL)=MAX(MIN(QCW(IL),QT(IL)),ZERO)
          ZCLF(IL)=MAX(MIN(ZCLF(IL),ONE),ZERO)
!
!         * ELIMINATE CLOUDSAND CONDENSATE IF CLOUD FRACTION OR
!         * CONDENSATE MIXING RATIO TOO LOW TO ENSURE CONSISTENT
!         * RESULTS FOR CLOUD FRACTION AND CLOUD CONDENSATE.
!
          IF ( ZCLF(IL).LT.0.01 .OR. QCW(IL).LT.1.E-12 ) THEN
            ZCLF(IL)=0.
            QCW (IL)=0.
            ZCRAUT(IL)=ZCR(Q1MINM,I2NDIE)
            QCWVAR(IL)=0.
          ENDIF
!
!         * TRACK UNPHYSICAL VALUES IN PREPARATION FOR ABORTING
!         * (DONE OUTSIDE LOOP IN ORDER TO KEEP OPTIMIZATION).
!
          IF ( ISUBG.EQ.1 .AND. QCW(IL).GT.QT(IL) .AND. ILBAD.EQ.0 ) &
                                                                 THEN
            ILBAD=IL
            SIGCBBAD=SIGMACB
            SIGMAXBAD=SIGMAMAX
            QCWTBAD=QCWT
          ENDIF
        ENDIF
      ENDDO
!
!     * PRINT OUT PROBLEM POINTS AND ABORT, IF THEY EXIST.
!
      IF(ILBAD.NE.0)                                            THEN
        IL=ILBAD
        PRINT*,'ICALL,LVL,IL,QCW,QT,RSS=',ICALL,LVL,ILBAD,QCW(IL), &
                QT(IL),SSH(IL)
        PRINT*,'SIGMA,SIGMACB,CVSG,SIGMAMAX,ZCLF,ZCRAUT,QCWT=', &
                SIGMA(IL),SIGCBBAD,CVSG(IL),SIGMAXBAD,ZCLF(IL), &
                ZCRAUT(IL),QCWTBAD
        CALL XIT('STATCLD5',-1)
      ENDIF
!
      END SUBROUTINE STATCLD
      FUNCTION ZCR(Q1,IMOD)
!
!     * JUN 26/2013 - K.VONSALZEN.
!
!     * CALLED BY STATCLD5.
!
!     * RATIO OF MOMENTS OF CLOUD LIQUID WATER CONTENT. THE RATIO
!     * <QLWC**COEFF>/<QLWC>**COEFF IS CALCULATED BY INTEGRATING
!     * OVER A GAUSSIAN DISTRIBUTION USING THE STATISTICAL CLOUD
!     * SCHEME BY MELLOR (1977). FOR IMOD=0 -> COEFF=2.47 AND
!     * FOR IMOD=1 -> COEFF=3.
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL :: ZCR
      REAL, PARAMETER :: Q1MIN=-2., Q1MAX=2.
      INTEGER, PARAMETER :: INUM=61
      REAL, DIMENSION(INUM) :: ZCRLD, ZCRKK, ZCRR
      DATA ZCRLD / 8889.942, 6454.475, 4721.926, 3480.747, 2584.975, &
                   1934.007, 1457.707, 1106.815,  846.462,  652.041, &
                    505.868,  395.250,  311.001,  246.407,  196.574, &
                    157.888,  127.668,  103.925,   85.150,   70.220, &
                     58.278,   48.672,   40.903,   34.586,   29.420, &
                     25.173,   21.665,   18.752,   16.322,   14.284, &
                     12.568,   11.117,    9.883,    8.830,    7.928, &
                      7.152,    6.482,    5.900,    5.395,    4.953, &
                      4.567,    4.227,    3.927,    3.662,    3.427, &
                      3.218,    3.032,    2.866,    2.716,    2.582, &
                      2.462,    2.353,    2.254,    2.165,    2.084, &
                      2.011,    1.944,    1.883,    1.827,    1.776, &
                      1.729 /
      DATA ZCRKK /  708.636,  560.461,  445.761,  356.524,  286.721, &
                    231.848,  188.502,  154.094,  126.637,  104.629, &
                     86.902,   72.556,   60.895,   51.368,   43.552, &
                     37.112,   31.781,   27.350,   23.650,   20.549, &
                     17.939,   15.733,   13.862,   12.269,   10.908, &
                      9.739,    8.733,    7.864,    7.111,    6.456, &
                      5.885,    5.385,    4.946,    4.559,    4.218, &
                      3.916,    3.649,    3.410,    3.198,    3.008, &
                      2.838,    2.685,    2.548,    2.424,    2.312, &
                      2.211,    2.119,    2.036,    1.961,    1.892, &
                      1.829,    1.772,    1.720,    1.672,    1.628, &
                      1.588,    1.551,    1.517,    1.485,    1.457, &
                      1.430 /
!
      Q1DELT=(Q1MAX-Q1MIN)/REAL(INUM-1)
      IF ( IMOD == 0 ) THEN
        ZCRR=ZCRKK
      ELSE
        ZCRR=ZCRLD
      ENDIF
!
      IF ( Q1 < Q1MIN ) THEN
        ZCR=ZCRR(1)
      ELSE IF ( Q1 > Q1MAX ) THEN
        ZCR=ZCRR(INUM)
      ELSE
        Q1V=(Q1-Q1MIN)/Q1DELT+1.
        IND=INT(Q1V)
        WGT=Q1V-REAL(IND)
        ZCR=WGT*ZCRR(IND+1)+(1.-WGT)*ZCRR(IND)
      ENDIF
!
      END FUNCTION ZCR
      SUBROUTINE VRTDF(DXDT,XROW,DTDT,THROW,DQDT,QROW,DQLDT,QLWC, &
                       DUDT,U,V,WSUB,RKH,RKM,RI,CDM,USTAR,ZSPD, &
                       ZSPDA,ZF,PF,TF,ZH,PH,DP,ALMC,ALMX,PBLT,TVP, &
                       QSENS,QLAT,ZCLF,TZERO,THLIQG,ZO,SHTJ,SHJ,DSHJ, &
                       PRESSG,DT,ZTOP,IL1,IL2,ILG,ILEV,ILEVP1, &
                       MSGP,NTRAC)
!
      USE SDPHYS
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      INTEGER, INTENT(IN) :: ILG,ILEV,ILEVP1,IL1,IL2,MSGP,NTRAC
      REAL, INTENT(OUT), DIMENSION(ILG) :: PBLT, CDM, ZSPD, ZSPDA, &
                                           QSENS, QLAT
      REAL, INTENT(INOUT), DIMENSION(ILG) :: USTAR, ZO
      REAL, INTENT(OUT), DIMENSION(ILG,ILEV) :: ALMC,ALMX,RKH,RKM,RI, &
                                                WSUB,TVP
      REAL, INTENT(INOUT), DIMENSION(ILG,ILEV) :: ZCLF
      REAL, INTENT(IN) :: DT
      REAL, INTENT(IN), DIMENSION(ILG) :: PRESSG, TZERO, THLIQG
      REAL, INTENT(IN), DIMENSION(ILG,ILEV) :: SHJ,DSHJ,TF,ZH,PH, &
                                               ZF,PF,DP
      REAL, INTENT(IN), DIMENSION(ILG,ILEVP1) :: SHTJ
      REAL, DIMENSION(ILG,ILEVP1), INTENT(INOUT) :: THROW,QROW
      REAL, DIMENSION(ILG,ILEV), INTENT(IN) :: QLWC
      REAL, DIMENSION(ILG,ILEV), INTENT(INOUT) :: U,V
      REAL, DIMENSION(ILG,ILEV,NTRAC) :: RKX
      REAL, DIMENSION(ILG,ILEVP1,NTRAC), INTENT(IN) :: XROW
      REAL, DIMENSION(ILG,ILEVP1,NTRAC), INTENT(OUT) :: DXDT
      REAL, DIMENSION(ILG,ILEVP1), INTENT(OUT) :: DQDT,DTDT
      REAL, DIMENSION(ILG,ILEV), INTENT(OUT) :: DQLDT
      REAL, DIMENSION(ILG,ILEV), INTENT(OUT) :: DUDT
!
      REAL, DIMENSION(ILG,ILEV) :: A,B,C,WORK,XROWT,DXDTT,RKXT,QT,HMN
      REAL, DIMENSION(ILG) :: UN,ZER,CL,CDVLT,RAUS,VINT,THT
      REAL, DIMENSION(ILG,ILEV) :: CVAR, ZCRAUT, QCWVAR, CVSG, CVDU, &
                                   ZFRAC, SHXKJ, DVDS, DTTDS, ALMXT
      REAL, DIMENSION(ILG,ILEVP1) :: SHTXKJ
      REAL, DIMENSION(ILG) :: PMBS, RRL, CPM, ALMIX, DHLDZ, DRWDZ, &
                              FLAGEST, QCW, SSH, VMODL, CDVLM, DVDZ
      REAL, DIMENSION(ILG) :: CDH, ZOSCLM, ZOSCLH, TVIRTS, TVIRTA, &
                              CRIB, CFLUX, EVAP, QZERO, PBLTI, QTG, &
                              HMNG
      INTEGER, DIMENSION(ILG) :: IPBL
      REAL, PARAMETER :: VKC=0.41, TVFA=0.608, SCALF=0.8

!
!-----------------------------------------------------------------------
!     * SURFACE PROPERTIES FOR BARE GROUND OR WATER.
!
      TFREZ=273.16
      DO I=IL1,IL2
         ZSPD(I)=SQRT(U(I,ILEV)**2+V(I,ILEV)**2) ! SURFACE WIND SPEED
         ESW=1.E+02*EXP(RW1+RW2/TZERO(I))*(TZERO(I)**RW3)
         WZERO=EPS1*ESW/(PRESSG(I)-ESW)
         Q0SAT=WZERO
!         IF( TZERO(I) >= TFREZ ) THEN
!           AC=17.269
!           BC=35.86
!         ELSE
!           AC=21.874
!           BC=7.66
!         ENDIF
!         WZERO=EPS1*611.0*EXP(AC*(TZERO(I)-TFREZ)/
!     1              (TZERO(I)-BC))/PRESSG(I)
!
!         Q0SAT=WZERO/(1.0+WZERO)     ! SATURATED SPECIFIC HUMIDITY
         IF( THLIQG(I)> 0.99 ) THEN  ! WATER SURFACE
           EVBETA=1.0
           QZERO(I)=Q0SAT
           ZO(I)=(0.016/GRAV)*USTAR(I)**2
         ELSE                        ! BARE SOIL
           THLMIN=0.1                ! MIMIMUM SOIL WATER CONTENT (M3/M3)
           THFC=0.4                  ! FIELD CAPACITY (M3/M3)
           IF( THLIQG(I) < (THLMIN+0.001) ) THEN
             IEVAP=0
             CEVAP=0.0
           ELSEIF( THLIQG(I) > THFC ) THEN
             IEVAP=1
             CEVAP=1.0
           ELSE
             IEVAP=1
             CEVAP=0.25*(1.0-COS(3.14159*THLIQG(I)/THFC))**2
           ENDIF
           EVBETA=CEVAP              ! SURFACE EVAPORATION EFFICIENCY
           QZERO(I)=EVBETA*Q0SAT+(1.0-EVBETA)*QROW(I,ILEVP1)
           IF( QZERO(I) > QROW(I,ILEVP1) .AND. IEVAP == 0) THEN
             EVBETA=0.0
             QZERO(I)=QROW(I,ILEVP1)
           ENDIF
         ENDIF
!         TVIRTS(I)=TZERO(I)*(1.0+0.61*QZERO(I))
         TVIRTS(I)=TZERO(I)*(1.+TVFA*QZERO(I)/(1.-QZERO(I)))
      ENDDO
      DO I=IL1,IL2
        ZRSLD=ZH(I,ILEVP1)+ZO(I)
        ZOSCLM(I)=ZO(I)/ZRSLD
        ZOSCLH(I)=ZOSCLM(I)
!        TVIRTA(I)=THROW(I,ILEVP1)*(1.0+0.61*QROW(I,ILEVP1))
        TVIRTA(I)=THROW(I,ILEVP1)*(1.+TVFA*QROW(I,ILEVP1) &
                                    /(1.-QROW(I,ILEVP1)))
        CRIB(I)=-GRAV*ZRSLD/(TVIRTA(I)*ZSPD(I)**2)
      ENDDO
!
!     * SURFACE DRAG COEFFICIENTS.
!
      CALL DRCOEF(CDM,CDH,CFLUX,ZOSCLM,ZOSCLH,CRIB,TVIRTS, &
                  TVIRTA,ZSPD,GRAV,VKC,ILG,IL1,IL2)
      USTAR=ZSPD*SQRT(CDM)
!
!     * SURFACE LATENT AND SENSIBLE HEAT FLUXES, AND EVAPORATION.
!
      DO I=IL1,IL2
        ZRHO0=PH(I,ILEVP1)/(RGAS*THROW(I,ILEVP1))
!!!!
        QSENS(I)=ZRHO0*CPRES*CFLUX(I)*(TZERO(I)-THROW(I,ILEVP1))
        EVAP(I)=ZRHO0*CFLUX(I)*(QZERO(I)-QROW(I,ILEVP1))
!
!        QSENS(I)=0.
!        EVAP(I)=0./RL
!
        QLAT(I)=RL*EVAP(I)
      ENDDO
!
!     * ANEMOMETER WIND SPEED (M/SEC).
!
      DO I=IL1,IL2
        ZRUF=ZO(I)
        ZANNOM=MAX(ZRUF,10.0)
        RATFCA=LOG(ZANNOM/ZRUF)/VKC
        RATFCA1=RATFCA*SQRT(CDM(I))
        RATFCA1=MIN(RATFCA1,1.)
        ZSPDA(I)=RATFCA1*ZSPD(I)
      ENDDO
!
!     * INSERT FLUXES INTO FIRST ATMOSPHERIC LEVEL ABOVE GROUND.
!
      DTDT=0.
      DQDT=0.
      DO I=IL1,IL2
        ZRHO0=PH(I,ILEVP1)/(RGAS*THROW(I,ILEVP1))
        FAC=GRAV/DP(I,ILEV)
!        THROW(I,ILEVP1)=THROW(I,ILEVP1)+DT*FAC*QSENS(I)/CPRES
!        QROW(I,ILEVP1)=QROW(I,ILEVP1)+DT*FAC*EVAP(I)
        DTDT(I,ILEVP1)=FAC*QSENS(I)/CPRES
        DQDT(I,ILEVP1)=FAC*EVAP(I)
!!!!
!        DU=DT*FAC*ZRHO0*CDM(I)*MAX(VMIN,ZSPD(IL))*U(I,ILEV)
!        U(I,ILEV)=U(I,ILEV)-DU
!        IF ( U(I,ILEV) >= 0. ) THEN
!          U(I,ILEV)=MAX(U(I,ILEV),0.)
!        ELSE
!          U(I,ILEV)=MIN(U(I,ILEV),0.)
!        ENDIF
      ENDDO
!
!     * PARAMETERS FOR CALCULATION OF VERTICAL DIFFUSION.
!
      VMIN=0.001
      DO IL=IL1,IL2
        UN   (IL)=1.
        ZER  (IL)=0.
        RAUS (IL)=PRESSG(IL)/GRAV
        CDVLT(IL)=0.
        VMODL(IL)=MAX(VMIN,ZSPD(IL))
      ENDDO
      SHXKJ=SHJ**RGOCP
      SHTXKJ=SHTJ**RGOCP
      TAUADJ=DT
      DO I=IL1,IL2
         CDVLM(I)=CDM(I)*VMODL(I)
         HEAT=TAUADJ*GRAV*SHXKJ(I,ILEV)/(RGAS*THROW(I,ILEV))
         FACMOM=1./(1.+HEAT*CDVLM(I)/DSHJ(I,ILEV))
!
!         CDVLM(I)=CDVLT(I)
!         FACMOM=1.
!
         UTMP =U(I,ILEV)*FACMOM
         VTMP =V(I,ILEV)*FACMOM
         DVDS(I,ILEV) = SQRT((UTMP-U(I,ILEV-1))**2 &
                     +(VTMP-V(I,ILEV-1))**2)/(SHJ(I,ILEV)-SHJ(I,ILEV-1))
      ENDDO
!
!     * EVALUATE DTTDS=D(THETA)/D(SIGMA) AT TOP INTERFACE OF LAYERS.
!
      L=1
      DO I=IL1,IL2
         ALWC=QLWC(I,L)
         THETVP=THROW(I,L)*(1.+TVFA*QROW(I,L)/(1.-QROW(I,L)) &
               -(1.+TVFA)*ALWC)/SHXKJ(I,L)
         TVP(I,L)=THETVP
      ENDDO
      DO L=2,ILEV
      DO I=IL1,IL2
         ALWC=QLWC(I,L)
         THETVP=THROW(I,L)*(1.+TVFA*QROW(I,L)/(1.-QROW(I,L)) &
               -(1.+TVFA)*ALWC)/SHXKJ(I,L)
         ALWC=QLWC(I,L-1)
         THETVM=THROW(I,L-1)*(1.+TVFA*QROW(I,L-1) &
               /(1.-QROW(I,L))-(1.+TVFA)*ALWC)/SHXKJ(I,L-1)
!!!!
!         THETVP=THROW(I,L)/SHXKJ(I,L)
!         THETVM=THROW(I,L-1)/SHXKJ(I,L-1)
         DTTDS(I,L)=(THETVP-THETVM)/(SHJ(I,L)-SHJ(I,L-1))
         TVP(I,L)=THETVP
      ENDDO
      ENDDO
!
!     * DVDS = MOD(DV/DSIGMA) AT TOP INTERFACE OF LAYERS.
!
      DO L=1,ILEV-2
      DO I=IL1,IL2
         DVDS(I,L+1)=SQRT((U(I,L+1)-U(I,L))**2 &
                         +(V(I,L+1)-V(I,L))**2)/(SHJ(I,L+1)-SHJ(I,L))
      ENDDO
      ENDDO
      DO I=IL1,IL2
         PBLTI(I)=REAL(ILEV)
!         IPBL(I)=1
      ENDDO
!
!     * RMS WIND SHEAR, DVMINS=(R*T/G)*(DV/DZ)=7.9 FOR DV/DZ=1.MS-1KM-1.
!     * DETERMINE PLANETARY BOUNDARY LAYER TOP AS LEVEL INDEX ABOVE
!     * WHICH THE RICHARDSON NUMBER EXCEEDS THE CRITICAL VALUE OF 1.00.
!
      DVMINS=7.9
!      TAUR=3600.
      TAUR=6.*3600.
      DO L=ILEV,2,-1
      DO I=IL1,IL2
         DVDS(I,L)=MAX(DVDS(I,L),DVMINS)
         RIT=-RGAS*SHTXKJ(I,L)*DTTDS(I,L)/(SHTJ(I,L)*DVDS(I,L)**2)
!!!!
         RI(I,L)=(RI(I,L)+(DT/TAUR)*RIT)/(1.+DT/TAUR)
!         RI(I,L)=RIT
      ENDDO
      ENDDO
      DO L=ILEV,3,-1
      DO I=IL1,IL2
!!!!
!         IF ( RI(I,L) < 1. .AND. RI(I,L-1) > 1. ) THEN
         IF ( RI(I,L) < 10. .AND. RI(I,L-1) > 10. ) THEN
            PBLX=REAL(L)
            PBLTI(I) = MIN(PBLTI(I), PBLX)
         ENDIF
      ENDDO
      ENDDO
!!!!
      PBLT=PBLTI
!      PBLT=MAX((PBLT+(DT/TAUR)*PBLTI)/(1.+DT/TAUR),2.)
!
      FACT=VKC*RGAS*273.
      XLMIN=10.
      BEEM=10.
      GAMRH=6.
      GAMRM=6.
      ALFAH=1.
      RINEUT=1.
      RIINF=0.25
      PRANDTL_MIN=1.
      PRANDTL_MAX=3.
      WSUB=0.
      DO L=2,ILEV
!
!        * CALCULATE THE DIFFUSION COEFFICIENTS (RKH,RKM)
!        * AT THE TOP INTERFACE OF LAYERS.
!        * HEAT: FINITE STABILITY CUTOFF.
!        * LOW XINGL USED SINCE PREVIOUSLY MIXED "INSTANTANEOUSLY".
!        * MOMENTUM: FINITE STABILITY CUTOFF.
!        * HIGH XINGL USED SINCE NOT PREVIOUSLY MIXED "INSTANTANEOUSLY".
!        * UNSTABLE CASE (RI <= 0.) - STABLE CASE (RI > 0.) .
!
         DO I=IL1,IL2
            DVDZ(I)=DVDS(I,L)*GRAV*SHTJ(I,L)/(RGAS*TF(I,L-1))
         ENDDO
!
         DO I=IL1,IL2
            FACT0=FACT*LOG(SHTJ(I,L))
            SARI=SQRT(ABS(RI(I,L)))
            LPBL=NINT(PBLT(I))
!
!           * LOWER CUTOFFS.
!
            ALL=0.5*VKC*ZF(I,LPBL-1)
            XIMINT=MAX(75.*ALL/(75.+ALL),XLMIN)
            ALL=0.5*VKC*ZH(I,L)
            XIMIN =MAX(75.*ALL/(75.+ALL),XLMIN)
!
!           * EFFECTIVE MIXING LENGTHS AS FUNCTION OF MIXING
!           * LENGTHS FOR UP- AND DOWNWARD MIXING.
!
            ALU=VKC*ZH(I,L)
            ALD=VKC*(ZF(I,LPBL-1)-ZH(I,L))
            IF ( ALD > 0. ) THEN
              XINGLH=MAX(ALU*ALD/(ALU+ALD),XIMIN)
              XINGLM=MAX(ALU*ALD/(ALU+ALD),XIMIN)
              ALMC(I,L)=XINGLH
            ELSE
              FAC=(PH(I,L)/PF(I,LPBL-1))**2.
              XINGLH=10.+(XIMINT-10.)*FAC
              XINGLM=10.+(XIMINT-10.)*FAC
              ALMC(I,L)=MIN(100.*FAC,VKC*(ZH(I,L)-ZF(I,LPBL-1)))
            END IF
            ALMX(I,L)=XINGLH
            ALMXT(I,L)=MAX(ALMX(I,L),ALMC(I,L),10.)
            XINGLH=(1.-ZCLF(I,L))*XINGLH+ZCLF(I,L)*ALMXT(I,L)
            XINGLM=XINGLH
!
!           * DIFFUSION COEFFICIENTS.
!
            ELH   = XINGLH**2
            EPSSH = 0.
            FACTH = 0.5*RI(I,L)*EPSSH*BEEM
            IF ( FACTH > 1. ) THEN
              XH  = 0.
            ELSE
              XH  = (1.-FACTH)**2
            ENDIF
            ELM   = XINGLM**2
            EPSSM = 0.
            FACTM = 0.5*RI(I,L)*EPSSM*BEEM
            IF ( FACTM > 1. ) THEN
              XM  = 0.
            ELSE
              XM  = (1.-FACTM)**2
            ENDIF
            IF ( RI(I,L) < 0. ) THEN
              RKH(I,L) = ELH*DVDZ(I)*(1.-ALFAH*GAMRH*BEEM*RI(I,L) / &
                         (GAMRH+ALFAH*BEEM*SARI))
              RKM(I,L) = ELM*DVDZ(I)*(1.-      GAMRM*BEEM*RI(I,L) / &
                         (GAMRM+      BEEM*SARI))
            ELSE
              RKH(I,L) = ELH*DVDZ(I)*XH/(1.+(1.-EPSSH)*BEEM*RI(I,L))
              RKM(I,L) = ELM*DVDZ(I)*XM/(1.+(1.-EPSSM)*BEEM*RI(I,L))
            ENDIF
!
!           * PRANDTL NUMBER SCALING FOR EDDY MOMENTUM DIFFUSIVITY,
!           * BASED ON APPROACH SUGGESTED BY SCHUMANN AND GERZ (1995).
!
            ATMP=0.
            IF ( RI(I,L) > 0. ) THEN
              ATMP=RINEUT*EXP(-RI(I,L)/(RINEUT*RIINF))
            ENDIF
            PRANDTL=ATMP+RI(I,L)/RIINF
            PRANDTL=MIN(MAX(PRANDTL,PRANDTL_MIN),PRANDTL_MAX)
            RKM(I,L)=RKM(I,L)*PRANDTL
!
!           * NO MIXING ABOVE SPECIFIED HEIGHT.
!
            IF ( ZH(I,L) > ZTOP ) THEN
!               RKH(I,L)=0.
!               RKM(I,L)=0.
            ENDIF
         ENDDO
      ENDDO
      DO I=IL1,IL2
         RKM(I,1)=RKM(I,2)
         RKH(I,1)=RKH(I,2)
      ENDDO
      DO N=1,NTRAC
        RKX(:,:,N)=RKH
      ENDDO
!
!     * DIAGNOSE SUBGRID-SCALE COMPONENT OF THE VERTICAL VELOCITY
!     * (STANDARD DEVIATION, GHAN ET AL., 1997). ACCORDING TO PENG
!     * ET AL. (2005), THE REPRESENTATIVE VERTICAL VELOCITY FOR
!     * AEROSOL ACTIVATION IS OBTAINED BY APPLYING A SCALING FACTOR
!     * TO THE VELOCITY STANDARD DEVIATION (SCALF).
!
      PI=3.141592653589793
      FACTS=SCALF*SQRT(2.*PI)
      DO L=2,ILEV
        WSUB(:,L)=FACTS*RKH(:,L)/(ZF(:,L-1)-ZF(:,L))
      ENDDO
!!!!
!      WSUB=10. &
! Fli ht 08
!      WSUB=0.05 &
! Fli ht 07, low
!      WSUB=0.1 &
! Fli ht 07, high
!      WSUB=0.2
!
!      WSUB=0.02
!
!     * MIX CHEMICAL TRACERS.
!
      DO N=1,NTRAC
        RKXT=RKX(:,:,N)
        THT=THROW(:,ILEVP1)
        CALL ABCVDQ6 ( A,B,C,CL,UN,CDVLT,GRAV, &
                       IL1,IL2,ILG,ILEV,ILEVP1,ILEV, &
                       RGAS,RKXT,SHTJ,SHJ,DSHJ, &
                       THT,TF,DT)
        XROWT=XROW(:,MSGP:ILEVP1,N)
        CALL IMPLVD7(A,B,C,CL,XROWT,ZER,IL1,IL2,ILG,ILEV, &
                     DT,DXDTT,DSHJ,RAUS,WORK,VINT)
        DXDT(:,MSGP:ILEVP1,N)=DXDTT(:,:)
      ENDDO
!
!     * MIX MOMENTUM.
!
      THT=THROW(:,ILEVP1)
      CALL ABCVDQ6 ( A,B,C,CL,UN,CDVLM,GRAV, &
                     IL1,IL2,ILG,ILEV,ILEVP1,ILEV, &
                     RGAS,RKM,SHTJ,SHJ,DSHJ, &
                     THT,TF,DT)
      CALL IMPLVD7(A,B,C,CL,U,ZER,IL1,IL2,ILG,ILEV, &
                   DT,DUDT,DSHJ,RAUS,WORK,VINT)
!
!     * TOTAL WATER MIXING RATIO AND CLOUD WATER STATIC ENERGY.
!
      QT=QROW(:,MSGP:ILEVP1)+QLWC
      HMN=CPRES*THROW(:,MSGP:ILEVP1)+GRAV*ZH-RL*QLWC
!
!     * LIQUID WATER STATIC ENERGY AND TOTAL WATER AT SURFACE.
!
      L=ILEV
      DO IL=IL1,IL2
        QTG(IL)=QZERO(IL)
        HMNG(IL)=CPRES*TZERO(IL)
      ENDDO
!
!     * MIX CLOUD WATER STATIC ENERGY AND TOTAL WATER.
!
      THT=THROW(:,ILEVP1)
      CALL ABCVDQ6 ( A,B,C,CL,UN,CDVLT,GRAV, &
                     IL1,IL2,ILG,ILEV,ILEVP1,ILEV, &
                     RGAS,RKH,SHTJ,SHJ,DSHJ, &
                     THT,TF,DT)
      CALL IMPLVD7(A,B,C,CL,HMN,HMNG,IL1,IL2,ILG,ILEV, &
                   DT,DXDTT,DSHJ,RAUS,WORK,VINT)
      HMN=HMN+DT*DXDTT
      CALL IMPLVD7(A,B,C,CL,QT,QTG,IL1,IL2,ILG,ILEV, &
                   DT,DXDTT,DSHJ,RAUS,WORK,VINT)
      QT=QT+DT*DXDTT
!
!     * CHECK CONSERVATION.
!
!      DQT=0.
!      DQ=0.
!      DO L=1,ILEV
!      DO I=1,1
!        DQT=DQT+DP(I,L)*DXDTT(I,L)/GRAV
!        DQ=DQ+DP(I,L)*QT(I,L)/GRAV
!      ENDDO
!      ENDDO
!      PRINT*,'DQ,DQT=',DQ,DQT
!
!     * TEMPERATURE AND WATER VAPOUR MIXING RATIO FROM STATISTICAL
!     * CLOUD SCHEME.
!
      FACTE=1./EPS1-1.
      I2NDIE=1
      FLAGEST=1.
      CVSG=0.
      CVDU=0.
      ZFRAC=0.
      ICVSG=1
      ISUBG=0
      ICALL=1
      DO JK=1,ILEV
        DO IL=1,ILG
          PMBS(IL)=0.01*PH(IL,JK)
          RRL(IL)=RL
          CPM(IL)=CPRES
          ALMIX(IL)=MAX(ALMC(IL,JK),ALMX(IL,JK),10.)
          IF(JK.GT.1)                              THEN
            DZ       =ZH  (IL,JK-1)-ZH (IL,JK)
            DHLDZ(IL)=(HMN(IL,JK-1)-HMN(IL,JK))/DZ
            DRWDZ(IL)=(QT (IL,JK-1)-QT (IL,JK))/DZ
          ELSE
            DHLDZ(IL)=0.
            DRWDZ(IL)=0.
          ENDIF
        ENDDO
        CALL STATCLD (QCW,ZCLF(1,JK),CVAR(1,JK),ZCRAUT(1,JK), &
                      QCWVAR(1,JK),SSH,CVSG(1,JK),QT(1,JK), &
                      HMN(1,JK),ZFRAC(1,JK),CPM, &
                      PMBS,ZH(1,JK),RRL,CVDU(1,JK),FLAGEST, &
                      ALMIX,DHLDZ,DRWDZ,I2NDIE, &
                      DT,ILEV,ILG,1,ILG,ICVSG,ISUBG, &
                      JK,ICALL)
!
        DO IL=1,ILG
          DTDT(IL,JK)=DTDT(IL,JK)+ &
                     ((HMN(IL,JK)-GRAV*ZH(IL,JK)+RRL(IL)*QCW(IL)) &
                       /CPM(IL)-THROW(IL,JK))/DT
          DQDT(IL,JK)=DQDT(IL,JK)+ &
                     ((QT(IL,JK)-QCW(IL))-QROW(IL,JK))/DT
          DQLDT(IL,JK)=(QCW(IL)-QLWC(IL,JK))/DT
        ENDDO
      ENDDO

!      DQT=0.
!      DQ=0.
!      DO JK=1,ILEV
!      DO IL=1,1
!        DQT=DQT+(QT(IL,JK)-QROW(IL,JK)-QLWC(IL,JK))*DP(IL,JK)/GRAV
!      ENDDO
!      ENDDO
!      PRINT*,'DQT=',DQT
!!!!
!        QROW(:,ILEVP1)=QZERO(:)
!        THROW(:,ILEVP1)=TZERO(:)
!
      END SUBROUTINE VRTDF
      SUBROUTINE DRCOEF(CDM,CDH,CFLUX,ZOMIN,ZOHIN,CRIB,TVIRTG, &
                        TVIRTA,VA,GRAV,VKC,ILG,IL1,IL2)
!
      IMPLICIT NONE

!     * CALCULATES DRAG COEFFICIENTS AND RELATED VARIABLES FOR CLASS.

!     * OUTPUT FIELDS ARE:
!     *    CDM    : STABILITY-DEPENDENT DRAG COEFFICIENT FOR MOMENTUM.
!     *    CDH    : STABILITY-DEPENDENT DRAG COEFFICIENT FOR HEAT.
!     *    CFLUX  : CD * MOD(V), BOUNDED BY FREE-CONVECTIVE LIMIT.
!     * INPUT FIELDS ARE:
!     *    ZOMIN/: ROUGHNESS HEIGHTS FOR MOMENTUM/HEAT NORMALIZED BY
!     *    ZOHIN   REFERENCE HEIGHT.
!     *    CRIB   : -RGAS*SLTHKEF/(VA**2), WHERE
!     *             SLTHKEF=-LOG(MAX(SGJ(ILEV),SHJ(ILEV)))
!     *    TVIRTG : "SURFACE" VIRTUAL TEMPERATURE.
!     *    TVIRTA : LOWEST LEVEL VIRTUAL TEMPERATURE.
!     *    VA     : AMPLITUDE OF LOWEST LEVEL WIND.
!     * OTHER VARIABLES ARE:
!     *    ZOM/  : WORK ARRAYS USED FOR SCALING ZOMIN/ZOHIN
!     *    ZOH     ON STABLE SIDE, AS PART OF CALCULATION.
!     *    RIB    : BULK RICHARDSON NUMBER.
!     *    USTAR  : FRICTION VELOCITY SCALE

!     * INTEGER CONSTANTS.

      INTEGER ILG,IL1,IL2,JL,I

!     * REAL CONSTANTS

      REAL GRAV,VKC

!     * OUTPUT ARRAYS.

      REAL CDM    (ILG),   CDH    (ILG),   RIB    (ILG),   CFLUX  (ILG)
      REAL USTAR  (ILG)

!     * INPUT ARRAYS.

      REAL ZOMIN  (ILG),   ZOHIN  (ILG),   CRIB   (ILG),   TVIRTG (ILG)
      REAL TVIRTA (ILG),   VA     (ILG)

!     * WORK ARRAYS.

      REAL ZOM    (ILG),   ZOH    (ILG)

!     * TEMPORARY VARIABLES.

      REAL AA,AA1,BETA,PR,ZLEV,ZS,ZOLN,ZMLN,CPR,ZI,OLSF,OLFACT, &
           ZL,ZMOL,ZHOL,XM,XH,BH1,BH2,BH,WB,WSTAR,RIB0,WSPEED, &
           AU1,OLS,PSIM1,PSIM0,PSIH1,PSIH0,TSTAR,WTS,AS1, &
           AS2,AS3,CLIMIT

!-------------------------------------------------------------
      AA=9.5285714
      AA1=14.285714
      BETA=1.2
      PR = 1.
!
      DO I=IL1,IL2
          RIB(I)=CRIB(I)*(TVIRTG(I)-TVIRTA(I))
          IF( RIB(I) >= 0. ) THEN
            ZLEV=-CRIB(I)*TVIRTA(I)*(VA(I)**2)/GRAV
            ZS=MAX(10.,5.*MAX(ZOMIN(I)*ZLEV, ZOHIN(I)*ZLEV))
            ZS=ZLEV*(1.+RIB(I))/(1.+(ZLEV/ZS)*RIB(I))
            ZOM(I)=ZOMIN(I)*ZLEV/ZS
            ZOH(I)=ZOHIN(I)*ZLEV/ZS
            RIB(I)=RIB(I)*ZS/ZLEV
          ELSE
            ZOM(I)=ZOMIN(I)
            ZOH(I)=ZOHIN(I)
          ENDIF
          ZOLN=LOG(ZOH(I))
          ZMLN=LOG(ZOM(I))
          IF( RIB(I) < 0. ) THEN
            CPR=MAX(ZOLN/ZMLN,0.74)
            CPR=MIN(CPR,1.0)
            ZI=1000.0
            OLSF=BETA**3*ZI*VKC**2/ZMLN**3
            OLFACT=1.7*(LOG(1.+ZOM(I)/ZOH(I)))**0.5+0.9
            OLSF=OLSF*OLFACT
            ZL = -CRIB(I)*TVIRTA(I)*(VA(I)**2)/GRAV
            ZMOL=ZOM(I)*ZL/OLSF
            ZHOL=ZOH(I)*ZL/OLSF
            XM=(1.00-15.0*ZMOL)**(0.250)
            XH=(1.00-9.0*ZHOL)**0.25
            BH1=-LOG(-2.41*ZMOL)+LOG(((1.+XM)/2.)**2*(1.+XM**2)/2.)
            BH1=BH1-2.*ATAN(XM)+ATAN(1.)*2.
            BH1=BH1**1.5
            BH2=-LOG(-0.25*ZHOL)+2.*LOG(((1.00+XH**2)/2.00))
            BH=VKC**3.*BETA**1.5/(BH1*(BH2)**1.5)
            WB=SQRT(GRAV*(TVIRTG(I)-TVIRTA(I))*ZI/TVIRTG(I))
            WSTAR=BH**(0.333333)*WB
            RIB0=RIB(I)

            WSPEED=SQRT(VA(I)**2+(BETA*WSTAR)**2)
            RIB(I)=RIB0*VA(I)**2/WSPEED**2
            AU1=1.+5.0*(ZOLN-ZMLN)*RIB(I)*(ZOH(I)/ZOM(I))**0.25
            OLS=-RIB(I)*ZMLN**2/(CPR*ZOLN)*(1.0+AU1/ &
                 (1.0-RIB(I)/(ZOM(I)*ZOH(I))**0.25))
            PSIM1=LOG(((1.00+(1.00-15.0*OLS)**0.250)/2.00)**2* &
                  (1.0+(1.00-15.0*OLS)**0.5)/2.0)-2.0*ATAN( &
                  (1.00-15.0*OLS)**0.250)+ATAN(1.00)*2.00
            PSIM0=LOG(((1.00+(1.00-15.0*OLS*ZOM(I))**0.250)/2.00)**2 &
                  *(1.0+(1.00-15.0*OLS*ZOM(I))**0.5)/2.0)-2.0* &
                  ATAN((1.00-15.0*OLS*ZOM(I))**0.250)+ATAN(1.00)*2.0
            PSIH1=LOG(((1.00+(1.00-9.0*OLS)**0.50)/2.00)**2)
            PSIH0=LOG(((1.00+(1.00-9.0*OLS*ZOH(I))**0.50)/2.00)**2)

            USTAR(I)=VKC/(-ZMLN-PSIM1+PSIM0)
            TSTAR=VKC/(-ZOLN-PSIH1+PSIH0)
            CDH(I)=USTAR(I)*TSTAR/PR
            WTS=CDH(I)*WSPEED*(TVIRTG(I)-TVIRTA(I))
            WSTAR=(GRAV*ZI/TVIRTG(I)*WTS)**(0.333333)

            WSPEED=SQRT(VA(I)**2+(BETA*WSTAR)**2)
            RIB(I)=RIB0*VA(I)**2/WSPEED**2
            AU1=1.+5.0*(ZOLN-ZMLN)*RIB(I)*(ZOH(I)/ZOM(I))**0.25
            OLS=-RIB(I)*ZMLN**2/(CPR*ZOLN)*(1.0+AU1/ &
                 (1.0-RIB(I)/(ZOM(I)*ZOH(I))**0.25))
            PSIM1=LOG(((1.00+(1.00-15.0*OLS)**0.250)/2.00)**2* &
                  (1.0+(1.00-15.0*OLS)**0.5)/2.0)-2.0*ATAN( &
                  (1.00-15.0*OLS)**0.250)+ATAN(1.00)*2.00
            PSIM0=LOG(((1.00+(1.00-15.0*OLS*ZOM(I))**0.250)/2.00)**2 &
                  *(1.0+(1.00-15.0*OLS*ZOM(I))**0.5)/2.0)-2.0* &
                  ATAN((1.00-15.0*OLS*ZOM(I))**0.250)+ATAN(1.00)*2.0
            PSIH1=LOG(((1.00+(1.00-9.0*OLS)**0.50)/2.00)**2)
            PSIH0=LOG(((1.00+(1.00-9.0*OLS*ZOH(I))**0.50)/2.00)**2)

          ELSE

            WSPEED=VA(I)
            AS1=10.0*ZMLN*(ZOM(I)-1.0)
            AS2=5.00/(2.0-8.53*RIB(I)*EXP(-3.35*RIB(I))+0.05*RIB(I)**2)
!<<<
            AS2=AS2*PR*SQRT(-ZMLN)/2.
            AS3=27./(8.*PR*PR)
!>>>
            OLS=RIB(I)*(ZMLN**2+AS3*AS1*(RIB(I)**2+AS2*RIB(I))) &
                /(AS1*RIB(I)-PR*ZOLN)
            PSIM1=-0.667*(OLS-AA1)*EXP(-0.35*OLS)-AA-OLS
            PSIM0=-0.667*(OLS*ZOM(I)-AA1)*EXP(-0.35*OLS*ZOM(I)) &
                  -AA-OLS*ZOM(I)
            PSIH1=-(1.0+2.0*OLS/3.0)**1.5-0.667*(OLS-AA1) &
                  *EXP(-0.35*OLS)-AA+1.0
            PSIH0=-(1.0+2.0*OLS*ZOH(I)/3.0)**1.5-0.667*(OLS*ZOH(I)-AA1) &
                  *EXP(-0.35*OLS*ZOH(I))-AA+1.0

          ENDIF

          USTAR(I)=VKC/(-ZMLN-PSIM1+PSIM0)

          TSTAR=VKC/(-ZOLN-PSIH1+PSIH0)

          CDM(I)=USTAR(I)**2.0
          CDH(I)=USTAR(I)*TSTAR/PR
!
!         * CALCULATE CD*MOD(V) UNDER FREE-CONVECTIVE LIMIT.
!
          IF( TVIRTG(I) > TVIRTA(I) ) THEN
            CLIMIT=1.9E-3*(TVIRTG(I)-TVIRTA(I))**0.333333
          ELSE
            CLIMIT=0.
          ENDIF
          CFLUX(I)=MAX(CDH(I)*WSPEED,CLIMIT)
      ENDDO
!
      END SUBROUTINE DRCOEF
      SUBROUTINE ABCVDQ6 (A,B,C,CL, EF ,CDVLH,GRAV, &
                          IL1,IL2,ILG,ILEV,LEV,LEVS, &
                          RGAS,RKQ,SHTJ,SHJ,DSHJ, &
                          THL,TF,TODT)

!     * JUL 15/88 - M.LAZARE : REVERSE ORDER OF LOCAL SIGMA ARRAYS.
!     * MAR 14/88 - R.LAPRISE: PREVIOUS HYBRID VERSION FOR GCM3H.

!     * CALCULATES THE THREE VECTORS FORMING THE TRI-DIAGONAL MATRIX FOR
!     * THE IMPLICIT VERTICAL DIFFUSION OF MOISTURE OH HYBRID VERSION OF
!     * MODEL. A IS THE LOWER DIAGONAL, B IS THE MAIN DIAGONAL
!     * AND C IS THE UPPER DIAGONAL.
!     * ILEV = NUMBER OF MODEL LEVELS,
!     * LEVS = NUMBER OF MOISTURE LEVELS.

      IMPLICIT REAL   (A-H,O-Z), &
      INTEGER (I-N)
      REAL   A   (ILG,ILEV),B   (ILG,ILEV),C   (ILG,ILEV),RKQ (ILG,ILEV)
      REAL   SHTJ(ILG, LEV),SHJ (ILG,ILEV),DSHJ(ILG,ILEV)
      REAL   TF  (ILG,ILEV),THL (ILG) ,    CL (ILG),EF (ILG),CDVLH (ILG)
!-----------------------------------------------------------------------
      MSG=ILEV-LEVS
      L=MSG+1
      M=L+1

      DO 50 I=IL1,IL2
         OVDS   = (GRAV*SHTJ(I,M)/RGAS)**2 &
                 /( DSHJ(I,L) * (SHJ(I,M)-SHJ(I,L)) )
         C(I,L) = OVDS*RKQ(I,M)*(1./TF(I,M))**2
   50 CONTINUE

      DO 75 I=IL1,IL2
         B(I,L) = -C(I,L)
   75 CONTINUE

      DO 100 L=MSG+2,ILEV
      DO 100 I=IL1,IL2
         OVDS   = (GRAV*SHTJ(I,L)/RGAS)**2 &
                 /( (SHJ(I,L)-SHJ(I,L-1)) * DSHJ(I,L) )
         A(I,L) = OVDS*(1./TF(I,L))**2*RKQ(I,L)
  100 CONTINUE

      DO 200 L=MSG+2,ILEV-1
      DO 200 I=IL1,IL2
         D      = DSHJ(I,L+1) / DSHJ(I,L)
         C(I,L) = A(I,L+1)*D
         B(I,L) =-A(I,L+1)*D - A(I,L)
  200 CONTINUE

      L=ILEV
      DO 250 I=IL1,IL2
         CL(I)=GRAV*SHJ(I,L)*EF(I)*CDVLH(I)/(RGAS*THL(I)*DSHJ(I,L))
  250 CONTINUE

      DO 300 I=IL1,IL2
         B(I,L) = -A(I,L) -CL(I)
  300 CONTINUE

!     * DEFINE MATRIX TO INVERT = I-2*DT*MAT(A,B,C).

      DO 500 L=MSG+1,ILEV-1
      DO 500 I=IL1,IL2
         A(I,L+1) = -TODT*A(I,L+1)
         C(I,L  ) = -TODT*C(I,L  )
  500 CONTINUE

      DO 550 L=MSG+1,ILEV
      DO 550 I=IL1,IL2
         B(I,L) = 1.-TODT*B(I,L)
  550 CONTINUE

      RETURN
      END
      SUBROUTINE IMPLVD7 (A,B,C, CL,X,XG, IL1,IL2,ILG,ILEV,TODT, &
                          TEND,DELSIG,RAUS,WORK,VINT)

!     * DEC 07/89 - D.VERSEGHY: SAME AS PREVIOUS VERSION IMPLVD6
!     *                         EXCEPT FOR REMOVAL OF UPDATE OF
!     *                         SURFACE FLUXES.

!     * CALCULATE TENDENCIES DUE TO VERTICAL DIFFUSION IN HYBRID MODEL.
!     * THE SCHEME IS IMPLICIT BACKWARD.
!     * A,B,C ARE RESPECTIVELY THE LOWER, MAIN AND UPPER DIAG.

      IMPLICIT REAL   (A-H,O-Z), &
      INTEGER (I-N)

      REAL   A     (ILG,ILEV),B     (ILG,ILEV),C     (ILG,ILEV)
      REAL   X     (ILG,ILEV),WORK  (ILG,ILEV)
      REAL   CL    (ILG),     XG    (ILG),     VINT(ILG)
      REAL   TEND  (ILG,ILEV),DELSIG(ILG,ILEV),RAUS  (ILG)
!-----------------------------------------------------------------------
!     * GET X+ (IN ARRAY TEND) FROM X-. SAVE X(I,ILEV) IN VINT(I).

      DO 100 I=IL1,IL2
         VINT(I)   = X(I,ILEV)
         X(I,ILEV) = X(I,ILEV) +TODT*CL(I)*XG(I)
  100 CONTINUE

      CALL VROSSR(TEND, A,B,C,X,WORK,ILG,IL1,IL2,ILEV)

      DO 150 I=IL1,IL2
         X(I,ILEV) = VINT(I)
  150 CONTINUE

!     * GET TENDENCY FROM X- AND X+.

      DO 200 L=1,ILEV
      DO 200 I=IL1,IL2
         TEND(I,L) = (TEND(I,L)-X(I,L))*(1./TODT)
  200 CONTINUE

!     * CALCULATE VERTICAL INTEGRAL VINT.

      DO 300 I=IL1,IL2
         VINT(I) = 0.
  300 CONTINUE

      DO 400 L=1,ILEV
      DO 400 I=IL1,IL2
         VINT(I) = VINT(I) +TEND(I,L)*DELSIG(I,L)
  400 CONTINUE

      RETURN
      END
      SUBROUTINE VROSSR(P, A,B,C,D,DELTA,ILG,IL1,IL2,M)

!     * CCRN MARS 18/85 - B.DUGAS. (VECTORISER...)
!     * PROGRAMMER  ANDREW STANIFORTH, RPN.
!     * SOLVE THE TRI-DIAGONAL MATRIX PROBLEM BELOW.

!  ** **                                      ****  ****     ***    **** &
!  ** **                                      ****  ****     ***    ****
!  ** B(1),C(1), 0  , 0  , 0  , - - - -  ,  0   **  **  P(1)  **    **
!  ** A(2),B(2),C(2), 0  , 0  , - - - -  ,  0   **  **  P(2)  **    **
!  **  0  ,A(3),B(3),C(3), 0  , - - - -  ,  0   **  **  P(3)  **    **
!  **  0  , 0  ,A(4),B(4),C(4), - - - -  ,  0   **  **  P(4)  **    **
!  **  0  , 0  , 0  ,A(5),B(5), - - - -  ,  0   **  **  P(5)  ** -- **
!  **  -                                    -   **  **    -   ** -- **
!  **  -                                    -   **  **    -   **    **
!  **  0  , - - , 0 ,A(M-2),B(M-2),C(M-2),  0   **  ** P(M-2) **    ** D
!  **  0  , - - , 0 ,  0   ,A(M-1),B(M-1),C(M-1)**  ** P(M-1) **    ** D
!  **  0  , - - , 0 ,  0   ,  0   , A(M) , B(M) **  **  P(M)  **    ** &
!  ** *                                       ****  ****    ****    **** &
!  ** *                                       ****  ****    ****    ****


!     * THE ROUTINE SOLVES IL2-IL1+1 OF THESE PROBLEMS SIMULTANIOUSLY,
!     * THAT IS, IT DOES ONE LATITUDE CIRCLE AT A TIME.
!     * DELTA IS A WORKING ARRAY OF DIMENSION ILG X M.

      IMPLICIT REAL   (A-H,O-Z), &
      INTEGER (I-N)

!      REAL   P(ILG,1),A(ILG,1),B(ILG,1),C(ILG,1),D(ILG,1)
!      REAL   DELTA(ILG,1)
      REAL   P(ILG,M),A(ILG,M),B(ILG,M),C(ILG,M),D(ILG,M)
      REAL   DELTA(ILG,M)
!-----------------------------------------------------------------------
      DO 10 I=IL1,IL2
         C(I,M)=0.
   10 CONTINUE

      DO 30 I=IL1,IL2
         X         =1./B(I,1)
         P(I,1)    =-C(I,1)*X
         DELTA(I,1)=D(I,1)*X
   30 CONTINUE

      DO 50 L=2,M
         DO 50 I=IL1,IL2
            X         =1./(B(I,L)+A(I,L)*P(I,L-1))
            P(I,L)    =-C(I,L)*X
            DELTA(I,L)=(D(I,L)-A(I,L)*DELTA(I,L-1))*X
   50 CONTINUE

      DO 60 I=IL1,IL2
         P(I,M)=DELTA(I,M)
   60 CONTINUE

      DO 70 L=M-1,1,-1
         DO 70 I=IL1,IL2
            P(I,L)=P(I,L)*P(I,L+1)+DELTA(I,L)
   70 CONTINUE

      RETURN
      END
      SUBROUTINE VADV(DXDT,XROW,DTDT,THROW,DQDT,QROW,DQLDT,QLWC, &
                      DUDT,U,W,ZF,ZH,PH,DP,ALMC,ALMX, &
                      DT,IL1,IL2,ILG,ILEV,ILEVP1,MSGP,NTRAC)
!
      USE SDPHYS
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL, INTENT(IN), DIMENSION(ILG,ILEV) :: ZF,ZH,PH,DP,ALMC,ALMX
      REAL, DIMENSION(ILG,ILEVP1), INTENT(INOUT) :: THROW,QROW
      REAL, DIMENSION(ILG,ILEV), INTENT(IN) :: QLWC
      REAL, DIMENSION(ILG,ILEV), INTENT(INOUT) :: U,W
      REAL, DIMENSION(ILG,ILEV,NTRAC) :: RKX
      REAL, DIMENSION(ILG,ILEVP1,NTRAC), INTENT(IN) :: XROW
      REAL, DIMENSION(ILG,ILEVP1,NTRAC), INTENT(OUT) :: DXDT
      REAL, DIMENSION(ILG,ILEVP1), INTENT(OUT) :: DQDT,DTDT
      REAL, DIMENSION(ILG,ILEV), INTENT(OUT) :: DQLDT
      REAL, DIMENSION(ILG,ILEV), INTENT(OUT) :: DUDT
      REAL, DIMENSION(ILG,ILEV) :: QT,HMN,WF
      REAL, DIMENSION(ILG,ILEV) :: QTN, HMNN, CVAR, ZCRAUT, QCWVAR, &
                                   CVSG, CVDU, ZFRAC, QC, VDPQ, &
                                   VDPH, VDPU, ZCLF
      REAL, DIMENSION(ILG,ILEV,NTRAC) :: VDPX
      REAL, DIMENSION(ILG) :: PMBS, RRL, CPM, ALMIX, DHLDZ, DRWDZ, &
                              FLAGEST, QCW, SSH, WGT, DZ, TMP
!
!     * TOTAL WATER MIXING RATIO AND CLOUD WATER STATIC ENERGY.
!
      QT=QROW(:,MSGP:ILEVP1)+QLWC
      HMN=CPRES*THROW(:,MSGP:ILEVP1)+GRAV*ZH-RL*QLWC
!
!     * VERTICAL WIND AT GRID CELL INTERFACES.
!
      WF(:,ILEV)=0.
      DO L=2,ILEV
        WGT(:)=(ZF(:,L)-ZH(:,L))/(ZH(:,L-1)-ZH(:,L))
        WF(:,L-1)=(1.-WGT)*W(:,L)+WGT*W(:,L-1)
      ENDDO
!
!     * ADVECT WATER MIXING RATIO, CLOUD WATER STATIC ENERGY VERTICALLY,
!     * WINDS, AND TRACERS USING EULER FORWARD DIFFERENCES.
!
      VDPQ(:,ILEV)=0.
      VDPH(:,ILEV)=0.
      VDPU(:,ILEV)=0.
      VDPX(:,ILEV,:)=0.
      DO L=3,ILEV
        WHERE ( WF(:,L-1) <= 0. )
          DZ=ZF(:,L-2)-ZF(:,L-1)
          TMP=MAX(WF(:,L-1)*DT/DZ,-1.)*DP(:,L-1)
          VDPQ(:,L-1)=QT(:,L-1)*TMP
          VDPH(:,L-1)=HMN(:,L-1)*TMP
          VDPU(:,L-1)=U(:,L-1)*TMP
        ELSEWHERE
          DZ=ZF(:,L-1)-ZF(:,L)
          TMP=MIN(WF(:,L-1)*DT/DZ,1.)*DP(:,L)
          VDPQ(:,L-1)=QT(:,L)*TMP
          VDPH(:,L-1)=HMN(:,L)*TMP
          VDPU(:,L-1)=U(:,L)*TMP
        ENDWHERE
        DO N=1,NTRAC
          WHERE ( WF(:,L-1) <= 0. )
            DZ=ZF(:,L-2)-ZF(:,L-1)
            TMP=MAX(WF(:,L-1)*DT/DZ,-1.)*DP(:,L-1)
            VDPX(:,L-1,N)=XROW(:,L-1,N)*TMP
          ELSEWHERE
            DZ=ZF(:,L-1)-ZF(:,L)
            TMP=MIN(WF(:,L-1)*DT/DZ,1.)*DP(:,L)
            VDPX(:,L-1,N)=XROW(:,L,N)*TMP
          ENDWHERE
        ENDDO
      ENDDO
      L=2
      VDPQ(:,1)=0.
      VDPH(:,1)=0.
      VDPU(:,1)=0.
      VDPX(:,1,:)=0.
      WHERE ( WF(:,L-1) > 0. )
        DZ=ZF(:,L-1)-ZF(:,L)
        TMP=MIN(WF(:,L-1)*DT/DZ,1.)*DP(:,L)
        VDPQ(:,L-1)=QT(:,L)*TMP
        VDPH(:,L-1)=HMN(:,L)*TMP
        VDPU(:,L-1)=U(:,L)*TMP
      ENDWHERE
      DO N=1,NTRAC
        WHERE ( WF(:,L-1) > 0. )
          DZ=ZF(:,L-1)-ZF(:,L)
          TMP=MIN(WF(:,L-1)*DT/DZ,1.)*DP(:,L)
          VDPX(:,L-1,N)=XROW(:,L,N)*TMP
        ENDWHERE
      ENDDO
      L=1
      QT(:,L)=QT(:,L)+VDPQ(:,L)/DP(:,L)
      HMN(:,L)=HMN(:,L)+VDPH(:,L)/DP(:,L)
      DUDT(:,L)=VDPU(:,L)/DP(:,L)/DT
      DO N=1,NTRAC
        DXDT(:,L,N)=VDPX(:,L,N)/DP(:,L)/DT
      ENDDO
      DO L=2,ILEV
        QT(:,L)=QT(:,L)-(VDPQ(:,L-1)-VDPQ(:,L))/DP(:,L)
        HMN(:,L)=HMN(:,L)-(VDPH(:,L-1)-VDPH(:,L))/DP(:,L)
        DUDT(:,L)=-(VDPU(:,L-1)-VDPU(:,L))/DP(:,L)/DT
        DO N=1,NTRAC
          DXDT(:,L,N)=-(VDPX(:,L-1,N)-VDPX(:,L,N))/DP(:,L)/DT
        ENDDO
      ENDDO
!
!     * TEMPERATURE AND WATER VAPOUR MIXING RATIO FROM STATISTICAL
!     * CLOUD SCHEME.
!
      FACTE=1./EPS1-1.
      I2NDIE=1
      FLAGEST=1.
      CVSG=0.
      CVDU=0.
      ZFRAC=0.
      ICVSG=1
      ISUBG=1
      ICALL=1
      DO JK=1,ILEV
        DO IL=1,ILG
          PMBS(IL)=0.01*PH(IL,JK)
          RRL(IL)=RL
          CPM(IL)=CPRES
          ALMIX(IL)=MAX(ALMC(IL,JK),ALMX(IL,JK),10.)
          IF(JK.GT.1)                              THEN
            DZT      =ZH  (IL,JK-1)-ZH (IL,JK)
            DHLDZ(IL)=(HMN(IL,JK-1)-HMN(IL,JK))/DZT
            DRWDZ(IL)=(QT (IL,JK-1)-QT (IL,JK))/DZT
          ELSE
            DHLDZ(IL)=0.
            DRWDZ(IL)=0.
          ENDIF
        ENDDO
        CALL STATCLD (QCW,ZCLF(1,JK),CVAR(1,JK),ZCRAUT(1,JK), &
                      QCWVAR(1,JK),SSH,CVSG(1,JK),QT(1,JK), &
                      HMN(1,JK),ZFRAC(1,JK),CPM, &
                      PMBS,ZH(1,JK),RRL,CVDU(1,JK),FLAGEST, &
                      ALMIX,DHLDZ,DRWDZ,I2NDIE, &
                      DT,ILEV,ILG,1,ILG,ICVSG,ISUBG, &
                      JK,ICALL)
!
        DO IL=1,ILG
          DTDT(IL,JK)=((HMN(IL,JK)-GRAV*ZH(IL,JK)+RRL(IL)*QCW(IL)) &
                       /CPM(IL)-THROW(IL,JK))/DT
          DQDT(IL,JK)=((QT(IL,JK)-QCW(IL))-QROW(IL,JK))/DT
          DQLDT(IL,JK)=(QCW(IL)-QLWC(IL,JK))/DT
        ENDDO
      ENDDO
!
      END SUBROUTINE VADV
      SUBROUTINE XIT(NAME,N)
!
!     * OCT 01/92 - E.CHAN. (CHANGE STOP 1 TO STOP)
!     * JUN 10/91 - E.CHAN. (TRANSLATE HOLLERITH LITERALS AND
!     *                      DIMENSION STRINGS)
!
!     * OCT 10/78 - J.D.HENDERSON.
!     * TERMINATES A PROGRAM BY PRINTING THE PROGRAM NAME AND
!     * A LINE ACROSS THE PAGE FOLLOWED BY A NUMBER N.
!
!     * N.GE.0 IS FOR A NORMAL END. THE LINE IS DASHED.
!     * NORMAL ENDS TERMINATE WITH   STOP.
!
!     * N.LT.0 IS FOR AN ABNORMAL END. THE LINE IS DOTTED.
!     * IF N IS LESS THAN -100 THE PROGRAM SIMPLY TERMINATES.
!     * OTHERWISE IF N IS LESS THAN ZERO THE PROGRAM ABORTS.
!
      IMPLICIT REAL (A-H,O-Z), &
      INTEGER (I-N)
      CHARACTER*(*) NAME
      CHARACTER*8   NAME8, DASH, STAR
!
      DATA DASH /'--------'/, STAR /'********'/
!---------------------------------------------------------------------
!
      NAME8 = NAME
      IF(N.GE.0) WRITE(6,6010) DASH,NAME8,(DASH,I=1,9),N
!
      IF(N.LT.0) WRITE(6,6010) STAR,NAME8,(STAR,I=1,9),N
!
      IF ( N.GE.0 .OR. N.LT.-100 ) THEN
        STOP
      ELSE
        CALL ABORT
      ENDIF
!
!---------------------------------------------------------------------
 6010 FORMAT('0',A8,'  END  ',A8,9A8,I8)
      END
      SUBROUTINE WRN(NAME,N)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     PRINTS OUT WARNING MESSAGE.
!
!     HISTORY:
!     --------
!     * APR 10/2006 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
      CHARACTER*(*) NAME
      CHARACTER*4   DASH, STAR
      CHARACTER*8   NAME8
!
      DATA DASH /'----'/, STAR /'****'/
!---------------------------------------------------------------------
!
      NAME8 = NAME
      IF(N.GE.0) WRITE(6,6010) (DASH,I=1,2),NAME8,(DASH,I=1,17),N
!
      IF(N.LT.0) WRITE(6,6010) (STAR,I=1,2),NAME8,(STAR,I=1,17),N
!
!---------------------------------------------------------------------
 6010 FORMAT('0',2A4,'  WARNING  ',A8,17A4,I8)
!
      END SUBROUTINE WRN
