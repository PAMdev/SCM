      PROGRAM PREPROC
!
      USE FPDEF           ! FLOATING POINT PRECISION INFORMATION
      USE RUNINFO         ! ATMOSPHERIC PROPERTIES AND OTHER INFORMATION
                          ! ABOUT GENERAL SETUP OF SIMULATION.
      USE PSIZES          ! CONTAINS DIMENSIONAL PARAMETERS FOR
                          ! ATMOSPHERIC MODEL
      USE IODAT           ! CONTAINS VARIABLES THAT ARE WRITTEN TO THE
                          ! OUTPUT FILE
      USE IODATIF         ! CONTAINS I/O SUBROUTINE INTERFACE DEFITIONS
      USE SDPHYS          ! PHYSICAL CONSTANTS
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=50) :: CFILE,CFILX
!
      REAL, ALLOCATABLE, DIMENSION(:,:) :: QR, QRV, QRC, RI, &
                                           RELHI, RELHIB, QRB, QRVB, &
                                           WG, TMP1, TMP2, TMP3, &
                                           ESW, RS, PHI, PFI, THI, &
                                           TFI, DP, VITRM, SVI, USPD, &
                                           ZCLF, ZCDNROW, USPDF, &
                                           PERTA, PERRV, TFIX, PFIX, &
                                           RHOA, TFIXX, THIXX
      REAL, ALLOCATABLE, DIMENSION(:) :: SMFRAC, THLIQG, ZO, &
                                         MODTIM, GTROW, ZHI, ZFI, &
                                         ZFIX, USTAR, PBLI, DZ, RHIP
!
      REAL, PARAMETER :: YSPDAY=24.*60.*60.
!
!     * INPUT/OUTPUT FILE UNIT NUMBERS.
!
      DATA NUPAR /5 /
!
!-----------------------------------------------------------------------
!     * OPEN AND READ FILE THAT CONTAINS BASIC INFORMATION ABOUT THE
!     * AIR PROPERTIES.
!
      OPEN(NUPAR,FILE='SCMPRE')
!
!-----------------------------------------------------------------------
!     * READ IN BASIC INFORMATION ABOUT SIMULATION (E.G. NAME, VERTICAL
!     * LEVELS, TIME STEP ETC.).
!
      MODL%NTSTP=INAX
      READ(NUPAR,NML=MODNML)
!
!-----------------------------------------------------------------------
!     * GRID DIMENSIONS.
!
      ILEV=MODL%PLEV   ! NUMBER OF VERTICAL GRID CELLS FOR ATMOSPHERE
      ILEVP1=ILEV+1
      ITS=MODL%NTSTP   ! NUMBER OF TIME STEPS
!
!-----------------------------------------------------------------------
!     * READ FILE THAT CONTAINS INFORMATION ABOUT AIR PROPERTIES.
!
      AIR%NPER=0
      AIR%CLDF=0.
      AIR%DTG=0.
      AIR%DTA=0.
      AIR%DRV=0.
      AIR%DTGON=1
      AIR%DTGOFF=1
      AIR%PERON=1
      AIR%PEROFF=1
      AIR%ZHI=0.
      REWIND(NUPAR)
      READ(NUPAR,NML=AIRNML)
      REWIND(NUPAR)
!
!-----------------------------------------------------------------------
!     * CHECK INPUT INFORMATION AND TRIM.
!
      CALL CHKIO
      CFILE=TRIM(MODL%RUNNAME)
      IF ( MODL%NTSTP == INAX ) CALL XIT('MAIN',-1)
      DT=MODL%DT
!
!-----------------------------------------------------------------------
!     * ALLOCATE MEMORY.
!
      ALLOCATE(MODTIM (ITS))
      ALLOCATE(DZ     (ILEV))
      ALLOCATE(ZHI    (ILEV))
      ALLOCATE(ZFI    (0:ILEV))
      ALLOCATE(QR     (ILEV,ITS))
      ALLOCATE(QRV    (ILEV,ITS))
      ALLOCATE(QRC    (ILEV,ITS))
      ALLOCATE(ZCDNROW(ILEV,ITS))
      ALLOCATE(RELHI  (ILEV,ITS))
      ALLOCATE(RELHIB (ILEV,ITS))
      ALLOCATE(QRB    (ILEV,ITS))
      ALLOCATE(QRVB   (ILEV,ITS))
      ALLOCATE(WG     (ILEV,ITS))
      ALLOCATE(TMP1   (ILEV,ITS))
      ALLOCATE(TMP2   (ILEV,ITS))
      ALLOCATE(TMP3   (ILEV,ITS))
      ALLOCATE(ESW    (ILEV,ITS))
      ALLOCATE(RS     (ILEV,ITS))
      ALLOCATE(RHOA   (ILEV,ITS))
      ALLOCATE(PHI    (ILEV,ITS))
      ALLOCATE(PFI    (0:ILEV,ITS))
      ALLOCATE(PFIX   (1:ILEVP1,ITS))
      ALLOCATE(DP     (ILEV,ITS))
      ALLOCATE(VITRM  (ILEV,ITS))
      ALLOCATE(THI    (ILEV,ITS))
      ALLOCATE(RHIP   (ILEV))
      ALLOCATE(TFI    (0:ILEV,ITS))
      ALLOCATE(TFIXX  (0:ILEV,ITS))
      ALLOCATE(TFIX   (1:ILEVP1,ITS))
      ALLOCATE(ZFIX   (1:ILEVP1))
      ALLOCATE(SVI    (ILEV,ITS))
      ALLOCATE(ZCLF   (ILEV,ITS))
      ALLOCATE(USPD   (ILEV,ITS))
      ALLOCATE(USPDF  (ILEV,ITS))
      ALLOCATE(RI     (ILEV,ITS))
      ALLOCATE(PERTA  (ILEV,ITS))
      ALLOCATE(PERRV  (ILEV,ITS))
!
      ALLOCATE(USTAR  (ITS))
      ALLOCATE(ZO     (ITS))
      ALLOCATE(THLIQG (ITS))
      ALLOCATE(GTROW  (ITS))
      ALLOCATE(PBLI   (ITS))
!
      ALLOCATE(THIXX(ILEV,ITS))
!
!-----------------------------------------------------------------------
!     * MODEL TIME.
!
      DO IZ=1,ITS
        MODTIM(IZ)=REAL(IZ-1)*DT
      ENDDO
!
!     * VERTICAL LEVEL HEIGHTS (IN METRES). "F" REFERS TO GRID CELL
!     * INTERFACES, "H" TO MID-POINTS.
!
      DZ=MODL%DZSFC
      ZFI(ILEV)=AIR%ZHI
      ZFI(ILEV-1)=ZFI(ILEV)+1.5*DZ(ILEV)
      DO IZ=ILEV-2,0,-1
        ZFI(IZ)=ZFI(IZ+1)+DZ(IZ+1)
      ENDDO
      ZHI(ILEV)=DZ(ILEV)
      DO IZ=ILEV-1,1,-1
        ZHI(IZ)=ZHI(IZ+1)+DZ(IZ+1)
      ENDDO
!
!-----------------------------------------------------------------------
!     * READ PROFILES OF TEMPERATURE, RELATIVE HUMIDITY, AND HORIZONTAL
!     * WIND SPEED. IT IS ASSUMED THAT CONTINUOUS VERTICAL PROFILES ARE
!     * AVAILABLE ABOVE A MIMINUM AND BELOW A MAXIMUM ALTITUDE, ACCORDING
!     * TO AIRCRAFT ALTITUDE RANGE.
!
      OPEN(33,FILE='PROFDAT')
      READ(33,'(1X)')
      IZT=0
      DO IZ=1,2*ILEV
        READ(33,'(F8.0)',END=777) ZTMP
        IF ( ZTMP > MODL%PLEV*MODL%DZSFC+1.E-20 ) GOTO 777
        IZT=IZT+1
        IF ( IZ == 1 ) THEN
          ZHIN=ZTMP
        ELSE
          IF ( ABS((ZTMP-ZTMPP)-MODL%DZSFC) > 1.E-20 ) &
            CALL XIT('MAIN',-2)
        ENDIF
        ZTMPP=ZTMP
      ENDDO
 777  CONTINUE
      ILEVI=IZT
      REWIND(33)
      READ(33,'(1X)')
!
!     * CHECK IF POINT IS EITHER A GRID POINT VALUE
!
      IF ( .NOT. MOD(ZHIN,MODL%DZSFC) < 1.E-20 ) CALL XIT('MAIN',-3)
      IF ( MOD(ZHIN,MODL%DZSFC) < 1.E-20 ) THEN
        IZT=NINT(ZHIN/MODL%DZSFC)-1
      ENDIF
      INUM=ILEVI
      IF ( INUM < 1 ) CALL XIT('MAIN',-4)
      IF ( INUM+IZT < MODL%PLEV ) THEN
        PRINT*,'REQUESTED NUMBER OF LEVELS = ',MODL%PLEV
        PRINT*,'AVAILABLE NUMBER OF LEVELS = ',INUM+IZT
        CALL XIT('MAIN',-5)
      ENDIF
!
!     * READ FIRST INTERFACE VALUE, IF AVAILABLE
!
      IZT=ILEV-IZT
      DO ILX=1,INUM
!
!       * READ GRID POINT VALUE
!
        READ(33,'(F8.3,3X,F7.3,4(8X,E11.4))') &
                ZHIP,THIP,RHIPT,UIP,ACLIP,ACDNC
        THI(IZT,:)=THIP
        USPD(IZT,:)=UIP
        RHIP(IZT)=MIN(RHIPT/100.,1.)
        QRC(IZT,:)=MAX(0.,ACLIP)*1.E-03
        ZCDNROW(IZT,:)=MAX(0.,ACDNC)*1.E+06
!
!       * LINEAR INTERPOLATION BETWEEN LOWEST AVAILABLE LEVEL AND GROUND
!
        IF ( ILX==1 ) THEN
          DO IZ=IZT+1,ILEV
            THI(IZ,:)=AIR%TEMPI+(THI(IZT,:)-AIR%TEMPI) &
                             *(ZHI(IZ)-AIR%ZHI)/(ZHI(IZT)-AIR%ZHI)
            USPD(IZ,:)=USPD(IZT,:) &
                             *(ZHI(IZ)-AIR%ZHI)/(ZHI(IZT)-AIR%ZHI)
            RHIP(IZ)=RHIP(IZT)
            QRC(IZ,:)=QRC(IZT,:)
            ZCDNROW(IZ,:)=ZCDNROW(IZT,:)
          ENDDO
        ENDIF
        IZT=IZT-1
      ENDDO
!
!     * SMOOTH WIND SPEED PROFILE, IF APPLICABLE.
!
      IOFF=0
      DO IZ=1,ILEV
        IZTT=0
        USPDF(IZ,:)=0.
        DO IZT=IZ-IOFF,IZ+IOFF
          IF ( IZT <= ILEV .AND. IZT >= 1) THEN
            USPDF(IZ,:)=USPDF(IZ,:)+USPD(IZT,:)
            IZTT=IZTT+1
          ENDIF
        ENDDO
        USPDF(IZ,:)=USPDF(IZ,:)/REAL(IZTT)
      ENDDO
      USPD=USPDF
!
!     * TEMPERATURE (IN KELVIN).
!
      TFI(ILEV,:)=AIR%TEMPI
      DO IZ=ILEV-1,1,-1
        WGT=(ZFI(IZ)-ZHI(IZ+1))/(ZHI(IZ)-ZHI(IZ+1))
        TFI(IZ,:)=WGT*THI(IZ,:)+(1.-WGT)*THI(IZ+1,:)
      ENDDO
      TFI(0,:)=THI(1,:)
!
!     * VERTICAL PRESSURE LEVELS, BASED ON HYDROSTATIC ASSUMPTION AND
!     * IDEAL GAS LAW (IN PASCAL).
!
      PFI(ILEV,:)=AIR%PRESI
      DO IZ=ILEV-1,0,-1
        PFI(IZ,:)=PFI(IZ+1,:)*EXP(-GRAV*DZ(IZ+1)/(RGAS*THI(IZ+1,:)))
      ENDDO
      PHI(ILEV,:)=PFI(ILEV,:)*EXP(-GRAV*(ZHI(ILEV)-ZFI(ILEV)) &
                                 /(RGAS*TFI(ILEV,:)))
      DO IZ=ILEV-1,1,-1
        PHI(IZ,:)=PHI(IZ+1,:)*EXP(-GRAV*(ZHI(IZ)-ZHI(IZ+1)) &
                                 /(RGAS*TFI(IZ,:)))
      ENDDO
      RHOA=PHI/(RGAS*THI)
!
!     * PRESSURE LEVEL DEPTH AND SCALING FACTOR FOR CALCULATION
!     * OF VERTICAL INTEGRALS.
!
      DO IZ=1,ILEV
        DP(IZ,:)=PFI(IZ,:)-PFI(IZ-1,:)
      ENDDO
      VITRM=DP/GRAV
!
!     * SATURATION MIXING RATIO (IN KG/KG).
!
      TMP3=RW1+RW2/THI
      TMP1=EXP(TMP3)
      TMP3=RW3
      TMP2=THI**TMP3
      ESW=1.E+02*TMP1*TMP2
      RS=EPS1*ESW/( PHI-ESW )
!
!     * INITIAL SUPERSATURATION IN CLOUDY PART OF THE GRID CELLS.
!     * USED TO GENERATE TOTAL WATER MIXING RATIO IN THE FOLLOWING.
!
      SVI=0.
!
!     * CORRESPONDING RELATIVE HUMIDITY FOR CLOUDY CONDITIONS.
!
      DO IZ=ILEV,1,-1
        RELHI(IZ,:)=SVI(IZ,:)+RHIP(IZ)
      ENDDO
!
!     * TOTAL WATER MIXING, CLOUD WATER AND WATER VAPOUR
!     * MIXING RATIO IN THE CLOUDY PARTS OF THE GRID CELLS.
!
      QRV=RS*RELHI/(1.+RS*(1.-RELHI)/EPS1)
      QRC=QRC/RHOA
      QR=QRV+QRC
!
!     * FRACTION OF GRID CELL IN WHICH LIQUID CLOUD WATER OCCURS.
!
      ZCLF=0.
!
!     * ADD SPECIFIED TEMPERATURE AND MOISTURE PERTURBATIONS.
!
      PERTA=0.
      PERRV=0.
      DO NC=1,AIR%NPER
        IF ( ABS(AIR%DTA(NC)) > 1.E-09 ) &
          PERTA(AIR%LVTOP(NC):AIR%LVBOT(NC), &
                 AIR%PERON(NC):AIR%PEROFF(NC))=AIR%DTA(NC)/YSPDAY
        IF ( ABS(AIR%DRV(NC)) > 1.E-09 ) &
          PERRV(AIR%LVTOP(NC):AIR%LVBOT(NC), &
                 AIR%PERON(NC):AIR%PEROFF(NC))=AIR%DRV(NC)/YSPDAY
      ENDDO
!
!     * LONGWAVE COOLING OF THE ATMOSPHERE (K/SEC)
!
      PERTA=PERTA-0.1/YSPDAY
!
!     * RESOLVED (WG) VERTICAL VELOCITY (M/SEC).
!
      DO IZ=1,ILEV
        WG(IZ,:)=-0.E-09*ZHI(IZ)
      ENDDO
!
!     * FRICTION VELOCITY (M/SEC, USED TO INITIALIZE CALCULATIONS).
!
      USTAR=0.2
!
!     * RICHARDSON NUMBER (DIMENSIONLESS, USED TO INITIALIZE CALCULATIONS).
!
      RI=0.
!
!     * BOUNDARY LAYER TOP LEVEL INDEX (DIMENSIONLESS, USED TO INITIALIAZE CALCULATIONS).
!
      PBLI=ILEV
!
!     * AERODYNAMIC SURFACE ROUGHNESS (M, ONLY USED FOR LAND SURFACES).
!
      ZO=0.0001
!
!     * SELECT MAXIMUM CLOUD DROPLET NUMBER CONCENTRATION.
!
      DO IT=1,ITS
        ACDNC=0.
        DO IZ=1,ILEV
          IF ( ZCDNROW(IZ,IT) > ACDNC ) ACDNC=ZCDNROW(IZ,IT)
        ENDDO
        ZCDNROW(:,IT)=ACDNC
      ENDDO
!
!-----------------------------------------------------------------------
!     * LAND-SURFACE PROPERTIES.
!
      GTROW=AIR%TEMPI    ! SURFACE TEMPERATURE (K, FROM CLASS).
      DO NC=1,AIR%NPER
        GTROW(AIR%DTGON(NC):AIR%DTGOFF(NC))= &
                      GTROW(AIR%DTGON(NC):AIR%DTGOFF(NC))+AIR%DTG(NC)
      ENDDO
!
!     * SOIL WATER CONTENT (KG/KG).
!
      THLIQG=1.
!
!=======================================================================
!     * OUTPUT.
!=======================================================================
!     * DEFINITION OF FIELD DIMENSIONS.
!
      ZFIX(1:ILEVP1)=ZFI(0:ILEV)
      CALL SETNCDIMPRE(ZHI,ZFIX,MODTIM,ILEV,ILEVP1,ITS)
!
!     * INITIALIZE INPUT AND OUTPUT ARRAYS.
!
      CALL INITIO(.FALSE.,.FALSE.)
!
!     * WRITE ATMOSPHERIC BOUNDARY CONDITIONS FOR CLOUD AND PLA
!     * PARAMETERIZATIONS.
!
      CALL W0DFLD('delt',DT)
!
      PFIX(1:ILEVP1,:)=PFI(0:ILEV,:)
      CALL W2DFLD('paf',PFIX,ILEVP1,ITS)
      CALL W2DFLD('pa',PHI,ILEV,ITS)
      TFIX(1:ILEVP1,:)=TFI(0:ILEV,:)
      CALL W2DFLD('taf',TFIX,ILEVP1,ITS)
      CALL W2DFLD('ta',THI,ILEV,ITS)
      CALL W2DFLD('ua',USPD,ILEV,ITS)
      CALL W2DFLD('wa',WG,ILEV,ITS)
      CALL W2DFLD('rv',QRV,ILEV,ITS)
      CALL W2DFLD('rl',QRC,ILEV,ITS)
      CALL W2DFLD('perta',PERTA,ILEV,ITS)
      CALL W2DFLD('perrv',PERRV,ILEV,ITS)
      CALL W1DFLD('rliqgnd',THLIQG,ITS)
      CALL W1DFLD('zo',ZO,ITS)
      CALL W1DFLD('tsurf',GTROW,ITS)
!
!     * WRITE BOUNDARY CONDITIONS TO OUTPUT FILE.
!
      CFILX=TRIM(CFILE)//'_bnd'
      CALL WNCDAT(CFILX)
!
!-----------------------------------------------------------------------
!     * WRITE INITIAL PROGNOSTIC FIELDS TO RESTART FILE.
!
      CALL W0DFLDRS('tstep',REAL(0))
      CALL W0DFLDRS('ustar',USTAR(1))
      CALL W1DFLDRS('ua',USPD(:,1),ILEV)
      CALL W1DFLDRS('ta',THI(:,1),ILEV)
      CALL W1DFLDRS('rv',QRV(:,1),ILEV)
      CALL W1DFLDRS('rl',QRC(:,1),ILEV)
      CALL W1DFLDRS('clf',ZCLF(:,1),ILEV)
      CALL W1DFLDRS('cldnc',ZCDNROW(:,1),ILEV)
      CALL W1DFLDRS('ri',RI(:,1),ILEV)
      CALL W0DFLDRS('indpbl',PBLI(1))
!
!     * WRITE THE DATA.
!
      CFILX=TRIM(CFILE)//'_tini'
      CALL WNCDATRS(CFILX)
!
      END PROGRAM PREPROC
