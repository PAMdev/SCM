      MODULE FPDEF
!
      INTEGER, PARAMETER :: R8 = selected_real_kind(12) ! 8 byte real
      INTEGER, PARAMETER :: R4 = selected_real_kind( 6) ! 4 byte real
      LOGICAL :: KDBL=.FALSE.
!
      END MODULE FPDEF
      PROGRAM ATMPROF
!
      IMPLICIT NONE
!     
!     * PARAMETERS FOR ATMOSPHERIC PROFILES.
!
      INTEGER, PARAMETER :: ILEV=50      ! NUMBER OF LEVELS
      REAL, PARAMETER :: ZFSFC=0.        ! HEIGHT OF THE SURFACE (m)
      REAL, PARAMETER :: TEMPSFC=279.    ! AIR TEMPERATURE AT THE SURFACE (K)
      REAL, PARAMETER :: PRESSFC=101300. ! AIR PRESSURE AT THE SURFACE (Pa)
      REAL, PARAMETER :: SCLR=-0.1       ! SUPERSATURATION IN CLEAR AIR
      REAL, PARAMETER :: SCLD=0.03       ! SUPERSATURATION IN CLOUDY AIR
      REAL, PARAMETER :: CLDF=1.         ! CLOUD FRACTION
      REAL, PARAMETER :: CDNC=50.        ! CLOUD DROPLET NUMBER CONCENTRATION (/CM3)
      REAL, PARAMETER :: DZ=20.          ! VERTICAL GRID SIZE (m)
      INTEGER, PARAMETER :: LVBOT=ILEV/2, LVTOP=1  ! VERTICAL EXTENT OF CLOUD
!
      REAL, PARAMETER :: GRAV   = 9.80616    ! GRAVITATIONAL CONTSTANT (M/S2)
      REAL, PARAMETER :: RGAS   = 287.04     ! GAS CONSTANT OF DRY AIR (J/KG/K)
      REAL, PARAMETER :: RGOCP  = 2./7. 
      REAL, PARAMETER :: CPRES  = RGAS/RGOCP ! HEAT CAPACITY AT CONSTANT PRESSURE
      REAL, PARAMETER :: EPS1   = 0.622      ! RATIO OF GAS CONSTANT FOR DRY
      REAL, PARAMETER :: RL     = 2.501E+06  ! LATENT HEAT OF VAPOURIZATION (J/KG)
      REAL, PARAMETER :: U0     = 1.         ! WIND SHEAR VELOCITY (m/s)
      REAL, PARAMETER :: Z0     = 1.         ! ROUGHNESS LENGTH (m)
!
!     * FITTING PARAMETERS FOR CALCULATION OF WATER VAPOUR SATURATION 
!     * MIXING RATIO.
!
      REAL, PARAMETER :: RW1    = 53.67957
      REAL, PARAMETER :: RW2    = -6743.769
      REAL, PARAMETER :: RW3    = -4.8451
      REAL, PARAMETER :: RI1    = 23.33086
      REAL, PARAMETER :: RI2    = -6111.72784
      REAL, PARAMETER :: RI3    = 0.15215
!
      REAL, DIMENSION(ILEV) :: TMP1,TMP2,TMP3
      REAL, DIMENSION(ILEV) :: ZHI,THI,PHI,DP,RHOAI,ESW,RS,SVI,RELHI, &
                               RELHIB,QR,QRC,QRV,QRB,QRVB,QTN,HMN,ZCLF, &
                               ZCDNC,ALWC1,UA
      REAL, DIMENSION(0:ILEV) :: ZFI,TFI,PFI
      LOGICAL, PARAMETER :: KDBL=.FALSE.
      INTEGER*4 :: LENG
      REAL :: ALPSR                     !LAPSE RATE
      INTEGER :: IFILE, IZ
!
!-----------------------------------------------------------------------
!     * VERTICAL LEVEL HEIGHTS (IN METRES). "F" REFERS TO GRID CELL 
!     * INTERFACES, "H" TO MID-POINTS.
!
      ZFI(ILEV)=ZFSFC
      DO IZ=ILEV-1,0,-1
        ZFI(IZ)=ZFI(ILEV)+REAL(ILEV-IZ)*DZ
      ENDDO
      DO IZ=1,ILEV
        ZHI(IZ)=.5*(ZFI(IZ)+ZFI(IZ-1))
      ENDDO
!
!     * LAPSE RATE.
!
      ALPSR=-GRAV/CPRES
!
!     * TEMPERATURE (IN KELVIN).
!
      TFI(ILEV)=TEMPSFC
      DO IZ=ILEV-1,0,-1
        TFI(IZ)=TFI(ILEV)+ALPSR*(ZFI(IZ)-ZFI(ILEV))
      ENDDO
      DO IZ=1,ILEV
        THI(IZ)=TFI(ILEV)+ALPSR*(ZHI(IZ)-ZFI(ILEV))
      ENDDO
!
!     * VERTICAL PRESSURE LEVELS, BASED ON HYDROSTATIC ASSUMPTION AND
!     * IDEAL GAS LAW (IN PASCAL).
!
      PFI(ILEV)=PRESSFC
      DO IZ=ILEV-1,0,-1
        PFI(IZ)=PFI(ILEV)*(TFI(IZ)/TFI(ILEV))**(-GRAV/(RGAS*ALPSR))
      ENDDO
      DO IZ=1,ILEV
        PHI(IZ)=PFI(ILEV)*(THI(IZ)/TFI(ILEV))**(-GRAV/(RGAS*ALPSR))
      ENDDO
!
!     * PRESSURE DIFFERENCE BETWEEN GRID CELL INTERFACE LEVELS.
!
      DO IZ=1,ILEV
        DP(IZ)=PFI(IZ)-PFI(IZ-1)
      ENDDO
!
!     * AIR DENSITY (IN KG/M**3).
!
      RHOAI=PHI/(RGAS*THI)
!
!     * SATURATION MIXING RATIO (IN KG/KG).
!
      LENG=ILEV
      TMP3=RW1+RW2/THI
      IF ( KDBL ) THEN
        CALL VEXP(TMP1,TMP3,LENG)
      ELSE
        CALL VSEXP(TMP1,TMP3,LENG)
      ENDIF
      TMP3=RW3
      IF ( KDBL) THEN
        CALL VPOW(TMP2,THI,TMP3,LENG)
      ELSE
        CALL VSPOW(TMP2,THI,TMP3,LENG)
      ENDIF
      ESW=1.E+02*TMP1*TMP2
      RS=EPS1*ESW/( PHI-ESW )
!
!     * INITIAL SUPERSATURATION IN CLOUDY PART OF THE GRID CELLS
!     * USE TO GENERATE TOTAL WATER MIXING RATIO IN THE FOLLOWING.
!
      SVI=SCLR
      SVI(LVTOP:LVBOT)=SCLD
!
!     * CORRESPONDING RELATIVE HUMIDITY FOR CLEAR AND CLOUDY 
!     * CONDITIONS.
!
      RELHIB=SCLR+1.
      RELHI=SVI+1.
!
!     * TOTAL WATER MIXING, CLOUD WATER AND WATER VAPOUR 
!     * MIXING RATIO (IN KG/KG) IN THE CLEAR RESP. CLOUDY PARTS OF
!     * THE GRID CELLS.
!
      QR=RS*RELHI/(1.+RS*(1.-RELHI)/EPS1)
      QRC=MAX(0.,QR-RS)
      QRV=QR-QRC
      QRB=RS*RELHIB/(1.+RS*(1.-RELHIB)/EPS1)
      QRVB=QRB
!
!     * TOTAL WATER MIXING RATIO AND LIQUID WATER STATIC ENERGY 
!     * IN CLOUDY PART OF THE GRID CELLS.
!
      QTN=QR
      HMN=CPRES*THI+GRAV*ZHI-RL*QRC
!
!     * CLOUD DROPLET NUMBER CONCENTRATION
!     * IN THE CLOUDY GRID CELLS
!
      ZCDNC=0.
      ZCDNC(LVTOP:LVBOT)=CDNC
!      
!     * HORIZONTAL WIND SPEED
!     * SIMPLE LOGARITHMIC WIND PROFILE
!      
      DO IZ=1,ILEV
        UA(IZ)=U0*LOG(ZHI(IZ)/Z0)
      ENDDO
!
!     * FRACTION OF GRID CELL IN WHICH LIQUID CLOUD WATER OCCURS.
!
      ZCLF=0.
      ZCLF(LVTOP:LVBOT)=CLDF
!
!     * NOW CALCULATE THE GRID-CELL AVERAGES.
!
      SVI  =(1.-ZCLF)*SCLR+ZCLF*SVI
      RELHI=(1.-ZCLF)*RELHIB+ZCLF*RELHI
      QRV  =(1.-ZCLF)*QRVB+ZCLF*QRV
      QR   =(1.-ZCLF)*QRB+ZCLF*QR
!
!     * OUTPUT FILE.
!
      IFILE=10
      OPEN(IFILE,FILE='OUT_SCM')
!
!     * ADDITIONAL FIELDS (REDUNDANT).
!
      ALWC1=QRC
!
!     * CONVERSION OF UNITS.
!
      QR=QR*1.E+03              ! kg/kg to g/kg
      RELHI=RELHI*1.E+02        ! fraction to percent
      PHI=PHI*1.E-02            ! Pa to hPa 
      ALWC1=ALWC1*RHOAI*1.E+03  ! kg/kg to g/m3
!
!     * OUTPUT OF RESULTS.
!
      CALL WTHERMO(IFILE,ZHI,THI,ALWC1,RELHI,ILEV,ZCDNC,UA)
!
      END PROGRAM ATMPROF
      SUBROUTINE WTHERMO(IFILE,ZH,TEMP,ALWC1,ARH,LEV,CDNC,UA)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     WRITES THERMODYNAMIC PROFILES.
!
!     HISTORY:
!     --------
!     * DEC 14/2007 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      IMPLICIT NONE
      CHARACTER(LEN=29), PARAMETER  :: FMT="(1X,F7.3,3X,F7.3,4(9X,E12.5))"
      INTEGER :: L, LEV, IFILE
!
      REAL, DIMENSION(LEV) :: ZH    !< Altitude (in m) 
      REAL, DIMENSION(LEV) :: TEMP  !< Temperature (in K)
      REAL, DIMENSION(LEV) :: ARH   !< Relative humidity (%)
      REAL, DIMENSION(LEV) :: ALWC1 !< Cloud liquid water content (kg/kg)
!
      REAL, DIMENSION(LEV) :: UA    !< horizontal wind speed (m/s)
      REAL, DIMENSION(LEV) :: CDNC  !< cloud droplet number concentration (cm-3) 
!
!---------------------------------------------------------------------
!
      WRITE(IFILE,'(A94)')'alti(m)   temp(K)         RH(%)               WS(m/s)             lwc(g/m3)         cdnc(cm-3)'
      DO L=LEV,1,-1
        WRITE(IFILE,FMT) ZH(L),TEMP(L),ARH(L),UA(L),ALWC1(L),CDNC(L)
      ENDDO
!
      END SUBROUTINE WTHERMO
      subroutine vexp(xout,xin,nin)
!=======================================================================
! vector exponential. substitute by appropriate function on supercomputer.
!=======================================================================
      USE FPDEF
      integer :: nin
      real(R4), dimension(nin) :: xout,xin
!
!      if ( kdbl ) then
        xout(1:nin) = exp(xin(1:nin))
!      else
!        xout(1:nin) = dexp(xin(1:nin))
!      endif
!
      end subroutine vexp
      subroutine vlog(xout,xin,nin)
!=======================================================================
! vector log. substitute by appropriate function on supercomputer.
!=======================================================================
      USE FPDEF
      integer :: nin
      real(R4), dimension(nin) :: xout,xin
!
!      if ( kdbl ) then
        xout(1:nin) = log(xin(1:nin))
!      else
!        xout(1:nin) = dlog(xin(1:nin))
!      endif
!
      end subroutine vlog
      subroutine vsqrt(xout,xin,nin)
!=======================================================================
! vector square root, substitute by appropriate function on supercomputer.
!=======================================================================
      USE FPDEF
      integer :: nin
      real(R4), dimension(nin) :: xout,xin
!
!      if ( kdbl ) then
        xout(1:nin) = sqrt(xin(1:nin))
!      else
!        xout(1:nin) = dsqrt(xin(1:nin))
!      endif
!
      end subroutine vsqrt
      subroutine vpow(xout,xin,yin,nin)
!=======================================================================
! vector power. substitute by appropriate function on supercomputer.
!=======================================================================
      USE FPDEF
      integer ::  nin
      real(R4), intent(in), dimension(nin) :: xin, yin
      real(R4), intent(out), dimension(nin) :: xout
!
      xout = xin**yin
!
      end subroutine vpow
      subroutine vsexp(xout,xin,nin)
!=======================================================================
! vector exponential. substitute by appropriate function on supercomputer.
!=======================================================================
      integer :: nin
      real, dimension(nin) :: xout,xin
!
      xout(1:nin) = exp(xin(1:nin))
!
      end subroutine vsexp
      subroutine vslog(xout,xin,nin)
!=======================================================================
! vector log. substitute by appropriate function on supercomputer.
!=======================================================================
      integer :: nin
      real, dimension(nin) :: xout,xin
!
      xout(1:nin) = log(xin(1:nin))
!
      end subroutine vslog
      subroutine vssqrt(xout,xin,nin)
!=======================================================================
! vector square root. substitute by appropriate function on supercomputer.
!=======================================================================
      integer :: nin
      real, dimension(nin) :: xout,xin
!
      xout(1:nin) = sqrt(xin(1:nin))
!
      end subroutine vssqrt
      subroutine vspow(xout,xin,yin,nin)
!=======================================================================
! vector power. substitute by appropriate function on supercomputer.
!=======================================================================
      integer ::  nin
      real, intent(in), dimension(nin) :: xin, yin
      real, intent(out), dimension(nin) :: xout
!
      xout = xin**yin
!
      end subroutine vspow
