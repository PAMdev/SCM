#!/bin/bash

test_case=$1
test_case_dir=$1
test_case_baseline=$1

#check if compiler specified (i.e. gfortran for CI)
if [ -n "$2" ]; then
    compiler=$2
	test_case_baseline=${compiler}_${test_case}
fi

#calculate sha1 sum for new test
rm -f ${test_case}_out.nc.sha1
sha1sum ${test_case}_out.nc > ${test_case}_out.nc.sha1
test_case_sha=$(<${test_case}_out.nc.sha1)
echo "test case sha1: ${test_case_sha}"
echo "file: ${test_case}_out.nc.sha1" 

#check if baseline sha1 sum exists
if [[ -f test_cases/${test_case_dir}/${test_case_baseline}_out.nc.sha1 ]] 
then
    baseline_sha=$(<test_cases/${test_case_dir}/${test_case_baseline}_out.nc.sha1)
    echo "baseline sha1: ${baseline_sha}"
    echo "file: test_cases/${test_case_dir}/${test_case_baseline}_out.nc.sha1"
else
    echo "File test_cases/${test_case_dir}/${test_case_baseline}_out.nc.sha1 does not exist."
    exit 1
fi

#calculate checksum diff
casediff=$(diff <(echo "$baseline_sha") <(echo "$test_case_sha"))


#compare baseline to new test
if [ "$casediff" != "" ]; then
  echo "$test_case checksum verification failed"
  exit 1
fi
echo "$test_case checksum verification passed"


#cleanup
rm -f ${test_case}_out.nc.sha1
