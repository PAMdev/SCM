      SUBROUTINE DRYDEPG(XROW,FLNDROW,GTROWL,GTROWI,PRESSG,DSHJ,THROW, &
                         SHJ,SICNROW,SNOROW,ZSOI,QROW,HMFNROW, &
                         ZFOR,DD4,ZTMST,ISO2,ILG,IL1,IL2,ILEV,NTRAC)
!
!     THIS ROUTINE CALCULATES THE  DRY DEPOSITION FLUX FOR SO2.
!
      USE SDPHYS
!
      IMPLICIT REAL   (A-H,O-Z), &
      INTEGER (I-N)

      REAL   GTROWL(ILG),GTROWI(ILG)
      REAL   DSHJ(ILG,ILEV),PRESSG(ILG),SHJ(ILG,ILEV)
      REAL   THROW(ILG,ILEV),SICNROW(ILG),SNOROW(ILG),QROW(ILG,ILEV)
      REAL   XROW(ILG,ILEV,NTRAC),FLNDROW(ILG),HMFNROW(ILG)
      REAL   ZVWC2,ZVW02,ZVWC4,ZVW04,ZSNCRI,ALFA
      REAL   ZSOI(ILG)
      REAL   ZFOR(ILG),DD4(ILG)
      REAL   T1S
!
      DATA ZERO /0./
!=====================================================================
!     M WATER EQUIVALENT  CRITICAL SNOW HEIGHT (FROM *SURF*)
      ZSNCRI=0.025
!
!     COEFFICIENTS FOR ZVDRD = FUNCTION OF SOIL MOISTURE
!
      ZVWC2=(0.8E-2 - 0.2E-2)/(1. - 0.9)
      ZVW02=ZVWC2-0.8E-2
      ZVWC4=(0.2E-2 - 0.025E-2)/(1. - 0.9)
      ZVW04=ZVWC4-0.2E-2
!
!*      2.    DRY DEPOSITION.
!             --- ----------
      T1S=273.16
      DO 205 IL=IL1,IL2
       ZRHO0=SHJ(IL,ILEV)*PRESSG(IL)/(RGAS*THROW(IL,ILEV))
       ZZDP=ZRHO0*ZTMST*GRAV/(DSHJ(IL,ILEV)*PRESSG(IL))
       ZMAXVDRY=1./ZZDP
       ZVDRD1NL=0.
       ZVDRD1L =0.
!
!      * DRY DEPOSITION OF SO2, SO4.
!
!      - SEA(WATER OR SEAICE) -
       IF(FLNDROW(IL).LT.1.) THEN
!        - MELTING/NOT MELTING SEAICE-
         IF(GTROWI(IL).GE.(T1S-0.1)) THEN
            ZVD2ICE=0.8E-2
            ZVD4ICE=0.2E-2
         ELSE
            ZVD2ICE=0.1E-2
            ZVD4ICE=0.025E-2
         ENDIF
         ALFA=SICNROW(IL)
         ZVDRD1NL=(1.-ALFA)*1.0E-2+ALFA*ZVD2ICE
         ZVDRD1NL=MIN(MAX(ZVDRD1NL,0.),ZMAXVDRY)
       ENDIF
!
!      - LAND -
       IF(FLNDROW(IL).GT.0.) THEN
!        - NON-FOREST AREAS -
!        - SNOW/NO SNOW -
         IF(SNOROW(IL).GT.ZSNCRI) THEN
!          - MELTING/NOT MELTING SNOW -
           IF(HMFNROW(IL).GT.0.) THEN
             ZVD2NOF=0.8E-2
             ZVD4NOF=0.2E-2
           ELSE
             ZVD2NOF=0.1E-2
             ZVD4NOF=0.025E-2
           ENDIF
         ELSE
!          - FROZEN/NOT FROZEN SOIL -
           IF(GTROWL(IL).LE.T1S) THEN
             ZVD2NOF=0.2E-2
             ZVD4NOF=0.025E-2
           ELSE
!          - WET/DRY -
             IF(ZSOI(IL).GE.0.99)     THEN
               ZVD2NOF=0.8E-2
               ZVD4NOF=0.2E-2
             ELSE IF(ZSOI(IL).LT.0.9) THEN
               ZVD2NOF=0.2E-2
               ZVD4NOF=0.025E-2
             ELSE
               ZVD2NOF=ZVWC2*ZSOI(IL)-ZVW02
               ZVD4NOF=ZVWC4*ZSOI(IL)-ZVW04
             ENDIF
           ENDIF
         ENDIF
         ZZFCROW=MAX(ZFOR(IL),0.)
         ZVDRD1L=ZZFCROW*0.8E-2 &
                        +(1.-ZZFCROW)*ZVD2NOF
         ZVDRD1L=MIN(MAX(ZVDRD1L,0.),ZMAXVDRY)
       ENDIF
       ZVDRD1 =FLNDROW(IL)*ZVDRD1L + (1.-FLNDROW(IL))*ZVDRD1NL
!
       FAC1NL = 1.-ZVDRD1NL*ZZDP
       FAC1L  = 1.-ZVDRD1L*ZZDP
       FAC1   = FLNDROW(IL)*FAC1L + (1.-FLNDROW(IL))*FAC1NL
!
!      * SULPHATE IS DONE REGARDLESS OF "IPAM".
!
       XROW(IL,ILEV,ISO2)=XROW(IL,ILEV,ISO2)*FAC1
       DD4(IL)=XROW(IL,ILEV,ISO2)*ZVDRD1*ZRHO0
  205 CONTINUE
!
      RETURN
      END

