      SUBROUTINE WETDEP4(ILG,IL1,IL2,ILEV,ZTMST, PCONS2, PXTP10, &
                         PXTP1C,DSHJ,SHJ,PRESSG,TH,QV,PMRATEP,ZMLWC, &
                         PFSNOW,PFRAIN,PDEP3D,ZCLF,CLRFR,PHENRY, &
                         CLRFS,PFEVAP,PFSUBL,PDCLR,PDCLD,JT,ISO4)
!
!     *WETDEP4* CALCULATES THE WET DEPOSITION OF TRACE GASES OR AEROSOLS
!
!     * FEB 18/2009 - K.VONSALZEN. NEW VERSION FOR GCM15H:
!     *                            - ADD DIAGNOSTICS FOR IN-CLOUD AND
!     *                              CLEAR-SKY SOURCES AND SINKS OF
!     *                              TRACERS.
!     * DEC 16/07 -  K. VONSALZEN. PREVIOUS VERSION WETDEP3 FOR GCM15G:
!     *                            - BUGFIXES FOR SCAVENGING.
!     * JUN 20/06 -  M. LAZARE.    PREVIOUS VERSION WETDEP2 FOR GCM15F:
!     *                            - USE VARIABLE INSTEAD OF CONSTANT
!     *                              IN INTRINSICS SUCH AS "MAX",
!     *                              SO THAT CAN COMPILE IN 32-BIT MODE
!     *                              WITH REAL*8.
!
!   P RPOSE
!  --------- &
!   T  CALCULATE THE WET SCAVENGING OF GASES OR AEROSOLS IN CLOUDS
!
!   I TERFACE
!  ------------- &
!   T IS ROUTINE IS CALLED FROM *XTCHEMIE*
!   C
!
      USE SDPHYS
!
      IMPLICIT REAL   (A-H,O-Z), &
      INTEGER (I-N)
!
      REAL   ZMLWC(ILG,ILEV),PXTP10(ILG,ILEV),  PXTP1C(ILG,ILEV), &
             DSHJ(ILG,ILEV),SHJ(ILG,ILEV), PMRATEP(ILG,ILEV), &
             PFSNOW(ILG,ILEV), TH(ILG,ILEV),PFEVAP(ILG,ILEV), &
             ZCLF(ILG,ILEV),CLRFR(ILG,ILEV),PRESSG(ILG),QV(ILG,ILEV), &
             PFRAIN(ILG,ILEV),CLRFS(ILG,ILEV),PFSUBL(ILG,ILEV)
      REAL   PDEP3D(ILG,ILEV),PHENRY(ILG,ILEV),ZDEPR(ILG),ZDEPS(ILG), &
             PDEPR(ILG,ILEV),PDEPS(ILG,ILEV),PDCLR(ILG,ILEV), &
             PDCLD(ILG,ILEV)
!
      DATA ZERO,ONE /0., 1./
!====================================================================
!     * PHYSICAL CONSTANTS
!
      PQTMST=1./ZTMST
      ZCONS1=5.2
      ZCOLLEFF=0.1
      ZMIN=1.E-20
      ZEP=1.
      VTMPC1=RGASV/RGAS-1.
!
      PDEPS=0.
      PDEPR=0.
      PDCLR=0.
      PDCLD=0.
      DO 100 IL=IL1,IL2
         ZDEPR(IL)=0.
         ZDEPS(IL)=0.
 100  CONTINUE
!
!     * MAJOR VERTICAL LOOP.
!
      DO 160 JK=1,ILEV
         DO 110 IL=IL1,IL2
          PXTP1C(IL,JK)=MAX(ZERO,PXTP1C(IL,JK))
          PXTP10(IL,JK)=MAX(ZERO,PXTP10(IL,JK))
  110    CONTINUE
!
!   2  IN-CLOUD SCAVENGING (GIORGI + CHAMEIDES)
!
         DO 120 IL=IL1,IL2
          IF(PMRATEP(IL,JK).GT.ZMIN) THEN
            ZICSCAV=PMRATEP(IL,JK)/ZMLWC(IL,JK)
            ZICSCAV=MAX(ZERO,MIN(ONE,ZICSCAV))
            ZEPS=ZEP*PHENRY(IL,JK)
            ZICSCAV=ZEPS*ZICSCAV
!
!           NO SCAVENGING IN ICE-CLOUDS
!
            PDEPR(IL,JK)=PDEPR(IL,JK)+PXTP1C(IL,JK)*ZICSCAV &
                   *ZCLF(IL,JK)
            PDCLD(IL,JK)=PDCLD(IL,JK)+PXTP1C(IL,JK)*ZICSCAV
!           PXTP1C(IL,JK)=PXTP1C(IL,JK)*(1.-ZICSCAV)
          END IF
  120    CONTINUE
!
!   3  BELOW CLOUD SCAVENGING FOR ICE AND WATER CLOUDS.
!
         DO 140 IL=IL1,IL2
          IF(PFSNOW(IL,JK).GT.ZMIN.AND.CLRFS(IL,JK).GE.0.05) THEN
            ZZEFF=ZCOLLEFF*3.
            ZFTOM=1./(DSHJ(IL,JK)*PRESSG(IL)*PCONS2)
            ZRHO0=PRESSG(IL)*SHJ(IL,JK)/(RGAS*TH(IL,JK) &
                     *(1.+VTMPC1*QV(IL,JK)))
            ZBCSCAV=ZCONS1*ZZEFF*(PFSNOW(IL,JK)/CLRFS(IL,JK)) &
                   *ZFTOM*ZRHO0
            ZBCSCAV=MAX(ZERO,MIN(ONE,ZBCSCAV))
            PDEPS(IL,JK)=PDEPS(IL,JK)+ZBCSCAV*PXTP10(IL,JK)* &
                         CLRFS(IL,JK)
            IF ( ZCLF(IL,JK) < 0.99 ) THEN
              PDCLR(IL,JK)=PDCLR(IL,JK)+ZBCSCAV*PXTP10(IL,JK) &
                          *CLRFS(IL,JK)/(1.-ZCLF(IL,JK))
            ENDIF
          END IF
  140    CONTINUE
         DO 145 IL=IL1,IL2
          IF(PFRAIN(IL,JK).GT.ZMIN.AND.CLRFR(IL,JK).GE.0.05) THEN
            ZZEFF=ZCOLLEFF*3.
            ZFTOM=1./(DSHJ(IL,JK)*PRESSG(IL)*PCONS2)
            ZRHO0=PRESSG(IL)*SHJ(IL,JK)/(RGAS*TH(IL,JK) &
                     *(1.+VTMPC1*QV(IL,JK)))
            ZBCSCAV=ZCONS1*ZZEFF*(PFRAIN(IL,JK)/CLRFR(IL,JK)) &
                   *ZFTOM*ZRHO0
            ZBCSCAV=MAX(ZERO,MIN(ONE,ZBCSCAV))
            PDEPR(IL,JK)=PDEPR(IL,JK)+ZBCSCAV*PXTP10(IL,JK)* &
                         CLRFR(IL,JK)
            IF ( ZCLF(IL,JK) < 0.99 ) THEN
              PDCLR(IL,JK)=PDCLR(IL,JK)+ZBCSCAV*PXTP10(IL,JK) &
                          *CLRFR(IL,JK)/(1.-ZCLF(IL,JK))
            ENDIF
          END IF
  145    CONTINUE
         DO IL=IL1,IL2
          ZMTOF=DSHJ(IL,JK)*PRESSG(IL)*PCONS2
          ZDEPR(IL)=ZDEPR(IL)+PDEPR(IL,JK)*ZMTOF
          ZDEPS(IL)=ZDEPS(IL)+PDEPS(IL,JK)*ZMTOF
         ENDDO
!
!   4  REEVAPORATION. ONLY COMPLETE EVAPORATION OF RRECIP IN THE
!      CLEAR-SKY PORTION OF THE GRID CELL THAT IS AFFECTED BY RAIN)
!      WILL CAUSE TRACERS TO BE RELEASED INTO THE ENVIRONMENT.
!
         DO IL=IL1,IL2
          ZFTOM=1./(DSHJ(IL,JK)*PRESSG(IL)*PCONS2)
          PDEPS(IL,JK)=PDEPS(IL,JK)-ZDEPS(IL)*ZFTOM*PFSUBL(IL,JK)
          IF ( ZCLF(IL,JK) < 0.99 ) THEN
            PDCLR(IL,JK)=PDCLR(IL,JK) &
                        -ZDEPS(IL)*ZFTOM*PFSUBL(IL,JK)/(1.-ZCLF(IL,JK))
          ENDIF
          ZDEPS(IL)=ZDEPS(IL)*(1.-PFSUBL(IL,JK))
         ENDDO
         DO IL=IL1,IL2
          ZFTOM=1./(DSHJ(IL,JK)*PRESSG(IL)*PCONS2)
          PDEPR(IL,JK)=PDEPR(IL,JK)-ZDEPR(IL)*ZFTOM*PFEVAP(IL,JK)
          IF ( ZCLF(IL,JK) < 0.99 ) THEN
            PDCLR(IL,JK)=PDCLR(IL,JK) &
                        -ZDEPR(IL)*ZFTOM*PFEVAP(IL,JK)/(1.-ZCLF(IL,JK))
          ENDIF
          ZDEPR(IL)=ZDEPR(IL)*(1.-PFEVAP(IL,JK))
         ENDDO
         DO IL=IL1,IL2
           PDEP3D(IL,JK)=PDEP3D(IL,JK)+PDEPR(IL,JK)+PDEPS(IL,JK)
           PDCLR (IL,JK)=MIN(PDCLR(IL,JK),PXTP10(IL,JK))
           PDCLD (IL,JK)=MIN(PDCLD(IL,JK),PXTP1C(IL,JK))
         ENDDO
  160 CONTINUE
!
      RETURN
      END
