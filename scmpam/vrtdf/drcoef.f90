      SUBROUTINE DRCOEF(CDM,CDH,CFLUX,ZOMIN,ZOHIN,CRIB,TVIRTG, &
                        TVIRTA,VA,GRAV,VKC,ILG,IL1,IL2)
!
      IMPLICIT NONE

!     * CALCULATES DRAG COEFFICIENTS AND RELATED VARIABLES FOR CLASS.

!     * OUTPUT FIELDS ARE:
!     *    CDM    : STABILITY-DEPENDENT DRAG COEFFICIENT FOR MOMENTUM.
!     *    CDH    : STABILITY-DEPENDENT DRAG COEFFICIENT FOR HEAT.
!     *    CFLUX  : CD * MOD(V), BOUNDED BY FREE-CONVECTIVE LIMIT.
!     * INPUT FIELDS ARE:
!     *    ZOMIN/: ROUGHNESS HEIGHTS FOR MOMENTUM/HEAT NORMALIZED BY
!     *    ZOHIN   REFERENCE HEIGHT.
!     *    CRIB   : -RGAS*SLTHKEF/(VA**2), WHERE
!     *             SLTHKEF=-LOG(MAX(SGJ(ILEV),SHJ(ILEV)))
!     *    TVIRTG : "SURFACE" VIRTUAL TEMPERATURE.
!     *    TVIRTA : LOWEST LEVEL VIRTUAL TEMPERATURE.
!     *    VA     : AMPLITUDE OF LOWEST LEVEL WIND.
!     * OTHER VARIABLES ARE:
!     *    ZOM/  : WORK ARRAYS USED FOR SCALING ZOMIN/ZOHIN
!     *    ZOH     ON STABLE SIDE, AS PART OF CALCULATION.
!     *    RIB    : BULK RICHARDSON NUMBER.
!     *    USTAR  : FRICTION VELOCITY SCALE

!     * INTEGER CONSTANTS.

      INTEGER ILG,IL1,IL2,JL,I

!     * REAL CONSTANTS

      REAL GRAV,VKC

!     * OUTPUT ARRAYS.

      REAL CDM    (ILG),   CDH    (ILG),   RIB    (ILG),   CFLUX  (ILG)
      REAL USTAR  (ILG)

!     * INPUT ARRAYS.

      REAL ZOMIN  (ILG),   ZOHIN  (ILG),   CRIB   (ILG),   TVIRTG (ILG)
      REAL TVIRTA (ILG),   VA     (ILG)

!     * WORK ARRAYS.

      REAL ZOM    (ILG),   ZOH    (ILG)

!     * TEMPORARY VARIABLES.

      REAL AA,AA1,BETA,PR,ZLEV,ZS,ZOLN,ZMLN,CPR,ZI,OLSF,OLFACT, &
           ZL,ZMOL,ZHOL,XM,XH,BH1,BH2,BH,WB,WSTAR,RIB0,WSPEED, &
           AU1,OLS,PSIM1,PSIM0,PSIH1,PSIH0,TSTAR,WTS,AS1, &
           AS2,AS3,CLIMIT

!-------------------------------------------------------------
      AA=9.5285714
      AA1=14.285714
      BETA=1.2
      PR = 1.
!
      DO I=IL1,IL2
          RIB(I)=CRIB(I)*(TVIRTG(I)-TVIRTA(I))
          IF( RIB(I) >= 0. ) THEN
            ZLEV=-CRIB(I)*TVIRTA(I)*(VA(I)**2)/GRAV
            ZS=MAX(10.,5.*MAX(ZOMIN(I)*ZLEV, ZOHIN(I)*ZLEV))
            ZS=ZLEV*(1.+RIB(I))/(1.+(ZLEV/ZS)*RIB(I))
            ZOM(I)=ZOMIN(I)*ZLEV/ZS
            ZOH(I)=ZOHIN(I)*ZLEV/ZS
            RIB(I)=RIB(I)*ZS/ZLEV
          ELSE
            ZOM(I)=ZOMIN(I)
            ZOH(I)=ZOHIN(I)
          ENDIF
          ZOLN=LOG(ZOH(I))
          ZMLN=LOG(ZOM(I))
          IF( RIB(I) < 0. ) THEN
            CPR=MAX(ZOLN/ZMLN,0.74)
            CPR=MIN(CPR,1.0)
            ZI=1000.0
            OLSF=BETA**3*ZI*VKC**2/ZMLN**3
            OLFACT=1.7*(LOG(1.+ZOM(I)/ZOH(I)))**0.5+0.9
            OLSF=OLSF*OLFACT
            ZL = -CRIB(I)*TVIRTA(I)*(VA(I)**2)/GRAV
            ZMOL=ZOM(I)*ZL/OLSF
            ZHOL=ZOH(I)*ZL/OLSF
            XM=(1.00-15.0*ZMOL)**(0.250)
            XH=(1.00-9.0*ZHOL)**0.25
            BH1=-LOG(-2.41*ZMOL)+LOG(((1.+XM)/2.)**2*(1.+XM**2)/2.)
            BH1=BH1-2.*ATAN(XM)+ATAN(1.)*2.
            BH1=BH1**1.5
            BH2=-LOG(-0.25*ZHOL)+2.*LOG(((1.00+XH**2)/2.00))
            BH=VKC**3.*BETA**1.5/(BH1*(BH2)**1.5)
            WB=SQRT(GRAV*(TVIRTG(I)-TVIRTA(I))*ZI/TVIRTG(I))
            WSTAR=BH**(0.333333)*WB
            RIB0=RIB(I)

            WSPEED=SQRT(VA(I)**2+(BETA*WSTAR)**2)
            RIB(I)=RIB0*VA(I)**2/WSPEED**2
            AU1=1.+5.0*(ZOLN-ZMLN)*RIB(I)*(ZOH(I)/ZOM(I))**0.25
            OLS=-RIB(I)*ZMLN**2/(CPR*ZOLN)*(1.0+AU1/ &
                 (1.0-RIB(I)/(ZOM(I)*ZOH(I))**0.25))
            PSIM1=LOG(((1.00+(1.00-15.0*OLS)**0.250)/2.00)**2* &
                  (1.0+(1.00-15.0*OLS)**0.5)/2.0)-2.0*ATAN( &
                  (1.00-15.0*OLS)**0.250)+ATAN(1.00)*2.00
            PSIM0=LOG(((1.00+(1.00-15.0*OLS*ZOM(I))**0.250)/2.00)**2 &
                  *(1.0+(1.00-15.0*OLS*ZOM(I))**0.5)/2.0)-2.0* &
                  ATAN((1.00-15.0*OLS*ZOM(I))**0.250)+ATAN(1.00)*2.0
            PSIH1=LOG(((1.00+(1.00-9.0*OLS)**0.50)/2.00)**2)
            PSIH0=LOG(((1.00+(1.00-9.0*OLS*ZOH(I))**0.50)/2.00)**2)

            USTAR(I)=VKC/(-ZMLN-PSIM1+PSIM0)
            TSTAR=VKC/(-ZOLN-PSIH1+PSIH0)
            CDH(I)=USTAR(I)*TSTAR/PR
            WTS=CDH(I)*WSPEED*(TVIRTG(I)-TVIRTA(I))
            WSTAR=(GRAV*ZI/TVIRTG(I)*WTS)**(0.333333)

            WSPEED=SQRT(VA(I)**2+(BETA*WSTAR)**2)
            RIB(I)=RIB0*VA(I)**2/WSPEED**2
            AU1=1.+5.0*(ZOLN-ZMLN)*RIB(I)*(ZOH(I)/ZOM(I))**0.25
            OLS=-RIB(I)*ZMLN**2/(CPR*ZOLN)*(1.0+AU1/ &
                 (1.0-RIB(I)/(ZOM(I)*ZOH(I))**0.25))
            PSIM1=LOG(((1.00+(1.00-15.0*OLS)**0.250)/2.00)**2* &
                  (1.0+(1.00-15.0*OLS)**0.5)/2.0)-2.0*ATAN( &
                  (1.00-15.0*OLS)**0.250)+ATAN(1.00)*2.00
            PSIM0=LOG(((1.00+(1.00-15.0*OLS*ZOM(I))**0.250)/2.00)**2 &
                  *(1.0+(1.00-15.0*OLS*ZOM(I))**0.5)/2.0)-2.0* &
                  ATAN((1.00-15.0*OLS*ZOM(I))**0.250)+ATAN(1.00)*2.0
            PSIH1=LOG(((1.00+(1.00-9.0*OLS)**0.50)/2.00)**2)
            PSIH0=LOG(((1.00+(1.00-9.0*OLS*ZOH(I))**0.50)/2.00)**2)

          ELSE

            WSPEED=VA(I)
            AS1=10.0*ZMLN*(ZOM(I)-1.0)
            AS2=5.00/(2.0-8.53*RIB(I)*EXP(-3.35*RIB(I))+0.05*RIB(I)**2)
!<<<
            AS2=AS2*PR*SQRT(-ZMLN)/2.
            AS3=27./(8.*PR*PR)
!>>>
            OLS=RIB(I)*(ZMLN**2+AS3*AS1*(RIB(I)**2+AS2*RIB(I))) &
                /(AS1*RIB(I)-PR*ZOLN)
            PSIM1=-0.667*(OLS-AA1)*EXP(-0.35*OLS)-AA-OLS
            PSIM0=-0.667*(OLS*ZOM(I)-AA1)*EXP(-0.35*OLS*ZOM(I)) &
                  -AA-OLS*ZOM(I)
            PSIH1=-(1.0+2.0*OLS/3.0)**1.5-0.667*(OLS-AA1) &
                  *EXP(-0.35*OLS)-AA+1.0
            PSIH0=-(1.0+2.0*OLS*ZOH(I)/3.0)**1.5-0.667*(OLS*ZOH(I)-AA1) &
                  *EXP(-0.35*OLS*ZOH(I))-AA+1.0

          ENDIF

          USTAR(I)=VKC/(-ZMLN-PSIM1+PSIM0)

          TSTAR=VKC/(-ZOLN-PSIH1+PSIH0)

          CDM(I)=USTAR(I)**2.0
          CDH(I)=USTAR(I)*TSTAR/PR
!
!         * CALCULATE CD*MOD(V) UNDER FREE-CONVECTIVE LIMIT.
!
          IF( TVIRTG(I) > TVIRTA(I) ) THEN
            CLIMIT=1.9E-3*(TVIRTG(I)-TVIRTA(I))**0.333333
          ELSE
            CLIMIT=0.
          ENDIF
          CFLUX(I)=MAX(CDH(I)*WSPEED,CLIMIT)
      ENDDO
!
      END SUBROUTINE DRCOEF
