      SUBROUTINE VRTDF(DXDT,XROW,DTDT,THROW,DQDT,QROW,DQLDT,QLWC, &
                       DUDT,U,V,WSUB,RKH,RKM,RI,CDM,USTAR,ZSPD, &
                       ZSPDA,ZF,PF,TF,ZH,PH,DP,ALMC,ALMX,PBLT,TVP, &
                       QSENS,QLAT,ZCLF,TZERO,THLIQG,ZO,SHTJ,SHJ,DSHJ, &
                       PRESSG,DT,ZTOP,IL1,IL2,ILG,ILEV,ILEVP1, &
                       MSGP,NTRAC)
!
      USE SDPHYS
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      INTEGER, INTENT(IN) :: ILG,ILEV,ILEVP1,IL1,IL2,MSGP,NTRAC
      REAL, INTENT(OUT), DIMENSION(ILG) :: PBLT, CDM, ZSPD, ZSPDA, &
                                           QSENS, QLAT
      REAL, INTENT(INOUT), DIMENSION(ILG) :: USTAR, ZO
      REAL, INTENT(OUT), DIMENSION(ILG,ILEV) :: ALMC,ALMX,RKH,RKM,RI, &
                                                WSUB,TVP
      REAL, INTENT(INOUT), DIMENSION(ILG,ILEV) :: ZCLF
      REAL, INTENT(IN) :: DT
      REAL, INTENT(IN), DIMENSION(ILG) :: PRESSG, TZERO, THLIQG
      REAL, INTENT(IN), DIMENSION(ILG,ILEV) :: SHJ,DSHJ,TF,ZH,PH, &
                                               ZF,PF,DP
      REAL, INTENT(IN), DIMENSION(ILG,ILEVP1) :: SHTJ
      REAL, DIMENSION(ILG,ILEVP1), INTENT(INOUT) :: THROW,QROW
      REAL, DIMENSION(ILG,ILEV), INTENT(IN) :: QLWC
      REAL, DIMENSION(ILG,ILEV), INTENT(INOUT) :: U,V
      REAL, DIMENSION(ILG,ILEV,NTRAC) :: RKX
      REAL, DIMENSION(ILG,ILEVP1,NTRAC), INTENT(IN) :: XROW
      REAL, DIMENSION(ILG,ILEVP1,NTRAC), INTENT(OUT) :: DXDT
      REAL, DIMENSION(ILG,ILEVP1), INTENT(OUT) :: DQDT,DTDT
      REAL, DIMENSION(ILG,ILEV), INTENT(OUT) :: DQLDT
      REAL, DIMENSION(ILG,ILEV), INTENT(OUT) :: DUDT
!
      REAL, DIMENSION(ILG,ILEV) :: A,B,C,WORK,XROWT,DXDTT,RKXT,QT,HMN
      REAL, DIMENSION(ILG) :: UN,ZER,CL,CDVLT,RAUS,VINT,THT
      REAL, DIMENSION(ILG,ILEV) :: CVAR, ZCRAUT, QCWVAR, CVSG, CVDU, &
                                   ZFRAC, SHXKJ, DVDS, DTTDS, ALMXT
      REAL, DIMENSION(ILG,ILEVP1) :: SHTXKJ
      REAL, DIMENSION(ILG) :: PMBS, RRL, CPM, ALMIX, DHLDZ, DRWDZ, &
                              FLAGEST, QCW, SSH, VMODL, CDVLM, DVDZ
      REAL, DIMENSION(ILG) :: CDH, ZOSCLM, ZOSCLH, TVIRTS, TVIRTA, &
                              CRIB, CFLUX, EVAP, QZERO, PBLTI, QTG, &
                              HMNG
      INTEGER, DIMENSION(ILG) :: IPBL
      REAL, PARAMETER :: VKC=0.41, TVFA=0.608, SCALF=0.8

!
!-----------------------------------------------------------------------
!     * SURFACE PROPERTIES FOR BARE GROUND OR WATER.
!
      TFREZ=273.16
      DO I=IL1,IL2
         ZSPD(I)=SQRT(U(I,ILEV)**2+V(I,ILEV)**2) ! SURFACE WIND SPEED
         ESW=1.E+02*EXP(RW1+RW2/TZERO(I))*(TZERO(I)**RW3)
         WZERO=EPS1*ESW/(PRESSG(I)-ESW)
         Q0SAT=WZERO
!         IF( TZERO(I) >= TFREZ ) THEN
!           AC=17.269
!           BC=35.86
!         ELSE
!           AC=21.874
!           BC=7.66
!         ENDIF
!         WZERO=EPS1*611.0*EXP(AC*(TZERO(I)-TFREZ)/
!     1              (TZERO(I)-BC))/PRESSG(I)
!
!         Q0SAT=WZERO/(1.0+WZERO)     ! SATURATED SPECIFIC HUMIDITY
         IF( THLIQG(I)> 0.99 ) THEN  ! WATER SURFACE
           EVBETA=1.0
           QZERO(I)=Q0SAT
           ZO(I)=(0.016/GRAV)*USTAR(I)**2
         ELSE                        ! BARE SOIL
           THLMIN=0.1                ! MIMIMUM SOIL WATER CONTENT (M3/M3)
           THFC=0.4                  ! FIELD CAPACITY (M3/M3)
           IF( THLIQG(I) < (THLMIN+0.001) ) THEN
             IEVAP=0
             CEVAP=0.0
           ELSEIF( THLIQG(I) > THFC ) THEN
             IEVAP=1
             CEVAP=1.0
           ELSE
             IEVAP=1
             CEVAP=0.25*(1.0-COS(3.14159*THLIQG(I)/THFC))**2
           ENDIF
           EVBETA=CEVAP              ! SURFACE EVAPORATION EFFICIENCY
           QZERO(I)=EVBETA*Q0SAT+(1.0-EVBETA)*QROW(I,ILEVP1)
           IF( QZERO(I) > QROW(I,ILEVP1) .AND. IEVAP == 0) THEN
             EVBETA=0.0
             QZERO(I)=QROW(I,ILEVP1)
           ENDIF
         ENDIF
!         TVIRTS(I)=TZERO(I)*(1.0+0.61*QZERO(I))
         TVIRTS(I)=TZERO(I)*(1.+TVFA*QZERO(I)/(1.-QZERO(I)))
      ENDDO
      DO I=IL1,IL2
        ZRSLD=ZH(I,ILEVP1)+ZO(I)
        ZOSCLM(I)=ZO(I)/ZRSLD
        ZOSCLH(I)=ZOSCLM(I)
!        TVIRTA(I)=THROW(I,ILEVP1)*(1.0+0.61*QROW(I,ILEVP1))
        TVIRTA(I)=THROW(I,ILEVP1)*(1.+TVFA*QROW(I,ILEVP1) &
                                    /(1.-QROW(I,ILEVP1)))
        CRIB(I)=-GRAV*ZRSLD/(TVIRTA(I)*ZSPD(I)**2)
      ENDDO
!
!     * SURFACE DRAG COEFFICIENTS.
!
      CALL DRCOEF(CDM,CDH,CFLUX,ZOSCLM,ZOSCLH,CRIB,TVIRTS, &
                  TVIRTA,ZSPD,GRAV,VKC,ILG,IL1,IL2)
      USTAR=ZSPD*SQRT(CDM)
!
!     * SURFACE LATENT AND SENSIBLE HEAT FLUXES, AND EVAPORATION.
!
      DO I=IL1,IL2
        ZRHO0=PH(I,ILEVP1)/(RGAS*THROW(I,ILEVP1))
!!!!
        QSENS(I)=ZRHO0*CPRES*CFLUX(I)*(TZERO(I)-THROW(I,ILEVP1))
        EVAP(I)=ZRHO0*CFLUX(I)*(QZERO(I)-QROW(I,ILEVP1))
!
!        QSENS(I)=0.
!        EVAP(I)=0./RL
!
        QLAT(I)=RL*EVAP(I)
      ENDDO
!
!     * ANEMOMETER WIND SPEED (M/SEC).
!
      DO I=IL1,IL2
        ZRUF=ZO(I)
        ZANNOM=MAX(ZRUF,10.0)
        RATFCA=LOG(ZANNOM/ZRUF)/VKC
        RATFCA1=RATFCA*SQRT(CDM(I))
        RATFCA1=MIN(RATFCA1,1.)
        ZSPDA(I)=RATFCA1*ZSPD(I)
      ENDDO
!
!     * INSERT FLUXES INTO FIRST ATMOSPHERIC LEVEL ABOVE GROUND.
!
      DTDT=0.
      DQDT=0.
      DO I=IL1,IL2
        ZRHO0=PH(I,ILEVP1)/(RGAS*THROW(I,ILEVP1))
        FAC=GRAV/DP(I,ILEV)
!        THROW(I,ILEVP1)=THROW(I,ILEVP1)+DT*FAC*QSENS(I)/CPRES
!        QROW(I,ILEVP1)=QROW(I,ILEVP1)+DT*FAC*EVAP(I)
        DTDT(I,ILEVP1)=FAC*QSENS(I)/CPRES
        DQDT(I,ILEVP1)=FAC*EVAP(I)
!!!!
!        DU=DT*FAC*ZRHO0*CDM(I)*MAX(VMIN,ZSPD(IL))*U(I,ILEV)
!        U(I,ILEV)=U(I,ILEV)-DU
!        IF ( U(I,ILEV) >= 0. ) THEN
!          U(I,ILEV)=MAX(U(I,ILEV),0.)
!        ELSE
!          U(I,ILEV)=MIN(U(I,ILEV),0.)
!        ENDIF
      ENDDO
!
!     * PARAMETERS FOR CALCULATION OF VERTICAL DIFFUSION.
!
      VMIN=0.001
      DO IL=IL1,IL2
        UN   (IL)=1.
        ZER  (IL)=0.
        RAUS (IL)=PRESSG(IL)/GRAV
        CDVLT(IL)=0.
        VMODL(IL)=MAX(VMIN,ZSPD(IL))
      ENDDO
      SHXKJ=SHJ**RGOCP
      SHTXKJ=SHTJ**RGOCP
      TAUADJ=DT
      DO I=IL1,IL2
         CDVLM(I)=CDM(I)*VMODL(I)
         HEAT=TAUADJ*GRAV*SHXKJ(I,ILEV)/(RGAS*THROW(I,ILEV))
         FACMOM=1./(1.+HEAT*CDVLM(I)/DSHJ(I,ILEV))
!
!         CDVLM(I)=CDVLT(I)
!         FACMOM=1.
!
         UTMP =U(I,ILEV)*FACMOM
         VTMP =V(I,ILEV)*FACMOM
         DVDS(I,ILEV) = SQRT((UTMP-U(I,ILEV-1))**2 &
                     +(VTMP-V(I,ILEV-1))**2)/(SHJ(I,ILEV)-SHJ(I,ILEV-1))
      ENDDO
!
!     * EVALUATE DTTDS=D(THETA)/D(SIGMA) AT TOP INTERFACE OF LAYERS.
!
      L=1
      DO I=IL1,IL2
         ALWC=QLWC(I,L)
         THETVP=THROW(I,L)*(1.+TVFA*QROW(I,L)/(1.-QROW(I,L)) &
               -(1.+TVFA)*ALWC)/SHXKJ(I,L)
         TVP(I,L)=THETVP
      ENDDO
      DO L=2,ILEV
      DO I=IL1,IL2
         ALWC=QLWC(I,L)
         THETVP=THROW(I,L)*(1.+TVFA*QROW(I,L)/(1.-QROW(I,L)) &
               -(1.+TVFA)*ALWC)/SHXKJ(I,L)
         ALWC=QLWC(I,L-1)
         THETVM=THROW(I,L-1)*(1.+TVFA*QROW(I,L-1) &
               /(1.-QROW(I,L))-(1.+TVFA)*ALWC)/SHXKJ(I,L-1)
!!!!
!         THETVP=THROW(I,L)/SHXKJ(I,L)
!         THETVM=THROW(I,L-1)/SHXKJ(I,L-1)
         DTTDS(I,L)=(THETVP-THETVM)/(SHJ(I,L)-SHJ(I,L-1))
         TVP(I,L)=THETVP
      ENDDO
      ENDDO
!
!     * DVDS = MOD(DV/DSIGMA) AT TOP INTERFACE OF LAYERS.
!
      DO L=1,ILEV-2
      DO I=IL1,IL2
         DVDS(I,L+1)=SQRT((U(I,L+1)-U(I,L))**2 &
                         +(V(I,L+1)-V(I,L))**2)/(SHJ(I,L+1)-SHJ(I,L))
      ENDDO
      ENDDO
      DO I=IL1,IL2
         PBLTI(I)=REAL(ILEV)
!         IPBL(I)=1
      ENDDO
!
!     * RMS WIND SHEAR, DVMINS=(R*T/G)*(DV/DZ)=7.9 FOR DV/DZ=1.MS-1KM-1.
!     * DETERMINE PLANETARY BOUNDARY LAYER TOP AS LEVEL INDEX ABOVE
!     * WHICH THE RICHARDSON NUMBER EXCEEDS THE CRITICAL VALUE OF 1.00.
!
      DVMINS=7.9
!      TAUR=3600.
      TAUR=6.*3600.
      DO L=ILEV,2,-1
      DO I=IL1,IL2
         DVDS(I,L)=MAX(DVDS(I,L),DVMINS)
         RIT=-RGAS*SHTXKJ(I,L)*DTTDS(I,L)/(SHTJ(I,L)*DVDS(I,L)**2)
!!!!
         RI(I,L)=(RI(I,L)+(DT/TAUR)*RIT)/(1.+DT/TAUR)
!         RI(I,L)=RIT
      ENDDO
      ENDDO
      DO L=ILEV,3,-1
      DO I=IL1,IL2
!!!!
!         IF ( RI(I,L) < 1. .AND. RI(I,L-1) > 1. ) THEN
         IF ( RI(I,L) < 10. .AND. RI(I,L-1) > 10. ) THEN
            PBLX=REAL(L)
            PBLTI(I) = MIN(PBLTI(I), PBLX)
         ENDIF
      ENDDO
      ENDDO
!!!!
      PBLT=PBLTI
!      PBLT=MAX((PBLT+(DT/TAUR)*PBLTI)/(1.+DT/TAUR),2.)
!
      FACT=VKC*RGAS*273.
      XLMIN=10.
      BEEM=10.
      GAMRH=6.
      GAMRM=6.
      ALFAH=1.
      RINEUT=1.
      RIINF=0.25
      PRANDTL_MIN=1.
      PRANDTL_MAX=3.
      WSUB=0.
      DO L=2,ILEV
!
!        * CALCULATE THE DIFFUSION COEFFICIENTS (RKH,RKM)
!        * AT THE TOP INTERFACE OF LAYERS.
!        * HEAT: FINITE STABILITY CUTOFF.
!        * LOW XINGL USED SINCE PREVIOUSLY MIXED "INSTANTANEOUSLY".
!        * MOMENTUM: FINITE STABILITY CUTOFF.
!        * HIGH XINGL USED SINCE NOT PREVIOUSLY MIXED "INSTANTANEOUSLY".
!        * UNSTABLE CASE (RI <= 0.) - STABLE CASE (RI > 0.) .
!
         DO I=IL1,IL2
            DVDZ(I)=DVDS(I,L)*GRAV*SHTJ(I,L)/(RGAS*TF(I,L-1))
         ENDDO
!
         DO I=IL1,IL2
            FACT0=FACT*LOG(SHTJ(I,L))
            SARI=SQRT(ABS(RI(I,L)))
            LPBL=NINT(PBLT(I))
!
!           * LOWER CUTOFFS.
!
            ALL=0.5*VKC*ZF(I,LPBL-1)
            XIMINT=MAX(75.*ALL/(75.+ALL),XLMIN)
            ALL=0.5*VKC*ZH(I,L)
            XIMIN =MAX(75.*ALL/(75.+ALL),XLMIN)
!
!           * EFFECTIVE MIXING LENGTHS AS FUNCTION OF MIXING
!           * LENGTHS FOR UP- AND DOWNWARD MIXING.
!
            ALU=VKC*ZH(I,L)
            ALD=VKC*(ZF(I,LPBL-1)-ZH(I,L))
            IF ( ALD > 0. ) THEN
              XINGLH=MAX(ALU*ALD/(ALU+ALD),XIMIN)
              XINGLM=MAX(ALU*ALD/(ALU+ALD),XIMIN)
              ALMC(I,L)=XINGLH
            ELSE
              FAC=(PH(I,L)/PF(I,LPBL-1))**2.
              XINGLH=10.+(XIMINT-10.)*FAC
              XINGLM=10.+(XIMINT-10.)*FAC
              ALMC(I,L)=MIN(100.*FAC,VKC*(ZH(I,L)-ZF(I,LPBL-1)))
            END IF
            ALMX(I,L)=XINGLH
            ALMXT(I,L)=MAX(ALMX(I,L),ALMC(I,L),10.)
            XINGLH=(1.-ZCLF(I,L))*XINGLH+ZCLF(I,L)*ALMXT(I,L)
            XINGLM=XINGLH
!
!           * DIFFUSION COEFFICIENTS.
!
            ELH   = XINGLH**2
            EPSSH = 0.
            FACTH = 0.5*RI(I,L)*EPSSH*BEEM
            IF ( FACTH > 1. ) THEN
              XH  = 0.
            ELSE
              XH  = (1.-FACTH)**2
            ENDIF
            ELM   = XINGLM**2
            EPSSM = 0.
            FACTM = 0.5*RI(I,L)*EPSSM*BEEM
            IF ( FACTM > 1. ) THEN
              XM  = 0.
            ELSE
              XM  = (1.-FACTM)**2
            ENDIF
            IF ( RI(I,L) < 0. ) THEN
              RKH(I,L) = ELH*DVDZ(I)*(1.-ALFAH*GAMRH*BEEM*RI(I,L) / &
                         (GAMRH+ALFAH*BEEM*SARI))
              RKM(I,L) = ELM*DVDZ(I)*(1.-      GAMRM*BEEM*RI(I,L) / &
                         (GAMRM+      BEEM*SARI))
            ELSE
              RKH(I,L) = ELH*DVDZ(I)*XH/(1.+(1.-EPSSH)*BEEM*RI(I,L))
              RKM(I,L) = ELM*DVDZ(I)*XM/(1.+(1.-EPSSM)*BEEM*RI(I,L))
            ENDIF
!
!           * PRANDTL NUMBER SCALING FOR EDDY MOMENTUM DIFFUSIVITY,
!           * BASED ON APPROACH SUGGESTED BY SCHUMANN AND GERZ (1995).
!
            ATMP=0.
            IF ( RI(I,L) > 0. ) THEN
              ATMP=RINEUT*EXP(-RI(I,L)/(RINEUT*RIINF))
            ENDIF
            PRANDTL=ATMP+RI(I,L)/RIINF
            PRANDTL=MIN(MAX(PRANDTL,PRANDTL_MIN),PRANDTL_MAX)
            RKM(I,L)=RKM(I,L)*PRANDTL
!
!           * NO MIXING ABOVE SPECIFIED HEIGHT.
!
            IF ( ZH(I,L) > ZTOP ) THEN
!               RKH(I,L)=0.
!               RKM(I,L)=0.
            ENDIF
         ENDDO
      ENDDO
      DO I=IL1,IL2
         RKM(I,1)=RKM(I,2)
         RKH(I,1)=RKH(I,2)
      ENDDO
      DO N=1,NTRAC
        RKX(:,:,N)=RKH
      ENDDO
!
!     * DIAGNOSE SUBGRID-SCALE COMPONENT OF THE VERTICAL VELOCITY
!     * (STANDARD DEVIATION, GHAN ET AL., 1997). ACCORDING TO PENG
!     * ET AL. (2005), THE REPRESENTATIVE VERTICAL VELOCITY FOR
!     * AEROSOL ACTIVATION IS OBTAINED BY APPLYING A SCALING FACTOR
!     * TO THE VELOCITY STANDARD DEVIATION (SCALF).
!
      PI=3.141592653589793
      FACTS=SCALF*SQRT(2.*PI)
      DO L=2,ILEV
        WSUB(:,L)=FACTS*RKH(:,L)/(ZF(:,L-1)-ZF(:,L))
      ENDDO
!!!!
!      WSUB=10. &
! Fli ht 08
!      WSUB=0.05 &
! Fli ht 07, low
!      WSUB=0.1 &
! Fli ht 07, high
!      WSUB=0.2
!
!      WSUB=0.02
!
!     * MIX CHEMICAL TRACERS.
!
      DO N=1,NTRAC
        RKXT=RKX(:,:,N)
        THT=THROW(:,ILEVP1)
        CALL ABCVDQ6 ( A,B,C,CL,UN,CDVLT,GRAV, &
                       IL1,IL2,ILG,ILEV,ILEVP1,ILEV, &
                       RGAS,RKXT,SHTJ,SHJ,DSHJ, &
                       THT,TF,DT)
        XROWT=XROW(:,MSGP:ILEVP1,N)
        CALL IMPLVD7(A,B,C,CL,XROWT,ZER,IL1,IL2,ILG,ILEV, &
                     DT,DXDTT,DSHJ,RAUS,WORK,VINT)
        DXDT(:,MSGP:ILEVP1,N)=DXDTT(:,:)
      ENDDO
!
!     * MIX MOMENTUM.
!
      THT=THROW(:,ILEVP1)
      CALL ABCVDQ6 ( A,B,C,CL,UN,CDVLM,GRAV, &
                     IL1,IL2,ILG,ILEV,ILEVP1,ILEV, &
                     RGAS,RKM,SHTJ,SHJ,DSHJ, &
                     THT,TF,DT)
      CALL IMPLVD7(A,B,C,CL,U,ZER,IL1,IL2,ILG,ILEV, &
                   DT,DUDT,DSHJ,RAUS,WORK,VINT)
!
!     * TOTAL WATER MIXING RATIO AND CLOUD WATER STATIC ENERGY.
!
      QT=QROW(:,MSGP:ILEVP1)+QLWC
      HMN=CPRES*THROW(:,MSGP:ILEVP1)+GRAV*ZH-RL*QLWC
!
!     * LIQUID WATER STATIC ENERGY AND TOTAL WATER AT SURFACE.
!
      L=ILEV
      DO IL=IL1,IL2
        QTG(IL)=QZERO(IL)
        HMNG(IL)=CPRES*TZERO(IL)
      ENDDO
!
!     * MIX CLOUD WATER STATIC ENERGY AND TOTAL WATER.
!
      THT=THROW(:,ILEVP1)
      CALL ABCVDQ6 ( A,B,C,CL,UN,CDVLT,GRAV, &
                     IL1,IL2,ILG,ILEV,ILEVP1,ILEV, &
                     RGAS,RKH,SHTJ,SHJ,DSHJ, &
                     THT,TF,DT)
      CALL IMPLVD7(A,B,C,CL,HMN,HMNG,IL1,IL2,ILG,ILEV, &
                   DT,DXDTT,DSHJ,RAUS,WORK,VINT)
      HMN=HMN+DT*DXDTT
      CALL IMPLVD7(A,B,C,CL,QT,QTG,IL1,IL2,ILG,ILEV, &
                   DT,DXDTT,DSHJ,RAUS,WORK,VINT)
      QT=QT+DT*DXDTT
!
!     * CHECK CONSERVATION.
!
!      DQT=0.
!      DQ=0.
!      DO L=1,ILEV
!      DO I=1,1
!        DQT=DQT+DP(I,L)*DXDTT(I,L)/GRAV
!        DQ=DQ+DP(I,L)*QT(I,L)/GRAV
!      ENDDO
!      ENDDO
!      PRINT*,'DQ,DQT=',DQ,DQT
!
!     * TEMPERATURE AND WATER VAPOUR MIXING RATIO FROM STATISTICAL
!     * CLOUD SCHEME.
!
      FACTE=1./EPS1-1.
      I2NDIE=1
      FLAGEST=1.
      CVSG=0.
      CVDU=0.
      ZFRAC=0.
      ICVSG=1
      ISUBG=0
      ICALL=1
      DO JK=1,ILEV
        DO IL=1,ILG
          PMBS(IL)=0.01*PH(IL,JK)
          RRL(IL)=RL
          CPM(IL)=CPRES
          ALMIX(IL)=MAX(ALMC(IL,JK),ALMX(IL,JK),10.)
          IF(JK.GT.1)                              THEN
            DZ       =ZH  (IL,JK-1)-ZH (IL,JK)
            DHLDZ(IL)=(HMN(IL,JK-1)-HMN(IL,JK))/DZ
            DRWDZ(IL)=(QT (IL,JK-1)-QT (IL,JK))/DZ
          ELSE
            DHLDZ(IL)=0.
            DRWDZ(IL)=0.
          ENDIF
        ENDDO
        CALL STATCLD (QCW,ZCLF(1,JK),CVAR(1,JK),ZCRAUT(1,JK), &
                      QCWVAR(1,JK),SSH,CVSG(1,JK),QT(1,JK), &
                      HMN(1,JK),ZFRAC(1,JK),CPM, &
                      PMBS,ZH(1,JK),RRL,CVDU(1,JK),FLAGEST, &
                      ALMIX,DHLDZ,DRWDZ,I2NDIE, &
                      DT,ILEV,ILG,1,ILG,ICVSG,ISUBG, &
                      JK,ICALL)
!
        DO IL=1,ILG
          DTDT(IL,JK)=DTDT(IL,JK)+ &
                     ((HMN(IL,JK)-GRAV*ZH(IL,JK)+RRL(IL)*QCW(IL)) &
                       /CPM(IL)-THROW(IL,JK))/DT
          DQDT(IL,JK)=DQDT(IL,JK)+ &
                     ((QT(IL,JK)-QCW(IL))-QROW(IL,JK))/DT
          DQLDT(IL,JK)=(QCW(IL)-QLWC(IL,JK))/DT
        ENDDO
      ENDDO

!      DQT=0.
!      DQ=0.
!      DO JK=1,ILEV
!      DO IL=1,1
!        DQT=DQT+(QT(IL,JK)-QROW(IL,JK)-QLWC(IL,JK))*DP(IL,JK)/GRAV
!      ENDDO
!      ENDDO
!      PRINT*,'DQT=',DQT
!!!!
!        QROW(:,ILEVP1)=QZERO(:)
!        THROW(:,ILEVP1)=TZERO(:)
!
      END SUBROUTINE VRTDF
