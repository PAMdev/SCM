      SUBROUTINE ABCVDQ6 (A,B,C,CL, EF ,CDVLH,GRAV, &
                          IL1,IL2,ILG,ILEV,LEV,LEVS, &
                          RGAS,RKQ,SHTJ,SHJ,DSHJ, &
                          THL,TF,TODT)

!     * JUL 15/88 - M.LAZARE : REVERSE ORDER OF LOCAL SIGMA ARRAYS.
!     * MAR 14/88 - R.LAPRISE: PREVIOUS HYBRID VERSION FOR GCM3H.

!     * CALCULATES THE THREE VECTORS FORMING THE TRI-DIAGONAL MATRIX FOR
!     * THE IMPLICIT VERTICAL DIFFUSION OF MOISTURE OH HYBRID VERSION OF
!     * MODEL. A IS THE LOWER DIAGONAL, B IS THE MAIN DIAGONAL
!     * AND C IS THE UPPER DIAGONAL.
!     * ILEV = NUMBER OF MODEL LEVELS,
!     * LEVS = NUMBER OF MOISTURE LEVELS.

      IMPLICIT REAL   (A-H,O-Z), &
      INTEGER (I-N)
      REAL   A   (ILG,ILEV),B   (ILG,ILEV),C   (ILG,ILEV),RKQ (ILG,ILEV)
      REAL   SHTJ(ILG, LEV),SHJ (ILG,ILEV),DSHJ(ILG,ILEV)
      REAL   TF  (ILG,ILEV),THL (ILG) ,    CL (ILG),EF (ILG),CDVLH (ILG)
!-----------------------------------------------------------------------
      MSG=ILEV-LEVS
      L=MSG+1
      M=L+1

      DO 50 I=IL1,IL2
         OVDS   = (GRAV*SHTJ(I,M)/RGAS)**2 &
                 /( DSHJ(I,L) * (SHJ(I,M)-SHJ(I,L)) )
         C(I,L) = OVDS*RKQ(I,M)*(1./TF(I,M))**2
   50 CONTINUE

      DO 75 I=IL1,IL2
         B(I,L) = -C(I,L)
   75 CONTINUE

      DO 100 L=MSG+2,ILEV
      DO 100 I=IL1,IL2
         OVDS   = (GRAV*SHTJ(I,L)/RGAS)**2 &
                 /( (SHJ(I,L)-SHJ(I,L-1)) * DSHJ(I,L) )
         A(I,L) = OVDS*(1./TF(I,L))**2*RKQ(I,L)
  100 CONTINUE

      DO 200 L=MSG+2,ILEV-1
      DO 200 I=IL1,IL2
         D      = DSHJ(I,L+1) / DSHJ(I,L)
         C(I,L) = A(I,L+1)*D
         B(I,L) =-A(I,L+1)*D - A(I,L)
  200 CONTINUE

      L=ILEV
      DO 250 I=IL1,IL2
         CL(I)=GRAV*SHJ(I,L)*EF(I)*CDVLH(I)/(RGAS*THL(I)*DSHJ(I,L))
  250 CONTINUE

      DO 300 I=IL1,IL2
         B(I,L) = -A(I,L) -CL(I)
  300 CONTINUE

!     * DEFINE MATRIX TO INVERT = I-2*DT*MAT(A,B,C).

      DO 500 L=MSG+1,ILEV-1
      DO 500 I=IL1,IL2
         A(I,L+1) = -TODT*A(I,L+1)
         C(I,L  ) = -TODT*C(I,L  )
  500 CONTINUE

      DO 550 L=MSG+1,ILEV
      DO 550 I=IL1,IL2
         B(I,L) = 1.-TODT*B(I,L)
  550 CONTINUE

      RETURN
      END
