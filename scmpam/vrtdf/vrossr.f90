      SUBROUTINE VROSSR(P, A,B,C,D,DELTA,ILG,IL1,IL2,M)

!     * CCRN MARS 18/85 - B.DUGAS. (VECTORISER...)
!     * PROGRAMMER  ANDREW STANIFORTH, RPN.
!     * SOLVE THE TRI-DIAGONAL MATRIX PROBLEM BELOW.

!  ** **                                      ****  ****     ***    **** &
!  ** **                                      ****  ****     ***    ****
!  ** B(1),C(1), 0  , 0  , 0  , - - - -  ,  0   **  **  P(1)  **    **
!  ** A(2),B(2),C(2), 0  , 0  , - - - -  ,  0   **  **  P(2)  **    **
!  **  0  ,A(3),B(3),C(3), 0  , - - - -  ,  0   **  **  P(3)  **    **
!  **  0  , 0  ,A(4),B(4),C(4), - - - -  ,  0   **  **  P(4)  **    **
!  **  0  , 0  , 0  ,A(5),B(5), - - - -  ,  0   **  **  P(5)  ** -- **
!  **  -                                    -   **  **    -   ** -- **
!  **  -                                    -   **  **    -   **    **
!  **  0  , - - , 0 ,A(M-2),B(M-2),C(M-2),  0   **  ** P(M-2) **    ** D
!  **  0  , - - , 0 ,  0   ,A(M-1),B(M-1),C(M-1)**  ** P(M-1) **    ** D
!  **  0  , - - , 0 ,  0   ,  0   , A(M) , B(M) **  **  P(M)  **    ** &
!  ** *                                       ****  ****    ****    **** &
!  ** *                                       ****  ****    ****    ****


!     * THE ROUTINE SOLVES IL2-IL1+1 OF THESE PROBLEMS SIMULTANIOUSLY,
!     * THAT IS, IT DOES ONE LATITUDE CIRCLE AT A TIME.
!     * DELTA IS A WORKING ARRAY OF DIMENSION ILG X M.

      IMPLICIT REAL   (A-H,O-Z), &
      INTEGER (I-N)

!      REAL   P(ILG,1),A(ILG,1),B(ILG,1),C(ILG,1),D(ILG,1)
!      REAL   DELTA(ILG,1)
      REAL   P(ILG,M),A(ILG,M),B(ILG,M),C(ILG,M),D(ILG,M)
      REAL   DELTA(ILG,M)
!-----------------------------------------------------------------------
      DO 10 I=IL1,IL2
         C(I,M)=0.
   10 CONTINUE

      DO 30 I=IL1,IL2
         X         =1./B(I,1)
         P(I,1)    =-C(I,1)*X
         DELTA(I,1)=D(I,1)*X
   30 CONTINUE

      DO 50 L=2,M
         DO 50 I=IL1,IL2
            X         =1./(B(I,L)+A(I,L)*P(I,L-1))
            P(I,L)    =-C(I,L)*X
            DELTA(I,L)=(D(I,L)-A(I,L)*DELTA(I,L-1))*X
   50 CONTINUE

      DO 60 I=IL1,IL2
         P(I,M)=DELTA(I,M)
   60 CONTINUE

      DO 70 L=M-1,1,-1
         DO 70 I=IL1,IL2
            P(I,L)=P(I,L)*P(I,L+1)+DELTA(I,L)
   70 CONTINUE

      RETURN
      END
