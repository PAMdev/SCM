      SUBROUTINE EMIDMS(DDMSDT,FOCN,DMSO,GT,ZSPD,DP,ILG,ILEV)
!
      USE SDPHYS
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL, INTENT(IN), DIMENSION(ILG) :: FOCN, DMSO, GT, ZSPD
      REAL, INTENT(IN), DIMENSION(ILG,ILEV) :: DP
      REAL, INTENT(OUT), DIMENSION(ILG) :: DDMSDT
!
!     * CALCULATE DMS EMISSIONS FOLLOWING LISS+MERLIVAT
!     * DMS SEAWATER CONC. FROM KETTLE ET AL.
!     * LIMIT ZSST SO FORMULATION FOR SCHMIDT NUMBER REMAINS
!     * POSITIVE.
!
      SSTMAX=36.
      DO IL=1,ILG
        ZDMSCON=DMSO(IL)*FOCN(IL)
        ZSST=MIN(GT(IL)-273.15,SSTMAX)
!
!       * SCHMIDT NUMBER ACCORDING TO SALTZMAN ET AL. (1993).
!
        ZSCHMIDT=2674.0-147.12*ZSST+3.726*ZSST**2-0.038*ZSST**3
!
!       * GAS TRANSFER VELOCITY AT SCHMIDT NUMBER OF 600
!       * ACCORDING TO NIGTINGALE ET AL. (2000).
!
        ZKW=0.222*ZSPD(IL)**2+0.333*ZSPD(IL)
!
!       * GAS TRANSFER VELOCITY AT CURRENT SCHMIDT NUMBER, AIRSIDE
!       * TRANSFER VELOCITY, AND FINAL TRANSFER VELOCITY (MCGILLIS
!       * ET AL., 2000).
!
        ZKW=ZKW*SQRT(600./ZSCHMIDT)
        AMWR=18.0153/62.13
        KA=659.*ZSPD(IL)*SQRT(AMWR)
        ALPHA=EXP(3525./(ZSST+273.15)-9.464)
        GAMMA=1./(1.+KA/(ALPHA*ZKW))
        ZVDMS=ZKW*(1.-GAMMA)
!
!       * DMS FLUX IN KG/M2/SEC, CONVERTED FROM NANOMOL/LTR*CM/HOUR.
!
        ZDMSEMISS=ZDMSCON*ZVDMS*32.064E-11/3600.
!
!       * NANOMOL/LTR*CM/HOUR --> KG-S/M**2/SEC
!
        DDMSDT(IL)=ZDMSEMISS*GRAV/DP(IL,ILEV)
      ENDDO
!
      END SUBROUTINE EMIDMS
