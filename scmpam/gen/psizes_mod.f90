      MODULE PSIZES
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     BASIC ARRAY DIMENSIONS.
!
!     HISTORY:
!     --------
!     * FEB 10/2010 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
!-----------------------------------------------------------------------
!     * Number of grid points in horizontal direction
      INTEGER :: ILG
!     * Vertical levels in driving atmospheric model.
      INTEGER :: ILEV
      INTEGER :: ILEVP1
      INTEGER :: LEVS
!     * Array dimensions for PLA aerosol calculations
      INTEGER :: MSGT
      INTEGER :: MSGP1
      INTEGER :: MSGP2
!     * Tracers
      INTEGER :: NTRAC
!     * Number of distinct canopy types in class
      INTEGER, PARAMETER :: ICAN = 4
!
      END MODULE PSIZES
