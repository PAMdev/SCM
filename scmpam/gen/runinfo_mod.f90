      MODULE RUNINFO
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     BASIC INFORMATION ABOUT EXPERIMENT AND AIR PROPERIES.
!
!     HISTORY:
!     --------
!     * SEP 19/2008 - K.VONSALZEN   ADD MUTLI LEVEL INFO
!     * AUG 11/2006 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
!-----------------------------------------------------------------------
!     * GENERAL MODEL INFORMATION (NAME, LEVELS, TIME STEP).
!
      INTEGER, PARAMETER :: IMAXL=50
      TYPE MPRP
        CHARACTER(LEN=IMAXL) :: RUNNAME
        REAL :: DZSFC, DT, WUP, LAT, SDOY
        REAL :: NUGALT, NUGVD, TAUX, TAUT, TAUQ, TAUM
        REAL :: CDN, CDNN, ZREF
        INTEGER :: PLEV, NTSTP
        INTEGER :: PLTFQ, TRAX, TRAT, TRAQ, TRUP, CASE
      END TYPE MPRP
      TYPE(MPRP) :: MODL
      NAMELIST /MODNML/ MODL
!
!     * INITIAL PROPERTIES OF THE AIR.
!
      INTEGER, PARAMETER :: NPERMX=100
      TYPE AIRPRP
        REAL :: ZHI, TEMPI, PRESI, SVICLR
        REAL, DIMENSION(NPERMX) ::  SVICLD, CLDF, DTG, DTA, DRV
        INTEGER :: NPER
        INTEGER, DIMENSION(NPERMX) :: &
                  LVBOT, LVTOP, PERON, PEROFF, DTGON, DTGOFF
      END TYPE AIRPRP
      TYPE(AIRPRP) :: AIR
      NAMELIST /AIRNML/ AIR
!
!     * EMISSIONS .
!
      TYPE EMISS
        REAL :: SO2EM, ORGEM, OHROW, O3ROW, &
                FLND, FOCN, &
                DOCEM1, DOCEM2, DOCEM3, DOCEM4, &
                DBCEM1, DBCEM2, DBCEM3, DBCEM4, &
                ST02ROW, ST03ROW, ST04ROW, ST06ROW, &
                ST13ROW, ST14ROW, ST15ROW, ST16ROW, ST17ROW, &
                FCANROW1, FCANROW2, FCANROW3, FCANROW4, SPOTROW, &
                SMFRAC, BSFRAC
      END TYPE EMISS
      TYPE(EMISS) :: EMIS
      NAMELIST /EMISNML/ EMIS
!
      END MODULE RUNINFO
