      SUBROUTINE TRINFO(NTRACP,IAINDT,ITRWET,NTRAC)
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     DUMMY ROUTINE REPLACING TRACER INFORMATION FOR ATMOSPHERIC MODEL.
!
!     HISTORY:
!     --------
!     * FEB 11/2010 - K.VONSALZEN   NEW
!
!-----------------------------------------------------------------------
!
      USE TRCIND, ONLY : ISO2,IHPO,IGS6,IGSP,IDMS ! GAS-PHASE TRACER INDICES
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
!     * MAXIMUM NUMBER OF AEROSOL TRACERS. MAXAERO SHOULD BE
!     * EQUAL TO THE NUMBER OF AEROSOL TRACERS THAT ARE SPECIFIED
!     * ACCORDING TO INPUT FILE "PARAM".
!
      INTEGER, PARAMETER :: MAXAERO=24
!
!     * NUMBER OF OTHER TRACERS IN THE MODEL.
!
      INTEGER, PARAMETER :: MAXOTHR=5
!
      CHARACTER(LEN=4) :: CHTMP
      CHARACTER(LEN=4), DIMENSION(NTRAC) :: CHMNAM
      INTEGER, DIMENSION(NTRAC) :: IAINDT, ITRWET
!
!-----------------------------------------------------------------------
!     * CHECK IF SUFFICIENT MEMORY IS AVAILABLE TO ACCOMMODATE TRACERS.
!     * INCREASE NTRAC, IF NECESSARY.
!
      IF ( NTRAC < MAXAERO+MAXOTHR ) CALL XIT('TRINFO',-1)
!
!     * GIVE WARNING IN CASE THE MEMORY REQUIRED FOR TRACERS IS LESS
!     * THAN SPECIFIED. CHANGE NTRAC ACCORDINGLY IF THIS WARNING APPEARS.
!
      IF ( NTRAC > MAXAERO+MAXOTHR ) CALL WRN('TRINFO',-1)
!
!     * ALSO MAKE SURE THAT TRACER NAMES FOR AEROSOL DON'T EXCEED 3
!     * LETTERS.
!
      IF ( MAXAERO > 99 ) CALL XIT('TRINFO',-2)
!
!     * INITIALIZATION OF TRACER NAMES.
!
      DO I=1,NTRAC
        WRITE(CHMNAM(I),'(A4)') '****'
      ENDDO
!
!-----------------------------------------------------------------------
!     * SPECIFY TRACER NAMES FOR AEROSOL.
!
      DO I=1,MAXAERO
        IF ( I >= 10 ) THEN
          WRITE(CHMNAM(I),'(1X,A1,I2)') 'A',I
        ELSE
          WRITE(CHMNAM(I),'(1X,A2,I1)') 'A0',I
        ENDIF
      ENDDO
!
!     * SPECIFY TRACER NAMES FOR GASES (NEEDS TO BE CONSISTENT WITH
!     * PARAMETER MAXOTHR).
!
      IF ( MAXOTHR /= 5 ) CALL XIT('TRINFO',-3)
      CHMNAM(MAXAERO+1)=' SO2'
      CHMNAM(MAXAERO+2)=' DMS'
      CHMNAM(MAXAERO+3)=' HPO'
      CHMNAM(MAXAERO+4)=' GS6'
      CHMNAM(MAXAERO+5)=' GSP'
!
!     * SUMMARY OF GAS TRACERS.
!
      WRITE(6,'(1X)')
      WRITE(6,'(A9)')    '*** Gases'
      WRITE(6,'(1X)')
      WRITE(6,'(2X,A62)')                                               &
            'Gas Index          Gas Type                             Tra&
      cer'
      WRITE(6,'(3X,A4,14X,A14,24X,I2)') 'ISO2','SO2           ', &
                                        MAXAERO+1
      WRITE(6,'(3X,A4,14X,A14,24X,I2)') 'IDMS','DMS           ', &
                                        MAXAERO+2
      WRITE(6,'(3X,A4,14X,A14,24X,I2)') 'IHPO','H2O2          ', &
                                        MAXAERO+3
      WRITE(6,'(3X,A4,14X,A14,24X,I2)') 'IGS6','H2SO4         ', &
                                        MAXAERO+4
      WRITE(6,'(3X,A4,14X,A14,24X,I2)') 'IGSP','SOA PRECURSORS', &
                                        MAXAERO+5
      WRITE(6,'(1X)')
!
!-----------------------------------------------------------------------
!     * SPECIFY CORRESPONDING TRACER INDICES. MAKE SURE THAT
!     * INDICES ARE CONSISTENT WITH SPECIFICATION OF COMMON BLOCK
!     * AND TRACER NAMES ABOVE!
!
      ISO2=MAXAERO+1
      IDMS=MAXAERO+2
      IHPO=MAXAERO+3
      IGS6=MAXAERO+4
      IGSP=MAXAERO+5
!
!     * FLAG FOR WET DEPOSTION.
!
      ITRWET(ISO2)=0
      ITRWET(IDMS)=0
      ITRWET(IHPO)=0
      ITRWET(IGS6)=1
      ITRWET(IGSP)=1
!
!-----------------------------------------------------------------------
!     * EXTRACT AEROSOL TRACERS FOR PLA CALCULATIONS. ALL TRACERS
!     * WITH NAMES THAT START WITH AN "A" AND ARE FOLLOWED BY A NUMBER
!     * ARE CONSIDERED AEROSOL TRACERS (I.E. FORMAT ' A##').
!
      WRITE(6,'(1X)')
      WRITE(6,'(A12)')    '*** Aerosols'
      WRITE(6,'(1X)')
      WRITE(6,'(2X,A62)')                                               &
            'Aer Index          Aer Type                             Tra&
      cer'
      IAINDT=-1
      NTRACP=0
      DO I=1,NTRAC
        ITRWET(I)=0
        CHTMP=CHMNAM(I)
        IF ( I <= MAXAERO ) THEN
          WRITE(6,'(3X,A2,A2,14X,A4,34X,I2)') 'IA',CHTMP(3:4),CHTMP,I
        ENDIF
        IF ( CHTMP(2:2) == 'A' &
             .AND. (CHTMP(3:3) == '0' .OR. CHTMP(3:3) == '1' &
               .OR. CHTMP(3:3) == '2' .OR. CHTMP(3:3) == '3' &
               .OR. CHTMP(3:3) == '4' .OR. CHTMP(3:3) == '5' &
               .OR. CHTMP(3:3) == '6' .OR. CHTMP(3:3) == '7' &
               .OR. CHTMP(3:3) == '8' .OR. CHTMP(3:3) == '9') &
             .AND. (CHTMP(4:4) == '0' .OR. CHTMP(4:4) == '1' &
               .OR. CHTMP(4:4) == '2' .OR. CHTMP(4:4) == '3' &
               .OR. CHTMP(4:4) == '4' .OR. CHTMP(4:4) == '5' &
               .OR. CHTMP(4:4) == '6' .OR. CHTMP(4:4) == '7' &
               .OR. CHTMP(4:4) == '8' .OR. CHTMP(4:4) == '9') &
            ) THEN
          NTRACP=NTRACP+1
          IF ( NTRACP > NTRAC ) CALL XIT('TRINFO',-4)
          IAINDT(NTRACP)=I
        ENDIF
      ENDDO
!
      END SUBROUTINE TRINFO
