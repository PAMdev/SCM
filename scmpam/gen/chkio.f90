      SUBROUTINE CHKIO
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     CHECKS AND TRIMS MODEL INPUT.
!
!     HISTORY:
!     --------
!     * FEB 12/2010 - K.VONSALZEN   ADD NEW PARAMETERS.
!     * SEP 14/2007 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      USE PSIZES
      USE RUNINFO
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
!---------------------------------------------------------------------
!     * CHECK PARAMETER RANGES.
!
      ILENG=LEN_TRIM(MODL%RUNNAME)
      IF ( ILENG == 0 .OR. ILENG > IMAXL ) CALL XIT('CHKIO',-1)
      IF ( MODL%PLEV < 1 ) CALL XIT('CHKIO',-2)
      IF ( MODL%DZSFC <= 0. ) CALL XIT('CHKIO',-3)
      IF ( MODL%DT <= 0. ) CALL XIT('CHKIO',-5)
      IF ( AIR%ZHI < 0. )  CALL XIT('CHKIO',-6)
      IF ( AIR%TEMPI <= 273.15-5. .OR. AIR%TEMPI >= 323. ) &
                           CALL XIT('CHKIO',-7)
      IF ( AIR%PRESI <= 30000. .OR. AIR%PRESI >= 110000. ) &
                           CALL XIT('CHKIO',-8)
      IF ( AIR%SVICLR < -1. .OR. AIR%SVICLR > 0.3 ) &
                           CALL XIT('CHKIO',-9)
      IF ( AIR%NPER < 0 .OR. AIR%NPER > NPERMX ) &
                           CALL XIT('CHKIO',-10)
      DO NC=1,AIR%NPER
        IF ( ABS(AIR%DRV(NC)) > 1.E-09 &
                                .OR. ABS(AIR%DTA(NC)) > 1.E-09 ) THEN
          IF ( AIR%LVBOT(NC) > ILEV .OR. AIR%LVTOP(NC) < 1 ) &
                           CALL XIT('CHKIO',-12)
          IF ( AIR%LVBOT(NC) < AIR%LVTOP(NC) ) &
                           CALL XIT('CHKIO',-13)
          IF ( AIR%PERON(NC) > AIR%PEROFF(NC) .OR. AIR%PERON(NC) < 1 &
             .OR. AIR%PEROFF(NC) > MODL%NTSTP ) &
                           CALL XIT('CHKIO',-14)
        ENDIF
        IF ( AIR%CLDF(NC) > 1. ) &
                           CALL XIT('CHKIO',-15)
        IF ( AIR%CLDF(NC) > 0. ) THEN
          IF ( AIR%SVICLD(NC) < -1. .OR. AIR%SVICLD(NC) > 0.3 ) &
                           CALL XIT('CHKIO',-16)
          IF ( AIR%LVBOT(NC) > ILEV .OR. AIR%LVTOP(NC) < 1 ) &
                           CALL XIT('CHKIO',-17)
          IF ( AIR%LVBOT(NC) < AIR%LVTOP(NC) ) &
                           CALL XIT('CHKIO',-18)
          IF ( AIR%PERON(NC) > AIR%PEROFF(NC) .OR. AIR%PERON(NC) < 1 &
             .OR. AIR%PEROFF(NC) > MODL%NTSTP ) &
                           CALL XIT('CHKIO',-19)
        ENDIF
        IF ( ABS(AIR%DTG(NC)) > 1.E-09 ) THEN
          IF ( AIR%DTGON(NC) > AIR%DTGOFF(NC) .OR. AIR%DTGON(NC) < 1 &
             .OR. AIR%DTGOFF(NC) > MODL%NTSTP ) &
                           CALL XIT('CHKIO',-20)
        ENDIF
      ENDDO
!
      END SUBROUTINE CHKIO
