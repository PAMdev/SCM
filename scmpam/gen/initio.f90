      SUBROUTINE INITIO(KTIME1,RDINPUT)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      LOGICAL, INTENT(IN) :: KTIME1
      LOGICAL, INTENT(IN) :: RDINPUT
!
!     * INITIALIZATIONS.
!
      DO IT=1,IMAXL
        CNA(IT:IT) = '*'
      ENDDO
      DO IT=1,ITOT
        IVAR(IT)%NAME=CNA
        IVAR(IT)%LNAME=CNA
        IVAR(IT)%UNITS=CNA
        IVAR(IT)%DIM=CNA
        IVARV(IT)%FLD0D=-YNAX
      ENDDO
      DO IT=1,ITOT
        OVAR(IT)%NAME=CNA
        OVAR(IT)%LNAME=CNA
        OVAR(IT)%UNITS=CNA
        OVAR(IT)%DIM=CNA
        OVARV(IT)%FLD0D=-YNAX
      ENDDO
      DO IT=1,ITOT
        RVAR(IT)%NAME=CNA
        RVAR(IT)%LNAME=CNA
        RVAR(IT)%UNITS=CNA
        RVAR(IT)%DIM=CNA
        RVARV(IT)%FLD0D=-YNAX
      ENDDO
!
!     * OPEN AND READ NAMELISTS.
!
      OPEN(20,FILE='OUTPUT')
      OPEN(30,FILE='RESTART')
      READ(20,NML=OVARNML)
      READ(30,NML=RVARNML)
!
      IF (RDINPUT) THEN     
        OPEN(10,FILE='INPUT')
        READ(10,NML=IVARNML)
      ENDIF
!
!     * SAVE DIMENSIONS AND ALLOCATE MEMORY FOR I/O ARRAYS.
!
      DO IT=1,ITOT
        IF ( IVAR(IT)%NAME /= CNA ) THEN
          IF ( .NOT.(IVAR(IT)%LNAME /= CNA &
               .AND. IVAR(IT)%UNITS /= CNA) ) THEN
            PRINT*,'MISSING LNAME OR UNITS FOR INPUT VARIABLE ', &
                    TRIM(IVAR(IT)%NAME)
            CALL XIT('INITIO',-1)
          ENDIF
          DO ITX=1,IT-1
            IF ( TRIM(IVAR(IT)%NAME) == TRIM(IVAR(ITX)%NAME) ) THEN
              PRINT*,'MULTIPLE OCCURRENCES OF INPUT VARIABLE ', &
                      TRIM(IVAR(IT)%NAME)
              CALL XIT('INITIO',-2)
            ENDIF
          ENDDO
          IDIM=0
          DO IS=1,ISMX
            CTMP=IVAR(IT)%DIM(IS)
            IF ( CTMP /= CNA ) THEN
              IDIM=IDIM+1
              ICHK=0
              DO ND=1,NDIM
                IF ( TRIM(CTMP) == TRIM(DIM(ND)%NAME) ) THEN
                  DIM(ND)%FLGI=1
                  IDIMA(IDIM)=DIM(ND)%SIZE
                  IF ( DIM(ND)%SIZE == INAX ) THEN
                    PRINT*,'DIMENSION ',TRIM(DIM(ND)%NAME),' UNDEFINED'
                    CALL XIT ('INITIO',-3)
                  ENDIF
                  ICHK=1
                ENDIF
              ENDDO
              IF ( ICHK == 0 ) THEN
                PRINT*,'INCORRECT DIMENSION FOR INPUT VARIABLE ', &
                       TRIM(IVAR(IT)%NAME)
                CALL XIT('INITIO',-4)
              ENDIF
            ENDIF
          ENDDO
          IF ( IDIM == 0 ) THEN
            IVARV(IT)%FLD0D=YNAX
          ELSE IF ( IDIM == 1 ) THEN
            ALLOCATE(IVARV(IT)%FLD1D(IDIMA(1)))
            IVARV(IT)%FLD1D(:)=YNAX
          ELSE IF ( IDIM == 2 ) THEN
            ALLOCATE(IVARV(IT)%FLD2D(IDIMA(1),IDIMA(2)))
            IVARV(IT)%FLD2D(:,:)=YNAX
          ELSE IF ( IDIM == 3 ) THEN
            ALLOCATE(IVARV(IT)%FLD3D(IDIMA(1),IDIMA(2),IDIMA(3)))
            IVARV(IT)%FLD3D(:,:,:)=YNAX
          ELSE IF ( IDIM == 4 ) THEN
            ALLOCATE(IVARV(IT)%FLD4D(IDIMA(1),IDIMA(2),IDIMA(3), &
                                                            IDIMA(4)))
            IVARV(IT)%FLD4D(:,:,:,:)=YNAX
          ELSE
            CALL XIT('INITIO',-5)
          ENDIF
        ENDIF
      ENDDO
      DO IT=1,ITOT
        IF ( OVAR(IT)%NAME /= CNA ) THEN
          IF ( .NOT.(OVAR(IT)%LNAME /= CNA &
               .AND. OVAR(IT)%UNITS /= CNA) ) THEN
            PRINT*,'MISSING LNAME OR UNITS FOR OUTPUT VARIABLE ', &
                    TRIM(OVAR(IT)%NAME)
            CALL XIT('INITIO',-6)
          ENDIF
          DO ITX=1,IT-1
            IF ( TRIM(OVAR(IT)%NAME) == TRIM(OVAR(ITX)%NAME) ) THEN
              PRINT*,'MULTIPLE OCCURRENCES OF OUTPUT VARIABLE ', &
                      TRIM(OVAR(IT)%NAME)
              CALL XIT('INITIO',-7)
            ENDIF
          ENDDO
          IDIM=0
          DO IS=1,ISMX
            CTMP=OVAR(IT)%DIM(IS)
            IF ( KTIME1 .AND. TRIM(CTMP) == 'time' ) THEN
              OVAR(IT)%DIM(IS)='time1'
              CTMP=OVAR(IT)%DIM(IS)
            ENDIF
            IF ( CTMP /= CNA ) THEN
              IDIM=IDIM+1
              ICHK=0
              DO ND=1,NDIM
                IF ( TRIM(CTMP) == TRIM(DIM(ND)%NAME) ) THEN
                  DIM(ND)%FLGO=1
                  IDIMA(IDIM)=DIM(ND)%SIZE
                  IF ( DIM(ND)%SIZE == INAX ) THEN
                    PRINT*,'DIMENSION ',TRIM(DIM(ND)%NAME),' UNDEFINED'
                    CALL XIT ('INITIO',-8)
                  ENDIF
                  ICHK=1
                ENDIF
              ENDDO
              IF ( ICHK == 0 ) THEN
                PRINT*,'INCORRECT DIMENSION FOR OUTPUT VARIABLE ', &
                       TRIM(OVAR(IT)%NAME)
                CALL XIT('INITIO',-9)
              ENDIF
            ENDIF
          ENDDO
          IF ( IDIM == 0 ) THEN
            OVARV(IT)%FLD0D=YNAX
          ELSE IF ( IDIM == 1 ) THEN
            ALLOCATE(OVARV(IT)%FLD1D(IDIMA(1)))
            OVARV(IT)%FLD1D(:)=YNAX
          ELSE IF ( IDIM == 2 ) THEN
            ALLOCATE(OVARV(IT)%FLD2D(IDIMA(1),IDIMA(2)))
            OVARV(IT)%FLD2D(:,:)=YNAX
          ELSE IF ( IDIM == 3 ) THEN
            ALLOCATE(OVARV(IT)%FLD3D(IDIMA(1),IDIMA(2),IDIMA(3)))
            OVARV(IT)%FLD3D(:,:,:)=YNAX
          ELSE IF ( IDIM == 4 ) THEN
            ALLOCATE(OVARV(IT)%FLD4D(IDIMA(1),IDIMA(2),IDIMA(3), &
                                                            IDIMA(4)))
            OVARV(IT)%FLD4D(:,:,:,:)=YNAX
          ELSE
            CALL XIT('INITIO',-10)
          ENDIF
        ENDIF
      ENDDO
      DO IT=1,ITOT
        IF ( RVAR(IT)%NAME /= CNA ) THEN
          IF ( .NOT.(RVAR(IT)%LNAME /= CNA &
               .AND. RVAR(IT)%UNITS /= CNA) ) THEN
            PRINT*,'MISSING LNAME OR UNITS FOR RESTART VARIABLE ', &
                    TRIM(RVAR(IT)%NAME)
            CALL XIT('INITIO',-11)
          ENDIF
          DO ITX=1,IT-1
            IF ( TRIM(RVAR(IT)%NAME) == TRIM(RVAR(ITX)%NAME) ) THEN
              PRINT*,'MULTIPLE OCCURRENCES OF RESTART VARIABLE ', &
                      TRIM(RVAR(IT)%NAME)
              CALL XIT('INITIO',-12)
            ENDIF
          ENDDO
          IDIM=0
          DO IS=1,ISMX
            CTMP=RVAR(IT)%DIM(IS)
            IF ( CTMP /= CNA ) THEN
              IDIM=IDIM+1
              ICHK=0
              DO ND=1,NDIM
                IF ( TRIM(CTMP) == TRIM(DIM(ND)%NAME) ) THEN
                  DIM(ND)%FLGRS=1
                  IDIMA(IDIM)=DIM(ND)%SIZE
                  IF ( DIM(ND)%SIZE == INAX ) THEN
                    PRINT*,'DIMENSION ',TRIM(DIM(ND)%NAME),' UNDEFINED'
                    CALL XIT ('INITIO',-13)
                  ENDIF
                  ICHK=1
                ENDIF
              ENDDO
              IF ( ICHK == 0 ) THEN
                PRINT*,'INCORRECT DIMENSION FOR RESTART VARIABLE ', &
                       TRIM(RVAR(IT)%NAME)
                CALL XIT('INITIO',-14)
              ENDIF
            ENDIF
          ENDDO
          IF ( IDIM == 0 ) THEN
            RVARV(IT)%FLD0D=YNAX
          ELSE IF ( IDIM == 1 ) THEN
            ALLOCATE(RVARV(IT)%FLD1D(IDIMA(1)))
            RVARV(IT)%FLD1D(:)=YNAX
          ELSE IF ( IDIM == 2 ) THEN
            ALLOCATE(RVARV(IT)%FLD2D(IDIMA(1),IDIMA(2)))
            RVARV(IT)%FLD2D(:,:)=YNAX
          ELSE IF ( IDIM == 3 ) THEN
            ALLOCATE(RVARV(IT)%FLD3D(IDIMA(1),IDIMA(2),IDIMA(3)))
            RVARV(IT)%FLD3D(:,:,:)=YNAX
          ELSE IF ( IDIM == 4 ) THEN
            ALLOCATE(RVARV(IT)%FLD4D(IDIMA(1),IDIMA(2),IDIMA(3), &
                                                             IDIMA(4)))
            RVARV(IT)%FLD4D(:,:,:,:)=YNAX
          ELSE
            CALL XIT('INITIO',-15)
          ENDIF
        ENDIF
      ENDDO
!
!     * CLOSE INPUT FILES.
!
      CLOSE(11)
      CLOSE(12)
      IF (RDINPUT) THEN
        CLOSE(10)
      ENDIF
!
      END SUBROUTINE INITIO
