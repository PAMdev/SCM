!>\file 
!>\brief Prints out warning message.
!!
!! @author K. von Salzen
!     
!-----------------------------------------------------------------------
      SUBROUTINE WRN(NAME,N)
!
      IMPLICIT NONE
!
      INTEGER :: I, N
      CHARACTER(LEN=*) :: NAME
      CHARACTER(LEN=4) :: DASH, STAR
      CHARACTER(LEN=8) :: NAME8
      CHARACTER(LEN=42) :: FRMT
!
      FRMT = "FORMAT('0',2A4,'  WARNING  ',A8,17A4,I8)"
      DATA DASH /'----'/, STAR /'****'/
!---------------------------------------------------------------------
!
      NAME8 = NAME
      IF(N.GE.0) WRITE(6,FRMT) (DASH,I=1,2),NAME8,(DASH,I=1,17),N
!
      IF(N.LT.0) WRITE(6,FRMT) (STAR,I=1,2),NAME8,(STAR,I=1,17),N
!
      END SUBROUTINE WRN
