!>\file 
!>\brief Terminates a program by printing the program name and
!>       a line across the page followed by a number N.
!!
!! @authors J.D. Henderson and E. Chan
!     
!-----------------------------------------------------------------------
      SUBROUTINE XIT(NAME,N)
!
      IMPLICIT NONE
!
      INTEGER :: I, N
      CHARACTER(LEN=*) :: NAME
      CHARACTER(LEN=8) :: NAME8, DASH, STAR
      CHARACTER(LEN=30) :: FRMT
!
      FRMT = "('0',A8,'  END  ',A8,9A8,I8)"
!
      DATA DASH /'--------'/, STAR /'********'/
!---------------------------------------------------------------------
!
      NAME8 = NAME
      IF(N.GE.0) WRITE(6,FRMT) DASH,NAME8,(DASH,I=1,9),N
!
      IF(N.LT.0) WRITE(6,FRMT) STAR,NAME8,(STAR,I=1,9),N
!
      IF ( N.GE.0 .OR. N.LT.-100 ) THEN
        STOP
      ELSE
        CALL ABORT
      ENDIF
!
      END
!> \file
!! \subsection ssec_details Details 
!!     N > 0 is for a normal end. The line is dashed.
!!     normal ends terminate with stop.
!!     N < 0 is for an abnormal end. The line is dotted.
!!     If N is less than -100 the program simply terminates.
!!     Otherwise if N is less than zero the program aborts.
