!>\file 
!>\brief Contains routines for basic math
!     
!-----------------------------------------------------------------------
      MODULE MATHMOD
!-----------------------------------------------------------------------
!
      CONTAINS
!
      subroutine vexp(xout,xin,nin)
!======================================================================= &
! vec or exponential. substitute by appropriate function on supercomputer.
!=======================================================================
      USE FPDEF
      integer :: nin
      real(R8), dimension(nin) :: xout,xin
!
      if ( kdbl ) then
        xout(1:nin) = EXP(xin(1:nin))
      else
        xout(1:nin) =                                                  &!PROC-DEPEND
                      EXP(xin(1:nin))
      endif
!
      end subroutine vexp
      subroutine vlog(xout,xin,nin)
!======================================================================= &
! vec or log. substitute by appropriate function on supercomputer.
!=======================================================================
      USE FPDEF
      integer :: nin
      real(R8), dimension(nin) :: xout,xin
!
      if ( kdbl ) then
        xout(1:nin) = LOG(xin(1:nin))
      else
        xout(1:nin) =                                                  &!PROC-DEPEND
                      LOG(xin(1:nin))
      endif
!
      end subroutine vlog
      subroutine vsqrt(xout,xin,nin)
!======================================================================= &
! vec or square root, substitute by appropriate function on supercomputer.
!=======================================================================
      USE FPDEF
      integer :: nin
      real(R8), dimension(nin) :: xout,xin
!
      if ( kdbl ) then
        xout(1:nin) = SQRT(xin(1:nin))
      else
        xout(1:nin) =                                                  &!PROC-DEPEND
                      SQRT(xin(1:nin))
      endif
!
      end subroutine vsqrt
      subroutine vpow(xout,xin,yin,nin)
!======================================================================= &
! vec or power. substitute by appropriate function on supercomputer.
!=======================================================================
      USE FPDEF
      integer ::  nin
      real(R8), intent(in), dimension(nin) :: xin, yin
      real(R8), intent(out), dimension(nin) :: xout
!
      xout = xin**yin
!
      end subroutine vpow
      subroutine vsexp(xout,xin,nin)
!======================================================================= &
! vec or exponential. substitute by appropriate function on supercomputer.
!=======================================================================
      integer :: nin
      real, dimension(nin) :: xout,xin
!
      xout(1:nin) = EXP(xin(1:nin))
!
      end subroutine vsexp
      subroutine vslog(xout,xin,nin)
!======================================================================= &
! vec or log. substitute by appropriate function on supercomputer.
!=======================================================================
      integer :: nin
      real, dimension(nin) :: xout,xin
!
      xout(1:nin) = LOG(xin(1:nin))
!
      end subroutine vslog
      subroutine vssqrt(xout,xin,nin)
!======================================================================= &
! vec or square root. substitute by appropriate function on supercomputer.
!=======================================================================
      integer :: nin
      real, dimension(nin) :: xout,xin
!
      xout(1:nin) = SQRT(xin(1:nin))
!
      end subroutine vssqrt
      subroutine vspow(xout,xin,yin,nin)
!======================================================================= &
! vec or power. substitute by appropriate function on supercomputer.
!=======================================================================
      integer ::  nin
      real, intent(in), dimension(nin) :: xin, yin
      real, intent(out), dimension(nin) :: xout
!
      xout = xin**yin
!
      end subroutine vspow
!
      END MODULE MATHMOD
