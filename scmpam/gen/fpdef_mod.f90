!>\file 
!>\brief Integer kind types
!     
!-----------------------------------------------------------------------
      MODULE FPDEF
!
      INTEGER, PARAMETER :: R8 = selected_real_kind(12) ! 8 byte real
      INTEGER, PARAMETER :: R4 = selected_real_kind( 6) ! 4 byte real
      LOGICAL :: KDBL
!
      END MODULE FPDEF
