      MODULE SDPHYS
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     BASIC PHYISCAL CONSTANTS.
!
!     HISTORY:
!     --------
!     * AUG 10/2006 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
!     * BASIC PHYSICAL CONSTANTS.
!
      REAL, PARAMETER :: RHOH2O = 1.E+03         ! DENSITY OF WATER (KG/M3)
      REAL, PARAMETER :: WH2O   = 18.015E-03     ! MOLECULAR WEIGHT OF H2O (KG/MOL)
      REAL, PARAMETER :: WA     = 28.97E-03      ! MOLECULAR WEIGHT OF AIR (KG/MOL)
      REAL, PARAMETER :: RGASM  = 8.31441        ! MOLAR GAS CONSTANT (J/MOL/K)
      REAL, PARAMETER :: RGAS   = 287.04         ! GAS CONSTANT OF DRY AIR (J/KG/K)
      REAL, PARAMETER :: RGOCP  = 2./7.
      REAL, PARAMETER :: CPRES  = RGAS/RGOCP     ! HEAT CAPACITY AT CONSTANT PRESSURE
                                                 ! FOR DRY AIR (J/KG/K)
      REAL, PARAMETER :: EPS1   = 0.622          ! RATIO OF GAS CONSTANT FOR DRY
                                                 ! AIR OVER GAS CONSTANT FOR VAPOUR
      REAL, PARAMETER :: GRAV   = 9.80616        ! GRAVITATIONAL CONTSTANT (M/S2)
      REAL, PARAMETER :: RL     = 2.501E+06      ! LATENT HEAT OF VAPOURIZATION (J/KG)
!
!     * FITTING PARAMETERS FOR CALCULATION OF WATER VAPOUR SATURATION
!     * MIXING RATIO.
!
      REAL, PARAMETER :: RW1    = 53.67957
      REAL, PARAMETER :: RW2    = -6743.769
      REAL, PARAMETER :: RW3    = -4.8451
      REAL, PARAMETER :: RI1    = 23.33086
      REAL, PARAMETER :: RI2    = -6111.72784
      REAL, PARAMETER :: RI3    = 0.15215
!
      REAL, PARAMETER :: PPA    = 21.656
      REAL, PARAMETER :: PPB    = 5418.
!
      END MODULE SDPHYS
