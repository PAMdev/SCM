      SUBROUTINE VADV(DXDT,XROW,DTDT,THROW,DQDT,QROW,DQLDT,QLWC, &
                      DUDT,U,W,ZF,ZH,PH,DP,ALMC,ALMX, &
                      DT,IL1,IL2,ILG,ILEV,ILEVP1,MSGP,NTRAC)
!
      USE SDPHYS
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL, INTENT(IN), DIMENSION(ILG,ILEV) :: ZF,ZH,PH,DP,ALMC,ALMX
      REAL, DIMENSION(ILG,ILEVP1), INTENT(INOUT) :: THROW,QROW
      REAL, DIMENSION(ILG,ILEV), INTENT(IN) :: QLWC
      REAL, DIMENSION(ILG,ILEV), INTENT(INOUT) :: U,W
      REAL, DIMENSION(ILG,ILEV,NTRAC) :: RKX
      REAL, DIMENSION(ILG,ILEVP1,NTRAC), INTENT(IN) :: XROW
      REAL, DIMENSION(ILG,ILEVP1,NTRAC), INTENT(OUT) :: DXDT
      REAL, DIMENSION(ILG,ILEVP1), INTENT(OUT) :: DQDT,DTDT
      REAL, DIMENSION(ILG,ILEV), INTENT(OUT) :: DQLDT
      REAL, DIMENSION(ILG,ILEV), INTENT(OUT) :: DUDT
      REAL, DIMENSION(ILG,ILEV) :: QT,HMN,WF
      REAL, DIMENSION(ILG,ILEV) :: QTN, HMNN, CVAR, ZCRAUT, QCWVAR, &
                                   CVSG, CVDU, ZFRAC, QC, VDPQ, &
                                   VDPH, VDPU, ZCLF
      REAL, DIMENSION(ILG,ILEV,NTRAC) :: VDPX
      REAL, DIMENSION(ILG) :: PMBS, RRL, CPM, ALMIX, DHLDZ, DRWDZ, &
                              FLAGEST, QCW, SSH, WGT, DZ, TMP
!
!     * TOTAL WATER MIXING RATIO AND CLOUD WATER STATIC ENERGY.
!
      QT=QROW(:,MSGP:ILEVP1)+QLWC
      HMN=CPRES*THROW(:,MSGP:ILEVP1)+GRAV*ZH-RL*QLWC
!
!     * VERTICAL WIND AT GRID CELL INTERFACES.
!
      WF(:,ILEV)=0.
      DO L=2,ILEV
        WGT(:)=(ZF(:,L)-ZH(:,L))/(ZH(:,L-1)-ZH(:,L))
        WF(:,L-1)=(1.-WGT)*W(:,L)+WGT*W(:,L-1)
      ENDDO
!
!     * ADVECT WATER MIXING RATIO, CLOUD WATER STATIC ENERGY VERTICALLY,
!     * WINDS, AND TRACERS USING EULER FORWARD DIFFERENCES.
!
      VDPQ(:,ILEV)=0.
      VDPH(:,ILEV)=0.
      VDPU(:,ILEV)=0.
      VDPX(:,ILEV,:)=0.
      DO L=3,ILEV
        WHERE ( WF(:,L-1) <= 0. )
          DZ=ZF(:,L-2)-ZF(:,L-1)
          TMP=MAX(WF(:,L-1)*DT/DZ,-1.)*DP(:,L-1)
          VDPQ(:,L-1)=QT(:,L-1)*TMP
          VDPH(:,L-1)=HMN(:,L-1)*TMP
          VDPU(:,L-1)=U(:,L-1)*TMP
        ELSEWHERE
          DZ=ZF(:,L-1)-ZF(:,L)
          TMP=MIN(WF(:,L-1)*DT/DZ,1.)*DP(:,L)
          VDPQ(:,L-1)=QT(:,L)*TMP
          VDPH(:,L-1)=HMN(:,L)*TMP
          VDPU(:,L-1)=U(:,L)*TMP
        ENDWHERE
        DO N=1,NTRAC
          WHERE ( WF(:,L-1) <= 0. )
            DZ=ZF(:,L-2)-ZF(:,L-1)
            TMP=MAX(WF(:,L-1)*DT/DZ,-1.)*DP(:,L-1)
            VDPX(:,L-1,N)=XROW(:,L-1,N)*TMP
          ELSEWHERE
            DZ=ZF(:,L-1)-ZF(:,L)
            TMP=MIN(WF(:,L-1)*DT/DZ,1.)*DP(:,L)
            VDPX(:,L-1,N)=XROW(:,L,N)*TMP
          ENDWHERE
        ENDDO
      ENDDO
      L=2
      VDPQ(:,1)=0.
      VDPH(:,1)=0.
      VDPU(:,1)=0.
      VDPX(:,1,:)=0.
      WHERE ( WF(:,L-1) > 0. )
        DZ=ZF(:,L-1)-ZF(:,L)
        TMP=MIN(WF(:,L-1)*DT/DZ,1.)*DP(:,L)
        VDPQ(:,L-1)=QT(:,L)*TMP
        VDPH(:,L-1)=HMN(:,L)*TMP
        VDPU(:,L-1)=U(:,L)*TMP
      ENDWHERE
      DO N=1,NTRAC
        WHERE ( WF(:,L-1) > 0. )
          DZ=ZF(:,L-1)-ZF(:,L)
          TMP=MIN(WF(:,L-1)*DT/DZ,1.)*DP(:,L)
          VDPX(:,L-1,N)=XROW(:,L,N)*TMP
        ENDWHERE
      ENDDO
      L=1
      QT(:,L)=QT(:,L)+VDPQ(:,L)/DP(:,L)
      HMN(:,L)=HMN(:,L)+VDPH(:,L)/DP(:,L)
      DUDT(:,L)=VDPU(:,L)/DP(:,L)/DT
      DO N=1,NTRAC
        DXDT(:,L,N)=VDPX(:,L,N)/DP(:,L)/DT
      ENDDO
      DO L=2,ILEV
        QT(:,L)=QT(:,L)-(VDPQ(:,L-1)-VDPQ(:,L))/DP(:,L)
        HMN(:,L)=HMN(:,L)-(VDPH(:,L-1)-VDPH(:,L))/DP(:,L)
        DUDT(:,L)=-(VDPU(:,L-1)-VDPU(:,L))/DP(:,L)/DT
        DO N=1,NTRAC
          DXDT(:,L,N)=-(VDPX(:,L-1,N)-VDPX(:,L,N))/DP(:,L)/DT
        ENDDO
      ENDDO
!
!     * TEMPERATURE AND WATER VAPOUR MIXING RATIO FROM STATISTICAL
!     * CLOUD SCHEME.
!
      FACTE=1./EPS1-1.
      I2NDIE=1
      FLAGEST=1.
      CVSG=0.
      CVDU=0.
      ZFRAC=0.
      ICVSG=1
      ISUBG=1
      ICALL=1
      DO JK=1,ILEV
        DO IL=1,ILG
          PMBS(IL)=0.01*PH(IL,JK)
          RRL(IL)=RL
          CPM(IL)=CPRES
          ALMIX(IL)=MAX(ALMC(IL,JK),ALMX(IL,JK),10.)
          IF(JK.GT.1)                              THEN
            DZT      =ZH  (IL,JK-1)-ZH (IL,JK)
            DHLDZ(IL)=(HMN(IL,JK-1)-HMN(IL,JK))/DZT
            DRWDZ(IL)=(QT (IL,JK-1)-QT (IL,JK))/DZT
          ELSE
            DHLDZ(IL)=0.
            DRWDZ(IL)=0.
          ENDIF
        ENDDO
        CALL STATCLD (QCW,ZCLF(1,JK),CVAR(1,JK),ZCRAUT(1,JK), &
                      QCWVAR(1,JK),SSH,CVSG(1,JK),QT(1,JK), &
                      HMN(1,JK),ZFRAC(1,JK),CPM, &
                      PMBS,ZH(1,JK),RRL,CVDU(1,JK),FLAGEST, &
                      ALMIX,DHLDZ,DRWDZ,I2NDIE, &
                      DT,ILEV,ILG,1,ILG,ICVSG,ISUBG, &
                      JK,ICALL)
!
        DO IL=1,ILG
          DTDT(IL,JK)=((HMN(IL,JK)-GRAV*ZH(IL,JK)+RRL(IL)*QCW(IL)) &
                       /CPM(IL)-THROW(IL,JK))/DT
          DQDT(IL,JK)=((QT(IL,JK)-QCW(IL))-QROW(IL,JK))/DT
          DQLDT(IL,JK)=(QCW(IL)-QLWC(IL,JK))/DT
        ENDDO
      ENDDO
!
      END SUBROUTINE VADV
