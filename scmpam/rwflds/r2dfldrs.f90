      SUBROUTINE R2DFLDRS(CNAME,AVAR,IDIM1,IDIM2,J1,J2,K1,K2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(OUT), DIMENSION(IDIM1,IDIM2) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1,IDIM2
      INTEGER, OPTIONAL :: J1,J2,K1,K2
      INTEGER :: J1T,J2T,K1T,K2T
      INTEGER, DIMENSION(2) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) ) THEN
        IF ( J1 < 1 .OR. K1 < 1 .OR. J2 > IDIM1 .OR. K2 > IDIM2 ) &
           CALL XIT('R2DFLDRS',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(RVARV(IT)%FLD2D) &
              .AND. TRIM(RVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(RVARV(IT)%FLD2D)
          IF ( ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 ) THEN
            AVAR(J1T:J2T,K1T:K2T)=RVARV(IT)%FLD2D(J1T:J2T,K1T:K2T)
          ELSE IF ( ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM1 ) THEN
            DO I1=K1T,K2T
            DO I2=J1T,J2T
              AVAR(I2,I1)=RVARV(IT)%FLD2D(I1,I2)
            ENDDO
            ENDDO
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('R2DFLDRS',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME), &
               ' IN LIST OF RESTART VARIABLES'
        CALL XIT('R2DFLDRS',-3)
      ENDIF
!
      print '("R2DFLDRS: ", a10, "   [",i2,"x",i2,"]  [",i2,"-",i2,"]  [",i2,"-",i2,"]", es12.2)', &
       CNAME, IDIM1,IDIM2, J1T,J2T, K1T,K2T, sum(avar)/size(avar)
      END SUBROUTINE R2DFLDRS
