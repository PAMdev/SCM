     SUBROUTINE R4DFLDRS(CNAME,AVAR,IDIM1,IDIM2,IDIM3,IDIM4, &
                          J1,J2,K1,K2,L1,L2,M1,M2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(OUT), DIMENSION(IDIM1,IDIM2,IDIM3,IDIM4) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3,IDIM4
      INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2,M1,M2
      INTEGER :: J1T,J2T,K1T,K2T,L1T,L2T,M1T,M2T
      INTEGER, DIMENSION(4) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) &
           .AND. PRESENT(L1) .AND. PRESENT(L2) &
           .AND. PRESENT(M1) .AND. PRESENT(M2) ) THEN
        IF (      J1 < 1 .OR. K1 < 1 .OR. L1 < 1 .OR. M1 < 1 &
             .OR. J2 > IDIM1 .OR. K2 > IDIM2 .OR. L2 > IDIM3 &
             .OR. M2 > IDIM4 ) &
           CALL XIT('R4DFLDRS',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
        L1T=L1
        L2T=L2
        M1T=M1
        M2T=M2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
        L1T=1
        L2T=IDIM3
        M1T=1
        M2T=IDIM4
      ENDIF
!
      print '(2a10,32i5)', 'R4DFLDRS',CNAME,IDIM1,IDIM2,IDIM3,IDIM4,J1T,J2T,K1T,K2T,L1T,L2T,M1T,M2T
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(RVARV(IT)%FLD4D) &
              .AND. TRIM(RVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(RVARV(IT)%FLD4D)
          IF (       ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM3 .AND. ISHAPE(4) == IDIM4 ) THEN
            AVAR(J1T:J2T,K1T:K2T,L1T:L2T,M1T:M2T)= &
                RVARV(IT)%FLD4D(J1T:J2T,K1T:K2T,L1T:L2T,M1T:M2T)
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('R4DFLDRS',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME), &
               ' IN LIST OF RESTART VARIABLES'
        CALL XIT('R4DFLDRS',-3)
      ENDIF
!
      END SUBROUTINE R4DFLDRS
