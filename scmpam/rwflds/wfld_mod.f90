      MODULE WFLD

      CONTAINS

      SUBROUTINE W0DFLD(CNAME,AVAR)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL :: AVAR
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ABS(OVARV(IT)%FLD0D+YNAX) > YSMALLX &
              .AND. TRIM(OVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
          OVARV(IT)%FLD0D=AVAR
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME),' IN LIST OF IO VARIABLES'
        CALL XIT('W0DFLD',-1)
      ENDIF
!
      END SUBROUTINE W0DFLD
      SUBROUTINE W0DFLDRS(CNAME,AVAR)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(IN) :: AVAR
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ABS(RVARV(IT)%FLD0D+YNAX) > YSMALLX &
              .AND. TRIM(RVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
          RVARV(IT)%FLD0D=AVAR
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME), &
               ' IN LIST OF RESTART VARIABLES'
        CALL XIT('W0DFLDRS',-1)
      ENDIF
!
      END SUBROUTINE W0DFLDRS
      SUBROUTINE W1DFLD(CNAME,AVAR,IDIM1,J1,J2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(IN), DIMENSION(IDIM1) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1
      INTEGER, OPTIONAL :: J1,J2
      INTEGER :: J1T,J2T
      INTEGER, DIMENSION(1) :: ISHAPE
!
      IF ( PRESENT(J1) .AND. PRESENT(J2) ) THEN
        IF ( J1 < 1 .OR. J2 > IDIM1 ) CALL XIT('W1DFLD',-1)
        J1T=J1
        J2T=J2
      ELSE
        J1T=1
        J2T=IDIM1
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(OVARV(IT)%FLD1D) &
              .AND. TRIM(OVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(OVARV(IT)%FLD1D)
          IF ( ISHAPE(1) == IDIM1 ) THEN
            OVARV(IT)%FLD1D(J1T:J2T)=AVAR(J1T:J2T)
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('W1DFLD',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME),' IN LIST OF IO VARIABLES'
        CALL XIT('W1DFLD',-3)
      ENDIF
!      print '("W1DFLD: ", a10, "   [",i2,"]  [",i2,"-",i2,"]", es12.2)', CNAME,IDIM1,J1T,J2T, sum(avar)/size(avar)
!
      END SUBROUTINE W1DFLD
      SUBROUTINE W1DFLDRS(CNAME,AVAR,IDIM1,J1,J2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(IN), DIMENSION(IDIM1) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1
      INTEGER, OPTIONAL :: J1,J2
      INTEGER :: J1T,J2T
      INTEGER, DIMENSION(1) :: ISHAPE
!
      IF ( PRESENT(J1) .AND. PRESENT(J2) ) THEN
        IF ( J1 < 1 .OR. J2 > IDIM1 ) CALL XIT('W1DFLDRS',-1)
        J1T=J1
        J2T=J2
      ELSE
        J1T=1
        J2T=IDIM1
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(RVARV(IT)%FLD1D) &
              .AND. TRIM(RVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(RVARV(IT)%FLD1D)
          IF ( ISHAPE(1) == IDIM1 ) THEN
            RVARV(IT)%FLD1D(J1T:J2T)=AVAR(J1T:J2T)
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('W1DFLDRS',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME), &
               ' IN LIST OF RESTART VARIABLES'
        CALL XIT('W1DFLDRS',-3)
      ENDIF
!
      END SUBROUTINE W1DFLDRS
      SUBROUTINE W2DFLD(CNAME,AVAR,IDIM1,IDIM2,J1,J2,K1,K2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1,IDIM2
      INTEGER, OPTIONAL :: J1,J2,K1,K2
      INTEGER :: J1T,J2T,K1T,K2T
      INTEGER, DIMENSION(2) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) ) THEN
        IF ( J1 < 1 .OR. K1 < 1 .OR. J2 > IDIM1 .OR. K2 > IDIM2 ) &
           CALL XIT('W2DFLD',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(OVARV(IT)%FLD2D) &
              .AND. TRIM(OVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(OVARV(IT)%FLD2D)
          IF ( ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 ) THEN
            OVARV(IT)%FLD2D(J1T:J2T,K1T:K2T)=AVAR(J1T:J2T,K1T:K2T)
          ELSE IF ( ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM1 ) THEN
            DO I1=K1T,K2T
            DO I2=J1T,J2T
              OVARV(IT)%FLD2D(I1,I2)=AVAR(I2,I1)
            ENDDO
            ENDDO
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('W2DFLD',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME),' IN LIST OF IO VARIABLES'
        CALL XIT('W2DFLD',-3)
      ENDIF
!      print '("W2DFLD: ", a10, "   [",i2,"x",i2,"]  [",i2,"-",i2,"]  [",i2,"-",i2,"]", es12.2)', &
!       CNAME, IDIM1,IDIM2, J1T,J2T, K1T,K2T, sum(avar)/size(avar)
!
      END SUBROUTINE W2DFLD
      SUBROUTINE W2DFLDRS(CNAME,AVAR,IDIM1,IDIM2,J1,J2,K1,K2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1,IDIM2
      INTEGER, OPTIONAL :: J1,J2,K1,K2
      INTEGER :: J1T,J2T,K1T,K2T
      INTEGER, DIMENSION(2) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) ) THEN
        IF ( J1 < 1 .OR. K1 < 1 .OR. J2 > IDIM1 .OR. K2 > IDIM2 ) &
           CALL XIT('W2DFLDRS',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(RVARV(IT)%FLD2D) &
              .AND. TRIM(RVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(RVARV(IT)%FLD2D)
          IF ( ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 ) THEN
            RVARV(IT)%FLD2D(J1T:J2T,K1T:K2T)=AVAR(J1T:J2T,K1T:K2T)
          ELSE IF ( ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM1 ) THEN
            DO I1=K1T,K2T
            DO I2=J1T,J2T
              RVARV(IT)%FLD2D(I1,I2)=AVAR(I2,I1)
            ENDDO
            ENDDO
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('W2DFLDRS',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME), &
               ' IN LIST OF RESTART VARIABLES'
        CALL XIT('W2DFLDRS',-3)
      ENDIF
!
      END SUBROUTINE W2DFLDRS
      SUBROUTINE W3DFLD(CNAME,AVAR,IDIM1,IDIM2,IDIM3, &
                        J1,J2,K1,K2,L1,L2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2,IDIM3) :: AVAR
      INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2
      INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3
      INTEGER :: J1T,J2T,K1T,K2T,L1T,L2T
      INTEGER, DIMENSION(3) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) &
           .AND. PRESENT(L1) .AND. PRESENT(L2) ) THEN
        IF (      J1 < 1 .OR. K1 < 1 .OR. L1 < 1 &
             .OR. J2 > IDIM1 .OR. K2 > IDIM2 .OR. L2 > IDIM3 ) &
           CALL XIT('W3DFLD',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
        L1T=L1
        L2T=L2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
        L1T=1
        L2T=IDIM3
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(OVARV(IT)%FLD3D) &
              .AND. TRIM(OVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(OVARV(IT)%FLD3D)
          IF (       ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM3 ) THEN
            OVARV(IT)%FLD3D(J1T:J2T,K1T:K2T,L1T:L2T)= &
                       AVAR(J1T:J2T,K1T:K2T,L1T:L2T)
          ELSE IF (  ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM3 &
               .AND. ISHAPE(3) == IDIM2 ) THEN
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              OVARV(IT)%FLD3D(J1T:J2T,I3,I2)=AVAR(J1T:J2T,I2,I3)
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM1 &
               .AND. ISHAPE(3) == IDIM3 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
              OVARV(IT)%FLD3D(I2,I1,L1T:L2T)=AVAR(I1,I2,L1T:L2T)
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM3 &
               .AND. ISHAPE(3) == IDIM1 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              OVARV(IT)%FLD3D(I2,I3,I1)=AVAR(I1,I2,I3)
            ENDDO
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM3 .AND. ISHAPE(2) == IDIM1 &
               .AND. ISHAPE(3) == IDIM2 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              OVARV(IT)%FLD3D(I3,I1,I2)=AVAR(I1,I2,I3)
            ENDDO
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM3 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM1 ) THEN
            DO I1=J1T,J2T
            DO I3=L1T,L2T
              OVARV(IT)%FLD3D(I3,K1T:K2T,I1)=AVAR(I1,K1T:K2T,I3)
            ENDDO
            ENDDO
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('W3DFLD',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME),' IN LIST OF IO VARIABLES'
        CALL XIT('W3DFLD',-3)
      ENDIF
!      print '("W3DFLD: ", a10, "   [",i2,"x",i2,"x",i2,"]  [",i2,"-",i2,"]  [",i2,"-",i2,"]  [",i2,"-",i2,"]", es12.2)', &
!       CNAME,IDIM1,IDIM2,IDIM3,J1T,J2T,K1T,K2T,L1T,L2T, sum(avar)/size(avar)
!
      END SUBROUTINE W3DFLD
      SUBROUTINE W3DFLDRS(CNAME,AVAR,IDIM1,IDIM2,IDIM3, &
                          J1,J2,K1,K2,L1,L2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2,IDIM3) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3
      INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2
      INTEGER :: J1T,J2T,K1T,K2T,L1T,L2T
      INTEGER, DIMENSION(3) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) &
           .AND. PRESENT(L1) .AND. PRESENT(L2) ) THEN
        IF (      J1 < 1 .OR. K1 < 1 .OR. L1 < 1 &
             .OR. J2 > IDIM1 .OR. K2 > IDIM2 .OR. L2 > IDIM3 ) &
           CALL XIT('W3DFLDRS',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
        L1T=L1
        L2T=L2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
        L1T=1
        L2T=IDIM3
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(RVARV(IT)%FLD3D) &
              .AND. TRIM(RVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(RVARV(IT)%FLD3D)
          IF (       ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM3 ) THEN
            RVARV(IT)%FLD3D(J1T:J2T,K1T:K2T,L1T:L2T)= &
                        AVAR(J1T:J2T,K1T:K2T,L1T:L2T)
          ELSE IF (  ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM3 &
               .AND. ISHAPE(3) == IDIM2 ) THEN
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              RVARV(IT)%FLD3D(J1T:J2T,I3,I2)=AVAR(J1T:J2T,I2,I3)
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM1 &
               .AND. ISHAPE(3) == IDIM3 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
              RVARV(IT)%FLD3D(I2,I1,L1T:L2T)=AVAR(I1,I2,L1T:L2T)
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM3 &
               .AND. ISHAPE(3) == IDIM1 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              RVARV(IT)%FLD3D(I2,I3,I1)=AVAR(I1,I2,I3)
            ENDDO
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM3 .AND. ISHAPE(2) == IDIM1 &
               .AND. ISHAPE(3) == IDIM2 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              RVARV(IT)%FLD3D(I3,I1,I2)=AVAR(I1,I2,I3)
            ENDDO
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM3 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM1 ) THEN
            DO I1=J1T,J2T
            DO I3=L1T,L2T
              RVARV(IT)%FLD3D(I3,K1T:K2T,I1)=AVAR(I1,K1T:K2T,I3)
            ENDDO
            ENDDO
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('W3DFLDRS',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME), &
               ' IN LIST OF RESTART VARIABLES'
        CALL XIT('W3DFLDRS',-3)
      ENDIF
!
      END SUBROUTINE W3DFLDRS
      SUBROUTINE W4DFLD(CNAME,AVAR,IDIM1,IDIM2,IDIM3,IDIM4, &
                        J1,J2,K1,K2,L1,L2,M1,M2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2,IDIM3,IDIM4) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3,IDIM4
      INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2,M1,M2
      INTEGER :: J1T,J2T,K1T,K2T,L1T,L2T,M1T,M2T
      INTEGER, DIMENSION(4) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) &
           .AND. PRESENT(L1) .AND. PRESENT(L2) &
           .AND. PRESENT(M1) .AND. PRESENT(M2) ) THEN
        IF (      J1 < 1 .OR. K1 < 1 .OR. L1 < 1 .OR. M1 < 1 &
             .OR. J2 > IDIM1 .OR. K2 > IDIM2 .OR. L2 > IDIM3 &
             .OR. M2 > IDIM4 ) &
           CALL XIT('W4DFLD',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
        L1T=L1
        L2T=L2
        M1T=M1
        M2T=M2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
        L1T=1
        L2T=IDIM3
        M1T=1
        M2T=IDIM4
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(OVARV(IT)%FLD4D) &
              .AND. TRIM(OVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(OVARV(IT)%FLD4D)
          IF (       ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM3 .AND. ISHAPE(4) == IDIM4 ) THEN
            OVARV(IT)%FLD4D(J1T:J2T,K1T:K2T,L1T:L2T,M1T:M2T)= &
                       AVAR(J1T:J2T,K1T:K2T,L1T:L2T,M1T:M2T)
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('W4DFLD',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME),' IN LIST OF IO VARIABLES'
        CALL XIT('W4DFLD',-3)
      ENDIF
!
      END SUBROUTINE W4DFLD
      SUBROUTINE W4DFLDRS(CNAME,AVAR,IDIM1,IDIM2,IDIM3,IDIM4, &
                          J1,J2,K1,K2,L1,L2,M1,M2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2,IDIM3,IDIM4) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3,IDIM4
      INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2,M1,M2
      INTEGER :: J1T,J2T,K1T,K2T,L1T,L2T,M1T,M2T
      INTEGER, DIMENSION(4) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) &
           .AND. PRESENT(L1) .AND. PRESENT(L2) &
           .AND. PRESENT(M1) .AND. PRESENT(M2) ) THEN
        IF (      J1 < 1 .OR. K1 < 1 .OR. L1 < 1 .OR. M1 < 1 &
             .OR. J2 > IDIM1 .OR. K2 > IDIM2 .OR. L2 > IDIM3 &
             .OR. M2 > IDIM4 ) &
           CALL XIT('W4DFLDRS',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
        L1T=L1
        L2T=L2
        M1T=M1
        M2T=M2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
        L1T=1
        L2T=IDIM3
        M1T=1
        M2T=IDIM4
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(RVARV(IT)%FLD4D) &
              .AND. TRIM(RVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(RVARV(IT)%FLD4D)
          IF (       ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM3 .AND. ISHAPE(4) == IDIM4 ) THEN
            RVARV(IT)%FLD4D(J1T:J2T,K1T:K2T,L1T:L2T,M1T:M2T)= &
                        AVAR(J1T:J2T,K1T:K2T,L1T:L2T,M1T:M2T)
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('W4DFLDRS',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME), &
               ' IN LIST OF RESTART VARIABLES'
        CALL XIT('W4DFLDRS',-3)
      ENDIF
!
      END SUBROUTINE W4DFLDRS

      END MODULE WFLD
