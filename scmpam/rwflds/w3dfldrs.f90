      SUBROUTINE W3DFLDRS(CNAME,AVAR,IDIM1,IDIM2,IDIM3, &
                          J1,J2,K1,K2,L1,L2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2,IDIM3) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3
      INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2
      INTEGER :: J1T,J2T,K1T,K2T,L1T,L2T
      INTEGER, DIMENSION(3) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) &
           .AND. PRESENT(L1) .AND. PRESENT(L2) ) THEN
        IF (      J1 < 1 .OR. K1 < 1 .OR. L1 < 1 &
             .OR. J2 > IDIM1 .OR. K2 > IDIM2 .OR. L2 > IDIM3 ) &
           CALL XIT('W3DFLDRS',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
        L1T=L1
        L2T=L2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
        L1T=1
        L2T=IDIM3
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(RVARV(IT)%FLD3D) &
              .AND. TRIM(RVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(RVARV(IT)%FLD3D)
          IF (       ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM3 ) THEN
            RVARV(IT)%FLD3D(J1T:J2T,K1T:K2T,L1T:L2T)= &
                        AVAR(J1T:J2T,K1T:K2T,L1T:L2T)
          ELSE IF (  ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM3 &
               .AND. ISHAPE(3) == IDIM2 ) THEN
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              RVARV(IT)%FLD3D(J1T:J2T,I3,I2)=AVAR(J1T:J2T,I2,I3)
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM1 &
               .AND. ISHAPE(3) == IDIM3 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
              RVARV(IT)%FLD3D(I2,I1,L1T:L2T)=AVAR(I1,I2,L1T:L2T)
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM3 &
               .AND. ISHAPE(3) == IDIM1 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              RVARV(IT)%FLD3D(I2,I3,I1)=AVAR(I1,I2,I3)
            ENDDO
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM3 .AND. ISHAPE(2) == IDIM1 &
               .AND. ISHAPE(3) == IDIM2 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              RVARV(IT)%FLD3D(I3,I1,I2)=AVAR(I1,I2,I3)
            ENDDO
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM3 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM1 ) THEN
            DO I1=J1T,J2T
            DO I3=L1T,L2T
              RVARV(IT)%FLD3D(I3,K1T:K2T,I1)=AVAR(I1,K1T:K2T,I3)
            ENDDO
            ENDDO
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('W3DFLDRS',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME), &
               ' IN LIST OF RESTART VARIABLES'
        CALL XIT('W3DFLDRS',-3)
      ENDIF
!
      END SUBROUTINE W3DFLDRS
