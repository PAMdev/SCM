     SUBROUTINE R1DFLD(CNAME,AVAR,IDIM1,J1,J2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(OUT), DIMENSION(IDIM1) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1
      INTEGER, OPTIONAL :: J1,J2
      INTEGER :: J1T,J2T
      INTEGER, DIMENSION(1) :: ISHAPE
!
      IF ( PRESENT(J1) .AND. PRESENT(J2) ) THEN
        IF ( J1 < 1 .OR. J2 > IDIM1 ) CALL XIT('R1DFLD',-1)
        J1T=J1
        J2T=J2
      ELSE
        J1T=1
        J2T=IDIM1
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(IVARV(IT)%FLD1D) &
              .AND. TRIM(IVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(IVARV(IT)%FLD1D)
          IF ( ISHAPE(1) == IDIM1 ) THEN
            AVAR(J1T:J2T)=IVARV(IT)%FLD1D(J1T:J2T)
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('R1DFLD',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME),' IN LIST OF IO VARIABLES'
        CALL XIT('R1DFLD',-3)
      ENDIF
!
!      print '("R1DFLD: ", a10, "   [",i2,"]  [",i2,"-",i2,"]", es12.2)', CNAME,IDIM1,J1T,J2T, sum(avar)/size(avar)
      END SUBROUTINE R1DFLD
