      SUBROUTINE STATCLD (QCW,ZCLF,SIGMA,ZCRAUT,QCWVAR,SSH,CVSG, &
                          QT,HMN,ZFRAC,CPM,P,Z,RRL,CONS,CRH, &
                          ALMIX,DHLDZ,DRWDZ,I2NDIE, &
                          ZTMST,ILEV,ILG,IL1,IL2,ICVSG,ISUBG, &
                          LVL,ICALL)
!
!     * AUG 16/2013 - K.VONSALZEN. Cosmetic change to abort condition
!     *                            where now is QCW.GT.QT instead of
!     *                            QCW.GE.QT. This does not change
!     *                            the answer but prevents infrequent
!     *                            crashes with small downward
!     *                            moisture fluxes causing very small
!     *                            negative total water.
!     * JUL 31/2013 - M.LAZARE.    Cosmetic change to pass in LVL
!     *                            and ICALL to aid in future debugging.
!     * JUN 25/2013 - K.VONSALZEN. New version for gcm17:
!     *                            - Second indirect effect parameter
!     *                              I2NDIE passed in and used.
!     *                            - Function ZCR replaces look-up
!     *                              table ZCR to calculate autoconversion
!     *                              efficiency.
!     * APR 29/2012 - K.VONSALZEN. PREVIOUS VERSION STATCLD4 FOR GCM16:
!     *                            ELIMINATE CLOUDS AND CONDENSATE IF
!     *                            CLOUD FRACTION OR CONDENSATE MIXING
!     *                            RATIO TOO LOW TO ENSURE CONSISTENT
!     *                            RESULTS FOR CLOUD FRACTION AND CLOUD
!     *                            CONDENSATE.
!     * MAR 25/2009 - L.SOLHEIM.   REVISED COSMETIC CHANGE:
!     *                            - USE REAL*4 OR REAL*8 ARGUMENT TO
!     *                              "ERF", DEPENDING ON THE VALUE OF
!     *                              "MACHINE", SO WILL WORK SEAMLESSLY ON
!     *                              PGI COMPILER AS WELL
!     * DEC 19/2007 - K.VONSALZEN/ PREVIOUS VERSION STATCLD3 FOR GCM15G/H/I:
!     *               M.LAZARE.    - ELIMINATE UNPHYSICAL RESULTS
!     *                              BY NOT PERMITTING THE INFERRED
!     *                              WATER VAPOUR MIXING RATIO (QCW)
!     *                              TO EXCEED A THRESHOLD (DEFINED
!     *                              BY QCWMAX).
!     *                            - PASSES IN ADELT FROM PHYSICS
!     *                              AS ZTMST INSTEAD OF ZTMST=2.*DELT.
!     * JAN 13/2007 - K.VONSALZEN. CALCULATE NORMALIZED CLOUD WATER
!     *                            VARIANCE TO BE USED IN CLOUD
!     *                            INHOMOGENEITY (QCWVAR).
!     * JUN 19/2006 - M.LAZARE.  NEW VERSION FOR GCM15F:
!     *                          - COSMETIC: USE VARIABLE INSTEAD OF
!     *                            CONSTANT IN INTRINSICS SUCH AS "MAX",
!     *                            SO THAT CAN COMPILE IN 32-BIT MODE
!     *                            WITH REAL*8.
!     * MAY 06/2006 - M.LAZARE/  PREVIOUS VERSION STATCLD FOR GCM15E.
!     *               K.VONSALZEN:

!
!     * THIS SUBROUTINE PERFORMS THE BULK OF THE STATISTICAL CLOUD
!     * SCHEME BASED ON CHABOUREAU AND BECHTHOLD (C&B). IT TAKES AS
!     * INPUT LIQUID STATIC ENERGY AND TOTAL WATER, AND OUTPUTS
!     * CONDENSED WATER/ICE, CLOUD FRACTION, THE VARIANCE OF MELLORS
!     * VARIABLE ("S"), THE EFFICIENCY FACTOR FOR AUTOCONVERSION
!     * FROM CLOUD WATER TO RAIN WATER, AND THE SATURATION SPECIFIC
!     * HUMIDITY.
!
!**** ***************************************************************
!     * DICTIONARY OF VARIABLES:
!
!     * OUTPUT:
!     * ------
!
!     * QCW:     TOTAL CONDENSED WATER/ICE (BOTH PHASES) IN KG/KG.
!     * ZCLF:    CLOUD FRACTION (DIMENSIONLESS)
!     * SIGMA:   VARIANCE OF MELLOR'S VARIABLE S IN KG/KG
!     * ZCRAUT:  EFFICIENCY OF AUTOCONVERSION FROM CLOUD WATER TO
!     *          RAIN WATER.
!     * QCWVAR:  NORMALIZED CLOUD WATER VARIANCE (<W'**2>/<W>**2)
!     *          (DIMENSIONLESS).
!     * SSH:     SATURATION SPECIFIC HUMIDITY IN KG/KG
!     * CVSG:    CONTRIBUTION TO TOTAL VARIANCE OF S FROM CONVECTIVE
!     *          PROCESSES IN KG/KG (ONLY IF SWITCH ICVSG=1,
!     *          OTHERWISE IT IS INPUT!).
!
!     * INPUT:
!     * -----
!
!     * QT:     TOTAL WATER (BOTH PHASES) IN KG/KG.
!     * HMN:    TOTAL LIQUID STATIC ENERGY IN JOULES/KG.
!     * ZFRAC:  FRACTION OF ICE PHASE.
!     * CPM:    SPECIFIC HEAT IN JOULES/(KG-DEGK).
!     * P:      MID-LAYER PRESSURE IN MBS.
!     * Z:      MID-LAYER HEIGHT ABOVE GROUND IN METRES.
!     * PBLT:   LAYER INDEX OF TOP OF PBL.
!     * RRL:    WATER/ICE WEIGHTED LATENT HEAT IN JOULES/KG.
!     * CONS:   FIELD CONTRIBUTING TO VARIANCE FROM CONVECTION.
!     * CRH:    FLAG (0/1) TO CONTROL CALCULATION.
!     * ALMIX:  MIXING LENGTH FOR C&B PARAMETERIZATION IN METRES.
!     * DHLDZ:  TOTAL WATER VERTICAL GRADIENT IN (M-1).
!     * DRWDZ:  LIQUID STATIC ENERGY GRADIENT IN JOULES/(KG-M). &
!**** ***************************************************************
!
      USE SDPHYS
      USE FPDEF
!
      IMPLICIT REAL   (A-H,O-Z), &
      INTEGER (I-N)
!
      REAL  , DIMENSION(ILG), INTENT(OUT) :: QCW,ZCLF,SIGMA,ZCRAUT, &
                                             QCWVAR,SSH,CVSG
      REAL  , DIMENSION(ILG), INTENT(IN)  :: HMN,QT,P,Z,CPM,ZFRAC, &
                                             CONS,CRH,RRL,ALMIX,DHLDZ, &
                                             DRWDZ
!
      REAL*4 :: R4VAL
      REAL*8 :: R8VAL
      INTEGER :: MACHINE
!
      ESW(TTT)    = EXP(RW1+RW2/TTT)*TTT**RW3
      ESI(TTT)    = EXP(RI1+RI2/TTT)*TTT**RI3
!
      DATA ZERO,ONE /0., 1./
!==============================================================================================
!     * PARAMETERS.
!
      ZEPCLC=1.E-2
      ZSEC=1.E-9
      CSIGMA=0.2
      YFR=0.50
      SQRT2=SQRT(2.)
      PI=3.141592653589793
      SQRT2PI=SQRT(2.*PI)
      Q1MIN1=-3.
      Q1MIN2=TAN((ZEPCLC-0.5)/0.36)/1.55
      Q1MIN=MAX(Q1MIN1,Q1MIN2)
      Q1MINM=Q1MIN/2.
      Q1MAX=2.
      Q1MAXM=Q1MAX/2.
      EXPM1=EXP(-1.)
      QDELT=2./30.
      TAUSIG=21600.
      SIGFAC=EXP(-ZTMST/TAUSIG)
      EPS2=1.-EPS1
      MACHINE=1
!
!     * INITIALIZATION.
!
      ILBAD=0
      DO IL=IL1,IL2
        QCW   (IL)=0.
        ZCLF  (IL)=0.
        ZCRAUT(IL)=1.
        SIGMA (IL)=0.
        SSH   (IL)=0.
      ENDDO
!
      DO IL=IL1,IL2
        IF ( CRH(IL).NE.0. ) THEN
!
!         * TEMPERATURE UNDER CLEAR-SKY CONDITIONS AS FIRST GUESS.
!
          TX=(HMN(IL)-GRAV*Z(IL))/CPM(IL)
          TL=TX
!
!         * WATER VAPOUR SATURATION MIXING RATIO UNDER CLEAR-SKY CONDITIONS.
!
          EST=(1.-ZFRAC(IL))*ESW(TX)+ZFRAC(IL)*ESI(TX)
          RSS=EPS1*EST/( P(IL)-EPS2*EST )
          DRSSDT=EPS1*P(IL) &
                   *((1.-ZFRAC(IL))*ESW(TX)*(RW3-RW2/TX) &
                        +ZFRAC(IL) *ESI(TX)*(RI3-RI2/TX)) &
                    /(TX*(P(IL)-EPS2*EST)**2)
          RSSTL=RSS
          DRSSDTL=DRSSDT
!
!         * ACCOUNT FOR CLOUD WATER/ICE IF SATURATION. USE
!         * SERIES REVERSION AND ITERATE.
!
          IF ( RSS.LT.QT(IL) ) THEN
!
!           * FIRST ITERATION.
!
            HMNPR=CPM(IL)*TX+GRAV*Z(IL)-HMN(IL)
            DHMNPR=CPM(IL)
            RC=QT(IL)-RSS
            HMNPR=HMNPR-RRL(IL)*RC
            DHMNPR=DHMNPR+RRL(IL)*DRSSDT
            TX=TX-HMNPR/DHMNPR
            EST=(1.-ZFRAC(IL))*ESW(TX)+ZFRAC(IL)*ESI(TX)
            RSS=EPS1*EST/( P(IL)-EPS2*EST )
            DRSSDT=EPS1*P(IL) &
                   *((1.-ZFRAC(IL))*ESW(TX)*(RW3-RW2/TX) &
                        +ZFRAC(IL) *ESI(TX)*(RI3-RI2/TX)) &
                    /(TX*(P(IL)-EPS2*EST)**2)
!
!           * SECOND ITERATION.
!
            HMNPR=CPM(IL)*TX+GRAV*Z(IL)-HMN(IL)
            DHMNPR=CPM(IL)
            RC=QT(IL)-RSS
            HMNPR=HMNPR-RRL(IL)*RC
            DHMNPR=DHMNPR+RRL(IL)*DRSSDT
            TX=TX-HMNPR/DHMNPR
            EST=(1.-ZFRAC(IL))*ESW(TX)+ZFRAC(IL)*ESI(TX)
            RSS=EPS1*EST/( P(IL)-EPS2*EST )
            DRSSDT=EPS1*P(IL) &
                   *((1.-ZFRAC(IL))*ESW(TX)*(RW3-RW2/TX) &
                        +ZFRAC(IL) *ESI(TX)*(RI3-RI2/TX)) &
                    /(TX*(P(IL)-EPS2*EST)**2)
!
!           * THIRD ITERATION.
!
            HMNPR=CPM(IL)*TX+GRAV*Z(IL)-HMN(IL)
            DHMNPR=CPM(IL)
            RC=QT(IL)-RSS
            HMNPR=HMNPR-RRL(IL)*RC
            DHMNPR=DHMNPR+RRL(IL)*DRSSDT
            TX=TX-HMNPR/DHMNPR
            EST=(1.-ZFRAC(IL))*ESW(TX)+ZFRAC(IL)*ESI(TX)
            RSS=EPS1*EST/( P(IL)-EPS2*EST )
            DRSSDT=EPS1*P(IL) &
                   *((1.-ZFRAC(IL))*ESW(TX)*(RW3-RW2/TX) &
                        +ZFRAC(IL) *ESI(TX)*(RI3-RI2/TX)) &
                    /(TX*(P(IL)-EPS2*EST)**2)
!
!           * TEMPERATURE-DERIVATIVE OF RSS FOR T=TL. THE DERIVATIVE
!           * IS USED IN A TAYLOR SERIES APPROXIMATION TO DETERMINE THE
!           * WATER VAPOUR SATURATION MIXING RATIO AT TEMPERATURE TX
!           * (I.E. AMBIENT TEMPERATURE) ABOUT TL (I.E. LIQUID WATER
!           * TEMPERATURE).
!
            IF( TX.NE.TL .AND. RSS > RSSTL ) THEN
              DRSSDT=(RSS-RSSTL)/(TX-TL)
            ELSE
              DRSSDT=DRSSDTL
              RSS=RSSTL
            ENDIF
          ENDIF
!
!         * SAVE FINAL ITTERATION OF SATURATION SPECIFIC HUMIDITY FOR
!         * USE ELSEWHERE IN CALCULATION RELATIVE HUMIDITIES.
!
          SSH(IL)=RSS
          IF (ISUBG.EQ.1)                                           THEN
!
!           * A AND B PARAMETERS FOR STATISTICAL CLOUD SCHEME.
!
            APA=1./(1.+RRL(IL)*DRSSDT/CPM(IL))
            APB=APA*DRSSDT
!
!           * DETERMINE VARIANCE BASED ON LOCAL APPROACH (CHABOUREAU AND
!           * BECHTHOLD).
!
            SIGMACB=CSIGMA*ALMIX(IL)* &
                    ABS(APA*DRWDZ(IL)-APB*DHLDZ(IL)/CPM(IL))
!
            IF(ICVSG.EQ.1)                                          THEN
!
!             * CALCULATE CONVECTIVE CONTRIBUTION TO VARIANCE.
!             * THIS MUST BE OUTPUT BECAUSE OF "AGEING".
!             * NOTE ** THIS IS ONLY DONE ONCE NEAR THE BEGINNING OF THE
!             *         PHYSICS, DUE TO THE "AGEING" CALCULATION (IE
!             *         ONE DOESN'T WANT MULTIPLE AGEING PER TIMESTEP FOR
!             *         EACH PHYSICS ROUTINE WHICH CALLS THIS SUBROUTINE.
!             *         THIS IS CONTROLLED BY THE SWITCH "ICVSG").
!
              IF(CONS(IL).GT.0.) THEN
                CVSG(IL)=3.E-03*CONS(IL)/APA
!old            CVSG(IL)=1.E-05*APA
              ELSE
                CVSG(IL)=CVSG(IL)*SIGFAC
              ENDIF
            ENDIF
!
!           * ADD CONVECTIVE CONTRIBUTION TO VARIANCE.
!
            SIGMASUM=SIGMACB+CVSG(IL)
!
!           * LIMIT VARIANCE TO AVOID UNPHYSICAL CASES WITH LARGE VARIANCES
!           * AN LOW MEAN VALUES FOR THE TOTAL WATER PROBABILITY DISTRIBUTION.
!
            SIGMAMAX=YFR*APA*QT(IL)/2.
            SIGMA(IL)=MIN(SIGMASUM,SIGMAMAX)
          ENDIF
!
!         * CLOUD FRACTION AND CLOUD WATER CONTENT BASED ON CHABOUREAU
!         * AND BECHTHOLD'S STATISTICAL CLOUD SCHEME AND AUTOCONVERSION
!         * SCALING FACTOR BASED ON MELLORS'S STATISTICAL CLOUD SCHEME
!         * (FOR THE 2.47TH MOMENT OF THE TOTAL WATER PROBABILITY
!         * DISTRIBUTION).
!
          QCWT=(QT(IL)-RSSTL)
          IF ( ISUBG .EQ. 1 .AND. SIGMA(IL) .GT. ZSEC ) THEN
            QCWT=QCWT*APA
            Q1=QCWT/SIGMA(IL)
            Q1M=Q1/2.
            INDX=NINT((Q1/2.+2.)/QDELT)+1
            IF ( Q1.LT.Q1MIN ) THEN
              ZCLF(IL)=0.
              QCW (IL)=0.
              ZCRAUT(IL)=ZCR(Q1MINM,I2NDIE)
              QCWVAR(IL)=0.
            ELSE IF ( Q1.GE.Q1MIN .AND. Q1.LT.0. ) THEN
              ZCLF(IL)=0.5+0.36*ATAN(1.55*Q1)
              QCW (IL)=SIGMA(IL)*EXP(1.2*Q1-1.)
              ZCRAUT(IL)=ZCR(Q1M,I2NDIE)
!
              IF (MACHINE.EQ.2) THEN
                R4VAL=Q1M/SQRT2
                AR=.5*(1.+ERF(R4VAL))
              ELSE
                R8VAL=Q1M/SQRT2
                AR=.5*(1.+ERF(R8VAL))
              ENDIF
!
              AEXP=EXP(-.5*Q1M**2)
              QL=AR*Q1M+AEXP/SQRT2PI
              QLVAR=AR*(1.+(Q1M-QL)**2)+AEXP*(Q1M/SQRT2PI-QL)
              QCWVAR(IL)=QLVAR/QL**2
            ELSE IF ( Q1.GE.0. .AND. Q1.LE.Q1MAX ) THEN
              ZCLF(IL)=0.5+0.36*ATAN(1.55*Q1)
              QCW (IL)=SIGMA(IL)*(EXPM1+0.66*Q1+0.086*Q1**2)
              ZCRAUT(IL)=ZCR(Q1M,I2NDIE)
!
              IF (MACHINE.EQ.2) THEN
                R4VAL=Q1M/SQRT2
                AR=.5*(1.+ERF(R4VAL))
              ELSE
                R8VAL=Q1M/SQRT2
                AR=.5*(1.+ERF(R8VAL))
              ENDIF
!
              AEXP=EXP(-.5*Q1M**2)
              QL=AR*Q1M+AEXP/SQRT2PI
              QLVAR=AR*(1.+(Q1M-QL)**2)+AEXP*(Q1M/SQRT2PI-QL)
              QCWVAR(IL)=QLVAR/QL**2
            ELSE
              ZCLF(IL)=1.
              QCW (IL)=QT(IL)-RSS
              ZCRAUT(IL)=1.
              QCWVAR(IL)=0.
            ENDIF
!
!           * IMPOSE UPPER BOUND ON CLOUD WATER. THIS IS NECESSARY SINCE
!           * THE EMPIRICAL FITS ON WHICH THE STATISTICAL CLOUD SCHEME IS
!           * IS BASED MAY PRODUCE UNPHYSICAL RESULTS UNDER CERTAIN
!           * CIRCUMSTANCES (I.E. EXCESSIVELY LARGE CLOUD WATER CONTENTS).
!           * THE APPROACH HERE IS NOT TO ALLOW THE INFERRED WATER VAPOUR
!           * MIXING RATIO IN THE CLEAR-SKY TO BECOME SMALLER THAN A
!           * GIVEN THRESHOLD (ZSEC).
!
            QCWMAX=MAX(QT(IL)-((1.-ZCLF(IL))*ZSEC+ZCLF(IL)*RSS),0.)
            IF ( Q1.GE.Q1MIN .AND. Q1.LE.Q1MAX &
                                         .AND. QCW(IL).GE.QCWMAX ) THEN
              QCW(IL)=QCWMAX
            ENDIF
          ELSE
            IF ( QCWT .LT. 0. ) THEN
              ZCLF(IL)=0.
              QCW (IL)=0.
              ZCRAUT(IL)=ZCR(Q1MINM,I2NDIE)
              QCWVAR(IL)=0.
            ELSE
              ZCLF(IL)=1.
              QCW (IL)=QT(IL)-RSS
              ZCRAUT(IL)=1.
              QCWVAR(IL)=0.
            ENDIF
          ENDIF
          QCW(IL)=MAX(MIN(QCW(IL),QT(IL)),ZERO)
          ZCLF(IL)=MAX(MIN(ZCLF(IL),ONE),ZERO)
!
!         * ELIMINATE CLOUDSAND CONDENSATE IF CLOUD FRACTION OR
!         * CONDENSATE MIXING RATIO TOO LOW TO ENSURE CONSISTENT
!         * RESULTS FOR CLOUD FRACTION AND CLOUD CONDENSATE.
!
          IF ( ZCLF(IL).LT.0.01 .OR. QCW(IL).LT.1.E-12 ) THEN
            ZCLF(IL)=0.
            QCW (IL)=0.
            ZCRAUT(IL)=ZCR(Q1MINM,I2NDIE)
            QCWVAR(IL)=0.
          ENDIF
!
!         * TRACK UNPHYSICAL VALUES IN PREPARATION FOR ABORTING
!         * (DONE OUTSIDE LOOP IN ORDER TO KEEP OPTIMIZATION).
!
          IF ( ISUBG.EQ.1 .AND. QCW(IL).GT.QT(IL) .AND. ILBAD.EQ.0 ) &
                                                                 THEN
            ILBAD=IL
            SIGCBBAD=SIGMACB
            SIGMAXBAD=SIGMAMAX
            QCWTBAD=QCWT
          ENDIF
        ENDIF
      ENDDO
!
!     * PRINT OUT PROBLEM POINTS AND ABORT, IF THEY EXIST.
!
      IF(ILBAD.NE.0)                                            THEN
        IL=ILBAD
        PRINT*,'ICALL,LVL,IL,QCW,QT,RSS=',ICALL,LVL,ILBAD,QCW(IL), &
                QT(IL),SSH(IL)
        PRINT*,'SIGMA,SIGMACB,CVSG,SIGMAMAX,ZCLF,ZCRAUT,QCWT=', &
                SIGMA(IL),SIGCBBAD,CVSG(IL),SIGMAXBAD,ZCLF(IL), &
                ZCRAUT(IL),QCWTBAD
        CALL XIT('STATCLD5',-1)
      ENDIF
!
      END SUBROUTINE STATCLD
