      FUNCTION ZCR(Q1,IMOD)
!
!     * JUN 26/2013 - K.VONSALZEN.
!
!     * CALLED BY STATCLD5.
!
!     * RATIO OF MOMENTS OF CLOUD LIQUID WATER CONTENT. THE RATIO
!     * <QLWC**COEFF>/<QLWC>**COEFF IS CALCULATED BY INTEGRATING
!     * OVER A GAUSSIAN DISTRIBUTION USING THE STATISTICAL CLOUD
!     * SCHEME BY MELLOR (1977). FOR IMOD=0 -> COEFF=2.47 AND
!     * FOR IMOD=1 -> COEFF=3.
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL :: ZCR
      REAL, PARAMETER :: Q1MIN=-2., Q1MAX=2.
      INTEGER, PARAMETER :: INUM=61
      REAL, DIMENSION(INUM) :: ZCRLD, ZCRKK, ZCRR
      DATA ZCRLD / 8889.942, 6454.475, 4721.926, 3480.747, 2584.975, &
                   1934.007, 1457.707, 1106.815,  846.462,  652.041, &
                    505.868,  395.250,  311.001,  246.407,  196.574, &
                    157.888,  127.668,  103.925,   85.150,   70.220, &
                     58.278,   48.672,   40.903,   34.586,   29.420, &
                     25.173,   21.665,   18.752,   16.322,   14.284, &
                     12.568,   11.117,    9.883,    8.830,    7.928, &
                      7.152,    6.482,    5.900,    5.395,    4.953, &
                      4.567,    4.227,    3.927,    3.662,    3.427, &
                      3.218,    3.032,    2.866,    2.716,    2.582, &
                      2.462,    2.353,    2.254,    2.165,    2.084, &
                      2.011,    1.944,    1.883,    1.827,    1.776, &
                      1.729 /
      DATA ZCRKK /  708.636,  560.461,  445.761,  356.524,  286.721, &
                    231.848,  188.502,  154.094,  126.637,  104.629, &
                     86.902,   72.556,   60.895,   51.368,   43.552, &
                     37.112,   31.781,   27.350,   23.650,   20.549, &
                     17.939,   15.733,   13.862,   12.269,   10.908, &
                      9.739,    8.733,    7.864,    7.111,    6.456, &
                      5.885,    5.385,    4.946,    4.559,    4.218, &
                      3.916,    3.649,    3.410,    3.198,    3.008, &
                      2.838,    2.685,    2.548,    2.424,    2.312, &
                      2.211,    2.119,    2.036,    1.961,    1.892, &
                      1.829,    1.772,    1.720,    1.672,    1.628, &
                      1.588,    1.551,    1.517,    1.485,    1.457, &
                      1.430 /
!
      Q1DELT=(Q1MAX-Q1MIN)/REAL(INUM-1)
      IF ( IMOD == 0 ) THEN
        ZCRR=ZCRKK
      ELSE
        ZCRR=ZCRLD
      ENDIF
!
      IF ( Q1 < Q1MIN ) THEN
        ZCR=ZCRR(1)
      ELSE IF ( Q1 > Q1MAX ) THEN
        ZCR=ZCRR(INUM)
      ELSE
        Q1V=(Q1-Q1MIN)/Q1DELT+1.
        IND=INT(Q1V)
        WGT=Q1V-REAL(IND)
        ZCR=WGT*ZCRR(IND+1)+(1.-WGT)*ZCRR(IND)
      ENDIF
!
      END FUNCTION ZCR
