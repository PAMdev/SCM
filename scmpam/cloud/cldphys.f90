      SUBROUTINE CLDPHYS(ZCLF,ZRFLN,CLRFR,CLRFS,ZFRAIN,ZFSNOW,ZFEVAP, &
                         ZFSUBL,ZMLWC,ZMRATEP,RH,RHC,HMN,QT,QR,QLR, &
                         ZCDNC,QCWA,TH,PH,ZH,DP,ALMC,ALMX,DT,ILG,ILEV, &
                         ICASE)
!
      USE SDPHYS
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL, INTENT(INOUT), DIMENSION(ILG,ILEV) :: QR, QLR, TH, ZCLF
      REAL, INTENT(IN), DIMENSION(ILG,ILEV) :: PH, ZH, ALMC, ALMX, &
                                               DP, ZCDNC
      REAL, INTENT(OUT), DIMENSION(ILG,ILEV) :: HMN, QT, RH, RHC, &
                                                ZMLWC, CLRFR, CLRFS, &
                                                ZFRAIN, ZFSNOW, ZFEVAP, &
                                                ZFSUBL, ZMRATEP, QCWA
      REAL, INTENT(OUT), DIMENSION(ILG) :: ZRFLN
      REAL, DIMENSION(ILG,ILEV) :: QTN, HMNN, CVAR, ZCRAUT, QCWVAR, &
                                   CVSG, CVDU, ZFRAC, QC
      REAL, DIMENSION(ILG) :: PMBS, RRL, CPM, ALMIX, DHLDZ, DRWDZ, &
                              FLAGEST, QCW, SSH
      REAL, DIMENSION(ILG) :: ZCLFRM, ZCLFR, ZCLRR, ZRNFT, &
                              ZRCLR, ZCLFSM, ZCLFS, ZSFLN, ZCLRS, &
                              ZSNFT, ZSCLR, VTR, ZRPR, ZEVP, ZXLB
      INTEGER, PARAMETER :: ITER=10
      INTEGER, PARAMETER :: ISPCDNC=0  ! SWITCH TO SPECIFIED CDNC
      REAL, PARAMETER :: SPCDNC=300.E+06
!
      ESW(TTT) = EXP(RW1+RW2/TTT)*TTT**RW3

!
!     * ADIABATIC CORE.
!
!     * TOTAL WATER MIXING RATIO AND CLOUD WATER STATIC ENERGY.
!
      DO L=1,ILEV
        QTN(:,L)=QR(:,ILEV)+QLR(:,ILEV)
        HMNN(:,L)=CPRES*TH(:,ILEV)+GRAV*ZH(:,ILEV)-RL*QLR(:,ILEV)
      ENDDO
      FACTE=1./EPS1-1.
      I2NDIE=1
      FLAGEST=1.
      CVSG=0.
      CVDU=0.
      ZFRAC=0.
      ICVSG=1
      ISUBG=0
      ICALL=1
      DO JK=1,ILEV
        DO IL=1,ILG
          PMBS(IL)=0.01*PH(IL,JK)
          RRL(IL)=RL
          CPM(IL)=CPRES
          ALMIX(IL)=MAX(ALMC(IL,JK),ALMX(IL,JK),10.)
          IF(JK.GT.1)                              THEN
            DZ       =ZH   (IL,JK-1)-ZH  (IL,JK)
            DHLDZ(IL)=(HMNN(IL,JK-1)-HMNN(IL,JK))/DZ
            DRWDZ(IL)=(QTN (IL,JK-1)-QTN (IL,JK))/DZ
          ELSE
            DHLDZ(IL)=0.
            DRWDZ(IL)=0.
          ENDIF
        ENDDO
        CALL STATCLD (QCW,ZCLF(1,JK),CVAR(1,JK),ZCRAUT(1,JK), &
                      QCWVAR(1,JK),SSH,CVSG(1,JK),QTN(1,JK), &
                      HMNN(1,JK),ZFRAC(1,JK),CPM, &
                      PMBS,ZH(1,JK),RRL,CVDU(1,JK),FLAGEST, &
                      ALMIX,DHLDZ,DRWDZ,I2NDIE, &
                      DT,ILEV,ILG,1,ILG,ICVSG,ISUBG, &
                      JK,ICALL)
!
        DO IL=1,ILG
          QCWA(IL,JK)=QCW(IL)
        ENDDO
      ENDDO
!
!     * TOTAL WATER MIXING RATIO AND CLOUD WATER STATIC ENERGY.
!
      QTN=QR+QLR
      HMNN=CPRES*TH+GRAV*ZH-RL*QLR
!
!     * CLOUD FRACTION, EVAPORATION, AND CONDENSATION FROM STATISTICAL
!     * CLOUD SCHEME.
!
      FACTE=1./EPS1-1.
      I2NDIE=1
      FLAGEST=1.
      CVSG=0.
      CVDU=0.
      ZFRAC=0.
      ICVSG=1
      ISUBG=0
      ICALL=1
      DO JK=1,ILEV
        DO IL=1,ILG
          PMBS(IL)=0.01*PH(IL,JK)
          RRL(IL)=RL
          CPM(IL)=CPRES
          ALMIX(IL)=MAX(ALMC(IL,JK),ALMX(IL,JK),10.)
          IF(JK.GT.1)                              THEN
            DZ       =ZH   (IL,JK-1)-ZH  (IL,JK)
            DHLDZ(IL)=(HMNN(IL,JK-1)-HMNN(IL,JK))/DZ
            DRWDZ(IL)=(QTN (IL,JK-1)-QTN (IL,JK))/DZ
          ELSE
            DHLDZ(IL)=0.
            DRWDZ(IL)=0.
          ENDIF
        ENDDO
        CALL STATCLD (QCW,ZCLF(1,JK),CVAR(1,JK),ZCRAUT(1,JK), &
                      QCWVAR(1,JK),SSH,CVSG(1,JK),QTN(1,JK), &
                      HMNN(1,JK),ZFRAC(1,JK),CPM, &
                      PMBS,ZH(1,JK),RRL,CVDU(1,JK),FLAGEST, &
                      ALMIX,DHLDZ,DRWDZ,I2NDIE, &
                      DT,ILEV,ILG,1,ILG,ICVSG,ISUBG, &
                      JK,ICALL)
!
        DO IL=1,ILG
          QCWI=ZFRAC(IL,JK)*QCW(IL)
          QCWL=(1.-ZFRAC(IL,JK))*QCW(IL)
!
!         * CONDENSATION AND EVAPORATION.
!
          ZCND=QCW(IL)-QLR(IL,JK)
          ZCND=MAX(ZCND,-QLR(IL,JK))
          ASCL=1.
          IF ( ZCND.GT.0. ) THEN
            ASCL=MIN(MAX((QR(IL,JK)-1.E-20)/ZCND,0.),1.)
          ENDIF
          ZCND=ASCL*ZCND
!
!         * UPDATE TEMPERATURE, VAPOUR, AND CONDENSATE.
!
          TH(IL,JK)=TH(IL,JK)+ZCND*RL/CPRES
          QR(IL,JK)=QR(IL,JK)-ZCND
          QLR(IL,JK)=QLR(IL,JK)+ZCND
!
!         * RELATIVE HUMIDITY FOR ALL- AND CLEAR-SKY. THE RELATIVE
!         * HUMIDITY IS CALCULATED BASED ON THE VAPOUR PRESSURE IF THE
!         * SATURATION VAPOUR PRESSURE EXCEEDS THE TOTAL AIR PRESSURE,
!         * I.E. FOR CONDITIONS THAT DO NOT PERMIT FORMATION OF CLOUDS.
!         * OTHERWISE, THE RELATIVE HUMIDITY IS CALCULATED BASED ON
!         * SPECIFIC HUMIDITY (SEE CHAPTER 4 IN EMANUEL'S TEXTBOOK ON
!         * CONVECTION) IN ORDER TO PERMIT CALCULATIONS FOR CLEAR-SKY.
!
          IF(ZCLF(IL,JK).LT..99) THEN
            QC (IL,JK)=(QR(IL,JK)-ZCLF(IL,JK)*SSH(IL))/(1.-ZCLF(IL,JK))
            RHC(IL,JK)=MIN(MAX((QC(IL,JK)*(1.+SSH(IL)*FACTE)) &
                              /(SSH(IL)*(1.+QC(IL,JK)*FACTE)),0.),1.)
            RH (IL,JK)=ZCLF(IL,JK)+(1.-ZCLF(IL,JK))*RHC(IL,JK)
          ELSE
            QC (IL,JK)=QR(IL,JK)
            RH (IL,JK)=1.
            RHC(IL,JK)=RH(IL,JK)
          ENDIF
!
!         * DIAGNOSE LIQUID WATER STATIC ENERGY AND TOTAL WATER
!         * IN THE CLEAR-SKY ENVIRONMENT OF THE CLOUD.
!
          QT(IL,JK)=QC(IL,JK)
          HMN(IL,JK)=CPRES*TH(IL,JK)+GRAV*ZH(IL,JK)
        ENDDO
      ENDDO
!
!     * CONSTANTS AND INTIALIZATIONS.
!
      ZSEC=1.E-9
      ZEPCLC=1.E-2
      API=3.1415926535897
      COAUT=3.
      COACC=1.15
      COTMP1=COAUT-2.
      COTMP2=COACC-1.
      ZLVDCP=RL/CPRES
      AKR=141.4
      ZKA=0.024
      ZRFLNL_MAX=0.028
      FACACC=1.
      ZCONS2=1./(DT*GRAV)
      RGASV=461.50
      EPSLIM=0.001
      ZXSEC=1.-1.E-12
      ZMRATEP=0.
      ZMLWC=0.
      CLRFR=0.
      CLRFS=0.
      ZFRAIN=0.
      ZFSNOW=0.
      ZFEVAP=0.
      ZFSUBL=0.
      ZCLFR=0.
      ZRFLN=0.
      ZCLFS=0.
      ZSFLN=0.
      DO JK=1,ILEV
      ZSR=0.
      DO IL=1,ILG
!
!     * RAIN AREA FRACTION FROM MAXIMUM/RANDOM CLOUD OVERLAP ASSUMPTION.
!     * ONLY CLOUDS THAT ARE SUBJECT TO WARM RAIN PROCESSES ARE
!     * CONSIDERED.
!
       ZCLFRM(IL)=ZCLFR(IL)
       IF ( ZRFLN(IL) .GT. 1.E-20 ) THEN
         ZCLFR(IL)=ZCLF(IL,JK)
       ELSE
         ZCLFR(IL)=0.
       ENDIF
       IF ( ZCLFRM(IL).LT.1. ) THEN
         ZCLRR(IL)=ZCLRR(IL)*(1.-MAX(ZCLFR(IL),ZCLFRM(IL))) &
                            /(1.-ZCLFRM(IL))
       ENDIF
       ZRNFT(IL)=1.-ZCLRR(IL)
!
!      * FRACTION OF GRID CELL FOR WHICH CLEAR-SKY IS AFFECTED BY RAIN.
!
       ZRCLR(IL)=MIN(MAX(ZRNFT(IL)-ZCLFR(IL),0.),1.)
!
!     * SNOW AREA FRACTION FROM MAXIMUM/RANDOM CLOUD OVERLAP ASSUMPTION.
!     * ONLY CLOUDS THAT ARE AFFECTED BY SNOW ARE CONSIDERED.
!
       ZCLFSM(IL)=ZCLFS(IL)
       IF ( ZSFLN(IL) .GT. 1.E-20 ) THEN
         ZCLFS(IL)=ZCLF(IL,JK)
       ELSE
         ZCLFS(IL)=0.
       ENDIF
       IF ( ZCLFSM(IL).LT.1. ) THEN
         ZCLRS(IL)=ZCLRS(IL)*(1.-MAX(ZCLFS(IL),ZCLFSM(IL))) &
                            /(1.-ZCLFSM(IL))
       ENDIF
       ZSNFT(IL)=1.-ZCLRS(IL)
!
!      * FRACTION OF GRID CELL FOR WHICH CLEAR-SKY IS AFFECTED BY SNOW.
!
       ZSCLR(IL)=MIN(MAX(ZSNFT(IL)-ZCLFS(IL),0.),1.)
!
!      * RAIN WATER MIXING RATIO FROM RAINFALL RATE (FROM ROTSTAYN)
!
       ZRHO0=PH(IL,JK)/(RGAS*TH(IL,JK))
       ZQRHO0=1.3/ZRHO0
       ZXRP1=0.
       VTR(IL)=0.
       IF ( ZRNFT(IL).GT.ZEPCLC ) THEN
         ZRFLNL=ZRFLN(IL)/ZRNFT(IL)
         ZRFLNL=MIN(ZRFLNL,ZRFLNL_MAX)
         IF ( ZRFLNL.GT.1.E-20 ) THEN
           ALAMBR=714.*(1.2/ZRHO0)**(1./9.)*ZRFLNL**(-0.22)
           VTR(IL)=1.94*AKR*SQRT(1.2/(ZRHO0*ALAMBR))
           ZXRP1=ZRFLNL/(ZRHO0*VTR(IL))
         ENDIF
       ENDIF
!
!      * AUTOCONVERSION AND ACCRETION. 4 DIFFERENT AUTOCONVERSION
!      * PARAMETERIZATIONS ARE AVAILABLE.
!
       ZXLB(IL)=QLR(IL,JK)
       IF( ZCLF(IL,JK).GT.ZEPCLC .AND. ZXLB(IL).GT.ZSEC ) THEN
         ZXLB(IL)=QLR(IL,JK)/ZCLF(IL,JK)
       ELSE
         ZXLB(IL)=QLR(IL,JK)
       ENDIF
       IF ( ISPCDNC == 1 ) THEN
         CDNC=SPCDNC
       ELSE
         CDNC=ZCDNC(IL,JK)
       ENDIF
       IF ( ZXLB(IL)*ZCLF(IL,JK).GE.ZSEC ) THEN
         IF ( ICASE <= 3 ) THEN
!
!          * PARAMETERIZATION BY WOOD (2005)
!
           IF ( ICASE .EQ. 1 ) THEN
!            * WITH CONSTANT CLOUD DROPLET NUMBER.
!
             CDNR=120.E+06
             CDNP=0.
             FACAUTN=1.3E+09
           ELSE IF ( ICASE .EQ. 2 ) THEN
!            * ORIGINAL WOOD PARAMETERIZATION.
!
             CDNR=0.
             CDNP=1.
             FACAUTN=1.3E+09
           ELSE IF ( ICASE .EQ. 3 ) THEN
!            * WITH PARAMETERS SPECIFIED ACCORDING TO LIU AND DAUM (2004).
!
             CDNR=0.
             CDNP=1.
             FACAUTN=1.08E+10
           ENDIF
           CDNE=(CDNR**(1.-CDNP))*CDNC**CDNP
           RVOL=1.E+06*(3.*ZXLB(IL)/(4.*API*RHOH2O*CDNE))**(1./3.)
           BETA6=((RVOL+3.)/RVOL)**(1./3.)
           R6=BETA6*RVOL
           R6C=7.5/(ZXLB(IL)**(1./6.)*SQRT(R6))
           EP=FACAUTN*(BETA6**6)
           IF ( R6 >= R6C ) THEN
             FAAUT=ZCRAUT(IL,JK)*EP/CDNE
           ELSE
             FAAUT=0.
           ENDIF
         ELSE IF ( ICASE .EQ. 4 ) THEN
!          * KHAIROUTDINOV AND KOGAN (2000)
!
           COAUT=2.47
           COTMP1=COAUT-2.
           FACAUT=2.5
           FAAUT=FACAUT*ZCRAUT(IL,JK)*1350.
           FAAUT=FAAUT*(CDNC/1.E+06)**(-1.79)
         ELSE
           CALL XIT('CLDPHYS',-1)
         ENDIF
!
!        * INITIAL GUESS AND SUBSEQUENT ITERATIONS.
!
         ZQEST1=(ZXLB(IL)**(1.-COAUT) &
               -DT*FAAUT*(1.-COAUT))**(1./(1.-COAUT))
         ZQEST=MIN(MAX(ZQEST1,ZSEC),ZXLB(IL))
         FAACC=0.
         IF ( ZXRP1.GT.ZSEC ) THEN
           FAACC=FACACC*67.*ZXRP1**COACC
           ZQEST2=(ZXLB(IL)**(1.-COACC) &
                 -DT*FAACC*(1.-COACC))**(1./(1.-COACC))
           ZQEST=MAX(MIN(ZQEST,ZQEST2),ZSEC)
           APK=-LOG(ZQEST/ZXLB(IL))/DT
           IF ( ZQEST.LT.ZXLB(IL) .AND. APK.GT.0. ) THEN
             ALPHA=((1.-ZQEST/ZXLB(IL))*ZXLB(IL)/(APK*DT)-ZQEST) &
                  /(ZXLB(IL)-ZQEST)
             ALPHA=MIN(MAX(ALPHA,0.),1.)
             DO IT=1,ITER
               AQHAT =ALPHA*ZXLB(IL)+(1.-ALPHA)*ZQEST
               AQHAT1=AQHAT**COTMP1
               AQHAT2=AQHAT**COTMP2
               ATERM1=FAAUT*AQHAT1*ZXLB(IL)+FAACC*AQHAT2
               ATERM2=FAAUT*AQHAT1*ZQEST   +FAACC*AQHAT2
               AEXPF =EXP(-FAACC*AQHAT2*DT)
               AFUN=ATERM1*ZQEST-AEXPF*ATERM2*ZXLB(IL)
               DQHDQ=1.-ALPHA
               ATERM3=FAAUT*COTMP1*AQHAT1*DQHDQ/AQHAT
               ATERM4=FAACC*COTMP2*AQHAT2*DQHDQ/AQHAT
               ADFUN=ATERM1+ZQEST*(ATERM3*ZXLB(IL)+ATERM4) &
                    +AEXPF*ZXLB(IL)*(DT*ATERM4*ATERM2 &
                                -ATERM3*ZQEST+FAAUT*AQHAT1+ATERM4)
               ZQEST=MAX(ZQEST-AFUN/ADFUN,ZSEC)
             ENDDO
           ENDIF
         ENDIF
         ZRPR(IL)=MIN(ZCLF(IL,JK)*ZXLB(IL), &
                      MAX(-ZCLF(IL,JK)*(ZQEST-ZXLB(IL)),0.))
       ELSE
         ZRPR(IL)=ZCLF(IL,JK)*ZXLB(IL)
       ENDIF
       IF(ZCLF(IL,JK).GT.ZEPCLC) THEN
         ZMRATEP(IL,JK)=ZRPR(IL)/ZCLF(IL,JK)
         ZMLWC(IL,JK)=ZXLB(IL)
       ENDIF
       ZZDRR=MAX(0.,ZCONS2*DP(IL,JK)*ZRPR(IL))
       ZRFLN(IL)=ZRFLN(IL)+ZZDRR
       ZSR=ZSR+ZRFLN(IL)
!
!      * EVAPORATION OF RAIN BELOW CLOUDS (FROM LOHMANN).
!
       ZEVP(IL)=0.
       EPS2=1.-EPS1
       IF ( ZSR .GT. 0. .AND. ZRFLN(IL).GT.0. .AND. ZRCLR(IL).GT.0. &
          ) THEN
         ZRFLNL=ZRFLN(IL)/ZRNFT(IL)
         ZRFLNL=MIN(ZRFLNL,ZRFLNL_MAX)
         RHODZ=DP(IL,JK)/GRAV
         ETMP=ESW(TH(IL,JK))
         ESTREF=0.01*PH(IL,JK)*(1.-EPSLIM)/(1.-EPSLIM*EPS2)
         IF ( ETMP.LT.ESTREF ) THEN
           ESAT=ETMP
         ELSE
           ESAT=ESTREF
         ENDIF
         QSW=EPS1*ESAT/(0.01*PH(IL,JK)-EPS2*ESAT)
         RHCW=MIN(MAX((QC(IL,JK)*(1.+QSW*FACTE)) &
                     /(QSW*(1.+QC(IL,JK)*FACTE)),0.),1.)
         SUSATW=RHCW-1.
         ZDV=2.21/PH(IL,JK)
         ZLV=ZLVDCP*CPRES
         ZAST=ZLV*(ZLV/(RGASV*TH(IL,JK))-1.)/(ZKA*TH(IL,JK))
         ZBST=RGASV*TH(IL,JK)/(ZDV*ESAT)
         ZZEPR1=870.*SUSATW*ZRFLNL**0.61/(SQRT(ZRHO0)*(ZAST+ZBST))
         ZZEPR=ZZEPR1*RHODZ
         ZPLIM=-ZXSEC*ZRFLN(IL)/ZRNFT(IL)
         ITOTEVP=0
         IF ( ZZEPR .LT. ZPLIM ) THEN
           ZZEPR=ZPLIM
           ITOTEVP=1
         ENDIF
         ZEVP(IL)=-ZZEPR*DT*ZRCLR(IL)/RHODZ
         ZEVP(IL)=MIN(ZEVP(IL), &
                 MAX(ZXSEC*((1.-RHCW)*QC(IL,JK))*ZRCLR(IL),0.))
         ZEVP(IL)=MAX(ZEVP(IL),0.)
         ZZEPR=-ZEVP(IL)*RHODZ/DT
         ZFRAIN(IL,JK)=ZFRAIN(IL,JK)+ZRFLN(IL)
         CLRFR(IL,JK)=ZRCLR(IL)
         IF ( ITOTEVP .EQ. 1 ) THEN
            ZFEVAP(IL,JK)=MIN(MAX(0.,-ZZEPR/ZRFLN(IL)),1.)
         ENDIF
         ZRFLN(IL)=ZRFLN(IL)+ZZEPR
       ENDIF
!
!      * TENDENCIES.
!
       TH(IL,JK)=TH(IL,JK)-ZEVP(IL)*RL/CPRES
       QR(IL,JK)=QR(IL,JK)+ZEVP(IL)
       QLR(IL,JK)=QLR(IL,JK)-ZRPR(IL)
      ENDDO
      ENDDO
!
      END SUBROUTINE CLDPHYS
