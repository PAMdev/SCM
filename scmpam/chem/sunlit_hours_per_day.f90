!> \file
!> \brief Compute the number of sunlit hours in a day at a given latitude
!!
!! @author Routine author name(s)
!
subroutine sunlit_hours_per_day(latitude,         & ! Input
                                sin_declination,  &
                                cos_declination,  &
                                il1,              &
                                il2,              &
                                ilg,              &
                                frac_sunlit_hours ) ! Output
implicit none
! Parameters
real, parameter :: pi=3.1415926
real, parameter :: rad2deg = 180.0/pi
real, parameter :: hours_per_day = 24.0 ! Hours per day [hours]
real, parameter :: earth_rotation_rate = 15.0 ! Rotation rate of Earth [degrees/hour]
! Input
integer, intent(in) :: il1 !< Index of first atmospheric column for calculations \f$[unitless]\f$
integer, intent(in) :: il2 !< Index of last atmospheric column for calculations \f$[unitless]\f$
integer, intent(in) :: ilg !< Total number of atmospheric columns \f$[unitless]\f$
real, intent(in)    :: sin_declination !< Sine   of declination angle for current timestep \f$[unitless]\f$
real, intent(in)    :: cos_declination !< Cosine of declination angle for current timestep \f$[unitless]\f$
real, intent(in), dimension(ilg) :: latitude !< Latitude for atmospheric columns \f$[degrees (radians?)]\f$
! Output
real, intent(out), dimension(ilg) :: frac_sunlit_hours !< Fraction of sunlit hours in a day for current latitude and timestep \f$[unitless]\f$
! Local
integer :: il
real :: cos_sunrise_hour_angle ! Cosine of the solar hour angle at sunrise [unitless]
real :: sunrise_hour_angle     ! Solar hour angle at sunrise [degrees]
real, dimension(ilg) :: sunlit_hours ! Number of sunlit hours [hours]
! Initialize arrays
sunlit_hours      = 0.0
frac_sunlit_hours = 0.0
do il=il1,il2
    cos_sunrise_hour_angle = -tan(latitude(il)) * (sin_declination/cos_declination)
    if (cos_sunrise_hour_angle <= -1.0) then ! Sun is up all day
        sunlit_hours(il) = hours_per_day
    elseif (cos_sunrise_hour_angle >= 1.0) then ! Sun is down all day
        sunlit_hours(il) = 0.0
    else ! Sun is up part of the day
        ! Compute the hour angle of the sunrise
        sunrise_hour_angle = acos(cos_sunrise_hour_angle) * rad2deg
        
        ! The portion of the day where the sun is up is symmetric about local solar noon.
        ! If one converts the sunrise solar angle to solar time, then assume 
        ! it is symmetric then one get the result below. E.g., the same time passes between 
        ! sunrise and local solar noon and then local solar noon and sunset.
        sunlit_hours(il) = 2.0 * sunrise_hour_angle / earth_rotation_rate
    endif ! cos_sunrise_hour_angle
enddo ! il
! Scale the number of sunlit hours by the number of hours per day.
frac_sunlit_hours = sunlit_hours / hours_per_day
return
end subroutine sunlit_hours_per_day
!> \file
!> This routine computes the number of sunlit hours at a particular latitude for a given timestep.
! This is done using the equations in the following reference,
!\n
! Forsythe et al., a model comparison for daylength as a function of latitude and day of year, ecological modelling, 1995.
! https://www.sciencedirect.com/science/article/pii/030438009400034f
! https://gist.github.com/anttilipp/ed3ab35258c7636d87de6499475301ce
