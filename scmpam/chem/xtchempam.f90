      SUBROUTINE XTCHEMPAM(XROW,TH,QV, &
                           SGPP,DSHJ,SHJ,CSZROW,ZDAYL,PRESSG,IAIND,NTRACP, &
                           SFRC,OHROW,H2O2ROW,O3ROW,NO3ROW, &
                           HNO3ROW,NH3ROW,NH4ROW, &
                           DOX4ROW,DOXDROW,NOXDROW,ZCLF,ZMRATEP,ZFSNOW, &
                           ZFRAIN,ZMLWC,ZFEVAP,CLRFR,CLRFS,ZFSUBL,ATAU, &
                           ITRWET,ISO2,IDMS,IHPO,ZTMST,NLATJ, &
                           NTRAC,ILG,IL1,IL2,ILEV)
!
!     * JUNE 2, 2013 - K.VONSALZEN. PLA VERSION OF XTCHEMIE.
!
      USE SDPHYS
!
      IMPLICIT REAL   (A-H,O-Z), &
      INTEGER (I-N)
!
      REAL   XROW(ILG,ILEV,NTRAC), SFRC(ILG,ILEV,NTRAC)
!
      REAL   TH  (ILG,ILEV),QV  (ILG,ILEV), &
             DSHJ(ILG,ILEV),SHJ (ILG,ILEV)
!
      REAL   CSZROW(ILG),PRESSG(ILG),ATAU(ILG)
!
!     * SPECIES PASSED IN VMR (DIMENSIONLESS).
!
      REAL   OHROW  (ILG,ILEV), H2O2ROW(ILG,ILEV), O3ROW  (ILG,ILEV), &
             NO3ROW (ILG,ILEV), HNO3ROW(ILG,ILEV), NH3ROW (ILG,ILEV), &
             NH4ROW (ILG,ILEV)
!
      REAL   DOX4ROW(ILG),DOXDROW(ILG),NOXDROW(ILG)
!
!     * ARRAYS SHARED WITH COND IN PHYSICS.
!
      REAL   ZCLF(ILG,ILEV),ZMRATEP(ILG,ILEV),ZFRAIN(ILG,ILEV), &
             CLRFR(ILG,ILEV),ZFSNOW(ILG,ILEV),ZMLWC(ILG,ILEV), &
             ZFEVAP(ILG,ILEV),CLRFS(ILG,ILEV),ZFSUBL(ILG,ILEV)
!
!     * CONTROL INDEX ARRAYS FOR TRACERS.
!
      INTEGER ITRWET(NTRAC)
!
!     * INTERNAL WORK ARRAYS:
!
!     * GENERAL WORK ARRAYS FOR XTCHEMIE5
!
      REAL  ,  DIMENSION(ILG,ILEV,NTRAC)  :: ZXTE, XCLD, XCLR
      REAL  ,  DIMENSION(ILG,ILEV)  :: ZXTP10,ZXTP1C,ZHENRY,ZSO4, &
                                       ZZOH,ZZH2O2,ZZO3,ZZO2, &
                                       ZZNO3,AGTHPO,ZRHO0,ZRHCTOX, &
                                       ZRHXTOC,ZDEP3D,PDCLR,PDCLD, &
                                       TMPSCL,XAROW,XCROW,DXCDT,DXADT, &
                                       DXDT
      REAL  ,  DIMENSION(ILG)  :: ZA21,ZA22,ZDT,ZE3,ZFAC1,ZF_O3, &
                                  ZH2O2M,ZSO2M,ZSO4M,ZSUMH2O2, &
                                  ZSUMO3,ZXTP1,ZZA,ZDAYL
      REAL  ,  DIMENSION(ILG,ILEV)  :: SGPP
!
!     * WORK ARRAYS USED IN OXISTR3 ONLY.
!
      PARAMETER (NAGE=1)
      PARAMETER (NEQP=13)
      LOGICAL, DIMENSION(ILG,ILEV)  ::  KCALC
      REAL  ,  DIMENSION(ILG,ILEV,NEQP)  :: ACHPA
      REAL  ,  DIMENSION(ILG,ILEV)  :: ROAROW,THG,AGTSO2,AGTSO4, &
                                       AGTO3,AGTCO2,AGTHO2,AGTHNO3, &
                                       AGTNH3,ANTSO2,ANTHO2,ANTSO4, &
                                       AOH2O2,ARESID,WRK1,WRK2,WRK3
      INTEGER, DIMENSION(ILG)       :: ILWCP,ICHEM
      INTEGER, DIMENSION(NTRACP) :: IAIND
!
      DATA YTAU / 1800. /
      DATA ZCTHR / .99 /
      DATA YSMALL / 9.E-30 /
!
      DATA ZERO,ONE,PLARGE /0., 1., 1.E+02/
      DATA ZFARR1,ZFARR2,ZFARR3,ZFARR4 /8.E+04, -3650., 9.7E+04, 6600./
!=======================================================================
!     * DEFINE FUNCTION FOR CHANGING THE UNITS
!     * FROM MASS-MIXING RATIO TO MOLECULES PER CM**3 AND VICE VERSA
!
      XTOC(X,Y)=X*6.022E+20/Y
      CTOX(X,Y)=Y/(6.022E+20*X)
!
!     * X = DENSITY OF AIR, Y = MOL WEIGHT IN GRAMM
!
      ZFARR(ZK,ZH,ZTPQ)=ZK*EXP(ZH*ZTPQ)
!-----------------------------------------------------------------------
!
!     * CONSTANTS.
!
      PCONS2=1./(ZTMST*GRAV)
      PQTMST=1./ZTMST
      ZMIN=1.E-20
      ASRSO4 = 0.9
      ASRPHOB= 0.
      ZK2I=2.0E-12
      ZK2=4.0E-31
      ZK2F=0.45
      ZK3=1.9E-13
      ZMOLGS=32.064
      ZMOLGAIR=28.84
      ZAVO=6.022E+23
      ZNAMAIR=1.E-03*ZAVO/ZMOLGAIR
      YSPHR=3600.
!
!     * DEFINE CONSTANT 2-D SLICES.
!
      DO 100 JK=1,ILEV
      DO 100 IL=IL1,IL2
        ZRHO0(IL,JK)=SHJ(IL,JK)*PRESSG(IL)/(RGAS*TH(IL,JK))
        ZRHXTOC(IL,JK)=XTOC(ZRHO0(IL,JK),ZMOLGS)
        ZRHCTOX(IL,JK)=CTOX(ZRHO0(IL,JK),ZMOLGS)
  100 CONTINUE
!
!     * OXIDANT CONCENTRATIONS.
!
      DO 115 JK=1,ILEV
      DO 115 IL=IL1,IL2
!
!       * Convert to molecules/cm3 from VMR
!
        FACTCON = ZRHO0(IL,JK)*1.E-03*ZAVO/ZMOLGAIR
        ZZOH  (IL,JK)=OHROW  (IL,JK)*FACTCON
        ZZH2O2(IL,JK)=H2O2ROW(IL,JK)*FACTCON
        ZZO3  (IL,JK)=O3ROW  (IL,JK)*FACTCON
        ZZNO3 (IL,JK)=NO3ROW (IL,JK)*FACTCON
!
!       * Oxygen concentration in molecules/cm3
!
        ZZO2 (IL,JK)=0.21*1.E-06*FACTCON
  115 CONTINUE
      SGPP=0.
!
!     * BACKGROUND AGING TIME SCALE FOR CARBONACEOUS AEROSOL.
!
      IF( NAGE.EQ.0 ) THEN
        ATAU=36.*YSPHR
      ELSE
        ATAU=12.*YSPHR
      ENDIF
!-----------------------------------------------------------------
!     * HYDROGEN PEROXIDE PRODUCTION AND INITIALIZATION.
!
      DO 120 JK=1,ILEV
      DO 120 IL=IL1,IL2
!
!---    convert molecules/cm**3 to kg-S/kg
!
        AGTHPO(IL,JK) = ZZH2O2(IL,JK) * 1.E+06 * 32.06E-03 &
                      / (6.022045E+23 * ZRHO0(IL,JK))
  120 CONTINUE
!
!     * INITIAL HYDROGEN PEROXIDE MIXING RATIOS FOR CLEAR AND CLOUDY SKY.
!
      TMPSCL(IL1:IL2,:)=1.
      WHERE ( ZCLF(IL1:IL2,:) < ZCTHR &
                                   .AND. (1.-ZCLF(IL1:IL2,:)) < ZCTHR )
        WHERE ( SFRC(IL1:IL2,:,IHPO) < -YSMALL )
          TMPSCL(IL1:IL2,:)=-XROW(IL1:IL2,:,IHPO) &
                             *(1.-ZCLF(IL1:IL2,:))/SFRC(IL1:IL2,:,IHPO)
        ELSEWHERE ( SFRC(IL1:IL2,:,IHPO) > YSMALL )
          TMPSCL(IL1:IL2,:)=XROW(IL1:IL2,:,IHPO) &
                                  *ZCLF(IL1:IL2,:)/SFRC(IL1:IL2,:,IHPO)
        ELSEWHERE
          TMPSCL(IL1:IL2,:)=0.
        ENDWHERE
      ENDWHERE
      TMPSCL(IL1:IL2,:)=MAX(MIN(TMPSCL(IL1:IL2,:),1.),0.)
      SFRC(IL1:IL2,:,IHPO)=TMPSCL(IL1:IL2,:)*SFRC(IL1:IL2,:,IHPO)
      XAROW=XROW(IL1:IL2,:,IHPO)
      XCROW=XROW(IL1:IL2,:,IHPO)
      WHERE ( ZCLF(IL1:IL2,:) < ZCTHR &
                                   .AND. (1.-ZCLF(IL1:IL2,:)) < ZCTHR )
        XAROW(IL1:IL2,:)=XAROW(IL1:IL2,:) &
                             +SFRC(IL1:IL2,:,IHPO)/(1.-ZCLF(IL1:IL2,:))
        XCROW(IL1:IL2,:)=XCROW(IL1:IL2,:) &
                                  -SFRC(IL1:IL2,:,IHPO)/ZCLF(IL1:IL2,:)
      ENDWHERE
!
!     * CHANGE IN CONCENTRATION IN CLOUDY AND CLEAR PORTIONS
!     * OF THE GRID CELL BY ADJUSTING CONCENTRATIONS TOWARDS BACKGROUND
!     * VALUES.
!
      DXCDT(IL1:IL2,:)=-(1./MAX(YTAU,ZTMST)) &
                                  *(XCROW(IL1:IL2,:)-AGTHPO(IL1:IL2,:))
      DXADT(IL1:IL2,:)=-(1./MAX(YTAU,ZTMST)) &
                                  *(XAROW(IL1:IL2,:)-AGTHPO(IL1:IL2,:))
      XCROW(IL1:IL2,:)=XCROW(IL1:IL2,:)+DXCDT(IL1:IL2,:)*ZTMST
      XAROW(IL1:IL2,:)=XAROW(IL1:IL2,:)+DXADT(IL1:IL2,:)*ZTMST
!
!     * ALL-SKY CHANGE.
!
      DXDT(IL1:IL2,:)=ZCLF(IL1:IL2,:)*DXCDT(IL1:IL2,:) &
                                 +(1.-ZCLF(IL1:IL2,:))*DXADT(IL1:IL2,:)
      XROW(IL1:IL2,:,IHPO)=XROW(IL1:IL2,:,IHPO) &
                                                 +DXDT(IL1:IL2,:)*ZTMST
!
!     * ADJUST CLEAR/CLOUDY CONCENTRATION DIFFERENCE TO ACCOUNT
!     * FOR PRODUCTION OF H2O2 IN CLOUDY PORTION OF THE GRID CELL.
!
      SFRC(IL1:IL2,:,IHPO)=0.
      WHERE ( ZCLF(IL1:IL2,:) < ZCTHR &
                                   .AND. (1.-ZCLF(IL1:IL2,:)) < ZCTHR )
        SFRC(IL1:IL2,:,IHPO)=ZCLF(IL1:IL2,:)*(1.-ZCLF(IL1:IL2,:)) &
                                   *(XAROW(IL1:IL2,:)-XCROW(IL1:IL2,:))
      ENDWHERE
!
      DO 200 JT=1,NTRAC
      DO 200 JK=1,ILEV
      DO 200 IL=IL1,IL2
        ZXTE(IL,JK,JT)=0.
  200 CONTINUE
!
      DO 212 JK=1,ILEV
        DO 210 IL=IL1,IL2
         ZHENRY(IL,JK)=0.
         IF(ZCLF(IL,JK).LT.1.E-04.OR.ZMLWC(IL,JK).LT.ZMIN) THEN
           ZMRATEP(IL,JK)=0.
           ZMLWC(IL,JK)=0.
         ENDIF
  210   CONTINUE
  212 CONTINUE
!
!     * PROCESSES WHICH ARE DIFERENT INSIDE AND OUTSIDE OF CLOUDS.
!
      DO 354 JT=1,NTRAC
       IF(ITRWET(JT).NE.0 .AND. JT.NE.ISO2 .AND. JT.NE.IHPO &
        .AND. (JT.LT.IAIND(1) .OR. JT.GT.IAIND(NTRACP))) THEN
        DO 333 JK=1,ILEV
         DO 332 IL=IL1,IL2
!
!          * DIAGNOSE INITIAL MIXING RATIOS FOR CLEAR AND CLOUDY SKY.
!
           TMPSCL(IL,JK)=1.
           IF ( ZCLF(IL,JK) < ZCTHR .AND. (1.-ZCLF(IL,JK)) < ZCTHR &
                                                                 ) THEN
             IF ( SFRC(IL,JK,JT) < -YSMALL ) THEN
               TMPSCL(IL,JK)=-XROW(IL,JK,JT) &
                                       *(1.-ZCLF(IL,JK))/SFRC(IL,JK,JT)
             ELSE IF ( SFRC(IL,JK,JT) > YSMALL ) THEN
               TMPSCL(IL,JK)=XROW(IL,JK,JT) &
                                            *ZCLF(IL,JK)/SFRC(IL,JK,JT)
             ELSE
               TMPSCL(IL,JK)=0.
             ENDIF
           ENDIF
           TMPSCL(IL,JK)=MAX(MIN(TMPSCL(IL,JK),1.),0.)
           SFRC(IL,JK,JT)=TMPSCL(IL,JK)*SFRC(IL,JK,JT)
           ZXTP10(IL,JK)=XROW(IL,JK,JT)
           ZXTP1C(IL,JK)=XROW(IL,JK,JT)
           IF ( ZCLF(IL,JK) < ZCTHR .AND. (1.-ZCLF(IL,JK)) < ZCTHR &
                                                                 ) THEN
             ZXTP10(IL,JK)=ZXTP10(IL,JK) &
                             +SFRC(IL,JK,JT)/(1.-ZCLF(IL,JK))
             ZXTP1C(IL,JK)=ZXTP1C(IL,JK) &
                                  -SFRC(IL,JK,JT)/ZCLF(IL,JK)
           ENDIF
           ZDEP3D(IL,JK)=0.
  332    CONTINUE
  333   CONTINUE
!
        IF(ITRWET(JT).EQ.1) THEN
         DO 334 JK=1,ILEV
         DO 334 IL=IL1,IL2
           ZHENRY(IL,JK)=ASRSO4
  334    CONTINUE
        ELSE
         DO 335 JK=1,ILEV
         DO 335 IL=IL1,IL2
           ZHENRY(IL,JK)=ASRPHOB
  335    CONTINUE
        ENDIF
!
        ISO4=0
        CALL WETDEP4(ILG,IL1,IL2,ILEV,ZTMST, PCONS2, ZXTP10, &
                     ZXTP1C,DSHJ,SHJ,PRESSG,TH,QV, ZMRATEP,ZMLWC, &
                     ZFSNOW,ZFRAIN,ZDEP3D,ZCLF,CLRFR,ZHENRY, &
                     CLRFS,ZFEVAP,ZFSUBL,PDCLR,PDCLD,JT,ISO4)
        DO 336 JK=1,ILEV
        DO 336 IL=IL1,IL2
          ZXTP1(IL)=(1.-ZCLF(IL,JK))*ZXTP10(IL,JK)+ &
                 ZCLF(IL,JK)*ZXTP1C(IL,JK)
          ZXTP1(IL)=ZXTP1(IL)-ZDEP3D(IL,JK)
          ZXTE(IL,JK,JT)=(ZXTP1(IL)-XROW(IL,JK,JT))*PQTMST
          XCLD(IL,JK,JT)=ZXTP1C(IL,JK)-PDCLD(IL,JK)
          XCLR(IL,JK,JT)=ZXTP10(IL,JK)-PDCLR(IL,JK)
  336   CONTINUE
!
       ENDIF
  354 CONTINUE
!
      JT=ISO2
!
!     * DAY-TIME CHEMISTRY.
!
      DOX4ROW=0.
      DOXDROW=0.
      NOXDROW=0.
      DO IL=IL1,IL2
        IF(CSZROW(IL).GT.0. .AND. NAGE.EQ.1) ATAU(IL)=1.*YSPHR
      ENDDO
      DO 412 JK=1,ILEV
       DO 410 IL=IL1,IL2
        IF(CSZROW(IL).GT.0.) THEN
         ZXTP1SO2=XROW(IL,JK,JT)+ZXTE(IL,JK,JT)*ZTMST
         IF(ZXTP1SO2.LE.ZMIN) THEN
           ZSO2=0.
         ELSE
           ZTK2=ZK2*(TH(IL,JK)/300.)**(-3.3)
           ZM=ZRHO0(IL,JK)*ZNAMAIR
           ZHIL=ZTK2*ZM/ZK2I
           ZEXP=LOG10(ZHIL)
           ZEXP=1./(1.+ZEXP*ZEXP)
           ZTK23B=ZTK2*ZM/(1.+ZHIL)*ZK2F**ZEXP
           ZSO2=ZXTP1SO2*ZRHXTOC(IL,JK)*ZZOH(IL,JK)*ZTK23B &
               *ZDAYL(IL)
           ZSO2=ZSO2*ZRHCTOX(IL,JK)
           ZSO2=MIN(ZSO2,ZXTP1SO2*PQTMST)
           ZXTE(IL,JK,JT)=ZXTE(IL,JK,JT)-ZSO2
         ENDIF
         SGPP(IL,JK)=ZSO2
!
         ZXTP1DMS=XROW(IL,JK,IDMS)+ ZXTE(IL,JK,IDMS)*ZTMST
         IF(ZXTP1DMS.LE.ZMIN) THEN
           ZDMS=0.
         ELSE
          T=TH(IL,JK)
!          ZTK1=(T*EXP(-234./T)+8.46E-10*EXP(7230./T)+
!     1         2.68E-10*EXP(7810./T))/(1.04E+11*T+88.1*EXP(7460./T))
!
!         * DMS+OH->SO2+MO2+CH2O (JPL 2010)
!
          ZTK1=1.2E-11*EXP(-280./T)
!
!         * DMS+OH+O2->0.75SO2+0.25MSA+MO2 (JPL 2010)
!
!          ZTKA=8.2E-39*EXP(5376./T)
          ZTKA=EXP(5376./T-87.69668)
          ZTKB=1.05E-05*EXP(3644./T)
          ZTK2=ZTKA*ZZO2(IL,JK)/(1.0+ZTKB*0.2095)
          ZDMS=ZXTP1DMS*ZRHXTOC(IL,JK)*ZZOH(IL,JK)*(ZTK1+ZTK2) &
              *ZDAYL(IL)
          ZDMS=ZDMS*ZRHCTOX(IL,JK)
          IF ( ZDMS > ZXTP1DMS*PQTMST ) THEN
            SCALF=ZXTP1DMS*PQTMST/ZDMS
            ZDMS=SCALF*ZDMS
          ENDIF
          ZXTE(IL,JK,IDMS)=ZXTE(IL,JK,IDMS)-ZDMS
          ZXTE(IL,JK,JT)=ZXTE(IL,JK,JT)+0.75*ZDMS
          SGPP(IL,JK)=SGPP(IL,JK)+0.25*ZDMS
         ENDIF
!
         FAC=DSHJ(IL,JK)*PRESSG(IL)/GRAV
         DOX4ROW(IL)=DOX4ROW(IL)+FAC*ZSO2
         DOXDROW(IL)=DOXDROW(IL)+FAC*ZDMS
        ENDIF
  410  CONTINUE
  412 CONTINUE
!
!     * NIGHT-TIME CHEMISTRY.
!
      DO IL=IL1,IL2
        IF(CSZROW(IL).LE.0. .AND. NAGE.EQ.1) ATAU(IL)=24.*YSPHR
      ENDDO
      DO 416 JK=1,ILEV
       DO 414 IL=IL1,IL2
        IF(CSZROW(IL).LE.0.) THEN
         ZXTP1DMS=XROW(IL,JK,IDMS)+ ZXTE(IL,JK,IDMS)*ZTMST
         IF(ZXTP1DMS.LE.ZMIN) THEN
           ZDMS=0.
         ELSE
!
!          *  DMS+NO3->SO2+HNO3+MO2+CH2O (JPL 2010)
!
!           ZTK3=ZK3*EXP(520./TH(IL,JK))
           ZTK3=1.9E-13*EXP(530./TH(IL,JK))
           ZDMS=ZXTP1DMS*ZRHXTOC(IL,JK)*ZZNO3(IL,JK)*ZTK3
           ZDMS=ZDMS*ZRHCTOX(IL,JK)
           ZDMS=MIN(ZDMS,ZXTP1DMS*PQTMST)
           ZXTE(IL,JK,IDMS)=ZXTE(IL,JK,IDMS)-ZDMS
           ZXTE(IL,JK,JT)=ZXTE(IL,JK,JT)+ZDMS
           FAC=DSHJ(IL,JK)*PRESSG(IL)/GRAV
           NOXDROW(IL)=NOXDROW(IL)+ZDMS*FAC
         ENDIF
        ENDIF
  414  CONTINUE
  416 CONTINUE
!
!     * FRACTION OF TRACERS IN CLOUDY PART OF THE GRID CELL.
!
      DO 422 JT=1,NTRAC
       DO 421 JK=1,ILEV
        DO 420 IL=IL1,IL2
          IF ( JT.LT.IAIND(1) .OR. JT.GT.IAIND(NTRACP) ) THEN
            XROW(IL,JK,JT)=XROW(IL,JK,JT)+ZXTE(IL,JK,JT)*ZTMST
            XROW(IL,JK,JT)=MAX(XROW(IL,JK,JT),ZERO)
            IF ( JT /= ISO2 .AND. JT /= IHPO ) THEN
              IF ( ITRWET(JT).NE.0 .AND. XROW(IL,JK,JT) > ZMIN &
                   .AND. ZCLF(IL,JK) > 1.E-02 ) THEN
                SFRC(IL,JK,JT)=0.
                IF ( ZCLF(IL,JK) < ZCTHR &
                                  .AND. (1.-ZCLF(IL,JK)) < ZCTHR ) THEN
                  SFRC(IL,JK,JT)=ZCLF(IL,JK)*(1.-ZCLF(IL,JK)) &
                                   *(XCLR(IL,JK,JT)-XCLD(IL,JK,JT))
                ENDIF
              ELSE
                SFRC(IL,JK,JT)=0.
              ENDIF
            ENDIF
          ENDIF
 420    CONTINUE
 421   CONTINUE
 422  CONTINUE
!
      RETURN
      END
