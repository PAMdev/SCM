      SUBROUTINE RDNCDAT(FILE_NAME)
!
      USE IODAT
      USE NETCDF
!
      IMPLICIT NONE
!
      INTEGER :: ND,IT,IV
      CHARACTER(LEN=*), INTENT(IN) :: FILE_NAME
      INTEGER :: ncid, fil_varid
      INTEGER, DIMENSION(NDIM) :: lvl_dimid, lvl_varid
      INTEGER, DIMENSION(ITOT) :: var_varid
!
      CALL CHECK( nf90_open(TRIM(FILE_NAME)//'.nc', &
                                        nf90_nowrite, ncid) )
!
!     * ALLOCATE MEMORY.
!
      ALLOCATE(DIMT(NDIM))
!
!     * READ THE DIMENSIONS.
!
      DO ND=1,NDIM
        IF ( DIM(ND)%FLGI == 1 ) THEN
          CALL CHECK( nf90_inq_dimid(ncid, TRIM(DIM(ND)%NAME), &
                                     lvl_dimid(ND)) )
          CALL CHECK( nf90_inquire_dimension(ncid, lvl_dimid(ND), &
                                 len=DIMT(ND)%SIZE) )
          IF ( DIMT(ND)%SIZE /= DIM(ND)%SIZE ) THEN
            PRINT*,'CONFLICTING FIELD DIMENSION ',TRIM(DIM(ND)%NAME)
            CALL XIT('RDNCDATRS',-2)
          ENDIF
        ENDIF
      ENDDO
!
!      FIND AND READ FIELDS.
!
      IV=0
      DO IT=1,ITOT
        IF ( ABS(IVARV(IT)%FLD0D+YNAX) > YSMALLX ) THEN
          IV=IV+1
          CALL CHECK( nf90_inq_varid(ncid,TRIM(IVAR(IT)%NAME), &
                                     var_varid(IV)) )
          CALL CHECK( nf90_get_var(ncid, var_varid(IV), &
                                     IVARV(IT)%FLD0D) )
        ELSE IF ( ALLOCATED(IVARV(IT)%FLD1D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_inq_varid(ncid,TRIM(IVAR(IT)%NAME), &
                                     var_varid(IV)) )
          CALL CHECK( nf90_get_var(ncid, var_varid(IV), &
                                     IVARV(IT)%FLD1D) )
        ELSE IF ( ALLOCATED(IVARV(IT)%FLD2D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_inq_varid(ncid,TRIM(IVAR(IT)%NAME), &
                                     var_varid(IV)) )
          CALL CHECK( nf90_get_var(ncid, var_varid(IV), &
                                     IVARV(IT)%FLD2D) )
        ELSE IF ( ALLOCATED(IVARV(IT)%FLD3D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_inq_varid(ncid,TRIM(IVAR(IT)%NAME), &
                                     var_varid(IV)) )
          CALL CHECK( nf90_get_var(ncid, var_varid(IV), &
                                     IVARV(IT)%FLD3D) )
        ELSE IF ( ALLOCATED(IVARV(IT)%FLD4D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_inq_varid(ncid,TRIM(IVAR(IT)%NAME), &
                                     var_varid(IV)) )
          CALL CHECK( nf90_get_var(ncid, var_varid(IV), &
                                     IVARV(IT)%FLD4D) )
        ENDIF
      ENDDO
!
!     * DEALLOCATION.
!
      DEALLOCATE(DIMT)
!
      CONTAINS
      SUBROUTINE CHECK(status)
      integer, intent (in) :: status

      if(status /= nf90_noerr) then
        print *, trim(nf90_strerror(status))
        stop 2
      end if
      END SUBROUTINE CHECK
      END SUBROUTINE RDNCDAT
