      SUBROUTINE SETNCDIMPRE(ALEV,ALEV1,ATIME,ILEV,ILEV1,ITIME)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      INTEGER, INTENT(IN) :: ILEV,ILEV1,ITIME
      REAL, INTENT(IN), DIMENSION(ILEV)    :: ALEV
      REAL, INTENT(IN), DIMENSION(ILEV1)   :: ALEV1
      REAL, INTENT(IN), DIMENSION(ITIME)   :: ATIME
!
!     * DEFINE NETCDF DIMENSIONS AND ASSOCIATE WITH DIMENSIONS
!     * IN THE MODEL.
!
      NDIM=10
      ALLOCATE(DIM(NDIM))
      DIM(:)%FLGI=0
      DIM(:)%FLGO=0
      DIM(:)%FLGRS=0
      DO ND=1,NDIM
      DO I=1,IMAXL
        DIM(ND)%NAME(I:I)=' '
        DIM(ND)%UNITS(I:I)=' '
        DIM(ND)%LNAME(I:I)=' '
      ENDDO
      ENDDO
!
      DIM(1)%NAME (1:12)='level       '
      DIM(1)%LNAME(1:12)='Altitude    '
      DIM(1)%UNITS(1:12)='m           '
      DIM(1)%SIZE=ILEV
      ALLOCATE(DIM(1)%VAL(DIM(1)%SIZE))
      DIM(1)%VAL(:)=ALEV(:)
!
      DIM(2)%NAME (1:12)='level1      '
      DIM(2)%LNAME(1:35)='Altitude of grid cell interfaces   '
      DIM(2)%UNITS(1:12)='m           '
      DIM(2)%SIZE=ILEV+1
      ALLOCATE(DIM(2)%VAL(DIM(2)%SIZE))
      DIM(2)%VAL(:)=ALEV1(:)
!
      DIM(3)%NAME (1:12)='time        '
      DIM(3)%LNAME(1:12)='Model time  '
      DIM(3)%UNITS(1:12)='s           '
      DIM(3)%SIZE=ITIME
      ALLOCATE(DIM(3)%VAL(DIM(3)%SIZE))
      DIM(3)%VAL(:)=ATIME(:)
!
      END SUBROUTINE SETNCDIMPRE
