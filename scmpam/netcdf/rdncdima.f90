      SUBROUTINE RDNCDIMA(FILE1,FILE2,ILEV,ILEV1,ITIME,IRDDR,NTRAC, &
                         KEXTT,KINTT,ISAINTT,NRMFLD,ISDUST,KTIME1)
!
      USE IODAT
      USE NETCDF
!
      IMPLICIT NONE
!
      CHARACTER(LEN=*), INTENT(IN) :: FILE1,FILE2
      INTEGER, INTENT(OUT) :: ILEV,ILEV1,ITIME,IRDDR,NTRAC,KEXTT, &
                              KINTT,ISAINTT,NRMFLD,ISDUST
      LOGICAL, INTENT(IN) :: KTIME1
      INTEGER, PARAMETER :: NFIL=2
      INTEGER, DIMENSION(NFIL) :: ncid
      INTEGER :: status,ND,NF,I
      INTEGER, DIMENSION(:), ALLOCATABLE :: lvl_dimid
!
!     * INITIALIZATION OF FIELD DIMENSIONS.
!
      ILEV=INAX
      ILEV1=INAX
      ITIME=INAX
      IRDDR=INAX
      NTRAC=INAX
      KEXTT=INAX
      KINTT=INAX
      ISAINTT=INAX
      NRMFLD=INAX
      ISDUST=INAX
!
!     * OPEN NETCDF FILES WITH INPUT AND RESTART DATA.
!
      status=nf90_open(TRIM(FILE1)//'.nc', nf90_nowrite, ncid(1))
      IF (status /= nf90_NoErr) CALL XIT('RDNCDIM',-1)
      status=nf90_open(TRIM(FILE2)//'.nc', nf90_nowrite, ncid(2))
      IF (status /= nf90_NoErr) CALL XIT('RDNCDIM',-2)
!
!     * DEFINE 10 NETCDF DIMENSION NAMES. AN ADDITIONAL TIME DIMENSION
!     * MAY BE GENERATED TO SUPPORT OUTPUT FIELDS, IF NECESSARY.
!
      NDIM=10
      IF ( KTIME1 ) NDIM=NDIM+1
!
!     * INITIALIZATION.
!
      ALLOCATE(DIM(NDIM))
      DO ND=1,NDIM
      DO I=1,IMAXL
        DIM(ND)%NAME(I:I)=' '
      ENDDO
      ENDDO
!
!     * DIMENSION NAMES.
!
      DIM(1 )%NAME(1:10)='level     '
      DIM(2 )%NAME(1:10)='level1    '
      DIM(3 )%NAME(1:10)='time      '
      DIM(4 )%NAME(1:10)='raddry    '
      DIM(5 )%NAME(1:10)='ntrac     '
      DIM(6 )%NAME(1:10)='kextt     '
      DIM(7 )%NAME(1:10)='kintt     '
      DIM(8 )%NAME(1:10)='isaintt   '
      DIM(9 )%NAME(1:10)='nrmfld    '
      DIM(10)%NAME(1:10)='isdust    '
!
!     * EXTRA TIME DIMENSION FOR OUTPUT.
!
      IF ( KTIME1 ) DIM(NDIM)%NAME(1:10)='time1     '
!
!     * INITIALIZATIONS.
!
      ALLOCATE(lvl_dimid(NDIM))
      DIM(:)%FLGI=0
      DIM(:)%FLGO=0
      DIM(:)%FLGRS=0
      DIM(:)%FLGX=0
!
!     * READ THE DIMENSIONS AND COORDINATES FROM THE FILES.
!
      DO NF=1,NFIL
      DO ND=1,NDIM
        IF ( DIM(ND)%FLGX==0 ) THEN
          status=nf90_inq_dimid(ncid(NF), TRIM(DIM(ND)%NAME), &
                                          lvl_dimid(ND))
          IF (status == nf90_NoErr) THEN
            DIM(ND)%FLGX=1
            status=nf90_inquire_dimension(ncid(NF), lvl_dimid(ND), &
                                          len=DIM(ND)%SIZE)
            status=nf90_get_att(ncid(NF), lvl_dimid(ND), "units", &
                                          DIM(ND)%UNITS )
            status=nf90_get_att(ncid(NF), lvl_dimid(ND), "long_name", &
                                          DIM(ND)%LNAME )
            status=nf90_inq_varid(ncid(NF), TRIM(DIM(ND)%NAME), &
                                            lvl_dimid(ND))
            ALLOCATE(DIM(ND)%VAL(DIM(ND)%SIZE))
            status=nf90_get_var(ncid(NF), lvl_dimid(ND), DIM(ND)%VAL)
!
!           * ASSOCIATE DIMENSIONS IN MODEL WITH NETCDF DIMENSIONS.
!
            SELECT CASE (ND)
              CASE (1)
                ILEV =DIM(ND)%SIZE
              CASE (2)
                ILEV1=DIM(ND)%SIZE
              CASE (3)
                ITIME=DIM(ND)%SIZE
              CASE (4)
                IRDDR=DIM(ND)%SIZE
              CASE (5)
                NTRAC=DIM(ND)%SIZE
              CASE (6)
                KEXTT=DIM(ND)%SIZE
              CASE (7)
                KINTT=DIM(ND)%SIZE
              CASE (8)
                ISAINTT=DIM(ND)%SIZE
              CASE (9)
                NRMFLD=DIM(ND)%SIZE
              CASE (10)
                ISDUST=DIM(ND)%SIZE
            END SELECT
          ENDIF
        ENDIF
      ENDDO
      ENDDO
      DO ND=1,NDIM
        IF ( DIM(ND)%FLGX==0 ) THEN
          PRINT*,'COULD NOT FIND DIMENSION ',TRIM(DIM(ND)%NAME), &
                  ' IN INPUT FILES'
        ENDIF
      ENDDO
!
      END SUBROUTINE RDNCDIMA
