      SUBROUTINE SETNCDIMPREPAM(ALEV,ALEV1,ATIME,ARDDR,ATRAC,AEXTT,AINTT, &
                                ASAIN,ANRMF,ASDUS, &
                                ILEV,ILEV1,ITIME,IRDDR,ITRAC,KEXTT,KINTT, &
                                ISAINTT,NRMFLD,ISDUST)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      INTEGER, INTENT(IN) :: ILEV,ILEV1,ITIME,IRDDR,ITRAC,KEXTT,KINTT, &
                             ISAINTT,NRMFLD,ISDUST
      REAL, INTENT(IN), DIMENSION(ILEV)    :: ALEV
      REAL, INTENT(IN), DIMENSION(ILEV1)   :: ALEV1
      REAL, INTENT(IN), DIMENSION(ITIME)   :: ATIME
      REAL, INTENT(IN), DIMENSION(IRDDR)   :: ARDDR
      REAL, INTENT(IN), DIMENSION(ITRAC)   :: ATRAC
      REAL, INTENT(IN), DIMENSION(KEXTT)   :: AEXTT
      REAL, INTENT(IN), DIMENSION(KINTT)   :: AINTT
      REAL, INTENT(IN), DIMENSION(ISAINTT) :: ASAIN
      REAL, INTENT(IN), DIMENSION(NRMFLD)  :: ANRMF
      REAL, INTENT(IN), DIMENSION(ISDUST)  :: ASDUS
!
!     * DEFINE NETCDF DIMENSIONS AND ASSOCIATE WITH DIMENSIONS
!     * IN THE MODEL.
!
      NDIM=10
      ALLOCATE(DIM(NDIM))
      DIM(:)%FLGI=0
      DIM(:)%FLGO=0
      DIM(:)%FLGRS=0
      DO ND=1,NDIM
      DO I=1,IMAXL
        DIM(ND)%NAME(I:I)=' '
        DIM(ND)%UNITS(I:I)=' '
        DIM(ND)%LNAME(I:I)=' '
      ENDDO
      ENDDO
!
      DIM(1)%NAME (1:12)='level       '
      DIM(1)%LNAME(1:12)='Altitude    '
      DIM(1)%UNITS(1:12)='m           '
      DIM(1)%SIZE=ILEV
      ALLOCATE(DIM(1)%VAL(DIM(1)%SIZE))
      DIM(1)%VAL(:)=ALEV(:)
!
      DIM(2)%NAME (1:12)='level1      '
      DIM(2)%LNAME(1:35)='Altitude of grid cell interfaces   '
      DIM(2)%UNITS(1:12)='m           '
      DIM(2)%SIZE=ILEV+1
      ALLOCATE(DIM(2)%VAL(DIM(2)%SIZE))
      DIM(2)%VAL(:)=ALEV1(:)
!
      DIM(3)%NAME (1:12)='time        '
      DIM(3)%LNAME(1:12)='Model time  '
      DIM(3)%UNITS(1:12)='s           '
      DIM(3)%SIZE=ITIME
      ALLOCATE(DIM(3)%VAL(DIM(3)%SIZE))
      DIM(3)%VAL(:)=ATIME(:)
!
      DIM(4)%NAME (1:12)='raddry      '
      DIM(4)%LNAME(1:12)='log10(R/R0) '
      DIM(4)%UNITS(1:12)='1           '
      DIM(4)%SIZE=IRDDR
      ALLOCATE(DIM(4)%VAL(DIM(4)%SIZE))
      DIM(4)%VAL(:)=ARDDR(:)
!
      DIM(5)%NAME (1:12)='ntrac       '
      DIM(5)%LNAME(1:12)='Tracer index'
      DIM(5)%UNITS(1:12)='1           '
      DIM(5)%SIZE=ITRAC
      ALLOCATE(DIM(5)%VAL(DIM(5)%SIZE))
      DIM(5)%VAL(:)=ATRAC(:)
!
      DIM(6)%NAME (1:12)='kextt       '
      DIM(6)%LNAME(1:30)='Externally mixed species index'
      DIM(6)%UNITS(1:12)='1           '
      DIM(6)%SIZE=KEXTT
      ALLOCATE(DIM(6)%VAL(DIM(6)%SIZE))
      DIM(6)%VAL(:)=AEXTT(:)
!
      DIM(7)%NAME (1:12)='kintt       '
      DIM(7)%LNAME(1:30)='Internally mixed species index'
      DIM(7)%UNITS(1:12)='1           '
      DIM(7)%SIZE=KINTT
      ALLOCATE(DIM(7)%VAL(DIM(7)%SIZE))
      DIM(7)%VAL(:)=AINTT(:)
!
      DIM(8)%NAME (1:12)='isaintt     '
      DIM(8)%LNAME(1:30)='Internally mixed section index'
      DIM(8)%UNITS(1:12)='1           '
      DIM(8)%SIZE=ISAINTT
      ALLOCATE(DIM(8)%VAL(DIM(8)%SIZE))
      DIM(8)%VAL(:)=ASAIN(:)
!
      DIM(9)%NAME (1:12)='nrmfld      '
      DIM(9)%LNAME(1:20)='Time level index    '
      DIM(9)%UNITS(1:12)='1           '
      DIM(9)%SIZE=NRMFLD
      ALLOCATE(DIM(9)%VAL(DIM(9)%SIZE))
      DIM(9)%VAL(:)=ANRMF(:)
!
      DIM(10)%NAME (1:12)='isdust       '
      DIM(10)%LNAME(1:30)='Dust section index           '
      DIM(10)%UNITS(1:12)='1           '
      DIM(10)%SIZE=ISDUST
      ALLOCATE(DIM(10)%VAL(DIM(10)%SIZE))
      DIM(10)%VAL(:)=ASDUS(:)
!
      END SUBROUTINE SETNCDIMPREPAM
