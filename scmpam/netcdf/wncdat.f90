      SUBROUTINE WNCDAT(FILE_NAME)
!
      USE IODAT
      USE NETCDF
!
      IMPLICIT NONE
!
      INTEGER :: IS,IT,ND,IV,ICHCK,IDIM

      CHARACTER(LEN=*), INTENT(IN) :: FILE_NAME
      INTEGER :: ncid, fil_varid
      INTEGER, DIMENSION(NDIM) :: lvl_dimid, lvl_varid
      INTEGER, DIMENSION(ITOT) :: var_varid
      INTEGER, DIMENSION(1) :: DIMD1D, CNT1D
      INTEGER, DIMENSION(2) :: DIMD2D, CNT2D
      INTEGER, DIMENSION(3) :: DIMD3D, CNT3D
      INTEGER, DIMENSION(4) :: DIMD4D, CNT4D
!
!     * CREATE OUTPUT FILE.
!
      CALL CHECK (nf90_create(TRIM(FILE_NAME)//'.nc', &
                              nf90_clobber, ncid) )
!
!     * DEFINE THE DIMENSIONS.
!
      DO ND=1,NDIM
        IF ( DIM(ND)%FLGO == 1 ) THEN
          CALL CHECK( nf90_def_dim(ncid, TRIM(DIM(ND)%NAME), &
                                   DIM(ND)%SIZE, lvl_dimid(ND)) )
        ENDIF
      ENDDO
!
!     * DEFINE THE COORDINDATE VARIABLES.
!
      DO ND=1,NDIM
        IF ( DIM(ND)%FLGO == 1 ) THEN
          CALL CHECK( nf90_def_var(ncid, TRIM(DIM(ND)%NAME), &
                                   NF90_REAL, lvl_dimid(ND), &
                                   lvl_varid(ND)) )
        ENDIF
      ENDDO
!
!     * ASSIGN ATTRIBUTES TO COORDINATE VARIABLES.
!
      DO ND=1,NDIM
        IF ( DIM(ND)%FLGO == 1 ) THEN
          CALL CHECK( nf90_put_att(ncid, lvl_varid(ND), "units", &
                                   TRIM(DIM(ND)%UNITS)) )
          CALL CHECK( nf90_put_att(ncid, lvl_varid(ND), "long_name", &
                                    TRIM(DIM(ND)%LNAME)) )
        ENDIF
      ENDDO
!
!      DEFINE THE NETCDF VARIABLES FOR EACH FIELD.
!
      IV=0
      DO IT=1,ITOT
        ICHCK=0
        IF ( ABS(OVARV(IT)%FLD0D+YNAX) > YSMALLX ) THEN
          ICHCK=1
          IV=IV+1
          CALL CHECK( nf90_def_var(ncid,TRIM(OVAR(IT)%NAME), &
                                   NF90_REAL, var_varid(IV)) )
        ELSE IF ( ALLOCATED(OVARV(IT)%FLD1D) ) THEN
          ICHCK=1
          IV=IV+1
          IDIM=0
          DO IS=1,ISMX
            CTMP=OVAR(IT)%DIM(IS)
            IF ( CTMP /= CNA ) THEN
              IDIM=IDIM+1
              DO ND=1,NDIM
                IF ( TRIM(CTMP) == TRIM(DIM(ND)%NAME) ) THEN
                  DIMD1D(IDIM)=lvl_varid(ND)
                ENDIF
              ENDDO
            ENDIF
          ENDDO
          CALL CHECK( nf90_def_var(ncid,TRIM(OVAR(IT)%NAME), &
                                   NF90_REAL, DIMD1D, var_varid(IV)) )
        ELSE IF ( ALLOCATED(OVARV(IT)%FLD2D) ) THEN
          ICHCK=1
          IV=IV+1
          IDIM=0
          DO IS=1,ISMX
            CTMP=OVAR(IT)%DIM(IS)
            IF ( CTMP /= CNA ) THEN
              IDIM=IDIM+1
              DO ND=1,NDIM
                IF ( TRIM(CTMP) == TRIM(DIM(ND)%NAME) ) THEN
                  DIMD2D(IDIM)=lvl_varid(ND)
                ENDIF
              ENDDO
            ENDIF
          ENDDO
          CALL CHECK( nf90_def_var(ncid,TRIM(OVAR(IT)%NAME), &
                                   NF90_REAL, DIMD2D, var_varid(IV)) )
        ELSE IF ( ALLOCATED(OVARV(IT)%FLD3D) ) THEN
          ICHCK=1
          IV=IV+1
          IDIM=0
          DO IS=1,ISMX
            CTMP=OVAR(IT)%DIM(IS)
            IF ( CTMP /= CNA ) THEN
              IDIM=IDIM+1
              DO ND=1,NDIM
                IF ( TRIM(CTMP) == TRIM(DIM(ND)%NAME) ) THEN
                  DIMD3D(IDIM)=lvl_varid(ND)
                ENDIF
              ENDDO
            ENDIF
          ENDDO
          CALL CHECK( nf90_def_var(ncid,TRIM(OVAR(IT)%NAME), &
                                   NF90_REAL, DIMD3D, var_varid(IV)) )
        ELSE IF ( ALLOCATED(OVARV(IT)%FLD4D) ) THEN
          ICHCK=1
          IV=IV+1
          IDIM=0
          DO IS=1,ISMX
            CTMP=OVAR(IT)%DIM(IS)
            IF ( CTMP /= CNA ) THEN
              IDIM=IDIM+1
              DO ND=1,NDIM
                IF ( TRIM(CTMP) == TRIM(DIM(ND)%NAME) ) THEN
                  DIMD4D(IDIM)=lvl_varid(ND)
                ENDIF
              ENDDO
            ENDIF
          ENDDO
          CALL CHECK( nf90_def_var(ncid,TRIM(OVAR(IT)%NAME), &
                                   NF90_REAL, DIMD4D, var_varid(IV)) )
        ENDIF
!
!       * ASSIGN ATTRIBUTES TO THE VARIABLE.
!
        IF ( ICHCK == 1 ) THEN
          CALL CHECK( nf90_put_att(ncid, var_varid(IV), "units", &
                                   TRIM(OVAR(IT)%UNITS) ) )
          CALL CHECK( nf90_put_att(ncid, var_varid(IV), "long_name", &
                                   TRIM(OVAR(IT)%LNAME) ) )
          CALL CHECK( nf90_put_att(ncid, var_varid(IV), "_FillValue", &
                                   YNAX) )
        ENDIF
      ENDDO
!
!     * END DEFINE MODE.
!
      CALL CHECK( nf90_enddef(ncid) )
!
!     * WRITE THE LEVEL DATA.
!
      DO ND=1,NDIM
        IF ( DIM(ND)%FLGO == 1 ) THEN
          CALL CHECK( nf90_put_var(ncid, lvl_varid(ND), DIM(ND)%VAL) )
        ENDIF
      ENDDO
!
!     * WRITE DATA FOR EACH FIELD.
!
      IV=0
      DO IT=1,ITOT
        IF ( ABS(OVARV(IT)%FLD0D+YNAX) > YSMALLX ) THEN
          IV=IV+1
          CALL CHECK( nf90_put_var(ncid, var_varid(IV), &
                      OVARV(IT)%FLD0D) )
        ELSE IF ( ALLOCATED(OVARV(IT)%FLD1D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_put_var(ncid, var_varid(IV), &
                      OVARV(IT)%FLD1D) )
        ELSE IF ( ALLOCATED(OVARV(IT)%FLD2D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_put_var(ncid, var_varid(IV), &
                      OVARV(IT)%FLD2D) )
        ELSE IF ( ALLOCATED(OVARV(IT)%FLD3D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_put_var(ncid, var_varid(IV), &
                      OVARV(IT)%FLD3D) )
        ELSE IF ( ALLOCATED(OVARV(IT)%FLD4D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_put_var(ncid, var_varid(IV), &
                      OVARV(IT)%FLD4D) )
        ENDIF
      ENDDO
!
!     * CLOSE THE FILE.
!
      CALL CHECK( nf90_close(ncid) )
!
      CONTAINS
      SUBROUTINE CHECK(status)
      integer, intent (in) :: status

      if(status /= nf90_noerr) then
        print *, trim(nf90_strerror(status))
        stop 2
      end if
      END SUBROUTINE CHECK
      END SUBROUTINE WNCDAT
